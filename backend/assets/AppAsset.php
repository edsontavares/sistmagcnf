<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
	
        //'css/site.css',
        '../biblioteca/css/theme.css',
        '../biblioteca/css/style.css',
        '../biblioteca/starter.css',

        /* CSS Implementing Plugins */
        '../biblioteca/vendor/font-awesome/css/fontawesome-all.min.css',
        '../biblioteca/vendor/animate.css/animate.min.css',
        '../biblioteca/vendor/hs-megamenu/src/hs.megamenu.css',
        '../biblioteca/vendor/fancybox/jquery.fancybox.css',
        '../biblioteca/vendor/slick-carousel/slick/slick.css',
        '../biblioteca/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',

        '../biblioteca/vendor/custombox/dist/custombox.min.css',
        '../biblioteca/vendor/animate.css',
        
        '../biblioteca/vendor/bootstrap-select/dist/css/bootstrap-select.min.css',
        '../biblioteca/vendor/cubeportfolio/css/cubeportfolio.min.css',
        '../biblioteca/vendor/summernote/dist/summernote-lite.css',
        '../biblioteca/vendor/bootstrap-tagsinput/css/bootstrap-tagsinput.css',

        // datatime and date
        '../biblioteca/vendor/flatpickr/dist/flatpickr.min.css',
        '../biblioteca/vendor/custombox/dist/custombox.min.css',
        '../biblioteca/vendor/chartist/dist/chartist.min.css',
        '../biblioteca/vendor/chartist-js-tooltip/chartist-plugin-tooltip.css',
        '../plugins/fullcalendar/css/fullcalendar.min.css',
        //'../plugins/fullcalendar/css/fullcalendar.print.min.css',

        //'../biblioteca/js/select2.min.css',
        

      

      
      
    ];
    public $js = [
        //counter
        '../biblioteca/vendor/appear.js',

        // vendor
        '../biblioteca/vendor/jquery/dist/jquery.min.js',
        '../biblioteca/vendor/jquery-migrate/dist/jquery-migrate.min.js',

        '../biblioteca/vendor/popper.js/dist/umd/popper.min.js',
        '../biblioteca/vendor/bootstrap/bootstrap.min.js',
        '../biblioteca/vendor/hs-megamenu/src/hs.megamenu.js',
        '../biblioteca/vendor/svg-injector/dist/svg-injector.min.js',
        '../biblioteca/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
        '../biblioteca/vendor/jquery-validation/dist/jquery.validate.min.js',
        '../biblioteca/vendor/fancybox/jquery.fancybox.min.js',
        '../biblioteca/vendor/typed.js/lib/typed.min.js',
        '../biblioteca/vendor/slick-carousel/slick/slick.js',
        '../biblioteca/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js',
        '../biblioteca/vendor/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js',
        '../biblioteca/vendor/summernote/dist/summernote-lite.js',
        '../biblioteca/vendor/custombox/dist/custombox.min.js',
        '../biblioteca/vendor/custombox/dist/custombox.legacy.min.js',
        '../biblioteca/vendor/datatables/media/js/jquery.dataTables.min.js',

        // maps
        //'../biblioteca/vendor/gmaps/gmaps.min.js',
        
        // datatime and date
        '../biblioteca/vendor/flatpickr/dist/flatpickr.min.js',
        '../biblioteca/vendor/chartist/dist/chartist.min.js',
        '../biblioteca/vendor/chartist-js-tooltip/chartist-plugin-tooltip.js',
        '../biblioteca/vendor/circles/circles.min.js',

        // Select
        '../biblioteca/vendor/bootstrap-select/dist/js/bootstrap-select.min.js',
        '../biblioteca/js/select2.full.js',
        '../biblioteca/js/dynamic_form.js',
        
        //'../biblioteca/js/select2.full.min.js',

        // component
        '../biblioteca/js/hs.core.js',
        '../biblioteca/js/components/hs.bg-video.js',
        '../biblioteca/js/components/hs.chartist-area-chart.js',
        '../biblioteca/js/components/hs.chartist-bar-chart.js',
        '../biblioteca/js/components/hs.chart-pie.js',
        '../biblioteca/js/components/hs.clipboard.js',
        '../biblioteca/js/components/hs.countdown.js',
        '../biblioteca/js/components/hs.counter.js',
        '../biblioteca/js/components/hs.cubeportfolio.js',
        '../biblioteca/js/components/hs.datatables.js',
        '../biblioteca/js/components/hs.focus-state.js',
        '../biblioteca/js/components/hs.g-map.js',
        '../biblioteca/js/components/hs.go-to.js',
        '../biblioteca/js/components/hs.datatables.js',
        '../biblioteca/js/components/hs.header.js',
        '../biblioteca/js/components/hs.header-fullscreen.js',
        '../biblioteca/js/components/hs.malihu-scrollbar.js',
        '../biblioteca/js/components/hs.modal-window.js',
        '../biblioteca/js/components/hs.onscroll-animation.js',
        '../biblioteca/js/components/hs.progress-bar.js',
        '../biblioteca/js/components/hs.range-datepicker.js',
        '../biblioteca/js/components/hs.scroll-effect.js',
        '../biblioteca/js/components/hs.scroll-nav.js',
        '../biblioteca/js/components/hs.selectpicker.js',
        '../biblioteca/js/components/hs.show-animation.js',
        '../biblioteca/js/components/hs.step-form.js',
        '../biblioteca/js/components/hs.sticky-block.js',
        '../biblioteca/js/components/hs.summernote-editor.js',
        '../biblioteca/js/components/hs.svg-injector.js',
        '../biblioteca/js/components/hs.toggle-state.js',
        '../biblioteca/js/components/hs.unfold.js',
        '../biblioteca/js/components/hs.validation.js',
        '../biblioteca/js/theme-custom.js',

        '../js/popup.js',
        '../plugins/jQuery-slimScroll/jquery.slimscroll.min.js',
        '../biblioteca/vendor/summernote/lang/summernote-pt-PT.js',

        '../plugins/fullcalendar/js/moment.min.js',
        '../https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.js',
        '../plugins/fullcalendar/js/fullcalendar.min.js',

        // Carousel

        '../biblioteca/vendor/svg-injector/dist/svg-injector.min.js',
        '../biblioteca/js/components/hs.svg-injector.js',
        '../biblioteca/js/components/hs.slick-carousel.js',

        '../js/newjs/bootbox.min.js',
        '../plugins/dropzone/dropzone.js',
        '../js/save.js',
    ];
    public $depends = [
         'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
