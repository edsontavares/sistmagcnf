<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * @author John Martin <john.itvn@gmail.com>
 * @since 1.0
 */
class CrudAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
      //'css/site.css',
     //-- Google Fonts -->
     '//fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700',

     // CSS Implementing Plugins -->
     '../biblioteca/vendor/font-awesome/css/fontawesome-all.min.css',
     '../biblioteca/vendor/animate.css/animate.min.css',
     '../biblioteca/vendor/hs-megamenu/src/hs.megamenu.css',
     '../biblioteca/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
     '../biblioteca/vendor/fancybox/jquery.fancybox.css',
     '../biblioteca/vendor/cubeportfolio/css/cubeportfolio.min.css',
     '../biblioteca/vendor/summernote/dist/summernote-lite.css',
     '../biblioteca/vendor/bootstrap-tagsinput/css/bootstrap-tagsinput.css',
     '../biblioteca/vendor/flatpickr/dist/flatpickr.min.css',
     '../biblioteca/vendor/custombox/dist/custombox.min.css',
     '../biblioteca/vendor/dzsparallaxer/dzsparallaxer.css',
     '../biblioteca/vendor/dzsparallaxer/dzsparallaxer.scss',
     '../biblioteca/vendor/chartist/dist/chartist.min.css',
     '../biblioteca/vendor/chartist-js-tooltip/chartist-plugin-tooltip.css',
     '../biblioteca/vendor/slick-carousel/slick/slick.css',

     // CSS Front Template -->
     '../biblioteca/css/theme.css',
     '../biblioteca/css/style.css',

     // Toaster
     '../css/libs/toastr.min.css',

     // Placeholder Loading
     // https://zalog.ro/placeholder-loading/
     '../css/libs/placeholder_loading.min.css',

     // Select
     '../biblioteca/vendor/bootstrap-select/dist/css/bootstrap-select.min.css',

     // Dropzone
     //'css/libs/dropzone.css',
     '../plugins/dropzone/dropzone.css',
     '../plugins/fullcalendar/css/fullcalendar.min.css',

  ];
  public $js = [

      'https://apis.google.com/js/platform.js',
      /* FULLCALENDAR PLUGINS */

      '../biblioteca/vendor/jquery/dist/jquery.min.js',
      '../biblioteca/vendor/jquery-migrate/dist/jquery-migrate.min.js',

      '../biblioteca/vendor/custombox/dist/custombox.min.js',
      '../biblioteca/vendor/custombox/dist/custombox.legacy.min.js',
      '../biblioteca/vendor/svg-injector/dist/svg-injector.min.js',

      '../biblioteca/js/components/hs.modal-window.js',
      '../biblioteca/js/components/hs.svg-injector.js',

      'js/libs/toastr.min.js',

      // Loadsh
      '../js/libs/lodash.min.js',

      // modal popup
      //'js/libs/popup.js',
       '../js/popup.js',

      // slim scroll
      '../js/libs/slimscroll.min.js',

       '../biblioteca/vendor/popper.js/dist/umd/popper.min.js',
       '../plugins/fullcalendar/js/moment.min.js',
       'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment-with-locales.js',
       '../plugins/fullcalendar/js/fullcalendar.min.js',

  ];
  public $depends = [
      //'yii\web\YiiAsset',
      //'yii\bootstrap\BootstrapAsset',
  ];
}
