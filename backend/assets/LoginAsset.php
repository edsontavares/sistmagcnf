<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
         '../biblioteca/css/theme.css',
         '../biblioteca/css/login.css',
         '../biblioteca/vendor/font-awesome/css/fontawesome-all.min.css',
    ];
    public $js = [
        '../assets/js/components/hs.validation.js',
        '../assets/js/hs.core.js',
        '../assets/vendor/jquery/dist/jquery.min.js',
        '../assets/vendor/jquery-migrate/dist/jquery-migrate.min.js',
        '../assets/vendor/popper.js/dist/umd/popper.min.js',
        '../assets/vendor/bootstrap/bootstrap.min.js',
    ];
    public $depends = [
       'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
