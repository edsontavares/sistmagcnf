<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'timezone' => 'Atlantic/Cape_Verde',
    'layout' => 'main',
    'modules' => [
        'user' => [
            // following line will restrict access to admin page
            'as backend' => 'dektrium\user\filters\BackendFilter',
            'layout' => '@backend/views/layouts/login',
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'authTimeout' => 5000,
           
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            //'class' => 'yii\web\DbSession',
            'timeout' => 10,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'maxSourceLines' => 200,
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                'login' => 'site/login',
                //'novo-curso' => 'site/criar-curso',
                'logout' => 'site/logout',
                'signup' => 'site/signup',
               // 'notf_delete/<id:\d+>' => 'site/delete',
                'paciente' => 'patient',
                'patient/create' => 'patient/create',
                'patient/delete/<id:\d+>' => 'patient/delete',
                'consulta' => 'query',
                'consulta-ortopidia' => 'query/index',
                'consulta-concluida' => 'query/query',
                'delete-query/<id:\d+>' => 'query/delete',
                'delete-consulta/<id:\d+>' => 'consulta/delete',
                'consulta-fisioterapia' => 'consulta/indexx',
                'location'=>'location/index',
                'location-delete/<id:\d+>'=>'location/delete',

                'categoria-delete/<id:\d+>' => 'employees-type/delete',


                'miShortOrthosis' => 'mi-short-orthosis/index',
                'Protese-tibial' => 'tribal-prosthesis/index',
                'Protese-femoral' => 'fermural-prosthesis/index',
                'adaptcao' => 'adptacao/index',
                'seguimento' => 'follow-up/index',
                'tipo-funcionario'=> 'employees-type/index',
                'funcionario'=>'employees/index',
                'funcionario-delete/<id:\d+>'=>'employees/delete',
                'feria'=>'fair/index',
                'feria_delete/<id:\d+>'=>'fair/delete',
                'feria_active/<id:\d+>'=>'fair/active',
                'feria_desable/<id:\d+>'=>'fair/desable',
                'view'=>'employees/view',
                'ortopidia'=>'site/ortopidia',
                'fisioterapia'=>'site/fisioterapia',
                'funcionario-criar'=>'employees/create',
                'calendar'=>'calendar/calendar',
                'calendar/create'=>'calendar/create',
                'lista'=>'calendar/lista',
                'agenda'=>'agenda/index',
                'agenda/create/<id:\d+>'=>'agenda/create',
                'agenda/calendario'=>'agenda/calendario',
                'agendas'=>'agendas/index',
                
            ],
        ],
        
    ],
    'params' => $params,
];
