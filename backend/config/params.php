<?php
return [
    'urlPath' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]",
    'adminEmail' => 'admin@example.com',
    'jquery' => '<script src="/assets/25490e29/jquery.js"></script>',
    'jqueryy' => '<script src="/assets/3c4261d7/jquery.js"></script>',
    'baseUrl' => (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]",
];
