<?php

namespace backend\controllers;

use Yii;
use common\models\Adptacao;
use common\models\AdptacaoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use Mpdf\Mpdf;
use common\componentes\Create_docad;

/**
 * AdptacaoController implements the CRUD actions for Adptacao model.
 */
class AdptacaoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Adptacao models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdptacaoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $item_numbers = Yii::$app->request->get('item_numbers');
         $adptacao = Adptacao::find()->orderBy(['create_at'=>SORT_DESC ]);
         $countQuery = clone  $adptacao;
         $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
          $adptacao   =   $adptacao  ->offset($pages->offset)->limit($pages->limit)->all();
 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'adptacao' =>  $adptacao,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

    /**
     * Displays a single Adptacao model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {   
        $model = $this->findModel($id);
        $query= \common\models\Query::findOne($model->id_query);
        $patient = \common\models\Patient::findOne($query->id_patient); 
        return $this->render('view', [
            'model' => $model,
            'patient'=> $patient,
        ]);
    }

    /**
     * Creates a new Adptacao model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Adptacao();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "criado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_adptacao]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Adptacao model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success-create', "Foi atualizado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_adptacao]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Adptacao model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success-create', "Foi Eliminado com sucesso");
        return $this->redirect(['index']);
    }


     // fsioterapia 
     public function actionConcluded($id)
     {
         $model = $this->findModel($id);
         $query = \common\models\query::findOne($model->id_query);
          $patient = \common\models\Patient::findOne($query->id_patient);
         $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
         $PatientState->state = 14;
         $PatientState->id_adptacao = $model->id_adptacao;
        
         if( $PatientState->save(false)){
             $model->state =20;
             $model->update_at = date('y-m-d');
             $model->save(false);

             $query = new \common\models\Query();
             $query->state =20;
             $model->save(false);

             //notificaco
             $notificacao = new \common\models\Notification();
             $notificacao->tables = 'ortopidia';
             $notificacao->id_conteudo= $model->id_adptacao;
             $notificacao->titile = 'ortopidia';
             //$notificacao->update_at = date('y-m-d');
             $notificacao->state = 1;
             $notificacao->save(false);
         }
         Yii::$app->session->setFlash('success-create', "foi concluido com sucesso.");
         return $this->redirect(['index']);
     }

   
    protected function findModel($id)
    {
        if (($model = Adptacao::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelByToken($id)
    {
        if (($model = Adptacao::find()->where(['id_adptacao' => $id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDownload($download = true)
    {
        
        new Create_docad($this->findModelByToken(Yii::$app->request->get('id')), $download);
    }
  
}
