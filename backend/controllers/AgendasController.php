<?php

namespace backend\controllers;

use Yii;
use common\models\Calendar;
use common\models\PatientState;
use common\models\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class AgendasController extends Controller
{
   
   /* public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    
    public function actionIndex()
    {
         
        $calendars = Calendar::find()->all();
         
        // foreach($calendars as $calendar){
        //     $calendar = \yii2fullcalendar\models\Calendar();
        //     $calendar->id_calendar = $calendar->id_calendar;
        //     $calendar->name = $calendar->name;
        //     $calendar->created_at = date('y-m-d');
        //     $calendars [] = $calendar;

        // }
        
        return $this->render('index', [
           'calendars' => $calendars,
        ]);
    }

    // public function actionIndex($id)
    // {
      
    //     $notification = \common\models\Notification::find()->where(['tables'=>'fisioterapia', 'titile'=>'FTP','id_conteudo'=>$id])->one();
    //     if (@$notification) {
    //         $notification->delete();
    //     }
    //     return $this->render('index', []);
    // }
    public function actionCreate()
    {
        $model = new Calendar();
         
        if ($model->load(Yii::$app->request->post())) {
             $model->create_at=date('y-m-d H:m:i');
             if($model->save(false)){               
                $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
                $PatientState->state = 10;
                $PatientState->id_calendar = $model->id_calendar;
                $PatientState->save(false);              
            }
            //Yii::$app->session->setFlash('error-create', yii::t('app','Foto de capa faq. Tente outra!'));
            Yii::$app->session->setFlash('success-create', "agendamento foi Realizado com sucesso.");
            return $this->redirect(['calendar']);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }
    public function actionLista()
    {
       
        $calendar = Calendar::find()->all();
        return $this->render('lista', [
            'calendar' => $calendar,
            
        ]);
    }
}