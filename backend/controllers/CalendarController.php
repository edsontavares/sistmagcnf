<?php

namespace backend\controllers;

use Yii;
use common\models\Calendar;
use common\models\PatientState;
use common\models\Patient;
use common\models\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class CalendarController extends Controller
{
   
   /* public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/
    
    public function beforeAction($action)
    {
        if ($action->id === "calendario" || $action->id === "Avaliacao" || $action->id === "delete" || $action->id === "calendar" ) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionCalendario(){
      
        $id_calendar = Yii::$app->request->get('id_calendar');
        $calendario = (new \yii\db\Query())->select(
            [
                "name_patient AS name_patient",
                "CONCAT(data, 'T', hora_inicio) AS start",
                "id_calendar",
                "data as date",
            ]
        )->from('calendar')->where(["id_calendar"=>$id_calendar])->all();
        // die(var_dump(json_encode($calendario)));
       
        return json_encode($calendario);
        //$html = str_replace(Yii::$app->params['jquery'], '', $html);
        //return $html;
    }

    public function actionAvaliacao(){

        $id_calendar = Yii::$app->request->get('id_calendar');
        $html = $this->renderAjax('calendar',[
            'id_calendar'=>$id_calendar,
        ]);
        $html = str_replace(Yii::$app->params['jquery'], '', $html);
        return $html;
    }

    
    public function actionCalendar()
    {
       
        $calendars = Calendar::find()->where(['name'=>'avaliacao']) ->all();
         
        $html = $this->render('index', [
           'calendars' => $calendars,
        ]);
        $html = str_replace(Yii::$app->params['jquery'], '', $html);
        return $html;
    }

   


    public function actionIndex($id)
    {
      
        $notification = \common\models\Notification::find()->where(['tables'=>'fisioterapia', 'titile'=>'FTP','id_conteudo'=>$id])->one();
        if (@$notification) {
            $notification->delete();
        }
        $html =  $this->render('index', []);
        $html = str_replace(Yii::$app->params['jquery'], '', $html);
        return $html;
    }
    public function actionCreate()
    {
        $model = new Calendar();

        $tipo = Yii::$app->request->get('tipo');
        
        if ($model->load(Yii::$app->request->post())) {
            $Patient = \common\models\Patient::find()->where(['id_patient'=> $model->id_patient])->one();
             $model->create_at=date('y-m-d H:m:i');
             $model->name_patient = $Patient->name;
             $model->type = $tipo;
             if($model->save(false)){               
                $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
                $PatientState->state = 2;
                $PatientState->id_calendar = $model->id_calendar;
                $PatientState->save(false);              
            }
            //Yii::$app->session->setFlash('error-create', yii::t('app','Foto de capa faq. Tente outra!'));
            Yii::$app->session->setFlash('success-create', "agendamento foi Realizado com sucesso.");
            return $this->redirect(['calendar']);
        }

        $html =  $this->renderAjax('create', [
            'model' => $model,
        ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);

    }
    public function actionLista()
    {
       
        $calendar = Calendar::find()->all();
        return $this->render('lista', [
            'calendar' => $calendar,
            
        ]);
    }

    public function actionCall()
    {
       
        $calendar = Calendar::find()->all();
        return $this->render('lista', [
            'calendar' => $calendar,
            
        ]);
    }
}