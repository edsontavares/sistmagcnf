<?php

namespace backend\controllers;

use Yii;
use common\models\Employees;
use common\models\Fair;
use common\models\EmployeesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\filters\AccessControl;
use yii\data\Pagination;
use common\models\Files;
use \common\models\UploadHandler;
use yii\db\Query;


class EmployeesController extends Controller
{
   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],

                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete','apagar'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

   
    public function actionAjaxSearch()
    {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $employeesSearch = Yii::$app->request->post()['search'];
        

        $result = Employees::find()
        ->andWhere(['like', 'name',$employeesSearch] )
        //->orderBy('employees.created_at desc')
        ->all();
        
        $i =0;
        foreach ($result as $res){
            if($res['state'] == 1): $status='Ativo';
            elseif($res['state'] == -1):  $status= 'Desativado'; endif;

            if($res['hiring'] == 0): $test='Tempo inteiro';
            elseif($res['hiring'] == 1):  $test= 'Temporário';
            elseif($res['hiring'] == 2):  $test= 'Prestação de serviço';
            elseif($res['hiring'] == 3):  $test= 'Part-Time';
            elseif($res['hiring'] == 4):  $test= 'Voluntariado';
            elseif($res['hiring'] == 5):  $test= 'Estagio'; endif;
            //$image = Yii::$app->mycomponent->verificarImg($res['logo']);
            $dados[$i]['id_employees'] = $res['id_employees'];
            $dados[$i]['name'] = $res['name'];
            $dados[$i]['type'] = $res->employeesTypes->name;
            $dados[$i]['hiring'] = $test;
            $dados[$i]['status'] = $status;
            $i++;
        }
        return @$dados;    
    }
    public function actionIndex()
    {
        $searchModel = new EmployeesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
        $employees = Employees::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $employees ;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $employees  = $employees ->offset($pages->offset)->limit($pages->limit)->all();
        $html = $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'employees' => $employees,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
            //'pagination' => $pagination,
        ]);
        return str_replace('<script src="/assets/3c426147/jquery.js"></script>', '', $html);

    }

   
    public function actionView($id)
    {
        $fair = Fair::find()->where(['id_employees' => $id])->andWhere('date_end  >= CURDATE()')->one();
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-assinatura']) {
                $modelFile = new UploadHandler('image-assinatura');

                if ($modelFile->validate()) {

                    $uploaded = $modelFile->upload('employees', true);
                } else {
                    return json_encode(['message' => 'Ficheiro  invalida. Tente outra!', 'type' => 'error']);
                }

                $modelFile = new Files();
                $extension = $uploaded['extension'];
                $filename = time() . '.' . $uploaded['extension'];
                $modelFile->nome = $uploaded['name'];
                $modelFile->model = 'employees';
                $modelFile->model_id = $model->id_employees;
                $modelFile->nome_original = $uploaded['original_name'];
                $modelFile->mim_type = $uploaded['type'];
                $modelFile->medio_type = $uploaded['extension'];
                $modelFile->full_path = $uploaded['full_path'];
                $modelFile->save(false);       
            }

            return $this->redirect(['view', 'id' => $model->id_employees]);

        }
    
        $html = $this->render('view', [
            'model' => $model,
            'fair'=> $fair,
        ]);
        $html = str_replace('<script src="/assets/1dfed011/jquery.js"></script>', '', $html);
        return $html;
    }

    public function actionApagar($id)
    {
        $files = Files::find()->where(['id_files' => $id])->one();
        $files->delete(false);
        Yii::$app->session->setFlash('success-ficheiro', "Ficheiro foi eliminado com sucesso");
        return $this->redirect(['view', 'id' =>  $files->model_id]);
    }

    
    public function actionCreate()
    {
        $model = new Employees();

        if ($model->load(Yii::$app->request->post())) {
          
          
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;

            if ($_FILES['image-perfil'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-perfil');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('employees', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success-create', "Funcionario ".$model->name." foi registado com sucesso.");
                return $this->redirect(['view', 'id' => $model->id_employees]);
            }

        }
    

        if (Yii::$app->request->isAjax){
            $html = $this->renderAjax('create', [
                'model' => $model
            ]);
            $html = str_replace(Yii::$app->params['jquery'], '', $html);
            return $html;
        }
        else
        $html = $this->render('create', [
                'model' => $model,
            ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    }
    public function actionCreat()
    {
      
        $model = new Employees();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_employees]);
        }

        return $this->renderAjax('form', [
            'model' => $model,
        ]);
    }





    public function actionForm($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
           
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_employees]);
        }
    
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if ($_FILES['image-perfil'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-perfil');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('employees', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
           
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Os dados do funcionario ".$model->name." foi atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_employees]);
        }
    
        $html = $this->render('update', [
            'model' => $model,
        ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);

    }

    public function actionDelete($id)
    {
       
        $model = $this->findModel($id);
        $model->delete();
        return json_encode(['type' => 'success']);
    }

    protected function findModel($id)
    {
        if (($model = Employees::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionActive($id)
    {
        $model= Employees::find()->where(['id_employees'=>$id])->one();
        $model->state = 1;
        $model->save(false);
        Yii::$app->session->setFlash('success-active', "Funcionario ".$model->name." foi Activado com sucesso.");
        return $this->redirect(['view', 'id' => $model->id_employees]);
    }
    
    public function actionDesable($id)
    {
        $model= Employees::find()->where(['id_employees'=>$id])->one();
        $model->state = -1;
        $model->save(false);
        Yii::$app->session->setFlash('success-desable', "Funcionario ".$model->name." foi Desativado com sucesso.");
        return $this->redirect(['view', 'id' => $model->id_employees]);
    }
}
