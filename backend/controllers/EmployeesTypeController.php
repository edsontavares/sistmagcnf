<?php

namespace backend\controllers;

use Yii;
use common\models\EmployeesType;
use common\models\EmployeesTypeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * EmployeesTypeController implements the CRUD actions for EmployeesType model.
 */
class EmployeesTypeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }
    public function beforeAction($action)
    {
        if ($action->id === "delete") {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all EmployeesType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmployeesTypeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
         $typeEmployees = EmployeesType::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone  $typeEmployees ;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
         $typeEmployees  =  $typeEmployees ->offset($pages->offset)->limit($pages->limit)->all();
        $model = new EmployeesType();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'typeEmployees'=>$typeEmployees,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers,
            'model'=> $model,
            
        ]);
    }

    /**
     * Displays a single EmployeesType model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmployeesType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmployeesType();
         
        if ($model->load(Yii::$app->request->post())) {
             $model->create_at=date('y-m-d H:m:i');
             $model->save(false);
             Yii::$app->session->setFlash('success-create', "Categoria foi criado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_employees_type]);
        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmployeesType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success-create', "Categoria ".$model->name." foi atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_employees_type]);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmployeesType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //$this->findModel($id)->delete();
        $model = $this->findModel($id);
        $model->delete();
        return json_encode(['type' => 'success']);
        // Yii::$app->session->setFlash('success-create', "Categoria ".$model->name." foi Eliminado com sucesso.");
        // return $this->redirect(['index']);
    }

    /**
     * Finds the EmployeesType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployeesType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployeesType::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
