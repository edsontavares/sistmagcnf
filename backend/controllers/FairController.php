<?php

namespace backend\controllers;

use Yii;
use common\models\Fair;
use common\models\FairSearch;
use PHPUnit\Framework\Constraint\IsFalse;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * FairController implements the CRUD actions for Fair model.
 */
class FairController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete','active','desable'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all Fair models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FairSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
        $fair = Fair::find()->orderBy(['create_at'=>SORT_DESC ]);
      
        $countQuery = clone $fair;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $fair = $fair->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'fair' => $fair,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

    /**
     * Displays a single Fair model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Fair model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Fair();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Feria foi Registado com sucesso.");
            return $this->redirect(['index']);
        }

        $html = $this->renderAjax('create', [
            'model' => $model,
        ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);

    }

    /**
     * Updates an existing Fair model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->save(False);
            Yii::$app->session->setFlash('success-create', "Feria foi Atualizado com sucesso.");
            return $this->redirect(['index']);
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        return json_encode(['type' => 'success']);
    }

    public function actionActive($id)
    {
        $model= Fair::find()->where(['id_fair'=>$id])->one();
        $model->state = 1;
        $model->save(false);
        // Yii::$app->session->setFlash('success-create', "Funcionario ".$model->name." foi Activado com sucesso.");
        // return $this->redirect(['index']);
        return json_encode(['model' => 'jobs', 'type' => 'success', 'message' => yii::t('app','Rascunho rejeitado com sucesso')]);

    }
    
    public function actionDesable($id)
    {
        $model = Fair::find()->where(['id_fair'=>$id])->one();
        $model->state = 0;
        $model->save(false);
        // Yii::$app->session->setFlash('success-create', "Funcionario ".$model->name." foi Desativado com sucesso.");
        // return $this->redirect(['index']);
        return json_encode(['model' => 'jobs', 'type' => 'success', 'message' => yii::t('app','Rascunho rejeitado com sucesso')]);

    }

    protected function findModel($id)
    {
        if (($model = Fair::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
