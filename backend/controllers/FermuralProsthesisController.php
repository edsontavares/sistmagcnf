<?php

namespace backend\controllers;

use Yii;
use common\models\FermuralProsthesis;
use common\models\FermuralProsthesisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf\Mpdf;
use yii\data\Pagination;

/**
 * FermuralProsthesisController implements the CRUD actions for FermuralProsthesis model.
 */
class FermuralProsthesisController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FermuralProsthesis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FermuralProsthesisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $item_numbers = Yii::$app->request->get('item_numbers');
        $fermuralProsthesis = FermuralProsthesis::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $fermuralProsthesis  ;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $fermuralProsthesis   =  $fermuralProsthesis  ->offset($pages->offset)->limit($pages->limit)->all();


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'fermuralProsthesis' =>  $fermuralProsthesis,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

    /**
     * Displays a single FermuralProsthesis model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FermuralProsthesis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FermuralProsthesis();
        $fittype="";
        $amputation ="";
        $suspension="";
        if ($model->load(Yii::$app->request->post())) {

            $listSide = $model->fit_type;
            if (!empty($listSide)) {
                $fittype = '"'. implode(',', $listSide).'"';
            }
            $model->fit_type= $fittype  ;
    
            $listAmputation= $model->amputation_pass;
            if (!empty($listAmputation)) {
                $amputation = '"'. implode(',', $listAmputation).'"';
            }
            $model->amputation_pass=  $amputation ;

            $listSuspension= $model->suspension;
            if (!empty($listSuspension)) {
                $suspension = '"'. implode(',', $listSuspension).'"';
            }
            $model->suspension=  $suspension ;

            $quey = \common\models\Query::find()->where(['id_patient'=>$model->id_patient])->one();
            $model->id_query  = $quey->id_query;

            $model->create_at=date('y-m-d');
            $model->state= 1;
            $model->save(false);

            Yii::$app->session->setFlash('success-create', "foi Registado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_fermural_Prosthesis]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FermuralProsthesis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $fittype="";
        $amputation ="";
        $suspension="";
        if ($model->load(Yii::$app->request->post())) {

            $listSide = $model->fit_type;
            if (!empty($listSide)) {
                $fittype = '"'. implode(',', $listSide).'"';
            }
            $model->fit_type= $fittype  ;
    
            $listAmputation= $model->amputation_pass;
            if (!empty($listAmputation)) {
                $amputation = '"'. implode(',', $listAmputation).'"';
            }
            $model->amputation_pass=  $amputation ;

            $listSuspension= $model->suspension;
            if (!empty($listSuspension)) {
                $suspension = '"'. implode(',', $listSuspension).'"';
            }
            $model->suspension=  $suspension ;

            $model->save(false);

            Yii::$app->session->setFlash('success-create', "foi atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_fermural_Prosthesis]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FermuralProsthesis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success-create', "foi eliminado com sucesso.");
        return $this->redirect(['index']);
    }

    /**
     * Finds the FermuralProsthesis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FermuralProsthesis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FermuralProsthesis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('download',[
        'model' => $model, 
          //'user' => $user,
          'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }


     // fsioterapia 
     public function actionConcluded($id)
     {
         $model = $this->findModel($id);
         $query = \common\models\query::findOne($model->id_query);
         $patient = \common\models\Patient::findOne($query->id_patient);
         $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
         $PatientState->state = 14;
         $PatientState->id_fermural_Prosthesis = $model->id_fermural_Prosthesis;
        
         if( $PatientState->save(false)){
             $model->state =20;
             $model->update_at = date('y-m-d');
             $model->save(false);
 
             $query = new \common\models\Query();
             $query->state =20;
             $model->save(false);
 
             //notificaco
             $notificacao = new \common\models\Notification();
             $notificacao->tables = 'ortopidia';
             $notificacao->id_conteudo= $model->id_fermural_Prosthesis;
             $notificacao->titile = 'ortopidia';
             //$notificacao->update_at = date('y-m-d');
             $notificacao->state = 1;
             $notificacao->save(false);
         }
         Yii::$app->session->setFlash('success-create', "foi concluido com sucesso.");
         return $this->redirect(['index']);
     }
}
