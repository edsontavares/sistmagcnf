<?php

namespace backend\controllers;

use Yii;
use common\models\FollowUp;
use common\models\FollowUpSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf\Mpdf;
use yii\data\Pagination;

/**
 * FollowUpController implements the CRUD actions for FollowUp model.
 */
class FollowUpController extends Controller
{
    /**
     * {@inheritdoc}
     */
   

    public function beforeAction($action)
        {
            if (in_array($action->id, array('delete'))) {
                $this->enableCsrfValidation = false;
            }
            return parent::beforeAction($action);
        }

    /**
     * Lists all FollowUp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FollowUpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       

        $item_numbers = Yii::$app->request->get('item_numbers');
        $followup = FollowUp::find()->orderBy(['create_at'=>SORT_DESC ]);
         $countQuery = clone$followup  ;
         $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $followup   = $followup  ->offset($pages->offset)->limit($pages->limit)->all();
 
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'followup' => $followup,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

    /**
     * Displays a single FollowUp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FollowUp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FollowUp();

        if ($model->load(Yii::$app->request->post())) {
            $model->create_at=date('y-m-d');
            $model->state= 1;
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Foi registado novo seguimento com sucesso");
            return $this->redirect(['view', 'id' => $model->id_follow_up]);
        }

        $html = $this->render('create', [
            'model' => $model,
        ]);
        $html = str_replace(Yii::$app->params['jquery'], '', $html);
        return $html;
    }

    /**
     * Updates an existing FollowUp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        
        $model = $this->findModel($id);
       
        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_follow_up]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FollowUp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FollowUp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FollowUp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FollowUp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        $follwup = FollowUp::find()->where(['id_patient'=>$model->id_patient])->all();
        $pdf_content = $this->renderPartial('download',[
        //'model' => $model, 
        'follwup' => $follwup , 
        //'user' => $user,
        'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }
}
