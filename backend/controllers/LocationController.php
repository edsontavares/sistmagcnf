<?php

namespace backend\controllers;

use Yii;
use common\models\Location;
use common\models\LocationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }

    public function beforeAction($action) 
    { 
        $this->enableCsrfValidation = false; 
        return parent::beforeAction($action); 
    }

    /**
     * Lists all Location models.
     * @return mixed
     */

    public function actionAjaxSearch()
    {
        
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $employeesSearch = Yii::$app->request->post()['search'];
        

        $result = Location::find()
        ->andWhere(['like', 'zone',$employeesSearch] )
        //->orderBy('employees.created_at desc')
        ->all();
    
        $i =0;
        foreach ($result as $res){
            //$image = Yii::$app->mycomponent->verificarImg($res['logo']);
            $dados[$i]['id_location'] = $res['id_location'];
            $dados[$i]['parents'] = $res['parents'];
            $dados[$i]['island'] = $res['island'];
            $dados[$i]['city'] = $res['city'];
            $dados[$i]['zone'] = $res['zone'];
            $dados[$i]['street'] = $res['street'];
            
            
            $i++;
        }
        return @$dados;    
    }
    public function actionIndex()
    {
        $searchModel = new LocationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
        $locations = Location::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $locations ;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $locations  = $locations ->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'locations' =>$locations,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
            //'pagination' => $pagination,
        ]);
    }

    /**
     * Displays a single Location model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Location model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Location();

        if ($model->load(Yii::$app->request->post())) {
             $model->state = 1;
             $model->create_at=date('y-m-d H:m:i');
            
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Localização foi criado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_location]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Location model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
           
            $model->update_at=date('y-m-d H:m:i');
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Localização  foi atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_location]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Location model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete(false);
        // Yii::$app->session->setFlash('success-create', "Localização foi eliminado com sucesso.");
        // return $this->redirect(['index']);
        return json_encode(['type' => 'success']);
    }

    /**
     * Finds the Location model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Location the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Location::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
