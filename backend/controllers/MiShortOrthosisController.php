<?php

namespace backend\controllers;

use Yii;
use common\models\MiShortOrthosis;
use common\models\Query;
use common\models\MiShortOrthosisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf\Mpdf;
use yii\data\Pagination;

/**
 * MiShortOrthosisController implements the CRUD actions for MiShortOrthosis model.
 */
class MiShortOrthosisController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all MiShortOrthosis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MiShortOrthosisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $item_numbers = Yii::$app->request->get('item_numbers');
        $miShortOrthosis = MiShortOrthosis::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $miShortOrthosis;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $miShortOrthosis  = $miShortOrthosis ->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'miShortOrthosis' => $miShortOrthosis,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers,
        ]);
    }

    /**
     * Displays a single MiShortOrthosis model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MiShortOrthosis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
           
        $model = new MiShortOrthosis();
        $pairing ="";
        $number ="";
        if ($model->load(Yii::$app->request->post())) {
           
            $listSide = $model->pairing_side;
           
            $quey = Query::find()->where(['id_patient'=>$model->id_patient])->one();
            
            if (!empty($listSide)) {
                $pairing = '"'. implode(',', $listSide).'"';
            }
            $model->pairing_side= $pairing ;

            $listSides = $model->side_number;
            if (!empty($listSides)) {
                $number = '"'. implode(',', $listSides).'"';
            }
            $model->side_number=  $number ;
            
            $model->id_query  = $quey->id_query;
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;
            $query = \common\models\query::findOne($model->id_query);
            $patient = \common\models\Patient::findOne($query->id_patient);
           
            //$model->save(false);
            if($model->save(false)){
                $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
                $PatientState->state = 11;
                $model->update_at=date('y-m-d H:m:i');
                $PatientState->id_mi_short_orthosis= $model->id_mi_short_orthosis;
                $PatientState->save(false);
            }
           
            Yii::$app->session->setFlash('success-create', "Foi registado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_mi_short_orthosis]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MiShortOrthosis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_mi_short_orthosis]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MiShortOrthosis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MiShortOrthosis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MiShortOrthosis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MiShortOrthosis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

     // fsioterapia 
     public function actionConcluded($id)
     {
         $model = $this->findModel($id);
         $query = \common\models\query::findOne($model->id_query);
         $patient = \common\models\Patient::findOne($query->id_patient);
         $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
         $PatientState->state = 14;
         $PatientState->id_query = $model->id_query;
        
         if( $PatientState->save(false)){
             $model->state =20;
             $model->update_at = date('y-m-d');
             $model->save(false);

             $query = new \common\models\Query();
             $query->state =20;
             $model->save(false);

             //notificaco
             $notificacao = new \common\models\Notification();
             $notificacao->tables = 'ortopidia';
             $notificacao->id_conteudo= $model->id_mi_short_orthosis;
             $notificacao->titile = 'ortopidia';
             //$notificacao->update_at = date('y-m-d');
             $notificacao->state = 1;
             $notificacao->save(false);
         }
         Yii::$app->session->setFlash('success-create', "foi concluido com sucesso.");
         return $this->redirect(['index']);
     }


    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('download',[
        'model' => $model, 
          //'user' => $user,
          'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }
}
