<?php

namespace backend\controllers;

use Yii;
use common\models\patient;
use common\models\Files;
use common\models\PatientState;
use common\models\patientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \common\models\UploadHandler;
use yii\web\UploadedFile;
use yii\data\Pagination;
use common\componentes\Create_doc;



class PatientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    /*public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }*/
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionAjaxSearch()
    {
        
        $result = Patient::find()
        ->orderBy('patient.created_at desc')
        ->all();

        return $this->render('index', [
            'result' => $result
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new patientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
        $patient = PatientState::find()->orderBy(['create_at'=>SORT_DESC ]);
      
        $countQuery = clone $patient;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $patient = $patient->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'patient' => $patient,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
            //'pagination' => $pagination,
        ]);
    }

    public function actionLista(){
       
        $item_numbers = Yii::$app->request->get('item_numbers');
        $patient = PatientState::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $patient;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $patient = $patient->offset($pages->offset)->limit($pages->limit)->all();

        return $this->render('lista', [
            'patient' => $patient,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

 
    public function actionView($id)
    {
            
        $notification = \common\models\Notification::find()->where(['tables'=>'ortopidia', 'titile'=>'ortopidia','id_conteudo'=>$id])->one();
        if (@$notification) {
            $notification->delete();
        }
           $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-assinatura']) {
                $modelFile = new UploadHandler('image-assinatura');

                if ($modelFile->validate()) {

                    $uploaded = $modelFile->upload('patient', true);
                } else {
                    return json_encode(['message' => 'Ficheiro  invalida. Tente outra!', 'type' => 'error']);
                }

                $modelFile = new Files();
                $extension = $uploaded['extension'];
                $filename = time() . '.' . $uploaded['extension'];
                $modelFile->nome = $uploaded['name'];
                $modelFile->model = 'patirnt';
                $modelFile->model_id = $model->id_patient;
                $modelFile->nome_original = $uploaded['original_name'];
                $modelFile->mim_type = $uploaded['type'];
                $modelFile->medio_type = $uploaded['extension'];
                $modelFile->full_path = $uploaded['full_path'];
                $modelFile->save(false);       
            }
            Yii::$app->session->setFlash('success-ficheiro', "Ficheiro foi enexado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_patient]);

        }
      
        $html =  $this->render('view', [
            'model' => $model,
        ]);
        $html = str_replace(Yii::$app->params['jqueryy'], '', $html);
          return $html;
    }


    public function actionApagar($id)
    {
        $files = Files::find()->where(['id_files' => $id])->one();
        $files->delete(false);
        Yii::$app->session->setFlash('success-ficheiro', "Ficheiro foi eliminado com sucesso");
        return $this->redirect(['view', 'id' =>  $files->model_id]);
    }

    
    public function actionCreate()
    {
        $model = new patient();

        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-perfil'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-perfil');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('pacientPerfil', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            

            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;

            //$arrayLanguages = Yii::$app->request->post('Language');
            //$arrayLanguageClean = json_encode($arrayLanguages['Language']);
            //$model->name_parents = $arrayLanguageClean;
          
            //save phase
            if($model->save(false)){
                $PatientState = new PatientState();
                $PatientState->state = 1;
                $PatientState->create_at=date('y-m-d H:m:i');
                $PatientState->id_patient = $model->id_patient;
            
                $PatientState->save(false);


                $file = UploadedFile::getInstancesByName('file');
                foreach ($file as $files) {
                    $modelFile = new Files();
                    $extension = $files->extension;
                    $filename = time() . '.' . $extension;
                    $modelFile->nome = $filename;
                    $modelFile->model = 'patient';
                    $modelFile->model_id = $model->id_patient;
                    $modelFile->nome_original = $files->name;
                    $modelFile->mim_type = $files->type;
                    $modelFile->medio_type = $extension;
                    $modelFile->full_path = 'recurso/' . $modelFile->model . '/' . $filename;
                    $files->saveAs(__DIR__ . '/../../' . $modelFile->full_path);
                    $modelFile->save(false);
                }
            }

            Yii::$app->session->setFlash('success-create', "Paciente foi criado com sucesso! verefica se os dados estão corecto.");
            return $this->redirect(['view', 'id' => $model->id_patient]);
        }
        // return $this->render('create', [
        //     'model' => $model,
        // ]);
        
        $html = $this->render('create', [
                'model' => $model,
        ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
        
    }

    /**
     * Updates an existing patient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-perfil'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-perfil');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('pacientPerfil', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem  = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            $files = UploadedFile::getInstance($model, 'bi_ficheiro');
            if ($files) {
                $urls = '../recurso/pacientPerfil/bi' . Yii::$app->security->generateRandomString() . '.' . $files->extension;
                if ($files->saveAs($urls))
                    $model->bi_ficheiro = $urls;
            }
            // $arrayRequirements = Yii::$app->request->post('Requirement');
            //     if (count($arrayRequirements['Requirement']) > 0) {
            //         $arrayRequirementsClean = array_map(array($this, "filterList"), $arrayRequirements['Requirement']);
            //         $arrayRequirementsClean = json_encode($arrayRequirementsClean);
            //         $model->requerimentos = $arrayRequirementsClean;
            //     }
            // $arrayLanguages = Yii::$app->request->post('Language');
            // if (count($arrayRequirements['Requirement']) > 0) {
            // $arrayLanguageClean = json_encode($arrayLanguages['Language']);
            // $model->name_parents = $arrayLanguageClean;
            // }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_patient]);
        }

        /*return $this->render('update', [
            'model' => $model,
        ]);*/
        $html = $this->render('update', [
            'model' => $model,
        ]);
        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    
    }

    /**
     * Deletes an existing patient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $patient = $this->findModel($id);
        $patientState = \common\models\PatientState::find()->where(['id_patient'=>$id])->all();
        if ($patientState) {
                foreach ($patientState as  $patientStates) {
                    $patientStates->delete(false);
                }
        }

        $query = \common\models\Query::find()->where(['id_patient'=>$id])->all();
        if ($query) {
            foreach ($query as  $queri) {
                $queri->delete(false);
            }
        }
        $payment = \common\models\Payment::find()->where(['id_patient'=>$id])->all();
        if ($payment) {
            foreach ($payment as  $payments) {
                $payments->delete(false);
            }
        }
        $adptacao = \common\models\Adptacao::find()->where(['id_patient'=>$id])->all();
        if ($adptacao ) {
            foreach ($adptacao  as  $adptaca ) {
                $adptaca->delete(false);
            }
        }
        $fermuralProsthesis = \common\models\FermuralProsthesis::find()->where(['id_patient'=>$id])->all();
        if ($fermuralProsthesis) {
            foreach ($fermuralProsthesis  as  $fermural) {
                $fermural->delete(false);
            }
        }
        $files = \common\models\Files::find()->where(['model_id'=>$id])->all();
        if ($files) {
            foreach ($files  as  $file) {
                $file->delete(false);
            }
        }
        $followUp = \common\models\FollowUp::find()->where(['id_patient'=>$id])->all();
        if ($followUp) {
            foreach ($followUp  as  $followUps) {
                $followUps->delete(false);
            }
        }
        $minerva = \common\models\Minerva::find()->where(['id_patient'=>$id])->all();
        if ($minerva) {
            foreach ($minervas as  $minervas) {
                $minervas->delete(false);
            }
        }
        $miShortOrthosis = \common\models\MiShortOrthosis::find()->where(['id_patient'=>$id])->all();
        if ($miShortOrthosis) {
            foreach ($miShortOrthosis as  $miShortOrthosiss) {
                $miShortOrthosiss->delete(false);
            }
        }
        $tribalProsthesis = \common\models\tribalProsthesis::find()->where(['id_patient'=>$id])->all();
        if ($tribalProsthesis) {
            foreach ($tribalProsthesis as  $tribalProsthesiss) {
                $tribalProsthesiss->delete(false);
            }
        }

        $notification = \common\models\Notification::find()->where(['id_conteudo'=>$id])->all();
        if ($notification) {
            foreach ($notification as  $notifications) {
                $notifications->delete(false);
            }
        }
        
        $patient ->delete(false);

        return json_encode(['type' => 'success']);
        //return $this->redirect(['index']);
    }

    /**
     * Finds the patient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return patient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = patient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    protected function findModelByToken($id)
    {
        if (($model = Patient::find()->where(['id_patient' => $id])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDownload($download = true)
    {
        
        new Create_doc($this->findModelByToken(Yii::$app->request->get('id')), $download);
    }
}
