<?php

namespace backend\controllers;

use Yii;
use common\models\Payment;
use common\models\PatientState;
use common\models\PaymentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use common\models\PaymentState;
use Mpdf\Mpdf;
use \common\mail\layouts\CriarEmail;


class PaymentController extends Controller
{
   
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    
    public function actionIndex()
    {
        $searchModel = new PaymentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $item_numbers = Yii::$app->request->get('item_numbers');
        $payment = Payment::find()->orderBy(['create_at'=>SORT_DESC ]);
        $countQuery = clone $payment;

        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
        $payment = $payment->offset($pages->offset)->limit($pages->limit)->all();
        $model = new Payment();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'payment' => $payment,
            'model'=>$model,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
        ]);
    }

    
    public function actionView($id)
    {
        $notificacao = \common\models\Notification::find()->where(['tables'=>'pagamento','id_conteudo'=>$id])->one();
        if (@$notificacao->state== 1){
        $notificacao->state = 2;
        $notificacao->save(false);
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
        
    }
    
    public function actionCriar($id)
    {
       
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
        {
           /* if($model->qt){
               
                $parcarious = \common\models\Precarious::find()->where(['code'=>$model->code])->one();
              
                $model = new PaymentState();
                $model->state = 1;
                $model->preco = $parcarious->price;
                $model->code = $parcarious->code;
                $model->save(false);
                //$this->sendMessagePayment($model);
                return $this->redirect(['update', 'id' => $model->id_payment]);
                
            }*/
            $model->update_at=date('y-m-d H:m:i');
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_payment]);
        }
         
        return $this->render('form', [
            'model' => $model,
        ]);
    }

    public function actionAdcionar($id)
    {

        die(var_dump($id));
       
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
        {
            $model->update_at=date('y-m-d H:m:i');
            $model->save(false);
            return $this->redirect(['criar', 'id' => $model->id_payment]);
        }
         
        return $this->render('form', [
            'model' => $model,
        ]);
    }

 
    public function actionCreate()
    {
        $model = new Payment();

        if ($model->load(Yii::$app->request->post()) ) {

          
            
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;

            if($model->interva == 4){
                $model->interva= 4;
            }
            
            if($model->type == 'Recibo'){
                
                if($model->save(false)){

                     
                    $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
                    
                    if($PatientState->state == 3){
                        $PatientState->state = 8;
                        $PatientState->id_payment= $model->id_payment;
                        $PatientState->save(false);
                    } elseif($PatientState->state == 16){
                        $PatientState->state = 7;
                        $PatientState->id_payment= $model->id_payment;
                        $PatientState->save(false);
                    }else{
                        $PatientState->state = $PatientState->state;
                        $PatientState->id_payment= $model->id_payment;
                        $PatientState->save(false);
                    }

                    //notificaco
                    $notificacao = new \common\models\Notification();
                    $notificacao->tables = 'pagamento';
                    $notificacao->id_conteudo= $model->id_payment;
                    $notificacao->titile = 'pagamento';
                    $notificacao->create_at = date('y-m-d');
                    $notificacao->state = 1;
                    $notificacao->save(false);
                }
            }
            $model->save(false);
            //$this->sendMessagePayment($model);
            return $this->redirect(['criar', 'id' => $model->id_payment]);
        }

        $html = $this->render('create', [
            'model' => $model,
        ]);
        return  str_replace('../biblioteca/vendor/jquery/dist/jquery.min.js','', $html);
    }

    
    public function actionUpdate($id)
    {
       
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) 
        {
            if($model->qt){
               
                $parcarious = \common\models\Precarious::find()->where(['code'=>$model->code])->one();
              
                $model = new PaymentState();
                $model->state = 1;
                $model->preco = $parcarious->price;
                $model->code = $parcarious->code;
                $model->save(false);
                //$this->sendMessagePayment($model);
                return $this->redirect(['update', 'id' => $model->id_payment]);
                
            }
            $model->update_at=date('y-m-d H:m:i');
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id_payment]);
        }
         
        return $this->render('update', [
            'model' => $model,
        ]);
    }

   
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        if (($model = Payment::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('recibo',[
        'model' => $model, 
        //'user' => $user,
        'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }

    private function sendMessagePayment($model){
        $to[] = 'edson.eng.info@gmail.com';
    //$to[] = info@waoempleos.com;
    $from = "Cenorf.com<edson.eng.info@gmail.com>";
    $ficheiros = [];
    $subject = 'Pago';
    $view =
        '<div  style="padding:0%; color:#222;">
            <p>'.$model->amount.'</p>
            <p>'.$model->value.'</p>
            <p>'.$model->type.'</p>
            <p>'.yii::t('app','Assunto').' - <strong style="color:#222; font-weight:600; font-size:18px;">'.$model->coin.'</strong></p>
            <p>'.yii::t('app','Mensagem').'<br><strong style="color:#222; font-weight:600; font-size:18px;">'.$model->create_at.'</strong></p>
        </div>';
    $emails = new CriarEmail($subject, $view);
    $emails->send($to[0], 'edson.eng.info@gmail.com>',['edson.eng.info@gmail.com>'=>'edson.eng.info@gmail.com>'], [],$ficheiros);
    }

    private function sendMessage($model)
       {

           $to = ["info@empregos.cv"];

           $from = "Empregos.cv<info@empregos.cv>";
           $ficheiros = [];

           $subject = "Registo de um nova Pedido de empregada na plataforma Empregos.CV " . $model->nome;

           $view =
               '<div  style="padding:0%; color:#222;">
               <p>Registo de um novo Pedido de serviços domesticos' . $model->nome. '</strong> na plataforma empregos.CV<br><br></p>

           <p>
               <a href="https://dev.empregos.cv/admin/index.php?r=procura/empregada" style="background:#39f; padding:6px 12px; border-radius:4px; color:#fff; text-decoration:none;">Ver empregada aqui</a>
               <br>
           </p>
       </div>';
           $emails = new CriarEmail($subject, $view);
           $emails->send($to, 'info@empregos.cv', ['info@empregos.cv' => 'Empregos.cv'], [], $ficheiros);
       }

}


