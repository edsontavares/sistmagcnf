<?php

namespace backend\controllers;

use common\models\Patient;
use Yii;
use backend\models\User;
use common\models\Query;
use common\models\PatientState;
use common\models\QuerySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Mpdf\Mpdf;
use kartik\mpdf\Pdf;
use yii\data\Pagination;


/**
 * QueryController implements the CRUD actions for Query model.
 */
class QueryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    // public function behaviors()
    // {
    //     return [
    //         'verbs' => [
    //             'class' => VerbFilter::className(),
    //             'actions' => [
    //                 'delete' => ['POST'],
    //             ],
    //         ],
    //     ];
    // }
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    
    public function actionIndex()
    {
        $id = Yii::$app->request->get('id');
        $notification = \common\models\Notification::find()->where(['tables'=>'pagamento','id_conteudo'=>$id])->one();
            // die(var_dump($notificacao));
            if (@ $notification) {
               $notification->delete();
            }
        $searchModel = new QuerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $ortopidia = Query::find()->where(['tepy_query'=>'OTP'])->orderBy(['create_at'=>SORT_DESC ]);
        $pagination = new Pagination([
            'defaultPageSize' => 20,
            'totalCount' =>  $ortopidia ->count(),
        ]);

       $ortopidia   = $ortopidia  ->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ortopidia' => $ortopidia ,
            'pagination' => $pagination,
        ]);
    }

    public function actionQuery()
    {
        $searchModel = new QuerySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Query::find();
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' =>   $query ->count(),
        ]);

        $query   =  $query  ->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('query', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'query' => $query,
            'pagination' => $pagination,
        ]);
    }

    
    public function actionView($id)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionViews($id)
    {
        
        $notification = \common\models\Notification::find()->where(['tables'=>'ortopidia', 'titile'=>'ortopidia','id_conteudo'=>$id])->one();
        if (@$notification) {
            $notification->delete();
        }
        $notificatio = \common\models\Notification::find()->where(['tables'=>'fisioterapia', 'titile'=>'FTP','id_conteudo'=>$id])->one();
        if (@$notificatio) {
            $notificatio->delete();
        }
        $patientState = PatientState::find()->One();
        return $this->render('views', [
            'model' => $this->findModel($id),
            'patientState'=>$patientState,
        ]);
    }

    public function actionImprimir($id)
    {
        return $this->render('imprimir', [
            'model' => $this->findModel($id),
        ]);
    }

    
    public function actionCreate()
    {
        $model = new Query();
          //$sides ="";
         
        if ($model->load(Yii::$app->request->post())) {
           
            //$listSide = $model->side;
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;
            $model->tepy_query = 'OTP';
            // if($model->tepy_query){
            //     $model->tepy_query = 'OTP';
            //     $sides = '"'. implode(',',$listSide).'"';
            //     $model->side= $sides;
            // }
            //$model->state= 1;
           
            if($model->save(false)){
                $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
                $PatientState->state = 21;
                $PatientState->id_query = $model->id_query;
                $PatientState->save(false);
            }
            Yii::$app->session->setFlash('create-sucesso', "Foi registado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_query]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    


    public function actionCreatee()
    {
        $model = new Query();
       
        if ($model->load(Yii::$app->request->post())) {
            
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 10;
            $model->tepy_query = 'FTP';           
            if($model->save(false)){               
                $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
                $PatientState->state = 21;
                $PatientState->id_query = $model->id_query;
                $PatientState->save(false);              
            }

            $model->save(false);
            
            return $this->redirect(['views', 'id' => $model->id_query]);
        }

        return $this->render('createe', [
            'model' => $model,
        ]);
    }
   
    public function actionUpdate($id)
    {
      
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('create-sucesso', "Os dados foi atualizado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_query]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    // update the entratamento 
    public function actionUpdated($id)
    {
 
        $model = $this->findModel($id);
        $model->state = 11;
        if($model->save(false)){
            $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
            $PatientState->state = 11;
            $model->update_at=date('y-m-d H:m:i');
            $PatientState->id_query = $model->id_query;
            $PatientState->save(false);
        }
        return $this->redirect(['index']);
        
    }

    
    public function actionDelete($id)
    {
    //   $fisio = \common\models\Notification::find()->where(['idconteudo' => $id, 'tables'=>'fisioterapia'])->all();
    //   if (@$fisio) {
    //     foreach ($fisio as $key => $fisios) {
    //         $fisios->delete();
    //     }
    //   }
        $this->findModel($id)->delete();
        //return $this->redirect(['index']);
        return json_encode(['type' => 'success']);
    }

   
    protected function findModel($id)
    {
        if (($model = Query::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRelatorio($id)
    {
        //$query = Query::find()->where(['state'=>10])->all();
        //$model = $this->findModel($id);
        $model= User::find()->one();
        return $this->render('relatorio', [
            
            'model' => $model, 
        ]);
    }

    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('relatorio',[
        'model' => $model, 
          //'user' => $user,
          'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }

    public function actionImprimirr($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('imprimir',[
        'model' => $model, 
          //'user' => $user,
          'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }
     // fsioterapia 
    public function actionConcluded($id)
    {
        $model = $this->findModel($id);
        
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 14;
        $PatientState->id_query = $model->id_query;
       
        if( $PatientState->save(false)){
            $model->state =20;
            $model->update_at = date('y-m-d');
            $model->save(false);
            //notificaco
            $notificacao = new \common\models\Notification();
            $notificacao->tables = 'fisioterapia';
            $notificacao->id_conteudo= $model->id_query;
            $notificacao->titile = $model->tepy_query;
            //$notificacao->update_at = date('y-m-d');
            $notificacao->state = 1;
            $notificacao->save(false);
        }
        Yii::$app->session->setFlash('success-create', "foi concluido com sucesso.");
        return $this->redirect(['query']);
    }

    public function actionCancelar($id)
    {
        $model = $this->findModel($id);
        
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 4;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
        if( $PatientState->save(false)){
            $model->state =31;
            $model->save(false);
        }
        Yii::$app->session->setFlash('success-create', "Foi cancelado com sucesso.");
        return $this->redirect(['query']);
    }

    public function actionOrtopidia($id)
    {
        $model = $this->findModel($id);
        
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 5;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
      
        if( $PatientState->save(false)){
            $model->state =30;
            $model->tepy_query = 'OTP';
            $model->save(false);           
        }
        Yii::$app->session->setFlash('success-create', "Foi enviado com sucesso.");
        return $this->redirect(['indexx']);
    }

    public function actionFisioterapia($id)
    {
        $model = $this->findModel($id);
        
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 6;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
      
        if( $PatientState->save(false)){
            $model->state =30;
            $model->tepy_query = 'FTP';
            $model->save(false);           
        }
        Yii::$app->session->setFlash('success-create', "Foi enviado com sucesso.");
        return $this->redirect(['index']);
    }

    public function actionMedico($id)
    {
        $model = $this->findModel($id);       
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 17;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
        if( $PatientState->save(false)){
            $model->state =33;  
            $model->save(false);
        }
        Yii::$app->session->setFlash('success-create', "Foi enviado com sucesso!");
        return $this->redirect(['indexx']);
    }

    public function actionPagamento($id)
    {
        $model = $this->findModel($id);       
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 3;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
        if( $PatientState->save(false)){
            $model ->state = 34;  
            $model->save(false);    
        }
        Yii::$app->session->setFlash('success-create', "Foi esnviado com sucesso.");
        return $this->redirect(['indexx']);
    }

    public function actionPagament($id)
    {
        $model = $this->findModel($id);       
        $PatientState = PatientState::find()->where(['id_patient'=> $model->id_patient])->one();
        $PatientState->state = 16;
        $PatientState->update_at = date('y-m-d H:m:i');
        $PatientState->id_query = $model->id_query;
        if( $PatientState->save(false)){
            $model ->state = 34;  
            $model->save(false);    
        }
        Yii::$app->session->setFlash('success-create', "Foi esnviado com sucesso.");
        return $this->redirect(['index']);
    }
   
}
