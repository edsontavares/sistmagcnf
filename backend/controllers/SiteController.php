<?php
namespace backend\controllers;

use Yii;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\SignupForm;
use \common\models\Patient;
use \common\models\Employees ;
use \common\models\AuthItem;
use common\models\Session;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['login', 'error','signup'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout' ,'signup'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

  
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

   


    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($year = null)

    {

        $patient = Patient::find();
        $totalsession = Session::find();
        $employees = Employees::find();
        $query = \common\models\Query::find();
        $query_otp = \common\models\Query::find()->where(['tepy_query'=>'OTP']);
        $query_ftp = \common\models\Query::find()->where(['tepy_query'=>'FTP']);
        $_paciente = (new Query())->select(['MONTH(create_at) as month', 'COUNT(id_patient) as total'])->from('patient')->groupBy('month');
        $_sessao = (new Query())->select(['MONTH(creat_at) as month', 'COUNT(id) as total'])->from('session')->where("MONTH(creat_at) IS NOT NULL")->groupBy('month');
        $_setion = (new Query())->select(['MONTH(creat_at) as month', 'COUNT(id) as total'])->from('session')->groupBy('month');

        $patientIsland = (new Query())
        ->select('COUNT(patient.id_patient) as total, island')
        ->from('patient')
        ->where('island is not null')
        ->groupBy('island');

        $_tepy_query_otp = (new Query())
        ->select(['MONTH(create_at) as month', 'COUNT(id_query) as total'])
        ->from('query')
        ->where("MONTH(create_at) IS NOT NULL")->groupBy('month')
        ->groupBy('create_at');

        if (@$year) {
            $patient = $patient->andWhere(['year(from_unixtime(`create_at`))' => $year]);
            $employees = $employees->andWhere(['year(from_unixtime(`create_at`))' => $year]);
            $query = $query->andWhere(['year(from_unixtime(`create_at`))' => $year]);
            $query_otp =  $query_otp->andWhere(['year(from_unixtime(`create_at`))' => $year]);
            $query_ftp =  $query_ftp->andWhere(['year(from_unixtime(`create_at`))' => $year]);
            $patientIsland = $patientIsland->andWhere(['year(date(patient.`create_at`))' => $year]);
            $_paciente  = $_paciente ->andWhere(['year(date(`create_at`))' => $year]);
            $_sessao = $_sessao->andWhere(['year(date(`creat_at`))' => $year]);
            $_tepy_query_otp = $_tepy_query_otp->andWhere(['year(date(`create_at`))' => $year]);
            $_setion = $_setion ->andWhere(['year(date(`creat_at`))' => $year]);




        }
        $totalsession= $totalsession->all();
        $patient = $patient->all();
        $employees = $employees->all();
        $query = $query->all();
        $query_otp =  $query_otp->all();
        $query_ftp =  $query_ftp->all();
        $patientIsland = $patientIsland->all();
        $_paciente = $_paciente->all();
        $_sessao = $_sessao->all();
        $_tepy_query_otp = $_tepy_query_otp->all();
        $_setion = $_setion->all();

        $patient = count($patient);
        $employees = count($employees);
        $query = count($query);
        $query_otp = count($query_otp);
        $query_ftp = count($query_ftp);
        $total_session = count($totalsession);


        $patientByIsland = [
            'data' => [],
            'label' => []
        ];
        foreach ($patientIsland as $item) {
            $patientByIsland['label'][] = $item['island'];
            $patientByIsland['data'][] = intval($item['total']);
        }

        $paciente = [
            'data' => [],
            'label' => []
        ];
        // $months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outobro', 'Novembro', 'Dezembro'];
        $months = [ yii::t('app','Jan'), yii::t('app','Fev'), yii::t('app','Mar'), yii::t('app','Abr'), yii::t('app','Mai'), yii::t('app','Jun'), yii::t('app','Jul'), yii::t('app','Ago'), yii::t('app','Set'), yii::t('app','Out'), yii::t('app','Nov'), yii::t('app','Dez')];
        foreach ($_paciente as $item) {
             $paciente['label'][] = $months[intval($item['month']) - 1];
             $paciente['data'][] = intval($item['total']);
        }

        $setion = [
            'data' => [],
            'label' => []
        ];
        foreach ($_setion as $item) {
            $setion['label'][] = $months[intval($item['month']) - 1];
            $setion['data'][] = intval($item['total']);
       }

        $sessao = [
            'data' => [],
            'label' => []
        ];
        foreach ($_sessao as $item) {
            $sessao['label'][] = $months[intval($item['month']) - 1];
            $sessao['data'][] = intval($item['total']);
        }


        $tepy_query_otp = [
            'data' => [],
            'label' => []
        ];
        foreach ($_tepy_query_otp as $item) {
            $tepy_query_otp['label'][] = $months[intval($item['month']) - 1];
            $tepy_query_otp['data'][] = intval($item['total']);
        }


        return $this->render('index', [
             'patient' => $patient,
             'employees'=>$employees,
             'query'=>$query,
             'query_otp'=>$query_otp,
             'query_ftp'=> $query_ftp,
             'patientByIsland' => $patientByIsland,
             'paciente' =>  $paciente,
             'sessao' => $sessao,
             'tepy_query_otp' => $tepy_query_otp,
             'total_session' => $total_session,
             'setion' => $setion
        ]);
    }

    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $notification = \common\models\Notification::find()->where(['id_conteudo'=>$id])->one();
        if (@$notification) {
            $notification->delete();
        }
        return json_encode(['type' => 'success']);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $this->layout = 'Login_layout';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $this->layout = 'Login_layout';
        $model = new SignupForm();
        $authItems =  AuthItem::find()->all();


        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->goHome();
        }

        return $this->render('signup', [
            'model' => $model,
            'authItems'=> $authItems,
        ]);
    }


    public function actionOrtopidia()
    {
        
        return $this->render('Ortopidia', [
            
        ]);
    }

    public function actionFisioterapia()
    {
        
        return $this->render('fisioterapia', [
            
        ]);
    }

    

}
