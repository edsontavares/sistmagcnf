<?php

namespace backend\controllers;

use Yii;
use common\models\TribalProsthesis;
use common\models\TribalProsthesisSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use Mpdf\Mpdf;

/**
 * TribalProsthesisController implements the CRUD actions for TribalProsthesis model.
 */
class TribalProsthesisController extends Controller
{
    /**
     * {@inheritdoc}
     */
   /* public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }*/

    /**
     * Lists all TribalProsthesis models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TribalProsthesisSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
         $item_numbers = Yii::$app->request->get('item_numbers');
         $tribalProsthesis = TribalProsthesis::find()->orderBy(['create_at'=>SORT_DESC ]);
         $countQuery = clone $tribalProsthesis  ;
         $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSizeLimit' => [$item_numbers, $item_numbers]]);
         $tribalProsthesis   =  $tribalProsthesis  ->offset($pages->offset)->limit($pages->limit)->all();
 
        
         return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tribalProsthesis' => $tribalProsthesis,
            'pagination' => $pages,
            'total' => $countQuery->count(),
            'item_numbers' => $item_numbers
             
        ]);
    }

    /**
     * Displays a single TribalProsthesis model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TribalProsthesis model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TribalProsthesis();
        $fit_types ="";

        if ($model->load(Yii::$app->request->post())) {
           
            $fit_type = $model->fit_type;
            if (!empty($fit_type)) {
                $fit_types = '"'. implode(',', $fit_type).'"';
            }
            $model->fit_type = $fit_types;
            $quey = \common\models\Query::find()->where(['id_patient'=>$model->id_patient])->one();
            $model->id_query  = $quey->id_query;
            $model->create_at=date('y-m-d H:m:i');
            $model->state= 1;

            $query = \common\models\query::findOne($model->id_query);
            $patient = \common\models\Patient::findOne($query->id_patient);
            if($model->save(false)){
                $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
                $PatientState->state = 11;
                $model->update_at=date('y-m-d H:m:i');
                $PatientState->id_tribal_prosthesis= $model->id_tribal_prosthesis;
                $PatientState->save(false);
            }
            Yii::$app->session->setFlash('success-create', "criado com sucesso");
            return $this->redirect(['view', 'id' => $model->id_tribal_prosthesis]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TribalProsthesis model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success-create', "foi atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id_tribal_prosthesis]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TribalProsthesis model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success-create', "foi eliminado com sucesso.");
        return $this->redirect(['index']);
    }

    /**
     * Finds the TribalProsthesis model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TribalProsthesis the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TribalProsthesis::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionBaixar($id){
        //$user = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $model = $this->findModel($id);
        //$model= Query::find()->one();
        $pdf_content = $this->renderPartial('download',[
        'model' => $model, 
        //'user' => $user,
        'type'=> 'download'
        ]);
        $mpdf = new Mpdf;
        $mpdf->WriteHTML($pdf_content);
        $mpdf->Output();
        exit;
    }

    // fsioterapia 
    public function actionConcluded($id)
    {
        $model = $this->findModel($id);
        $query = \common\models\query::findOne($model->id_query);
        $patient = \common\models\Patient::findOne($query->id_patient);
        $PatientState = \common\models\PatientState::find()->where(['id_patient'=> $patient->id_patient])->one();
        $PatientState->state = 14;
        $PatientState->id_tribal_prosthesis = $model->id_tribal_prosthesis;
       
        if( $PatientState->save(false)){
            $model->state =20;
            $model->update_at = date('y-m-d');
            $model->save(false);

            $query = new \common\models\Query();
            $query->state =20;
            $model->save(false);

            //notificaco
            $notificacao = new \common\models\Notification();
            $notificacao->tables = 'ortopidia';
            $notificacao->id_conteudo= $model->id_tribal_prosthesis;
            $notificacao->titile = 'ortopidia';
            //$notificacao->update_at = date('y-m-d');
            $notificacao->state = 1;
            $notificacao->save(false);
        }
        Yii::$app->session->setFlash('success-create', "foi concluido com sucesso.");
        return $this->redirect(['index']);
    }

}
