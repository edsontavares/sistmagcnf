<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \common\models\UploadHandler;
use frontend\models\ChangePasswordForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UsersController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $user = User::find()->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' =>  $user
        ]);
    }

    public function actionPerfil($id)
    {
        $users = User::find()->where(['id'=>$id])->one();
        return $this->render('perfil', [
            'users' =>  $users
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-user'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-user');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('imgUtilizador', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->image = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            $model->create_at=date('y-m-d H:m:i');
            $model->status= 1;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-user'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-user');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('imgUtilizador', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            $model->updated_at=date('y-m-d H:m:i');
            $model->status= 1;
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Utilizador foi Atualizado com sucesso.");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $html = $this->renderAjax('update', [
            'model' => $model,
        ]);

        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    }

    public function actionUpdates($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($_FILES['image-user'] /*&& $_FILES['image-capa']['name'] != $capa['original_name']*/) {
                $file = new UploadHandler('image-user');
                if ($file->validate()) {
                    // salvar foto
                    $uploaded = $file->upload('imgUtilizador', true);
                    //setcookie('cur_cap', serialize($uploaded), time() + 50000, '/');
                    $model->imagem = $uploaded['full_path'];
                } else {
                    Yii::$app->session->setFlash('error-create', "Foto de perfil invalida. Tente outra!");
                }
            }
            $model->updated_at=date('y-m-d H:m:i');
            $model->status= 1;
            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Utilizador foi Atualizado com sucesso.");
            return $this->redirect(['perfil', 'id' => $model->id]);
        }

        $html = $this->renderAjax('form', [
            'model' => $model,
        ]);

        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    }

    public function actionEditar($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->save(false);
            Yii::$app->session->setFlash('success-create', "Palavra passe foi Atualizado com sucesso.");
            return $this->redirect(['perfil', 'id' => $model->id]);
        }

        $html = $this->renderAjax('editar', [
            'model' => $model,
        ]);

        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    }

    public function actionDifinicao()
    {
        $modelUser = User::find()->where(['id'=>Yii::$app->user->identity->id])->one();
        $html = $this->render('difinicao', [
            'modelUser' => $modelUser
        ]);

        return str_replace('<script src="/sistmagcnf/admin/../biblioteca/vendor/jquery/dist/jquery.min.js"></script>', '', $html);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('error', "Utilizador foi Eliminado com sucesso.");
        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionMudar()
    {
        try {

            if (Yii::$app->user->isGuest) {
                return $this->redirect('/site/login');
            }

            $model = new ChangePasswordForm();

            if (Yii::$app->request->isAjax) {
                if ($_POST) {
                    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->changePassword()) {
                        return json_encode([
                            'status' => 'success'
                        ]);
                    } else {
                        if ($model->errors) return json_encode([
                            'status' => 'error',
                            'message' => "Senha incorreta",
                        ]);
                        else return json_encode([
                            'status' => 'error',
                            'message' => 'Não foi possivel atualizar sua senha.',
                        ]);
                    }
                } else {
                    return $this->renderAjax('resetPassword', [
                        'model' => $model,
                    ]);
                }
            }
            return $this->redirect('/empresa/empresa/perfil');
        } catch (\Exception $e) {
            return json_encode([
                'status' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
}
