<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;




class UtilizadorController extends Controller
{


    public function beforeAction($action)
    {
        if (in_array($action->id, array('delete'))) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index', [
            
        ]);
    }
}