<div class="row mx-gutters-2">
    <?php
        foreach ($modelArtigos as $modelArtigo):?>
            <?php
            $user = \frontend\models\User::findOne($modelArtigo->editor_id);
            ?>
            <div class="col-lg-4 mb-3" style="margin-bottom: 10px;">
                <article class="card">

                    <?php if ($modelArtigo->imagen_capa != ''): ?>
                        <div class="card_img" style="height: 150px; overflow: hidden;">
                            <img class="img-fluid rounded" src="<?= Yii::$app->params['urlPath'] ?>/<?= $modelArtigo->imagen_capa ?>" title="empregos.cv">

                        </div>
                    <?php endif ?>
                    <div class="px-4">
                        <ul class="list-inline d-flex align-items-center pt-3">
                            <li class="list-inline-item d-flex align-items-center pr-2">
                                <div class="u-sm-avatar mr-2">
                                    <img class="img-fluid rounded-circle" src="/<?= @$user->photo ?>">
                                </div>
                                <a class="text-secondary font-size-1"
                                   href="<?= \yii\helpers\Url::to(['/u/perfil?user_id=' . $user->id]) ?>"><?= $user->profile->name ?></a>
                            </li>
                            <li class="list-inline-item ml-auto">
                                <a class="d-flex align-items-center small text-secondary" href="javascript:;">
                                    <span class="far fa-heart mr-2"></span>
                                    3 Gosto
                                </a>
                            </li>
                        </ul>

                        <div class="row mx-gutters-2">
                            <div class="col-lg-6">
                              <?php if($modelArtigo->criado_em):?>
                                <h6 class="upper" data-value="<?= \Carbon\Carbon::parse($modelArtigo->criado_em)->format('Y-m-d') ?>">
                                    <small style="font-size: 12px; color: #77838f;"><?= ucwords(\frontend\models\Defaults::formatDate(date_create($modelArtigo->criado_em), '%d, %m %Y')) ?></small>

                                </h6>
                                <?php endif ?>
                            </div>
                            <div class="col-lg-6">
                                <?php
                                    $id = Yii::$app->security->generateRandomString(8);
                                ?>
                                <div class="position-relative" style="float: right;">
                                    <a id="settingsDropdown1InvokerExample<?= $id ?>"
                                       class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded"
                                       href="javascript:;"
                                       role="button"
                                       aria-controls="<?= $id ?>"
                                       aria-haspopup="true"
                                       aria-expanded="false"
                                       data-unfold-event="click"
                                       data-unfold-target="#<?= $id ?>"
                                       data-unfold-type="css-animation"
                                       data-unfold-duration="300"
                                       data-unfold-delay="300"
                                       data-unfold-hide-on-scroll="true"
                                       data-unfold-animation-in="slideInUp"
                                       data-unfold-animation-out="fadeOut">
                                        <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                                    </a>

                                    <div id="<?= $id ?>" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="settingsDropdown1InvokerExample<?= $id ?>" style="min-width: 180px;">
                                        <a href="#" class="paga dropdown-item" title="Remover" data-key="<?= $modelArtigo->id ?>">
                                            <i class="fas fa-times mr-2"></i> Eliminar
                                        </a>
                                        <a class="open-modal dropdown-item" href="#" id="edita"
                                           data-url="<?= Yii::$app->params['urlPath'] ?>/<?= $modelArtigo->company->url ?>/artigos/editar?artigo_id=<?= $modelArtigo->id ?>"
                                           data-modal-size="modal-xl" title="Editar artigo">
                                            <i class="fas fa-pencil-alt mr-2"></i> Editar</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h2 class="h6 pb-2">
                            <a class="open-modal" data-modal-size="modal-xl" href="#"
                               data-url="<?= Yii::$app->params['urlPath'] ?>/<?= $modelArtigo->company->url ?>/artigos/view?artigo_id=<?= $modelArtigo->id ?>"><?= $modelArtigo->title ?>
                            </a>
                        </h2>
                    </div>
                </article>
                <!-- End Article -->
            </div>
        <?php endforeach; ?>
</div>