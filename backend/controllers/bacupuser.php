<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>


    <?php $form = ActiveForm::begin(); ?>
    <div class="row mx-gutters-2">
        <div class="col-md-4">
            <label class="form-label">username <span class="text-danger">*</span></label>
            <?= $form->field($model, 'username')->textInput(['maxlength' => true, "placeholder" => "utlizador", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-4">
            <label class="form-label">email <span class="text-danger">*</span></label>
            <?= $form->field($model, 'email')->textInput(['maxlength' => true, "placeholder" => "email", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-4">
            <label class="form-label">photo <span class="text-danger">*</span></label>
            <?= $form->field($model, 'photo')->textInput(['maxlength' => true, "placeholder" => "photo", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>

        <div class="col-md-5">
            <label class="form-label">password_hash <span class="text-danger">*</span></label>
            <?= $form->field($model, 'password_hash')->textInput(['maxlength' => true, "placeholder" => "password", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-5">
            <label class="form-label">auth_key <span class="text-danger">*</span></label>
            <?= $form->field($model, 'auth_key')->textInput(['maxlength' => true, "placeholder" => "key", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-2">
            <label class="form-label">confirmed_at <span class="text-danger">*</span></label>
            <?= $form->field($model, 'confirmed_at')->textInput(['maxlength' => true, "placeholder" => "data", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>

        <div class="col-md-2">
            <label class="form-label">email_un <span class="text-danger">*</span></label>
            <?= $form->field($model, 'unconfirmed_email')->textInput(['maxlength' => true, "placeholder" => "email", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-2">
            <label class="form-label">block <span class="text-danger">*</span></label>
            <?= $form->field($model, 'blocked_at')->textInput(['maxlength' => true, "placeholder" => "block", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-2">
            <label class="form-label">ip <span class="text-danger">*</span></label>
            <?= $form->field($model, 'registration_ip')->textInput(['maxlength' => true, "placeholder" => "ip", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>

        <div class="col-md-2">
            <label class="form-label">creat_at <span class="text-danger">*</span></label>
            <?= $form->field($model, 'created_at')->textInput(['maxlength' => true, "placeholder" => "data", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-2">
            <label class="form-label">data_update <span class="text-danger">*</span></label>
            <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true, "placeholder" => "data", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
        <div class="col-md-2">
            <label class="form-label">flags <span class="text-danger">*</span></label>
            <?= $form->field($model, 'flags')->textInput(['maxlength' => true, "placeholder" => "utlizador", "class" => "form-control form-control-sm"])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary btn-sm rounded']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
