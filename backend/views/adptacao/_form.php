<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\QueryTepy;
use common\models\Patient;
use common\models\Query;
?>

<div class="adptacao-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-6">
            <label for="" class="type-label" >Nome de Paciente<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Selecionar Paciente'],
                    'pluginOptions' => [
                    'allowClear' => true,
                    'class' => 'form-control  ',
                    'required' => true,
                    'data-msg' => 'Por favor Selecionar Paciente.',
                    'data-error-class' => 'u-has-error',
                    'data-success-class' => 'u-has-success',
                    ],
                ])->label(false) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Técnico Responsavel<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'nameEmployees')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\employees::find()->all(), 'name', 'name'),
                    // 'language' => 'es',
                    'options' => [

                    'required' => true,
                    'placeholder' => yii::t('app','Técnico Responsavel'),
                    'data-msg' => yii::t('app','Por favor Técnico Responsavel'),
                    ],
                    'pluginOptions' => [
                    'tags'=>true,
                    'class' => 'form-control  ',
                    
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    ],
                ])->label(false) ?>
            </div>        
        </div>
        <div class="col-sm-12">
            <label for="" class="type-label" >Patologia</label>
            <div class="form-group">
                <?=  $form->field($model, 'id_query')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Query::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_query', 'pathology'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Selecionar Patologia'],
                    'pluginOptions' => [
                    'allowClear' => false,
                    'class' => 'form-control  ',
                    'required' => true,
                    'data-msg' => 'Por favor Selecionar Patologia.',
                    'data-error-class' => 'u-has-error',
                    'data-success-class' => 'u-has-success',
                    ],
                ])->label(false) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Almofada</label>
            <div class="form-group">
                <?= $form->field($model, 'pillow')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Almofada",
                    'aria-label' => "Introduzir Almofada",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Almofada",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Colete</label>
            <div class="form-group">
                <?= $form->field($model, 'vest')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Colete",
                    'aria-label' => "Introduzir Colete",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Colete",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Cinto Seguramça</label>
            <div class="form-group">
                <?= $form->field($model, 'seat_belt')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Cinto Seguramça",
                    'aria-label' => "Introduzir Cinto Seguramça",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Cinto Seguramça",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Apoio Cabeça</label>
            <div class="form-group">
                <?= $form->field($model, 'head_support')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Apoio Cabeça",
                    'aria-label' => "Introduzir Apoio Cabeça",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Apoio Cabeça",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Proteção de Rodas</label>
            <div class="form-group">
                <?= $form->field($model, 'wheel_protection')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Proteção de Rodas",
                    'aria-label' => "Introduzir Proteção de Rodas",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Proteção de Rodas",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Enchimento Lateral</label>
            <div class="form-group">
                <?= $form->field($model, 'side_filling')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Enchimento Lateral",
                    'aria-label' => "Introduzir Enchimento Lateral",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Enchimento Lateral",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Enchimento Posterior</label>
            <div class="form-group">
                <?= $form->field($model, 'backfill')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Enchimento Posterior",
                    'aria-label' => "Introduzir Enchimento Posterior",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Enchimento Posterior",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Cinto Proteção Pês</label>
            <div class="form-group">
                <?= $form->field($model, 'foot_support_climb')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Separador de Coixa",
                    'aria-label' => "Introduzir Separador de Coixa",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Separador de Coixa",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Cinto proteÇão Coxa</label>
            <div class="form-group">
                <?= $form->field($model, 'protection_belt')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Cinto proteÇão Coxa",
                    'aria-label' => "Introduzir Cinto proteÇão Coxa",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Cinto proteÇão Coxa",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Cinto Proteção Perna</label>
            <div class="form-group">
                <?= $form->field($model, 'foot_protection_belt')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Cinto Proteção Perna",
                    'aria-label' => "Introduzir Cinto Proteção Perna",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Cinto Proteção Perna",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        
        <div class="col-sm-6">
            <label for="" class="type-label" >Cinto Proteção Pês</label>
            <div class="form-group">
                <?= $form->field($model, 'nest_separator')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Cinto Proteção Pês",
                    'aria-label' => "Introduzir Cinto Proteção Pês",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Cinto Proteção Pês",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Outros</label>
            <div class="form-group">
                <?= $form->field($model, 'others')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Outros",
                    'aria-label' => "Introduzir Outros",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Outros",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                ])->label(false)?>
            </div>
        </div>
        <div class="col-sm-12">
            <label for="" class="type-label" >Observações</label>
            <!--?= $form->field($model, 'macha')->textarea(['rows' => 6]) ->label(false) ?-->
            <div class="u-summernote-editor">
                <textarea id="summernote" name="Adptacao[description]" rows="3" class="form-control"><?= $model->description?></textarea>
            </div>
        </div>
    </div>
    <div class="col-sm-12 text-right mt-4">
        <div class="form-group">
            <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
            <!--?= Html::submitButton('Guardar', ['class' => 'btn btn-indigo btn-sm rounded']) ?-->
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
       .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>
