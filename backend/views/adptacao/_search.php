<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\AdptacaoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adptacao-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_adptacao') ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'entry_date') ?>

    <?= $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'pillow') ?>

    <?php // echo $form->field($model, 'vest') ?>

    <?php // echo $form->field($model, 'seat_belt') ?>

    <?php // echo $form->field($model, 'head_support') ?>

    <?php // echo $form->field($model, 'wheel_protection') ?>

    <?php // echo $form->field($model, 'side_filling') ?>

    <?php // echo $form->field($model, 'backfill') ?>

    <?php // echo $form->field($model, 'nest_separator') ?>

    <?php // echo $form->field($model, 'protection_belt') ?>

    <?php // echo $form->field($model, 'foot_protection_belt') ?>

    <?php // echo $form->field($model, 'foot_support_climb') ?>

    <?php // echo $form->field($model, 'others') ?>

    <?php // echo $form->field($model, 'measure_right') ?>

    <?php // echo $form->field($model, 'id_employees') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
