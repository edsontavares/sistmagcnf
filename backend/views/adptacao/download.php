<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;
$query= \common\models\Query::findOne($model->id_query);
$patient = \common\models\Patient::findOne($query->id_patient); 

?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
    <header style=" margin-top: -0.5cm;" >    
        <div style="float:left; width:20%;">
            <img src="./../recurso/img/Logo.svg"  width="70" height="65" style="margin-top: -0.5cm;" >
        </div>
        <div style="float:left; width:80%;">
        <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
        <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
        </div>
        <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
    </header>
        <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
            <tr>
                        <td class="align-middle"><b>Nome de Paciente<b></></td>
                        <td class="text-secondary"><?= $patient->name?></td>
                    </tr>
            
            </div>
        </div>
</div>


<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>



