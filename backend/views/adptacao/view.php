<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

\yii\web\YiiAsset::register($this);
?>
<div class="query-view">

    <div class="row mx-gutters-2">
         
        <div class="col-sm-12">
            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= Yii::$app->session->getFlash("success-create") ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
         
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação Adaptações Cadeira de Roda</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/adaptcao'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i>Descaregar', ['adptacao/download', 'id'=>$model->id_adptacao], ['class' => 'dropdown-item transition-3d-hover' ,'target'=>"_blank"]) ?>  
                     <?php if($model->state == 1): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_adptacao], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>
                </div>
                </div>
            </div>
        </div>
        
    </div>
    <hr>
    <div class="card shadow-lg ">
        <div class="card-body shadow-sm p-4">
            <table style="width:100%">
                <h3 class=" font-weight-bold">Dados de Paciente </h3>
                <tbody>
                    <tr>
                        <td class="align-middle"><b>Nome de Paciente<b></></td>
                        <td class="text-secondary"><?= $patient->name?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Almofada<b></></td>
                        <td class="text-secondary" ><?= $model->pillow?></td>
                    </tr> 
                    
                    <tr>
                        <td class="align-middle"><b>Colete</b></td> <td class="text-secondary" >
                        <?= $model->vest?>
                        </td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Cinto Seguramça</b></td>       
                        <td class="text-secondary"><?= $model->seat_belt?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Apoio Cabeça</b></td>
                        <td class="text-secondary"><?= $model->head_support?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Proteção de Rodas</b></td> 
                        <td class="text-secondary"><?= $model->wheel_protection?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Enchimento Lateral</b></td> 
                        <td class="text-secondary"><?= $model->side_filling?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Enchimento Posterior</b></td> 
                        <td class="text-secondary"><?= $model->backfill?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Cinto Proteção Pês</b></td> 
                        <td class="text-secondary"><?= $model->foot_support_climb?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Cinto Proteção Perna</b></td> 
                        <td class="text-secondary"><?= $model->foot_protection_belt?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Cinto Proteção Pês</b></td>
                        <td class="text-secondary"><?= $model->nest_separator?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Outros</b></td> 
                        <td class="text-secondary"><?= $model->others?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Data Criação</b></td> 
                        <td class="text-secondary"><?= $model->create_at?></td>
                    </tr>
                </tbody>
            </table>
            <?php if($model->nameEmployees):?>
                <h5 class="">Técnico Responsável</h5>
                <p> - <?= $model->nameEmployees?></p>
            <?php endif;?>
            <?php if($model->description):?>
                <h5 class="mt-4 font-weight-bold">Observação</h5>
                <p><?= $model->description?></p>
            <?php endif;?>
            
        </div>
    </div>

<style>
    td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
    }
</style>
