<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Patient;
use common\models\Employees;
?>

<div class="calendar">

    <?php $form = ActiveForm::begin(
        
    ); ?>
        <div class="row">
            <div class="col-sm-12">  
                <label for="" class="type-label" >Técnico responsavel<span class="text-danger">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'id_employees')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Employees::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_employees', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Selecionar técnico'],
                        'pluginOptions' => [
                        'maxlength' => true,
                        'class' => 'form-control  ',
                        'required' => true,
                        'data-msg' => 'Por favor Selecionar técnico.',
                        'data-error-class' => 'u-has-error',
                        'data-success-class' => 'u-has-success',
                        ],
                    ])->label(false) ?>
                </div>
            </div>
            <div class="col-md-12">
            <label class="" for="signinSrEmailExample3"><?= yii::t('app', 'Data') ?></label>
                <?= $form->field($model, 'date')->textInput([
                    'placeholder' => yii::t('app','Data'),
                    'aria-label' => yii::t('app','Data'),
                    'class'=>'form-control-sm form-control',
                    'required' => true,
                    'min' =>'',
                    'aria-describedby' => "dataEntrevistaLabel",
                    'data-msg' =>yii::t('app','Por favor introduza data .'),
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'data-rp-date-format' => "Y.m.d-H.i.s",
                    'type' => 'date'    
                ]) ->label(false)?>
            </div>
            <div class="col-md-6">
                <label class="" for="signinSrEmailExample3"><?= yii::t('app', 'Hora inicio') ?></label>
                <?= $form->field($model, 'hora_inicio')->textInput(['type' => 'time', 'class'=>'form-control-sm form-control','id' => 'hora_inicio'])->label(false) ?>
            </div>
            <div class="col-md-6">
                <label class="form-labe" for="signinSrEmailExample3"><?= yii::t('app', 'Hora fim') ?> </label>
                <?= $form->field($model, 'hora_fim')->textInput(['type' => 'time','class'=>'form-control-sm form-control','id' => 'hora_fim'])->label(false)?>
            </div>
        </div>
</div>
<hr>
</div class="row">
    <div class="col-sm-12 text-right">
        <div class="form-group">  
             <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover" data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
               <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<style>
           .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>