<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="calendar">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>