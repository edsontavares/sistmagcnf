<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
$this->title =yii::t('app','Agendamento');

?>
<div class="row mb-2">
    <div class="col-sm-6">
        <a href="#" class="btn btn-nr btn-indigo font-weight-bold open-modal" id="anucio_automatico" data-modal-size="modal-lg"  data-url="<?= Url::to(['create']) ?>" data-title="<?= Yii::t('app', 'Agenda') ?>">
            <?= Yii::t('app', 'Marcar Agenda') ?>
        </a>
    </div>
    <div class="col-md-6 text-right">
       <h4 class="font-weight-bold">Agenda de Consulta fisioterapia</h4>
    </div>
</div>
<div class="card">
    <div class="card-body p-4">
        <div id="calendar"></div>
    </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js"></script>
<?php
    $script = <<<JS
    $(document).ready(function() {
      var calendar = $('#calendar').fullCalendar({
          editable:true,
          header:{
              left:'prev,next today',
              center:'title',
              right:'month,agendaWeek,agendaDay',
          }, 
          defaultDate:'2021-01-14',
          navLinks:true,
          editable:true,
          eventLimit:true,
          events: [
              {
                  title:'all day Event',
                  start:'2021-01-14',
              }
          ]

      });
     console.log(calendar);
        
    });
JS;
$this->registerJS($script);

?>
<style>
table th {
    color: #1e2022!important;
}
</style>



