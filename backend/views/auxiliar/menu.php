<?php
  use yii\helpers\Html;
  use common\models\curso\Curso;
    $this->title = 'Criar Formação';
    $controler = Yii::$app->controller->id;
?>

  <div class="bg-light">

    <ul class="nav nav-classic online" id="menu">
      <li>
          <a class="nav-link <?= $controler == 'query' ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to(['query/index']) ?>">
           Ortopedista
          </a>
      </li>
     
      <li>
          <a class="nav-link <?= $controler == 'consulta' ? 'active' : '' ?>" href="<?= \yii\helpers\Url::to(['consulta/indexx']) ?>">
           Fisioterapeuta
          </a>
      </li>
    </ul>
  </div>
  <style>
      /* .bg-light{
        height: 90px!important;
      } */
      .nav-link {
        font-size: 1.5em;
        
      }
  </style>