<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\PatientState;
use common\models\Patient;
use backend\assets\AppAsset;
$this->title =yii::t('app','Agendamento');
$urlPath = Yii::$app->params['urlPath'];
AppAsset::register($this);

?>
<div id="calendar" data-language="<?= yii::$app->language ?>" data-title-view="<?= Yii::t('app','Entrevista de trabalho com') ?>">
</div>

<?php
    $script = <<<JS
    $(document).ready(function() {
      var calendar = $('#calendar').fullCalendar({
        defaultView: 'month',
        header: {
            right: 'prev today next',
            center: 'title',
            left: 'month,agendaWeek,agendaDay'
        },
        locale: $('#calendar').attr('data-language'),
        eventSources: [
            // your event source
            {
                url: '/sistmagcnf/admin/calendar/calendario?id_calendar='+$('#calendar-select').val(),
                type: 'get',
                error: function () {
                    alert('there was an error while fetching events!');
                },
                success: function (res) {
                    console.log('ggggggggggggggggggggggggggggggggg');
                    console.log(res);
                },
                color: '#00c9a7',  // a non-ajax option
                textColor: 'white' // a non-ajax option
            },
        ],
        editable: true,
        droppable: false,
        eventResize: function (event, delta, revertFunc) {
                bootbox.confirm({
                    title: 'Alterar a hora da Entrevista?',
                    message: 'Ao efectuar essa ação o candidato será notificado a alteração da hora da entrevista',
                    //size: 'small',
                    buttons: {
                        confirm: {
                            label: 'Sim',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Não',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {

                        if (result === true) {
                            var start = event.start.format('HH:mm:SS');
                            var end = (event.end == null) ? start : event.end.format('HH:mm:SS');
                            var date = event.start.format('YYYY-MM-DD');
                            var url = "";
                            var type = 'post';
                            var formData = {
                                'hora_inicio': start,
                                'data': date,
                                'id_calendar': event.id_calendar
                            };

                            $.ajax({
                                type: type,
                                url: url,
                                data: formData,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.type === 'error') {
                                        revertFunc();
                                        toastr.error(data.message, {timeOut: 5000}).css("width", "300px");
                                    }
                                    // else {
                                    //     startDashboard();
                                    // }
                                }
                            });
                        } else {
                            revertFunc();
                        }
                    }
                });
            },

      });
     console.log(calendar);
        
    });
    
JS;
$this->registerJS($script);

?>

<style>
table th {
    color: #1e2022!important;
}
</style>



