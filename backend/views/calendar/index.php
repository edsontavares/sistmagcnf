<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;
use common\models\PatientState;
use common\models\Patient;
use common\models\Calendar;
use backend\assets\AppAsset;
$this->title =yii::t('app','Agendamento');
$urlPath = Yii::$app->params['urlPath'];
AppAsset::register($this);

?>
<div class="row mb-2">
    <div class="col-sm-6">
        <a href="#" class="btn btn-nr btn-indigo font-weight-bold open-modal" id="anucio_automatico"
            data-modal-size="modal-lg" data-url="<?= Url::to(['create']) ?>"
            data-title="<?= Yii::t('app', 'Agenda') ?>">
            <?= Yii::t('app', 'Marcar Agenda') ?>
        </a>

    </div>
    <div class="col-md-6 text-right">
        <h4 class="font-weight-bold">Agenda de avaliação</h4>
    </div>
</div>

<div class="card">
    <div class="shadow-soft bg-white">
        <div class="py-4 px-0 mx-4 model-lg">
            <div class="row mx-gutters-2 justify-content-sm-between">

                <div class="col-lg mb-lg-0">
                    <h5 class="font-weight-normal h6"><?= yii::t('app', 'Filtrar por paciente') ?></h5>
                </div>
                <div class="col-lg ml-2 has-success">
                    <?php
                        $patientList = yii\helpers\ArrayHelper::map(Calendar::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_calendar', 'name_patient');
                        echo yii\helpers\Html::dropDownList('calendar-select', null, $patientList, [
                            'id' => 'calendar-select',
                            'class' => 'form-control form-control-sm'
                        ]);
                    ?>
                </div>


            </div>
        </div>
    </div>
    <div class="card-body p-4">
        <div class="loader" style="display:none; padding: 15px;"><img src="/img/loading.gif" /></div>
        <div class="load-calendar"></div>
    </div>
    <div id="calendar" data-language="<?= yii::$app->language ?>"
        data-title-view="<?= Yii::t('app','Entrevista de trabalho com') ?>">
    </div>
</div>

<?php
    $script = <<<JS

    var id_calendar = $('#calendar-select').val();
    $('.load-calendar').load("/sistmagcnf/admin/calendar/avaliacao?id_calendar=" + id_calendar);

    $(document).on('change', '#calendar-select', function() {
            var id_calendar = $(this).val();
        $('.load-calendar').load("/sistmagcnf/admin/calendar/avaliacao?id_calendar=" + id_calendar);
    });

JS;
$this->registerJS($script);

?>