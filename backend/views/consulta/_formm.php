<?php

use common\models\Patient;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\QueryTepy;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use common\models\Session;
?>
<div class="query-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="container">
        <div class="row">

            <div class="col-sm-12">
                <label for="" class="type-label">Nome de Paciente<span class="text-danger">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Selecionar Paciente'],
                        'pluginOptions' => [
                        'allowClear' => true,
                        'class' => 'form-control  ',
                        'required' => true,
                        'data-msg' => 'Por favor Selecionar Paciente.',
                        'data-error-class' => 'u-has-error',
                        'data-success-class' => 'u-has-success',
                        ],
                    ])->label(false) ?>
                </div>
            </div>
            <div class="col-sm-6">
                <label for="" class="type-label">Inicio de TTO <span class="text-danger">*</span></label>
                <?= $form->field($model, 'start_date')->textInput([
                        "class" => "form-control  input_date",
                        'id'=> 'data_validade',
                        'placeholder' => "Introduzir data fim de curso",
                        'aria-label' => "Introduzir data fim de curso",
                      
                        'aria-describedby' => "dataInicioLabel",
                        'data-error-class' => "u-has-error",
                                'data-msg' => "Por favor Introduzir data fim de curso.",
                        'data-success-class' => "u-has-success",
                        'data-rp-date-format' => "Y.m.d-H.i.s",
                        //'type' => 'date'    
                ])->label(false) ?>
            </div>
            <div class="col-sm-6">
                <label for="" class="type-label">Fim de TTO<span class="text-danger">*</span> </label>
                <?= $form->field($model, 'end_date')->textInput([
                        "class" => "form-control  input_date",
                        'id'=> 'data_validade',
                        'placeholder' => "Introduzir data fim de curso",
                        'aria-label' => "Introduzir data fim de curso",
                       
                        'aria-describedby' => "dataInicioLabel",
                        'data-error-class' => "u-has-error",
                        'data-msg' => "Por favor Introduzir data fim de curso.",
                        'data-success-class' => "u-has-success",
                        'data-rp-date-format' => "Y.m.d-H.i.s",
                        //'type' => 'date'    
                ])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <label for="" class="type-label">Idade</label>
                <?= $form->field($model, 'age')->textInput([
                        "class" => "form-control",
                        'placeholder' => "Introduzir Idade",
                        'aria-label' => "Introduzir Idade",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " Introduzir Idade",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                ])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <label for="" class="session">Nº Sessões</label>
                <?= $form->field($model, 'session')->textInput([
                        "class" => "form-control",
                        'placeholder' => "Introduzir Nº Sessões",
                        'aria-label' => "Introduzir Nº Sessões",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " Introduzir Nº Sessões",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                ])->label(false) ?>
            </div>
            <div class="col-sm-4">
                <label for="" class="type-label">Frequência</label>
                <?= $form->field($model, 'frequency')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Frequência",
                        'aria-label' => "Introduzir Frequência",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Frequência",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
            </div>
              <!-- Sesao -->
              <?php if($model->id_query):?>
                <div class="col-sm-12">
                  <div class="card shadow-lg mb-4 mt-2">
                      <div class="card-header pt-4 pb-3 px-0 mx-4" style="border-bottom: none;">
                          <div class="mb-4">

                              <a href="#" class="btn btn-nr btn-indigo font-weight-bold open-modal" id="anucio_automatico"
                                  data-modal-size="modal-mb"
                                  data-url="<?= Url::to(['consulta/form', 'id'=>$model->id_query]) ?>"
                                  data-title="<?= Yii::t('app', 'Sessão') ?>">
                                  <i class="far fa-plus-square mr-1"></i>
                                  <?= Yii::t('app', 'Adicionar Sessão ') ?>
                              </a>
                          </div>
                          <div class="all-menus">
                                <?php 
                            $sessao = Session::find()->where(['id_query'=>$model->id_query])->LIMIT(3)->all();
                            foreach($sessao as $sessaos):?>

                                <div class="card bg-light mb-2">
                                    <div class="card-body">
                                        <div class="media align-items-center">

                                            <div class="media-body">
                                                <!-- File Attachment Button -->
                                                <label class="" for="fileAttachmentBtnn">
                                                    Sessão N <?= $sessaos->numero ?> <?= $sessaos->creat_at ?>
                                                </label>
                                                <!-- End File Attachment Button -->
                                            </div>
                                            <div class="media-body text-right">

                                                <a href="#" class=" open-modal" id="anucio_automatico"
                                                    data-modal-size="modal-mb"
                                                    data-url="<?= Url::to(['consulta/updat', 'id'=>$sessaos->id]) ?>"
                                                    data-title="<?= Yii::t('app', 'Sessão') ?>">
                                                    <i class="far fa-edit btn-soft-primary mr-2"></i>
                                                </a>
                                                <?= Html::a('<i class="fas fa-trash-alt"></i>', ['consulta/delet', 'id' => $sessaos->id], [
                                                  'class' => ' btn-icon btn-soft-danger remover-modulo',
                                                  'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
                                                  'data' =>
                                                    [
                                                        'confirm' => 'Deseja eliminar este item?',
                                                        'method' => 'post',
                                                    ],
                                                 ]) ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php  endforeach; ?>
                          </div>
                      </div>
                  </div>
                </div>
              <?php endif;?>

                <div class="col-sm-12">
                    <label for="" class="type-label">Diagonóstico</span></label>
                    <!--?= $form->field($model, 'macha')->textarea(['rows' => 6]) ->label(false) ?-->
                    <?= $form->field($model, 'diagnosis')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-12">
                    <label for="" class="type-label">Histórico da doença</span></label>
                    <!--?= $form->field($model, 'macha')->textarea(['rows' => 6]) ->label(false) ?-->
                    <?= $form->field($model, 'historic')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>

                <div class="col-sm-12">
                    <label for="" class="type-label">Antecedentes Pessoais</label>
                    <?= $form->field($model, 'associates')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-12">
                    <label for="" class="type-label">Modeficação atual<span class="bg-active">*</span></label>
                    <?= $form->field($model, 'medication')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-12">
                    <label for="" class="type-label">Exames Complimentaris<span class="bg-active">*</span></label>
                    <?= $form->field($model, 'food_examination')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Exames Complimentaris",
                        'aria-label' => "Introduzir Exames Complimentaris",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Exames Complimentaris",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
                </div>
            </div>
            <div class="row">


                <div class="col-sm-6">
                    <label for="" class="type-label">Exploração Fisica:</label>
                    <?= $form->field($model, 'physical_exploration')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Medidas:</label>
                    <?= $form->field($model, 'measures')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Avaliação Moscular:</label>
                    <?= $form->field($model, 'moscular_evaluation')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Avaliação Articular:</label>
                    <?= $form->field($model, 'joint_evaluation')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Avaliação Neurológica:</label>
                    <?= $form->field($model, 'neurological_evaluation')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Avaliação Neurológic",
                        'aria-label' => "Introduzir Avaliação Neurológic",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Avaliação Neurológic",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Sensibilidade:</label>
                    <?= $form->field($model, 'sensitivity')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Sensibilidade",
                        'aria-label' => "Introduzir Sensibilidade",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Sensibilidade",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Reflexos:</label>
                    <?= $form->field($model, 'reflections')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Reflexos",
                        'aria-label' => "Introduzir Reflexos",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Reflexos",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label">Teste Ortopidia e outros:</label>
                    <?= $form->field($model, 'orthopedic_test')->textInput([
                        "class" => "form-control ",
                        'placeholder' => "Introduzir Teste Ortopidia e outros",
                        'aria-label' => "Introduzir Teste Ortopidia e outros",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Teste Ortopidia e outros",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                ]) ->textInput()->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <label for="" class="type-label">Objectivo de Tratamento:</label>
                    <?= $form->field($model, 'treatment')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>


                </div>
                <div class="col-sm-12">
                    <label for="" class="type-label">Resultado de Tratamento:</label>
                    <?= $form->field($model, 'recommendation')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12 text-right mt-4">
                <div class="form-group">
                    <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
                    <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
    <?php
        $script = <<<JS
           $('.mais').click(function(){
              $(".all-menus").css({"display": "none"});
            });

            $('.menus').click(function(){
                $(".subcategory").css({"display": "block"});
            });
    JS;
        $this->registerJS($script);
    ?>

    <style>
    .type-label {
        display: block;
        /* text-transform: lowercase; */
        font-size: 80%;
        font-weight: 600;

    }

    .select2-container--krajee .select2-selection--single .select2-selection__arrow {
        height: 48px !important;
    }

    .select2-container--krajee .select2-selection--single {
        height: 50px !important;
        padding: 14px 24px 6px 16px !important;
    }

    .flatpickr-current-month {
        padding: 0px !important;
    }

    .flatpickr-months {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        /* display: flex; */
        position: relative;
        background-color: #2d1582;
        border-top-left-radius: 0.25rem;
        border-top-right-radius: 0.25rem;
        padding: .75rem;
    }

    span.cur-month {
        color: #fff !important;
    }

    .numInputWrapper {
        color: #fff !important;
    }

    svg {
        fill: rgba(255, 255, 255, 0.7);
    }

    .flatpickr-day.today {
        border-color: #959ea9;
    }

    .flatpickr-day.today {
        border-color: #2d1582 !important;
    }
    </style>