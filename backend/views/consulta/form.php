<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmployeesType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-type-form">

    <?php $form = ActiveForm::begin(
        
    ); ?>

    
    <div class="col-sm-12">
        <label for="" class="type-label" >Sessão  <span class="text-danger">*</span></label>
        <?= $form->field($model, 'numero')->dropDownList(
            [
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
                '11' => '11',
                '12' => '12',
                '13' => '13',
                '14' => '14',
                '15' => '15',
                '16' => '16',
                '17' => '17',
                '18' => '18',
                '19' => '19',
                '20' => '20',
                
            ],
            [
                // 'value' => $model->nivel_curso,
                'class' => 'form-control form-control-sm',
                'required' => true,
                'data-msg' => 'Por favor selecionar o nível.',
                'data-error-class' => 'u-has-error',
                'data-success-class' => 'u-has-success',
                'prompt' => 'Selecionar o nível'
            ]
        )->label(false) ?>
    </div>
   

    

    <div class="col-sm-12 text-right">
        <div class="form-group">  
        <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover" data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
  
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
