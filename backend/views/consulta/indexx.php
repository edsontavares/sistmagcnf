<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;


?>
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<?= $this->render('/auxiliar/menu',[

]) ?>
<div class="employees-index mt-6">

    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('<i class="far fa-plus-square mr-1"></i> Registar avaliação', ['createe'], ['class' => 'btn btn-nr btn-indigo font-weight-bold ']) ?>

      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>
    <table class="table">
  <thead class="top">
    <tr>
      
     
      <th scope="col">Paciente</th>
      <th scope="col">Data de Inicio</th>
      <th scope="col">Data Fim</th>
      <th scope="col">Estado</th>
      <th scope="col">Opção</th>

    </tr>
  </thead>
  <tbody>
  <?php  foreach ($fisioterapia as $fisioterapias) {  ?>
    <?php if($fisioterapias->state == 10 || $fisioterapias->state == 11 || $fisioterapias->state == 12): ?>
      <tr>
        <td>
        <?php if($fisioterapias->patients):?>
           <?= 
                 $fisioterapias->patients->name?>
        <?php endif;?>
        </td>
        <td><?= $fisioterapias->start_date?></td>
        <td>
            <?= $fisioterapias->end_date?>
        
          </td>
          <td>
          <?php if($fisioterapias->state == 11):?>
            <span class="badge badge-primary">Em Tratamento</span>
          <?php elseif($fisioterapias->state == 12):?>
            <span class="badge badge-warning">Agendado paraa iniciar tratamento</span>
          <?php elseif($fisioterapias->state == 10):?>
            <span class="badge badge-info">Em Avaliação</span>
          <?php elseif($fisioterapias->state == 33):?>
            <span class="badge badge-warning">Enviado Ao Medico</span>
          <?php endif;?>
          </td>
          <td> 
              <div class="btn-group position-relative ">
                  <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                      aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                      data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                      data-unfold-type="css-animation" data-unfold-duration="300"
                      data-unfold-delay="300"
                      data-unfold-animation-in="slideInUp"
                      data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                      <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                  </a>

                  <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                    
                    <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i> Ver', ['views', 'id' => $fisioterapias->id_query], ['class' => 'dropdown-item']) ?>

                    <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i> Editar', ['updates', 'id' => $fisioterapias->id_query], ['class'=>"dropdown-item"])?>

                    <a class="delete_consulta dropdown-item" href="#" data-url="<?= Url::to(['/'.'delete-consulta/'.$fisioterapias->id_query])?>"
                      data-sim="<?= Yii::t('app', 'Sim') ?>"
                      data-nao="<?= Yii::t('app', 'Não') ?>"
                      data-title="<?= Yii::t('app', 'Desejas eliminar este registo?') ?>"
                      data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                      data-key="<?= $fisioterapias->id_query?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
                    </a>
                  </div>
                </div>
            </td>
        </tr>
      <?php endif;?>  
    <?php }?>  
  </tbody>
</table>
<div class="pagination">
  <?= LinkPager::widget(['pagination'=> $pagination])?>
</div>


</div>
<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    .grid-view td:nth-child(4) {
        white-space: nowrap;
    }
    tbody tr:nth-child(odd) {
      background-color: #e0e0ea;
    }
    th {
        color: #fff;
    }
  .top{
    background-color: #2d1582;
    box-shadow: 0 3px 6px 0 rgb(140 152 164 / 25%) !important;
  }
  .table {
  border: 1px solid #e7eaf3;
  }
</style>
<?php
  $script = <<<JS
  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  $(function() {
   
    $(document).on('click','.delete_consulta',function(e){
      e.preventDefault();
      var key = $(this).data('key');
      var _title = $(this).data('title');
      var url = $(this).data('url');
      var _message = $(this).data('message');
      var _sim = $(this).data('sim');
      var _nao = $(this).data('nao');
      bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-indigo btn-xs'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax( {
              method: "post",
              url:url,
            })
            .done(function( respond ) {
              data = JSON.parse(respond);
              if (data.type === 'error'){
                console.log('dont delete');
              }
              else{
                window.open('consulta-fisioterapia?r=consulta-fisioterapia&action=delete','_self');
              }
            });
          }
        }
      });
    });



   
  });
JS;

$this->registerJs($script);
?>

