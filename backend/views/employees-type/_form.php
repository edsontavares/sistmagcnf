<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmployeesType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-type-form">

    <?php $form = ActiveForm::begin(
        
    ); ?>

    
    <div class="col-sm-12">
        <label for="" class="type-label" >Categoria do funcionario <span class="text-danger">*</span></label>
        <?= $form->field($model, 'name')->textInput([
            "class" => "form-control ",
            'placeholder' => "Introduzir categoria de funcionario",
            'aria-label' => "Introduzir categoria de funcionario",
            'required' => true,
            'aria-describedby' => "dataInicioLabel",
            'data-msg' => "Introduzir Tipo de Funcionario",
            'data-error-class' => "u-has-error",
            'data-success-class' => "u-has-success",
        ]) ->label(false)?>
    </div>
    <div class="col-sm-12">
        <label for="" class="type-label" >Descrição <span class="text-danger">*</span></label>
        <?= $form->field($model, 'description')->textarea(['id'=>'jobs-description'])->label(false) ?>
    </div>

    

    <div class="col-sm-12 text-right">
        <div class="form-group">  
        <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover" data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
  
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
