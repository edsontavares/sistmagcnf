<?php

use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="employees-type-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
