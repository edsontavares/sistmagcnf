<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Atualizar tipo de Funcionario '

?>
<div class="employees-type-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
