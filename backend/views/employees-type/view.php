<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
\yii\web\YiiAsset::register($this);
?>
<div class="location-view">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Catigoria do Funcionario</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/tipo-funcionario'])?>">Voltar</a>
            <div class="btn-group position-relative ">

                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                    <a href="#" class="open-modal dropdown-item open-modal" id="anucio_automatico" data-modal-size="modal-lg"  data-url="<?= Url::to(['create']) ?>" data-title="<?= Yii::t('app', 'Categoria') ?>">
                        <i class="far fa-plus-square mr-2"></i>
                        <?= Yii::t('app', 'Registar') ?>
                    </a>
                    <a href="#" class="open-modal dropdown-item"  data-toggle="tooltip",data-placement="top",  data-modal-size="modal-lg" data-toggle="modal" data-url="<?= Url::to(['update', 'id'=>$model->id_employees_type]) ?>" data-title="<?= Yii::t('app', 'Categoria') ?> " >
                        <i class="far fa-edit btn-soft-primary mr-2"></i> Atualizar
                    </a>
                    <a class="delete_categoria dropdown-item" href="#" data-url="<?= Url::to(['/'.'categoria-delete/'.$model->id_employees_type])?>"
                        data-sim="<?= Yii::t('app', 'Sim') ?>"
                        data-nao="<?= Yii::t('app', 'Não') ?>"
                        data-title="<?= Yii::t('app', 'Desejas eliminar este categoria?') ?>"
                        data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                        data-key="<?= $model->id_employees_type?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
                    </a>
                    
                </div>
                </div>
            </div>
         </div>
    </div>
    <hr>
</div>
<?php
  $script = <<<JS


  
    $(function() {
   
    $(document).on('click','.delete_categoria',function(e){
        e.preventDefault();
        var key = $(this).data('key');
        var _title = $(this).data('title');
        var url = $(this).data('url');
        var _message = $(this).data('message');
        var _sim = $(this).data('sim');
        var _nao = $(this).data('nao');
        bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
            cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
            },
            confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-indigo btn-xs'
            }
        },
        callback: function (result) {
            if(result){
            $.ajax( {
                method: "post",
                url:url,
            })
            .done(function( respond ) {
                data = JSON.parse(respond);
                if (data.type === 'error'){
                console.log('dont delete');
                }
                else{
                window.open('tipo-funcionario?r=tipo-funcionario&action=delete','_self');
                }
            });
            }
        }
        });
   });



  
 });

JS;

$this->registerJs($script);
?>


<div class="bg-light border shadow-soft rounded p-6 mb-4">
      <div class="media">
          <div class="u-lg-avatar mb-3 mx-auto">
            
          </div>

          <div class="media-body ml-4">

              <div class="d-md-flex align-items-md-center">
                      <!-- Location -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h2 class="small text-secondary mb-0">Catigoria</h2>
                          <span class="align-middle"><?= $model->name?></span>
                      </div>
                      <!-- End Location -->

                      <!-- Posted -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h3 class="small text-secondary mb-0">Data Criação</h3>
                          <span class="align-middle"><?= $model->create_at?></span>
                      </div>
                      <!-- End Posted -->
                  </div>
                  <?php if($model->description):?>
                    <h5 class="mt-4 font-weight-bold font-weight-bold">Observação</h5>
                    <p><?= $model->description?></p>
            <?php endif;?>
          </div>

         
             <!-- End Settings -->

              
      </div>
</div>

