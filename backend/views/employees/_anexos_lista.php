<?php
use common\widgets\Alert;
use common\models\curso\Categoria;
use common\models\curso\Subcategoria;
use yii\widgets\ActiveForm;
use common\models\Files;
use yii\helpers\Html;

$files = Files::find()->where(['model_id' => $model->id_employees])->all();

?>
<?php
if (empty($files)) {
    echo ' <p >Nenhum documento encontrado.</p>';
} else {
    foreach ($files as $file) { ?>
        <span class="btn btn-sm mb-2 border shadow-soft" id="file-<?= $file->id_files ?>"><!-- btn-pill -->
            <a class="text-secondary mr-4" href="<?= '/user/file/download?id=' . $file->id_files ?>">
                <?= $file->nome_original ?>
            </a>
            <a href="" class="mr-2 text-primary" onclick='window.open("http://localhost/sistmagcnf/<?=  '/' . $file->full_path ?>" );'>
                   <i class="fa fa-eye"></i>
             </a>
            <?= Html::a('<i class="fas fa-trash-alt"></i>', ['apagar', 'id' => $file->id_files], [
                  'class' => 'text-danger',
                  'data-toggle'=>"tooltip",'data-placement'=>"top",
                  'data' =>
                      [
                          'confirm' => 'Deseja eliminar este ficheiro?',
                          'method' => 'post',
                      ],
              ]) ?>
              
        </span>
    <?php }
}
?>