<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\EmployeesType;
use common\models\Location;
use common\models\Employees;
use yii\helpers\Url;
use borales\extensions\phoneInput\PhoneInput;
?>
    <?php $form = ActiveForm::begin([
            
    'options' => [
        
        'enctype' => 'multipart/form-data',
    ],]
    ); ?>
        <div class="row"> 
                <div class="col-sm-3"> 
                <?php if (@$model->imagem ==''): ?>
            <!-- File Attachment Input -->
            <label id="nameLabel" class="form-label">
                Foto do funcionario
                <span class="text-danger">*</span>
            </label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Foto do funcionario</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label"
                              data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="Patient[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '/' . $model->imagem?>" style="height: 200px; width: 100%;display: none;"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                      class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                      style="width: 100%; display: none;" for="floorplanAttachmentInput">
                    Mudar foto
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                          style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar foto 
                    </label>
                <?php endif; ?>
            </div>
          <?php else:?>
            <!-- File Attachment Input -->
            <label id="nameLabel" class="form-label">
                Foto do funcionario
                <span class="text-danger">*</span>
            </label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Foto do funcionario</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label"
                              data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="Patient[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '../../' . $model->imagem ?>" style="height: 200px; width: 100%;"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                      class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                      style="width: 100%; display: none;" for="floorplanAttachmentInput">
                    Mudar foto
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                          style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar foto 
                    </label>
                <?php endif; ?>
            </div>
          <?php endif; ?>
                
                </div>

                <div class="col-sm-9"> 
                        <div class="row">
                        
                <div class="col-sm-12">

                        <label for="" class="type-label" >Nome completo <span class="text-danger">*</span></label>
                        <?= $form->field($model, 'name')->textInput([
                                "class" => "form-control ",
                                'placeholder' => "Nome de funcionario",
                                'aria-label' => "Nome de funcionario",
                                'required' => true,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => "Nome de funcionario",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                        ]) ->textInput()->label(false)?>

                </div>
                <div class="col-sm-6">
                        <label for="" class="type-label" >Categoria de funcionario <span class="text-danger">*</span></label>
                        <div class="form-group">
                                <?=  $form->field($model, 'id_employees_type')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(EmployeesType::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_employees_type', 'name'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Selecionar serviço'],
                                        'pluginOptions' => [
                                        'allowClear' => true,
                                        'class' => 'form-control  ',
                                        'required' => true,
                                        'data-msg' => 'Por favor Selecionar Serviço.',
                                        'data-error-class' => 'u-has-error',
                                        'data-success-class' => 'u-has-success',
                                        ],
                                ])->label(false) ?>
                        </div>
                        <!--small id="teacherHelp" class="form-text text-muted">
                            Caso o Formandor não estiver registado na plataforma
                            <a class=""
                               href="#basicExampleModal"
                               data-modal-target="#basicExampleModal"
                               data-modal-effect="fadein">
                                Adicione-o
                            </a>
                        </!--small-->
                </div>
                <div class="col-sm-6">
                         <label for="" class="type-label" >Morada <span class="text-danger">*</span></label>
                        <div class="form-group">
                                <?=  $form->field($model, 'id_location')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(Location::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_location', 'zone'),
                                        'language' => 'en',
                                        'options' => ['placeholder' => 'Selecionar serviço'],
                                        'pluginOptions' => [
                                        'allowClear' => true,
                                        'class' => 'form-control custom-select',
                                        'required' => true,
                                        'data-msg' => 'Por favor Selecionar Serviço.',
                                        'data-error-class' => 'u-has-error',
                                        'data-success-class' => 'u-has-success',
                                        ],
                                ])->label(false) ?>
                        </div>
                </div>
                <div class="col-sm-6">
                <label for="" class="type-label" >Email</label>
                        <?= $form->field($model, 'email')->textInput([
                                 "class" => "form-control ",
                                'placeholder' => "exemplo@gmail.com",
                               
                                'aria-label' => "Introduzir email",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => "Introduzir emai",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'email'
                        ]) ->textInput()->label(false) ?>


                </div> 
                <div class="col-sm-6">
                        <label for="" class="type-label" >Telemovel </label>
                        <?= $form->field($model, 'telephone')->textInput( [
                                "class" => "form-control",
                                'placeholder' => "Introduzir Numero Telefone",
                                'placeholder' => "Introduzir Telefone",
                                'aria-label' => "Introduzir Telefone",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => "Introduzir o preço",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                        ]) ->textInput()->label(false) ?>
                </div>
               
               
                </div>
                </div>  
                      
                
               
              

                <div class="col-sm-4">
                <label for="" class="type-label" >Telefone </label>
                        <?= $form->field($model, 'mobilephone')->textInput([
                                  "class" => "form-control",
                                  'placeholder' => "Introduzir Numero Telemovel",
                                  'placeholder' => "Introduzir Telemovel",
                                  'aria-label' => "Introduzir Telemoveç",
                                  'required' => false,
                                  'aria-describedby' => "dataInicioLabel",
                                  'data-msg' => "Introduzir o preço",
                                  'data-error-class' => "u-has-error",
                                  'data-success-class' => "u-has-success",
                                  'type' => 'number'
                        ]) ->textInput()->label(false) ?>
                      

                </div>
                <div class="col-sm-4">
                
                <label for="" class="type-label" >Cni/Bi <span class="text-danger">*</span></label>
                        <?= $form->field($model, 'cni')->textInput([
                                "class" => "form-control",
                                'placeholder' => "Introduzir cni/bi",
                                'aria-label' => "Introduzir Telemove",
                                'required' => true,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => "Introduzir cni/bi",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                
                        ])->label(false) ?>
                </div>
                <div class="col-sm-4">
                        <label for="" class="type-label" >Nif <span class="text-danger">*</span></label>
                        <?= $form->field($model, 'nif')->textInput([
                                "class" => "form-control",
                                'placeholder' => "Introduzir nif",
                                'aria-label' => "Introduzir nif",
                                'required' => true,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " Introduzir nif",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                        ]) ->textInput()->label(false) ?>
                </div>
              
                <div class="col-sm-4">
                        <label for="" class="type-label" >Nome de banco</label>
                          <?php
                                echo $form->field($model, 'bank_name')->dropDownList(Employees::$NOME_BANCO, [
                                        'prompt' => 'Escolha o teu banco',
                        ])->label(false) ?>

                </div>
                <div class="col-sm-4">
                        <label for="" class="type-label" >Número da conta </label>
                        <?= $form->field($model, 'accountnumber')->textInput([
                                "class" => "form-control",
                                'placeholder' => "Introduzir Número de conta",
                                'aria-label' => "Introduzir Número de conta",
                                'required' => true,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " Introduzir Número de conta",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                        ])-> label(false) ?>
                </div>
                <div class="col-sm-4">
                        <label for="" class="type-label" >Nib </label>
                        <?= $form->field($model, 'nib')->textInput([
                                "class" => "form-control",
                                'placeholder' => "Introduzir Nib",
                                'aria-label' => "Introduzir Nib",
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " Introduzir Nib",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                        ])->label(false) ?>
        
                </div>
        </div>
       
        <div class="row">    
                
                <div class="col-sm-4">
                        <label for="" class="type-label" >Tipo de contratando <span class="text-danger">*</span></label>
                        <?php
                                echo $form->field($model, 'hiring')->dropDownList(Employees::$CONTRATO, [
                                'prompt' => 'Escolher o tipo de contrato','required' => true,
                        ])->label(false) ?>

                </div>
                <div class="col-sm-4">
                        <label for="" class="type-label" >Data de início <span class="text-danger">*</span></label>

                        <?= $form->field($model, 'date_start')->textInput([
                                "class" => "form-control  input_date",
                                'id'=> 'data_validade',
                                'placeholder' => "Introduzir data fim de curso",
                                'aria-label' => "Introduzir data fim de curso",
                                'required' => true,
                                'aria-describedby' => "dataInicioLabel",
                                'data-error-class' => "u-has-error",
                                'data-msg' => "Por favor Introduzir data fim de curso.",
                                'data-success-class' => "u-has-success",
                                'data-rp-date-format' => "Y.m.d-H.i.s",
                                'type' => 'date'
                                   
                        ])->label(false) ?>
                </div>
                <div class="col-sm-4" id="fim">
                        <label for="" class="type-label" >Data final </label>
                        <?= $form->field($model, 'date_end')->textInput([
                                "class" => "form-control  input_date",
                                'id'=> 'data_validade',
                                'placeholder' => "Introduzir data fim de curso",
                                'aria-label' => "Introduzir data fim de curso",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-error-class' => "u-has-error",
                                'data-msg' => "Por favor Introduzir data fim de curso.",
                                'data-success-class' => "u-has-success",
                                'data-rp-date-format' => "Y.m.d-H.i.s",
                                'type' => 'date'    
                        ])->label(false) ?>
                </div>

                <!--div class="col-sm-6">
                        <label for="" class="type-label" >Cni/Bi</label>
                        <?php //= yii\helpers\Html::fileInput('bi[]', null, ['multiple' => true, 'classe'=>'btn btn-xs btn-primary rounded uploadVideo']) ?>                </!--div>
                <div class="col-sm-6">
                        <label for="" class="type-label" >Nif</label>
                        <?php //= yii\helpers\Html::fileInput('nif[]', null, ['multiple' => true]) ?>
                </div>
               
                <div class="col-sm-6 mt-4">
                        <label for="" class="type-label" >Curriculo</label>
                        <?php //= yii\helpers\Html::fileInput('files[]', null, ['multiple' => true]) ?>                </div>
                <div-- class="col-sm-6 mt-4">
                        <label for="" class="type-label" >vvvvv</label>
                        <?php //= yii\helpers\Html::fileInput('file[]', null, ['multiple' => true]) ?>
                </div-->

                <div class="col-sm-12 text-right mt-3">
                        <div class="form-group">
                                 <a href="<?= Url::to(['/funcionario'])?> " class="btn btn-sm btn-soft-secondary rounded">Cancelar</a>

                                 <!--?= Html::submitButton('Registar', ['class' => 'btn btn-indigo btn-sm rounded ']) ?-->
                                 <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
                        </div>

                </div>
        </div>

        <?php ActiveForm::end(); ?>




     
    <!--script src="../biblioteca/vendor/js/select2.full.js"></!--script>
    <script-- src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script-->
<?php
    //$this->registerJsFile('../biblioteca/vendor/jquery/dist/jquery.min.js');
?>
<style>
        .type-label{
        display: block;
        /* text-transform: lowercase; */
        font-size: 80%;
        font-weight: 600;

        }  
        .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
.flatpickr-current-month {
      padding: 0px!important;
  }
  .flatpickr-months {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    /* display: flex; */
    position: relative;
    background-color: #2d1582;
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
    padding: .75rem;
  }
  span.cur-month {
    color: #fff!important;
  }
  .numInputWrapper {
      color: #fff!important;
  }
  svg {
    fill: rgba(255, 255, 255, 0.7);
  }
.flatpickr-day.today {
      border-color: #959ea9;
  }
  .flatpickr-day.today {
      border-color: #2d1582!important;
  }
</style>
<?php
    $scipt = <<<JS
    $(document).ready(function () {
        $('#floorplanAttachmentInput').change(function () {

            var reader = new FileReader()

            reader.onload = function (e) {

                $('#capa_preview').attr('src', e.target.result)
                $('#image-capa-btn-attach').css('display', 'block')
                $('#change').css('display', 'none')
                $('#capa-continer').css('display', 'none')
                $('#capa_preview').css('display', 'block')
            }
            reader.readAsDataURL(this.files[0])
        })
})

JS;
    $this->registerJS($scipt);
?>

         
      
   

    

    


