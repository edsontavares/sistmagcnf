<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;


?>
<?php  foreach ($employees as $employeess) {
      
      ?>
      
      <tr>
        <td><?= $employeess->name?></td>
        <td>
          <?php if($employeess->employeesTypes):?>
            <?= $employeess->employeesTypes->name?>
          <?php endif;?>
        <td>
          <?php if($employeess->hiring == 0):?>
            Tempo inteiro
          <?php elseif($employeess->hiring == 1):?>
            Temporário
          <?php elseif($employeess->hiring == 2):?>
            Prestação de serviço
          <?php elseif($employeess->hiring == 3):?>
            Part-Time
            <?php elseif($employeess->hiring == 4):?>
              Voluntariado
            <?php elseif($employeess->hiring == 5):?>
                Estagio
            <?php endif;?>
        </td>
        </td>
        <td>
          <?php if($employeess->state== 1):?>
            <span class="badge badge-success">Ativo</span>
            <?php elseif($employeess->state == -1):?>
              <span class="badge badge-danger">Inactivo</span>
              
            <?php endif;?>
          </td>
        <td> 

          <div class="btn-group position-relative ">
            <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                data-unfold-type="css-animation" data-unfold-duration="300"
                data-unfold-delay="300"
                data-unfold-animation-in="slideInUp"
                data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                <span class="fas fa-ellipsis-v btn-icon__inner"></span>
            </a>

            <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
              
               <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i> Ver', ['view', 'id' => $employeess->id_employees], ['class' => 'dropdown-item','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>

              <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i> Editar', ['update', 'id' => $employeess->id_employees], ['class'=>"dropdown-item",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>

              <a class="delete_employees dropdown-item" href="#" data-url="<?= Url::to(['/'.'funcionario-delete/'.$employeess->id_employees])?>"
                data-sim="<?= Yii::t('app', 'Sim') ?>"
                data-nao="<?= Yii::t('app', 'Não') ?>"
                data-title="<?= Yii::t('app', 'Desejas eliminar este funcionario?') ?>"
                data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                data-key="<?= $employeess->id_employees?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
              </a>
            </div>
          </div>
        </td>
      </tr>
      <?php }?>  