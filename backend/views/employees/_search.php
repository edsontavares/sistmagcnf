<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\EmployeesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_employees') ?>

    <?= $form->field($model, 'id_employees_type') ?>

    <?= $form->field($model, 'id_location') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'mobilephone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'cni') ?>

    <?php // echo $form->field($model, 'bi') ?>

    <?php // echo $form->field($model, 'nif') ?>

    <?php // echo $form->field($model, 'nib') ?>

    <?php // echo $form->field($model, 'insurance_number') ?>

    <?php // echo $form->field($model, 'accountnumber') ?>

    <?php // echo $form->field($model, 'date_start') ?>

    <?php // echo $form->field($model, 'date_end') ?>

    <?php // echo $form->field($model, 'renewal_date') ?>

    <?php // echo $form->field($model, 'hiring') ?>

    <?php // echo $form->field($model, 'insurance_numberaccontnumber') ?>

    <?php // echo $form->field($model, 'bank_name') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
