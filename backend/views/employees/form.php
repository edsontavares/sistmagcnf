<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\EmployeesType;
use common\models\Location;
use common\models\Employees;

/* @var $this yii\web\View */
/* @var $model common\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

    <?php $form = ActiveForm::begin([
            
    ]); ?>
        <div class="row">
                        
                <div class="col-sm-12">
                        <label for="" class="type-label" >Data de Início </label>

                        <?= $form->field($model, 'date_hurt_start')->textInput([
                                "class" => "form-control  input_date",
                                'id'=> 'data_validade',
                                'placeholder' => "Introduzir data fim de curso",
                                'aria-label' => "Introduzir data fim de curso",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-error-class' => "u-has-error",
                                        'data-msg' => "Por favor Introduzir data fim de curso.",
                                'data-success-class' => "u-has-success",
                                'data-rp-date-format' => "Y.m.d-H.i.s",
                                'type' => 'date'
                                   
                        ])->label(false) ?>
                </div>
                 <div class="col-sm-12">
                        <label for="" class="type-label" >Data de fim </label>

                        <?= $form->field($model, 'date_hurt_end')->textInput([
                                "class" => "form-control  input_date",
                                'id'=> 'data_validade',
                                'placeholder' => "Introduzir data fim de curso",
                                'aria-label' => "Introduzir data fim de curso",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-error-class' => "u-has-error",
                                        'data-msg' => "Por favor Introduzir data fim de curso.",
                                'data-success-class' => "u-has-success",
                                'data-rp-date-format' => "Y.m.d-H.i.s",
                                'type' => 'date'
                                   
                        ])->label(false) ?>
                </div>
                <div class="col-sm-12 text-right mt-3">
                        <div class="form-group">
                         <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
                        </div>

                </div>
        </div>

        <?php ActiveForm::end(); ?>




    
<style>
        .type-label{
        display: block;
        /* text-transform: lowercase; */
        font-size: 80%;
        font-weight: 600;

        }  
        .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>