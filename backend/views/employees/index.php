<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;


?>
<div class="employees-index">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert alert-info  alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('<i class="far fa-plus-square mr-1"></i> Registar funcionario', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc employees-search" id="search-keyword" placeholder="Pesquisar por nome" >
                  <!--button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button-->
              </div>
          </form>
      </div>
    </div>


  <table class="table" id="myTableEmployees">
    <thead class="top">
      <tr>
        
        <th scope="col">Nome de Funcionario</th>
        <th scope="col">Catigoria</th>
        <th scope="col">Tipo de Contratação</th>
        <th scope="col">Estado</th>
        <th scope="col">Opção</th>

      </tr>
    </thead>
    <tbody id="entity-content-body">
        <?= $this->render('_lista_employees',[
            'employees'=>$employees,
        ])?>
    </tbody>
  </table>
 <?php if ($total > 12 || (@Yii::$app->request->get("page") && intval(Yii::$app->request->get("page")) > 1)): ?>
    <?= $this->render('../../../common/componentes/pagination', [
      'total' => @$total,
      'item_numbers' => $item_numbers,
      'prettyUrl' => 'active',
      'url' => Url::to(['/employees/index'])
  ]) ?>
 <?php endif; ?>




</div>
<?php
  $script = <<<JS
  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  $(function() {
   
    $(document).on('click','.delete_employees',function(e){
      e.preventDefault();
      var key = $(this).data('key');
      var _title = $(this).data('title');
      var url = $(this).data('url');
      var _message = $(this).data('message');
      var _sim = $(this).data('sim');
      var _nao = $(this).data('nao');
      bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-indigo btn-xs'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax( {
              method: "post",
              url:url,
            })
            .done(function( respond ) {
              data = JSON.parse(respond);
              if (data.type === 'error'){
                console.log('dont delete');
              }
              else{
                window.open('funcionario?r=funcionario&action=delete','_self');
              }
            });
          }
        }
      });
    });
  });


 


  

JS;

$this->registerJs($script);
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
var search_body = $('#entity-content-body');
$('#search-keyword').on('input', _.debounce(search_user, 800));
function search_user(event) {
event.preventDefault();
var keyword = $('#search-keyword').val();
console.log(keyword);
load_progress.css('display', 'block');
var url = "/admin/curso/categoria/search?keyword=" + keyword
$.get(location.origin + url, function (data) {
load_progress.css('display', 'none');
search_body.empty();
search_body.append(data);
})
}

});
</script>
<style>
.modal-header {
  background-color: #f8f9fa !important;
  border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
</style>
<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    .grid-view td:nth-child(4) {
        white-space: nowrap;
    }
    tbody tr:nth-child(odd) {
      background-color: #e0e0ea;
    }
    th {
        color: #fff;
    }
  .top{
    background-color: #2d1582;
    box-shadow: 0 3px 6px 0 rgb(140 152 164 / 25%) !important;
  }
  .table {
  border: 1px solid #e7eaf3;
  }
    
</style>



