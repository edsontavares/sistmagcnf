<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use common\models\Fair;

    function getBadgeClass($state)
    {
        if ($state == 1)
            return '<span class="badge badge-success btn-block pt-3 py-3">Activo</span>';
        elseif ($state == -1)
            return '<span class="badge badge-danger btn-block pt-3 py-3">Inactivo</span>';
        else
            return '<span class="badge badge-danger btn-block pt-3 py-3">Rascunho</span>';
    }
    $fair = Fair::find()->where(['id_employees'=>$model->id_employees])->one();
?>

<section>
<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Funcionario</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/funcionario'])?>">Voltar</a>
            <?php if (\Yii::$app->user->can('admin')):?>
            <div class="btn-group position-relative ">

                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>
                
                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                    <!--a class="dropdown-item" href=""><span class="fas fa-bullhorn mr-2"></span> activar</a-->
                    <?php if($model->state == -1):?>
                    <?= Html::a('<i class="fas fa-code-branch dropdown-item-icon"></i> Activar', ['employees/active', 'id' => $model->id_employees], ['class'=>"dropdown-item"])?>
                    <?php elseif($model->state == 1):?>
                        <?= Html::a('<i class="fas fa-code-branch dropdown-item-icon"></i> Desativar', ['employees/desable', 'id' => $model->id_employees], ['class'=>"dropdown-item"])?>
                    <?php endif;?>
                    
                </div>
                </div>
            </div>
            <?php endif;?>
         </div>
        
    </div>
   
    <hr>
    <div style="margin-top: 0%;">
        <div class="row mx-gutters-2">
            <div class="col-lg-3 mb-7">
                <!-- Profile Card -->
                <div class="card shadow-sm mb-4">
                    <div class="card-body text-center">
                        <div class="mb-3 pt-3">
                            <img class="u-lg-avatar rounded-circle img-fluid" src="../<?=$model->imagem?>" title="">
                        </div>

                        <div class="mb-2">
                            <h1 class="h5 font-weight-medium mb-1"><?= $model->name?></h1>
                            <span><?= $model->email?></span>
                            <small><br>
                            <?php if($model->employeesTypes):?>
                                <?= $model->employeesTypes->name?>
                             <?php endif;?>
                            </small>
                            
                        </div>
                        
                        <div class="mb-4">
                            <!--a class="btn btn-sm btn-block btn-soft-primary transition-3d-hover open-modal" data-url="" href="javascript:void(0)"  data-toggle="modal" data-modal-size="modal-xl" data-title="Editar perfil">
                                <span class="fas fa-user-edit small mr-2"></span>
                                Editar Perfil
                            </a-->
                            <?= Html::a('<i class="fas fa-user-edit"></i> Editar', ['update', 'id' => $model->id_employees], ['class'=>"btn btn-sm btn-block btn-soft-primary transition-3d-hover",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>

                        </div>

                        <hr class="py-1">

                          <!-- contato -->
                            <div class="mb-0 text-left">
                                <!--h1 class="font-weight-mediu pb-2 mb-2 h6">Contatos</h1-->
                                <ul class="list-group list-group-flush list-group-borderless">
                                    <li class="list-group-item">
                                        <span class="d-block font-size-1">
                                        <i class="fas fa-map-marked-alt text-secondary mr-2"></i>
                                        <?php if($model->location):?>
                                            <?= $model->location->zone?>
                                        <?php endif;?>
                                        </span>
                                    </li>

                                    <li class="list-group-item">
                                        <span class="d-block font-size-1">
                                        <i class="fas fa-phone text-secondary mr-2"></i><?=$model->telephone?>
                                        </span>
                                    </li>

                                    <li class="list-group-item">
                                        <span class="d-block font-size-1">
                                        <i class="fas fa-phone  text-secondary mr-2"></i><?=$model->mobilephone?>
                                        </span>
                                    </li>
                                </ul>
                            </div>
                            <!-- fim de contato -->
                            <?php if($fair):?>
                            <hr>
                            <div class="media-body text-left">
                                <span class="text-dark line-clamp">Periodo de feria</span>
                                <small class="d-block text-secondary">
                                <?= Yii::$app->formatter->asDate($fair->date_start, 'long');?>  - <?= Yii::$app->formatter->asDate($fair->date_end, 'long');?>
                                </small>
                            </div>
                            <?php endif;?>
                            <hr>
                            <?= getBadgeClass($model->state) ?>
                              

                                

                           
                            

                        <!--div class="text-left">
                          <h1 class="h6 mb-3">Redes sociais</h1>

                          <!-- Linked Account >
                     
                          <a class="media align-items-center mb-2" target="_blank" href="http://www.facebook.com/">
                            <div class="btn btn-sm btn-icon btn-soft-secondary rounded mr-2">
                              <span class="fab fa-facebook-f btn-icon__inner"></span>
                            </div>
                            <div class="media-body">
                              <h4 class="font-size-1 text-dark mb-0">Facebook</h4>
                              <small class="d-block text-secondary">Facebook</small>
                            </div>
                          </a>
                        
                          <!-- End Linked Account >

                          <!-- Linked Account >
                          <a class="media align-items-center mb-2" target="_blank" target="_blank" href="http://www.linkedin.com/">
                            <div class="btn btn-sm btn-icon btn-soft-secondary rounded mr-2">
                              <span class="fab fa-linkedin btn-icon__inner"></span>
                            </div>
                            <div class="media-body">
                              <h4 class="font-size-1 text-dark mb-0">Linkedin</h4>
                              <small class="d-block text-secondary">linkidin</small>
                            </div>
                          </a>
                        
                          <!-- End Linked Account >

                          <a class="media align-items-center mb-2" target="_blank" target="_blank" href="http://www.instagram.com/">
                            <div class="btn btn-sm btn-icon btn-soft-secondary rounded mr-2">
                              <span class="fab fa-twitter btn-icon__inner"></span>
                            </div>
                            <div class="media-body">
                              <h4 class="font-size-1 text-dark mb-0">Instagram</h4>
                              <small class="d-block text-secondary">instagram</small>
                            </div>
                          </a>
                        </!--div-->

                    </div>
                </div>
                <!-- End Profile Card -->
            </div>

            <!-- -------      ------------------------- --->

            <div class="col-lg-9">

            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
            <div class="alert-success alert alert-dismissible fade show" role="alert">
                <?= Yii::$app->session->getFlash("success-create") ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash("success-ficheiro")): ?>
            <div class="alert-success alert alert-dismissible fade show" role="alert">
                <?= Yii::$app->session->getFlash("success-ficheiro") ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash("success-active")): ?>
            <div class="alert-success alert alert-dismissible fade show" role="alert">
                <?= Yii::$app->session->getFlash("success-active") ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash("success-desable")): ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?= Yii::$app->session->getFlash("success-desable") ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
                  <!-- User Details -->
                  <div class="mb-3 mt-2">
                      <h1 class="h5 font-weight-medium mb-1">Biografia</h1>
                  </div>
                  <!-- End User Details -->

                  <!-- Info -->
                  <div class="mb-0 mt-2">
                      <p>descricao</p>
                  </div>
                  <!-- End Info -->
                  <div class="d-md-flex align-items-md-center">
                      <!-- Location -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h2 class="small text-secondary mb-0">Tipo de contrato</h2>
                         
                          <span class="align-middle">
                            <?php if($model->hiring == 0):?>
                                Tempo inteiro
                                <?php elseif($model->hiring == 1):?>
                                    Temporário
                                <?php elseif($model->hiring == 2):?>
                                    Prestação de serviço
                                <?php elseif($model->hiring == 3):?>
                                    Part-Time
                                <?php elseif($model->hiring == 4):?>
                                    Voluntariado
                                <?php elseif($model->hiring == 5):?>
                                    estagio
                                <?php endif;?>
                          </span>
                      </div>
                      <!-- End Location -->

                      <!-- Posted -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h3 class="small text-secondary mb-0">Data inicio</h3>
                          <span class="align-middle"><?=$model->date_start?></span>
                      </div>
                      <!-- End Posted -->
                      <!-- Posted -->
                      <?php if($model->date_end):?>
                        <div class="u-ver-divider u-ver-divider--none-md pr-4 mbdata final-3 mb-md-0 mr-4">
                          <h3 class="small text-secondary mb-0">data final</h3>
                          <span class="align-middle"><?=$model->date_end?></span>
                      </div>
                      <?php endif;?>
                      <!-- End Posted -->
                        <!-- Posted -->
                        <?php if($model->cni):?>
                        <div class="mb-3 mb-md-0">
                            <h5 class="small text-secondary mb-0">Nº de BI/CNI</h5>
                            <span class="align-middle"><?=$model->cni?></span>
                        </div>
                      <?php endif;?>
                      <!-- End Posted -->
                  </div>
                
                
                  <!-- dados bancários -->
                  <div class="alert alert-info mb-4 mt-4">
                      <div class="mb-3 mt-2">
                          <h1 class="h6 font-weight-medium mb-1">Dados bancários</h1>
                      </div>
                      <div class="d-md-flex align-items-md-center">
                          <!-- Location -->
                            
                              <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mr-4">
                                  <h2 class="small text-secondary mb-0">Nome do Banco</h2>
                                  <span class="align-middle"> 
                                      <?php if($model->bank_name == 0):?>
                                        BAI(Banco africano de investimento)
                                      <?php elseif($model->bank_name == 1):?>
                                        BCA(Banco comercial do atlântico)
                                      <?php elseif($model->bank_name == 2):?>
                                        BCN(Banco caboverdiano de negócios)
                                      <?php elseif($model->bank_name == 3):?>
                                        BI(Banco Interatlantico)
                                      <?php elseif($model->bank_name == 4):?>
                                        Caixa económica
                                      <?php elseif($model->bank_name == 5):?>
                                        Ecobank
                                      <?php elseif($model->bank_name == 6):?>
                                        iib Cabo Verde
                                      <?php endif;?>
                                  </span>
                              </div>
                            
                          <!-- End Location -->

                          <!-- Posted -->
                         
                            <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mr-4">
                                <h3 class="small text-secondary mb-0">Nº Conta</h3>
                                <a><span class="align-middle"><?= $model->accountnumber?></span></a>
                            </div>
                        
                          <!-- End Posted -->

                          <!-- Posted -->
                          
                            <div class=" pr-4 mb-3 mr-4">
                                <h4 class="small text-secondary mb-0">Nº Nib</h4>
                                <a><span class="align-middle"><?= $model->nib?></span></a>
                            </div>
                          
                      </div>
                  </div>
                  <!-- fim de dados bancários -->
               
                <div class="card shadow-lg mb-4 mt-2"> <div class="card-header pt-4 pb-3 px-0 mx-4" style="border-bottom: none;">
                    <h2 class="h6 mb-0"><span class="fas fa-paperclip text-primary mr-2" aria-hidden="true"></span>Documentos do funcionario</h2>
                    </div>
                    <hr class="mt-0 mb-0">
                    <div class="card-body p-4">
                        <!--ANEXOS DE INSCRICAO-->
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="anexos-file">
                                <?= $this->render('_anexos_lista', ['model' => $model]) ?>
                                </div>
                            </div>
                        </div>
                        <!--FIM ANEXOS INSCRICAO-->
                        <div class="row">
                            <!-- adicionar documentos -->
                            <div class="col-sm-12">
                                <a class="text-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fa fa-plus mr-2"></i>Adicionar documentos
                                </a>
                                <div class="collapse mt-3 ID_DA_DIV" id="collapseExample">
                                    <?php $form = ActiveForm::begin([
                                    'id' => 'form-formador',
                                    'options' => ['enctype' => 'multipart/form-data'],
                                    //'action' => \yii\helpers\Url::to(['/user/formador/create']),
                                ]) ?>
                                    <?= $form->field($model, 'name')->hiddenInput(['maxlength' => true])->label(false) ?>
                                    <div class="d-flex load-form">
                                        <!-- File Attachment Button -->
                                        <label class="btn btn-sm btn-soft-primary file-attachment-btn" for="">
                                        <i class="fas fa-file-pdf mr-2"></i>caregar documento
                                        <input id="inputArquivo" name="image-assinatura" type="file" class="file-attachment-btn__label nomeCertificacao" value=" " autocomplete="off">
                                            </label>
                                        <label class="botaoArquivo" id="datatable_users_filter"></label>
                                        <!-- End File Attachment Button -->
                                        <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Adcionar', ['class' => $model->isNewRecord ? 'btn btn-sm btn-primary save buton_save ml-2' : 'btn btn-sm btn-primary save buton_save ml-2 ','id'=>'btn-save']) ?>
                                    </div>
                                <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                            <!-- fim de adicionar documentos -->
                        </div>
        
                    </div>
</div>
            </div>

        </div>
    </div>
</section>
 
<script type="text/javascript">
    var div = document.getElementsByClassName("botaoArquivo")[0];
    var input = document.getElementById("inputArquivo");

    div.addEventListener("click", function(){
        input.click();
    });
    input.addEventListener("change", function(){
        var nome = "Não há arquivo selecionado. Selecionar arquivo...";
        if(input.files.length > 0) nome = input.files[0].name;
        div.innerHTML = nome;
    });
</script>
