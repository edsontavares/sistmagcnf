<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\employees;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
?>
<div class="fair-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-sm-12">
            <label for="" class="type-label" >Nome do funcionario<span class="text-danger">*</span></label>
            <div class="form-group">
            <?=  $form->field($model, 'id_employees')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Employees::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_employees', 'name'),
                'language' => 'en',
                'options' => ['placeholder' => 'Selecionar Funcionario'],
                'pluginOptions' => [
                'maxlength' => true,
                'class' => 'form-control  ',
                'required' => true,
                'data-msg' => 'Por favor Selecionar Funcionario.',
                'data-error-class' => 'u-has-error',
                'data-success-class' => 'u-has-success',
                ],
            ])->label(false) ?>
       </div>
    </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Inicio de feria<span class="text-danger">*</span></label>
            <?= $form->field($model, 'date_start')->textInput([
                "class" => "form-control  input_date",
                'id'=> 'data_validade',
                'placeholder' => "Introduzir data Inicio",
                'aria-label' => "Introduzir data Inicio",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-error-class' => "u-has-error",
                        'data-msg' => "Por favor Introduzir data Inicio.",
                'data-success-class' => "u-has-success",
                'data-rp-date-format' => "Y.m.d-H.i.s",
                'type' => 'date'       
            ])->label(false) ?>
        </div>
     <div class="col-sm-6">
        <label for="" class="type-label" >Fim de feria<span class="text-danger">*</span></label>
        <?= $form->field($model, 'date_end')->textInput([
            "class" => "form-control  input_date",
            'id'=> 'data_validade',
            'placeholder' => "Introduzir data Fim",
            'aria-label' => "Introduzir data Fim",
            'required' => true,
            'aria-describedby' => "dataInicioLabel",
            'data-error-class' => "u-has-error",
            'data-msg' => "Por favor Introduzir data Inicio.",
            'data-success-class' => "u-has-success",
            'data-rp-date-format' => "Y.m.d-H.i.s",
            'type' => 'date'       
        ])->label(false) ?>
     </div>
    
    </div>

    <div class="col-sm-12 text-right mt-10 mb-4">
        <div class="foter form-group">
          <hr style="color: darkgray">
          <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover " data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
          <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar': '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded mr-4' : 'btn btn-indigo btn-sm rounded mr-4','id'=>'btn-save']) ?>
    </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<style>

.foter {
    position: absolute;
    bottom: -15px;
    width: 100%;
    background: white;
}
  .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>
<script>
   $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');
</script>
<?php

  if (Yii::$app->request->isAjax){

     $this->registerJsFile('/biblioteca/vendor/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js');
     $this->registerCssFile('/biblioteca/vendor/bootstrap-tagsinput/css/bootstrap-tagsinput.css');
  }