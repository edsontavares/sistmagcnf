<?php
use yii\bootstrap\Modal;
use \common\models\Agenda;
$this->title =yii::t('app','Coaching');
$urlPath = Yii::$app->params['urlPath'];
?>
<div id="full-calendar-fair " data-language="<?= yii::$app->language ?>" data-title-view="<?= Yii::t('app','Agendamento de coach') ?>">
</div>
<script>

    $(document).ready(function(){
        $('#full-calendar-fair ').fullCalendar({
            defaultView: 'month',
            header: {
                right: 'prev today next',
                center: 'title',
                left: 'month,agendaWeek,agendaDay'
            },
            locale: $('#full-calendar-fair ').attr('data-language'),
            eventSources: [
                // your event source
                {
                    url: '',
                    type: 'get',
                    error: function () {
                        alert('there was an error while fetching events!');
                    },
                    success: function (res) {
                      console.log('ggggggggggggggggggggggggggggggggg');
                      console.log(res);
                    },
                    color: '#00c9a7',  // a non-ajax option
                    textColor: 'white' // a non-ajax option
                },
            ],
            editable: true,
            droppable: false,
            eventResize: function (event, delta, revertFunc) {
                bootbox.confirm({
                    title: 'Alterar a hora da Entrevista?',
                    message: 'Ao efectuar essa ação o candidato será notificado a alteração da hora da entrevista',
                    //size: 'small',
                    buttons: {
                        confirm: {
                            label: 'Sim',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'Não',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {

                        if (result === true) {
                            var start = event.start.format('HH:mm:SS');
                            var date = event.start.format('YYYY-MM-DD');
                            var url = "";
                            var type = 'post';
                            var formData = {
                                'hora': start,
                                'data': date,
                                'id': event.id
                            };
                            $.ajax({
                                type: type,
                                url: url,
                                data: formData,
                                dataType: 'json',
                                success: function (data) {
                                    if (data.type === 'error') {
                                        revertFunc();
                                        toastr.error(data.message, {timeOut: 5000}).css("width", "300px");
                                    }
                                    // else {
                                    //     startDashboard();
                                    // }
                                }
                            });
                        } else {
                            revertFunc();
                        }
                    }
                });
            },

            eventClick: function (event, jsEvent, view) {
                  openModal(" ";
            },
            eventDrop: function (event, delta, revertFunc) {
                revertFunc();
            }

        });

    });

</script>
<?php
    $this->registerCssFile('../plugins/fullcalendar/css/fullcalendar.min.css');
?>
