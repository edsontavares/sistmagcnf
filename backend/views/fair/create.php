<?php

use yii\helpers\Html;
?>
<div class="fair-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
