<?php

use common\models\Adptacao;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use common\models\EmployeesType;
use common\models\Fair;

?>
<div class="employees-index">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<div class="row mb-3">
  <div class="col-sm-6">
      <?php // Html::a('Adicionar Tipo de Funcionario', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      <a href="#" class="btn btn-nr btn-indigo font-weight-bold open-modal" data-modal-size="modal-md" data-toggle="modal" data-url="<?= Url::to(['create']) ?>" data-title="<?= Yii::t('app', 'Marcação de feria dos funcionarios') ?>">
      <i class="far fa-plus-square mr-1"></i>
      <?= Yii::t('app', 'Marcar feria') ?>
    </a>
  </div>
  <div class="col-sm-6">
      
  </div>
</div>
<div class="border-bottom mb-2">
    <h1 class="font-weight-bold h5"><?= yii::t('app', 'Coaching') ?></h1>
</div>
<div class="sessao_layout">
  <div class="card session">
      <div class="shadow-soft bg-white">
        <div class="py-4 px-0 mx-4 model-lg">
            <div class="row mx-gutters-2 justify-content-sm-between">
                    <div class="col-lg mb-lg-0">
                      <h5 class="font-weight-normal h6"><?= yii::t('app', 'Filtrar por cliente') ?></h5>
                    </div>
                    <div class="col-lg ml-2 has-success">
                        <?php
                            $CoachList = yii\helpers\ArrayHelper::map(Fair::find()->orderBy('id_fair DESC')->all(), 'id_fair', 'name_employees');
                            echo yii\helpers\Html::dropDownList('empleyees-select', null, $CoachList, [
                                'id' => 'empleyees-select',
                                'class' => 'form-control form-control-sm'
                            ]);
                        ?>
                    </div>
            </div>
        </div>
      </div>
      <div class="card-body p-4">
        <div class="loader" style="display:none; padding: 15px;"><img src="/img/loading.gif" /></div>
        <div class="load-calendar"></div>
      </div>
  </div>
  <div class="full-calendar-fair session" data-language="<?= yii::$app->language ?>" data-title-view="<?= Yii::t('app','Agendar Feria') ?>">
  </div>
  <div class="mt-4 show_coach_view" style="display: none">
    <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
      <li class="breadcrumb-item">
        <a class="text-primary" href="<?= Url::to(['/sessao' ]) ?>">
             Sair >
        </a>
      </li>
    </ol>
  </div>
</div>
<script src="https://dev.formacao.cv/biblioteca/vendor/jquery/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js"></script>
<style>
    .load-calendar .fc-head .fc-day-header span{
      text-transform: capitalize!important;
    }

    .load-calendar .fc-center h2{
      text-transform: capitalize!important;
    }

    .select2-container--krajee .select2-selection--single {
        height: 45px;
    }

    .select2-container--krajee .select2-selection--single .select2-selection__rendered {}

    .select2-container--krajee .select2-selection--single .select2-selection__arrow {
        height: 43px;
    }

    .select2-container--krajee .select2-selection--single .select2-selection__arrow {
        border-left: 1px solid #eee !important;
    }

    .select2-container--krajee .select2-selection {
        border: 1px solid #eee !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
    }

    .select2-container--krajee .select2-selection__clear {
        font-weight: 300 !important;
        top: 0.9rem !important;
    }

    .select2-container--open .select2-dropdown--below,
    .select2-container--open .select2-dropdown--above {
        background: #fff !important;
    }
    .modal-dialog {
    max-width: 856px!important;
    margin: 1.75rem auto;
}
</style>
<script type="text/javascript">
    var id_employees = $('#empleyees-select').val();

    $('.load-calendar').load("/user/coach/calendario-coach?id_employees=" + id_employees);

    $(document).on('change', '#empleyees-select', function() {
      var id_employees = $(this).val();
        $('.load-calendar').load("/user/coach/calendario-coach?id_employees=" + id_employees);
    });
</script>



