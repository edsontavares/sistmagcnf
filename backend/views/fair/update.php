<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Fair */


?>
<div class="fair-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
