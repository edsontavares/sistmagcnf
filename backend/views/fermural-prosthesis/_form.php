<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Patient;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

?>



    <?php $form = ActiveForm::begin( [
        'options' => [
        'class' => "js-validate js-step-form",
        'data-progress-id' => "#progressStepForm",
        'data-steps-id' => "#contentStepForm",
        'novalidate' => "novalidate",
        'enctype' => 'multipart/form-data',
     ],

    ]); ?>






    <!-- Step Form -->
<!--form class="js-validate js-step-form"
      data-progress-id="#progressStepForm"
      data-steps-id="#contentStepForm"
      novalidate="novalidate"-->
 
    <!--div class="card-header p-5">
      <nav id="progressStepForm" class="js-step-progress nav nav-icon nav-justified text-center">
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-user-circle nav-icon-action-inner"></span>
          </span>
          Select One
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-file-invoice-dollar nav-icon-action-inner"></span>
          </span>
          Select Two
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-paperclip nav-icon-action-inner"></span>
          </span>
          Select Three
        </a>
      </nav>
    </div-->

    <!-- Content Step Form -->
    <div id="contentStepForm" class="card-body p-5">
      <div id="selectStepOne" class="active">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Dados pessoais<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
        <div class="row">
        <div class="col-sm-6">
            <label for="" class="type-label" >Nome de Paciente<span class="bg-active">*</span></label>
            <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                'language' => 'en',
                'options' => ['placeholder' => 'Selecionar Paciente'],
                'pluginOptions' => [
                'allowClear' => true,
                'class' => 'form-control  ',
                'required' => true,
                'data-msg' => 'Por favor Selecionar Paciente.',
                'data-error-class' => 'u-has-error',
                'data-success-class' => 'u-has-success',
                ],
            ])->label(false) ?>
            
            
            </div>
            <div class="col-sm-6">
            <label for="" class="type-label" >Técnico Responsavel<span class="text-danger">*</span></label>
            <div class="form-group">
                    <?=  $form->field($model, 'nameEmployees')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\common\models\employees::find()->all(), 'name', 'name'),
                        // 'language' => 'es',
                        'options' => [

                        'required' => true,
                        'placeholder' => yii::t('app','Técnico Responsavel'),
                        'data-msg' => yii::t('app','Por favor Técnico Responsavel'),
                        ],
                        'pluginOptions' => [
                        'tags'=>true,
                        'class' => 'form-control  ',
                        
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        ],
                    ])->label(false) ?>
                </div>        
           </div>
            <div class="col-sm-4">
                        <label class="form-label mb-1">Tipo de Encaixe: </label>
                        <?php echo $form->field($model, 'fit_type[]')->checkboxList(
                            [ 1 => 'Quadrangular', 2 => 'RIO']
                        )->label(false) ;
                        ?>
                </div>
                <div class="col-sm-4">
                        <label class="form-label mb-1">Lado de amputação: ></label>
                        <?php echo $form->field($model, 'amputation_pass[]')->checkboxList(
                            [ 1 => 'Dto', 2 => 'Esq', 3 => 'BIL']
                        )->label(false) ;
                        ?>
                </div>
                <div class="col-sm-4">
                        <label class="form-label mb-1">Suspensão:</label>
                        <?php echo $form->field($model, 'suspension[]')->checkboxList(
                            [ 1 => 'Válvula', 2 => 'Cinto Salesiano', 3 => 'BIL']
                        )->label(false) ;
                        ?>
                </div>
        </div>
         <div class="row">
        
         <div class="col-sm-3">
            <label for="" class="type-label mt-15" >Cercunferênçia Nº 28 </label>
            <?= $form->field($model, 'circumference_m_one')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-3" >Altura Nº 30</label>
            <?= $form->field($model, 'height_m')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label " >Cercunferênçia Nº 31</label>
            <?= $form->field($model, 'circumference_m_two')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label " >Cercunferênçia Nº 32</label>
            <?= $form->field($model, 'circumference_m_three')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
         </div>
         <div class="col-sm-6">
            <img src="../../recurso/img/proteseF.png" class="femural mt-5" alt="user">
         </div>
         <div class="col-sm-3"> 
         <label for="" class="type-label mt-5" > Angulação Nº 27 </label>
            <?= $form->field($model, 'angulation_m')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label " > Comprimento Nº 29 </label>
            <?= $form->field($model, 'length_m')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-9" >Angulação Nº 33 </label>
            <?= $form->field($model, 'angled_m')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
         </div>
        </div>
        <div class="d-flex justify-content-end">
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepTwo">Próximo</button>
        </div>
      </div>

      <div id="selectStepTwo" style="display: none;">
      <div class="border-bottom pb-0 mb-2">
            <div class="border-bottom pb-0 mb-2">
                <h4 class="h5 font-weight-normal">Dados pessoais 2<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
            </div>
            <div class="row">
                <div class="col-sm-7">
                    <img src="../../recurso/img/peoteseFemural.png" alt="user" class="femuralm">

                    <label for="" class="type-label mt-5" > Comprimento Nº 42</label>
                    <?= $form->field($model, 'length_a')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>
                </div>
                <div class="col-sm-5">   
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <label for="" class="type-label mt-9" >34 </label>
                                    <?= $form->field($model, 'cercunferencia_n_one')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                                <div class="col-sm-6">
                                    <label for="" class="type-label mt-9" > 38</label>
                                    <?= $form->field($model, 'cercunferencia_n_five')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                            </div>
                        </div>
                    </div>  
        
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <label for="" class="type-label" >35 </label>
                                    <?= $form->field($model, 'cercunferencia_n_two')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                                <div class="col-sm-6">
                                    <label for="" class="type-label" > 39</label>
                                    <?= $form->field($model, 'cercunferencia_n_six')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <label for="" class="type-label" >36 </label>
                                    <?= $form->field($model, 'cercunferencia_n_three')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                                <div class="col-sm-6">
                                    <label for="" class="type-label" > 40</label>
                                    <?= $form->field($model, 'cercunferencia_n_seven')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <label for="" class="type-label" >37 </label>
                                    <?= $form->field($model, 'cercunferencia_n_four')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                                <div class="col-sm-6">
                                    <label for="" class="type-label " > 41</label>
                                    <?= $form->field($model, 'cercunferencia_n_eight')->textInput([
                                        'maxlength' => true,
                                        "class" => "form-control",
                                        'placeholder' => "",
                                        'aria-label' => "",
                                        'required' => false,
                                        'aria-describedby' => "dataInicioLabel",
                                        'data-msg' => " ",
                                        'data-error-class' => "u-has-error",
                                        'data-success-class' => "u-has-success",
                                        'type' => 'number'
                                    ]) ->label(false)?>
                                </div>
                            </div>
                        </div>
                    </div>   
                </div> 
            </div>

        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary transition-3d-hover mr-1 rounded" href="javascript:;" data-previous-step="#selectStepOne">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepThree">Próximo</button>
        </div>
      </div>

     
    </div>
    <div id="selectStepThree" style="display: none;">
            <div class="border-bottom pb-0 mb-2">
             <h4 class="h5 font-weight-normal">Dados pessoais3<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
            </div>
            <div class="row mt-3">
                <div class="col-sm-3">
                    <label for="" class="type-label mtp" > Cercunferência Nº 44</label>
                    <?= $form->field($model, 'circumference_c_one')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>

                    <label for="" class="type-label " > Cercunferência Nº 45</label>
                    <?= $form->field($model, 'circumference_c_two')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>
                </div>
                <div class="col-sm-6">
                 <img src="../../recurso/img/femural3.png" alt="user" class="femuralb">
                    <label for="" class="type-label " > Tamanho Nº 46</label>
                    <?= $form->field($model, 'length')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>
                </div>
                <div class="col-sm-3">
                    <label for="" class="type-label mtd" > Comprimento Nº 42</label>
                    <?= $form->field($model, 'length_c_one')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>
                        <label for="" class="type-label " >Comprimento 43</label>
                    <?= $form->field($model, 'length_c_two')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "",
                        'aria-label' => "",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " ",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                    ]) ->label(false)?>
                </div>
            </div>

        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary transition-3d-hover mr-1 rounded" href="javascript:;" data-previous-step="#selectStepTwo">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepFour">Próximo</button>
        </div>
      </div>

      <div id="selectStepFour" style="display: none;">
            <div class="border-bottom pb-0 mb-2">
             <h4 class="h5 font-weight-normal">Dados pessoais3<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
            </div>
            <div class="row mt-3">
                <div class="col-sm-6">
                    <div class="row mt-5">
                        <div class="col-sm-6">
                            <label for="" class="type-label mt-5 " > Espaçamento AP 47 </label>
                            <?= $form->field($model, 'spacing_ap_one')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                        <div class="col-sm-6">
                            <label for="" class="type-label mt-5"> Espaçamento ML 49 </label>
                            <?= $form->field($model, 'spacing_ml_one')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <label for="" class="type-label " > Espaçamento AP 48</label>
                            <?= $form->field($model, 'spacing_ap_two')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                        <div class="col-sm-6">
                            <label for="" class="type-label ">Espaçamento ML 50</label>
                            <?= $form->field($model, 'spacing_ml_two')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="../../recurso/img/femural4.png" alt="user" class="femurall">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                        <label for="" class="type-label mt-20"> Calcanhar 54</label>
                        <?= $form->field($model, 'heel')->textInput([
                            'maxlength' => true,
                            "class" => "form-control",
                            'placeholder' => "",
                            'aria-label' => "",
                            'required' => false,
                            'aria-describedby' => "dataInicioLabel",
                            'data-msg' => " ",
                            'data-error-class' => "u-has-error",
                            'data-success-class' => "u-has-success",
                            'type' => 'number'
                        ]) ->label(false)?>
                </div>
                <div class="col-sm-6">
                    <img src="../../recurso/img/femural5.png" alt="user" class="femurall">
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="" class="type-label ">51</label>
                            <?= $form->field($model, 'circumference_p_one')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                        <div class="col-sm-6">
                            <label for="" class="type-label ">52</label>
                            <?= $form->field($model, 'circumference_p_two')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                        </div>
                        <label for="" class="type-label mt-5 "> Tamanho 53</label>
                            <?= $form->field($model, 'sideP')->textInput([
                                'maxlength' => true,
                                "class" => "form-control",
                                'placeholder' => "",
                                'aria-label' => "",
                                'required' => false,
                                'aria-describedby' => "dataInicioLabel",
                                'data-msg' => " ",
                                'data-error-class' => "u-has-error",
                                'data-success-class' => "u-has-success",
                                'type' => 'number'
                            ]) ->label(false)?>
                    </div>

                </div>
            </div>

        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary transition-3d-hover mr-1 rounded" href="javascript:;" data-previous-step="#selectStepThree">Voltar</a>
          <?= Html::submitButton('Gurdar', ['class' => 'btn btn-sm btn-indigo rounded']) ?>
        </div>
      </div>
    <!-- End Content Step Form -->
  </div>
<!--/form-->
<!-- End Step Form -->

    <?php ActiveForm::end(); ?>
    <style>
        .mt-50{
            margin-top: 85px;
        }
        .mt-9{
            margin-top: 170px;
        }
        .femural{
          width: 445px;
        }
        .mt-15{
            margin-top: 87px;
        }
        .femuralm{
            width: 445px;
            height: 486px;
        }
        .femuralb{
            width: 391px;
        }
        .mtp{
            margin-top: 144px;
        }
        .mtd{
            margin-top: 83px;
        }
        .mt-20{
            margin-top: 210px;
        }

        .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}

    </style>


