<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FermuralProsthesisSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fermural-prosthesis-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_fermural_Prosthesis') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'orthopedist') ?>

    <?= $form->field($model, 'help') ?>

    <?php // echo $form->field($model, 'angular_frame') ?>

    <?php // echo $form->field($model, 'river') ?>

    <?php // echo $form->field($model, 'amputation_pass') ?>

    <?php // echo $form->field($model, 'circumference_m_one') ?>

    <?php // echo $form->field($model, 'circumference_m_two') ?>

    <?php // echo $form->field($model, 'circumference_m_three') ?>

    <?php // echo $form->field($model, 'height_m') ?>

    <?php // echo $form->field($model, 'angled_m') ?>

    <?php // echo $form->field($model, 'length_m') ?>

    <?php // echo $form->field($model, 'angulation_m') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_one') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_two') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_three') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_four') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_five') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_six') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_seven') ?>

    <?php // echo $form->field($model, 'cercunferencia_n_eight') ?>

    <?php // echo $form->field($model, 'length_a') ?>

    <?php // echo $form->field($model, 'circumference_c_one') ?>

    <?php // echo $form->field($model, 'circumference_c_two') ?>

    <?php // echo $form->field($model, 'length_c_one') ?>

    <?php // echo $form->field($model, 'length_c_two') ?>

    <?php // echo $form->field($model, 'length') ?>

    <?php // echo $form->field($model, 'spacing_ap_one') ?>

    <?php // echo $form->field($model, 'spacing_ap_two') ?>

    <?php // echo $form->field($model, 'spacing_ml_one') ?>

    <?php // echo $form->field($model, 'spacing_ml_two') ?>

    <?php // echo $form->field($model, 'circumference_p_one') ?>

    <?php // echo $form->field($model, 'circumference_p_two') ?>

    <?php // echo $form->field($model, 'sideP') ?>

    <?php // echo $form->field($model, 'heel') ?>

    <?php // echo $form->field($model, 'id_employees') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
