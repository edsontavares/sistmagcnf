<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style="text-align: center; margin-top: -0.5cm;" >    
    <!--h6><img src="./recurso/img/Logo.jpg" style="margin-top: -0.5cm;text-align: center;width: 50px; height: 50px;" alt="user"></h6-->
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->
<div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">

                <div class="row">
                  <div class="col-sm-3">
                    <?php if($model->circumference_m_one):?>
                        <label for="" class="type-label mt-15 font-weight-bold" >Cercunferênçia Nº 28 </label>
                        <p><?= $model->circumference_m_one?></p>
                    <?php endif;?>

                    <?php if($model->height_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" >Altura Nº 30</label>
                        <p><?= $model->height_m?></p>
                    <?php endif;?>

                    <?php if($model->circumference_m_two):?>
                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 31</label>
                        <p><?= $model->circumference_m_two?></p>
                    <?php endif;?>
                    
                    <?php if($model->circumference_m_three):?>
                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 32</label>
                        <p><?= $model->circumference_m_three?></p>
                    <?php endif;?>


                  </div>
                  <div class="col-sm-6">
                     <img src="./../recurso/img/proteseF.png" class="femural mt-5" alt="user">
                  </div>
                  <div class="col-sm-3">
                    <?php if($model->angulation_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" > Angulação Nº 27 </label>
                        <p><?= $model->angulation_m?></p>
                    <?php endif;?>

                    <?php if($model->length_m):?>
                        <label for="" class="type-label mt-5 font-weight-bold" > Comprimento Nº 29 </label>
                        <p><?= $model->length_m?></p>
                    <?php endif;?>

                    <?php if($model->angled_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" >Angulação Nº 33 </label>
                        <p><?= $model->angled_m?></p>
                    <?php endif;?>
                  </div>
                </div>
                <hr>

                <div class="row">
                <div class="col-sm-7">
                    <img src="./../recurso/img/peoteseFemural.png" alt="user" class="femuralm">
                    <?php if($model->length_a):?>
                        <label for="" class="type-label mt-5 font-weight-bold" > Comprimento Nº 42</label>
                        <p><?= $model->length_a?></p>
                    <?php endif;?>
                 
                </div>
                <div class="col-sm-5 pt-10">   
                    <div class="row mt-10">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_one):?>
                                        <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 34 </label>
                                        <p><?= $model->cercunferencia_n_one?></p>
                                    <?php endif;?>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_five):?>
                                        <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 38</label>
                                        <p><?= $model->cercunferencia_n_five?></p>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>  
        
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_two):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 35 </label>
                                        <p><?= $model->cercunferencia_n_two?></p>
                                    <?php endif;?>
                                   
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_six):?>
                                        <label for="" class="type-label font-weight-bold" > Cercunferênçia Nº 39</label>
                                        <p><?= $model->cercunferencia_n_six?></p>
                                    <?php endif;?>
                                
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if( $model->cercunferencia_n_three):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 36 </label>
                                        <p><?= $model->cercunferencia_n_three?></p>
                                    <?php endif;?>
                                   
                                </div>
                                <div class="col-sm-6">
                                    <?php if( $model->cercunferencia_n_seven):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 40</label>
                                        <p><?= $model->cercunferencia_n_seven?></p>
                                    <?php endif;?>
                                  
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_four):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 37 </label>
                                        <p><?= $model->cercunferencia_n_four?></p>
                                    <?php endif;?>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_four):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 41</label>
                                        <p><?= $model->cercunferencia_n_four?></p>
                                    <?php endif;?>
                                   
                                </div>
                            </div>
                        </div>
                    </div>   
                </div> 
            </div>
            <hr>

            <div class="row mt-3">
                <div class="col-sm-3">
                    <?php if($model->circumference_c_one):?>
                        <label for="" class="type-label mtp font-weight-bold" > Cercunferência Nº 44</label>
                        <p><?= $model->circumference_c_one?></p>
                    <?php endif;?>
                    
                    <?php if($model->circumference_c_two):?>
                        <label for="" class="type-label font-weight-bold" > Cercunferência Nº 45</label>
                        <p><?= $model->circumference_c_two?></p>
                    <?php endif;?>
                   
                </div>
                <div class="col-sm-6">
                 <img src="./../recurso/img/femural3.png" alt="user" class="femuralb">
                    <?php if($model->length):?>
                        <label for="" class="type-label font-weight-bold" > Tamanho Nº 46</label>
                        <p><?= $model->length?></p>
                    <?php endif;?>
                    
                </div>
                <div class="col-sm-3">
                    <?php if( $model->length_c_one):?>
                        <label for="" class="type-label mtd font-weight-bold" > Comprimento Nº 42</label>
                        <p><?= $model->length_c_one?></p>
                    <?php endif;?>
                    
                    <?php if($model->length_c_two):?>
                        <label for="" class="type-label font-weight-bold" >Comprimento 43</label>
                        <p><?= $model->length_c_two?></p>
                    <?php endif;?>
                    
                </div>
            </div>
            <hr>
            <div class="row mt-3">
                <div class="col-sm-6 mt-10">
                    <div class="row mt-5">
                        <div class="col-sm-6">
                            <?php if($model->spacing_ap_one):?>
                                <label for="" class="type-label mt-5 font-weight-bold" > Espaçamento AP 47 </label>
                                <p><?= $model->spacing_ap_one?></p>
                            <?php endif;?>
                           
                        </div>
                        <div class="col-sm-6">
                            <?php if($model->spacing_ml_one):?>
                                <label for="" class="type-label mt-5 font-weight-bold"> Espaçamento ML 49 </label>
                                <p><?= $model->spacing_ml_one?></p>
                            <?php endif;?>
                            
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <?php if( $model->spacing_ap_two):?>
                                <label for="" class="type-label font-weight-bold" > Espaçamento AP 48</label>
                                <p><?= $model->spacing_ap_two?></p>
                            <?php endif;?>
                         
                        </div>
                        <div class="col-sm-6">
                            <?php if($model->spacing_ml_two):?>
                                <label for="" class="type-label font-weight-bold">Espaçamento ML 50</label>
                                <p><?= $model->spacing_ml_two?></p>
                            <?php endif;?>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="./../recurso/img/femural4.png" alt="user" class="femurall">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <?php if($model->heel):?>
                        <label for="" class="type-label mt-20 font-weight-bold"> Calcanhar 54</label>
                        <p><?= $model->heel?></p>
                    <?php endif;?>
                        
                </div>
                <div class="col-sm-6">
                    <img src="./../recurso/img/femural5.png" alt="user" class="femurall">
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-6 mt-5">
                            <?php if($model->circumference_p_one):?>
                                <label for="" class="type-label  font-weight-bold"> Cercunferênçia Nº 51</label>
                                <p><?= $model->circumference_p_one?></p>
                            <?php endif;?>
                            
                        </div>
                        <div class="col-sm-6 mt-5">
                            <?php if($model->circumference_p_two):?>
                                <label for="" class="type-label  font-weight-bold">Cercunferênçia Nº 52</label>
                                <p><?= $model->circumference_p_two?></p>
                            <?php endif;?>
                            
                        </div>
                        <?php if($model->sideP):?>
                            <label for="" class="type-label mt-10 font-weight-bold"> Tamanho 53</label>
                            <p><?= $model->sideP?></p>
                        <?php endif;?>
                            
                    </div>

                </div>
            </div>
        </div>

</div>



<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
    .logoap{
  width: 319px;
}
.mt-14{
   
    margin-top: 9.5rem !important;

}
.mt-30{
    margin-top: 9.5rem !important;
}

.mt-50{
    margin-top: 85px;
}
.mt-70{
    margin-top: 170px;
}
.img-pe{
    width: 319px;
}
</style>
