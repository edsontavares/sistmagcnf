<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
$query= \common\models\Query::findOne($model->id_query);
$patient = \common\models\Patient::findOne($query->id_patient); 
\yii\web\YiiAsset::register($this);
?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-sm-12">
            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= Yii::$app->session->getFlash("success-create") ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Prótese femoral</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/Protese-femoral'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i>Descaregar', ['baixar', 'id'=>$model->id_fermural_Prosthesis], ['class' => 'dropdown-item transition-3d-hover' ,'target'=>"_blank"]) ?>   
                     <?php if($model->state == 1): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' =>$model->id_fermural_Prosthesis], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>
                   
                </div>
                </div>
            </div>
         </div>
        
    </div>
   
    <hr>

    <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
                <div class="row mx-gutters-2">
                    <div class="col-sm-6">
                        <label for="" class="type-label font-weight-bold" ><b>Nome de Paciente</b></label>
                        <p><?= $patient->name?></p>
                    </div>
                    <div class="col-sm-6">
                        <label for="" class="type-label font-weight-bold" ><b>Técnico Responsavel</b></label>
                        <p><?= $model->nameEmployees?></p>
                    </div>
                    <?php if($model->fit_type):?>
                        <div class="col-sm-4">
                            <label for="" class="type-label font-weight-bold" ><b>Tipo de Encaixe:</b></label>
                                <p>
                                    <?php if(strpos($model->fit_type, '1')):?>
                                        <span>Quadrangular</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->fit_type, '2')):?>
                                        <span>RIO</span>
                                    <?php endif; ?>
                                </p> 
                        </div>
                    <?php endif;?>
                    <?php if($model->amputation_pass):?>
                        <div class="col-sm-4">
                            <label for="" class="type-label font-weight-bold" ><b>Lado de amputação: </b></label>
                                <p>
                                    <?php if(strpos($model->amputation_pass, '1')):?>
                                        <span>Dto</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->amputation_pass, '2')):?>
                                        <span>Esq</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->amputation_pass, '3')):?>
                                        <span>BIL</span>
                                    <?php endif; ?>
                                </p> 
                        </div>
                    <?php endif;?>
                    <?php if($model->suspension):?>
                        <div class="col-sm-4">
                            <label for="" class="type-label font-weight-bold" ><b>Suspensão:</b></label>
                                <p>
                                    <?php if(strpos($model->suspension, '1')):?>
                                        <span>Válvula</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->suspension, '2')):?>
                                        <span>Cinto Salesiano</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->suspension, '3')):?>
                                        <span>BIL</span>
                                    <?php endif; ?>
                                </p> 
                        </div>
                    <?php endif;?>
                </div>
                <hr>
                <div class="row">
                  <div class="col-sm-3">
                    <?php if($model->circumference_m_one):?>
                        <label for="" class="type-label mt-15 font-weight-bold" >Cercunferênçia Nº 28 </label>
                        <p><?= $model->circumference_m_one?></p>
                    <?php endif;?>

                    <?php if($model->height_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" >Altura Nº 30</label>
                        <p><?= $model->height_m?></p>
                    <?php endif;?>

                    <?php if($model->circumference_m_two):?>
                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 31</label>
                        <p><?= $model->circumference_m_two?></p>
                    <?php endif;?>
                    
                    <?php if($model->circumference_m_three):?>
                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 32</label>
                        <p><?= $model->circumference_m_three?></p>
                    <?php endif;?>


                  </div>
                  <div class="col-sm-6">
                     <img src="../../recurso/img/proteseF.png" class="femural mt-5" alt="user">
                  </div>
                  <div class="col-sm-3">
                    <?php if($model->angulation_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" > Angulação Nº 27 </label>
                        <p><?= $model->angulation_m?></p>
                    <?php endif;?>

                    <?php if($model->length_m):?>
                        <label for="" class="type-label mt-5 font-weight-bold" > Comprimento Nº 29 </label>
                        <p><?= $model->length_m?></p>
                    <?php endif;?>

                    <?php if($model->angled_m):?>
                        <label for="" class="type-label mt-9 font-weight-bold" >Angulação Nº 33 </label>
                        <p><?= $model->angled_m?></p>
                    <?php endif;?>
                  </div>
                </div>
                <hr>

                <div class="row">
                <div class="col-sm-7">
                    <img src="../../recurso/img/peoteseFemural.png" alt="user" class="femuralm">
                    <?php if($model->length_a):?>
                        <label for="" class="type-label mt-5 font-weight-bold" > Comprimento Nº 42</label>
                        <p><?= $model->length_a?></p>
                    <?php endif;?>
                 
                </div>
                <div class="col-sm-5 pt-10">   
                    <div class="row mt-10">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_one):?>
                                        <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 34 </label>
                                        <p><?= $model->cercunferencia_n_one?></p>
                                    <?php endif;?>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_five):?>
                                        <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 38</label>
                                        <p><?= $model->cercunferencia_n_five?></p>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>  
        
                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_two):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 35 </label>
                                        <p><?= $model->cercunferencia_n_two?></p>
                                    <?php endif;?>
                                   
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_six):?>
                                        <label for="" class="type-label font-weight-bold" > Cercunferênçia Nº 39</label>
                                        <p><?= $model->cercunferencia_n_six?></p>
                                    <?php endif;?>
                                
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if( $model->cercunferencia_n_three):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 36 </label>
                                        <p><?= $model->cercunferencia_n_three?></p>
                                    <?php endif;?>
                                   
                                </div>
                                <div class="col-sm-6">
                                    <?php if( $model->cercunferencia_n_seven):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 40</label>
                                        <p><?= $model->cercunferencia_n_seven?></p>
                                    <?php endif;?>
                                  
                                </div>
                            </div>
                        </div>
                    </div>   


                    <div class="row">
                        <div class="col-sm-12">        
                            <div class="row">
                                <div class="col-sm-6"> 
                                    <?php if($model->cercunferencia_n_four):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 37 </label>
                                        <p><?= $model->cercunferencia_n_four?></p>
                                    <?php endif;?>
                                    
                                </div>
                                <div class="col-sm-6">
                                    <?php if($model->cercunferencia_n_four):?>
                                        <label for="" class="type-label font-weight-bold" >Cercunferênçia Nº 41</label>
                                        <p><?= $model->cercunferencia_n_four?></p>
                                    <?php endif;?>
                                   
                                </div>
                            </div>
                        </div>
                    </div>   
                </div> 
            </div>
            <hr>

            <div class="row mt-3">
                <div class="col-sm-3">
                    <?php if($model->circumference_c_one):?>
                        <label for="" class="type-label mtp font-weight-bold" > Cercunferência Nº 44</label>
                        <p><?= $model->circumference_c_one?></p>
                    <?php endif;?>
                    
                    <?php if($model->circumference_c_two):?>
                        <label for="" class="type-label font-weight-bold" > Cercunferência Nº 45</label>
                        <p><?= $model->circumference_c_two?></p>
                    <?php endif;?>
                   
                </div>
                <div class="col-sm-6">
                 <img src="../../recurso/img/femural3.png" alt="user" class="femuralb">
                    <?php if($model->length):?>
                        <label for="" class="type-label font-weight-bold" > Tamanho Nº 46</label>
                        <p><?= $model->length?></p>
                    <?php endif;?>
                    
                </div>
                <div class="col-sm-3">
                    <?php if( $model->length_c_one):?>
                        <label for="" class="type-label mtd font-weight-bold" > Comprimento Nº 42</label>
                        <p><?= $model->length_c_one?></p>
                    <?php endif;?>
                    
                    <?php if($model->length_c_two):?>
                        <label for="" class="type-label font-weight-bold" >Comprimento 43</label>
                        <p><?= $model->length_c_two?></p>
                    <?php endif;?>
                    
                </div>
            </div>
            <hr>
            <div class="row mt-3">
                <div class="col-sm-6 mt-10">
                    <div class="row mt-5">
                        <div class="col-sm-6">
                            <?php if($model->spacing_ap_one):?>
                                <label for="" class="type-label mt-5 font-weight-bold" > Espaçamento AP 47 </label>
                                <p><?= $model->spacing_ap_one?></p>
                            <?php endif;?>
                           
                        </div>
                        <div class="col-sm-6">
                            <?php if($model->spacing_ml_one):?>
                                <label for="" class="type-label mt-5 font-weight-bold"> Espaçamento ML 49 </label>
                                <p><?= $model->spacing_ml_one?></p>
                            <?php endif;?>
                            
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-6">
                            <?php if( $model->spacing_ap_two):?>
                                <label for="" class="type-label font-weight-bold" > Espaçamento AP 48</label>
                                <p><?= $model->spacing_ap_two?></p>
                            <?php endif;?>
                         
                        </div>
                        <div class="col-sm-6">
                            <?php if($model->spacing_ml_two):?>
                                <label for="" class="type-label font-weight-bold">Espaçamento ML 50</label>
                                <p><?= $model->spacing_ml_two?></p>
                            <?php endif;?>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <img src="../../recurso/img/femural4.png" alt="user" class="femurall">
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-sm-3">
                    <?php if($model->heel):?>
                        <label for="" class="type-label mt-20 font-weight-bold"> Calcanhar 54</label>
                        <p><?= $model->heel?></p>
                    <?php endif;?>
                        
                </div>
                <div class="col-sm-6">
                    <img src="../../recurso/img/femural5.png" alt="user" class="femurall">
                </div>
                <div class="col-sm-3">
                    <div class="row">
                        <div class="col-sm-6 mt-5">
                            <?php if($model->circumference_p_one):?>
                                <label for="" class="type-label  font-weight-bold"> Cercunferênçia Nº 51</label>
                                <p><?= $model->circumference_p_one?></p>
                            <?php endif;?>
                            
                        </div>
                        <div class="col-sm-6 mt-5">
                            <?php if($model->circumference_p_two):?>
                                <label for="" class="type-label  font-weight-bold">Cercunferênçia Nº 52</label>
                                <p><?= $model->circumference_p_two?></p>
                            <?php endif;?>
                            
                        </div>
                        <?php if($model->sideP):?>
                            <label for="" class="type-label mt-10 font-weight-bold"> Tamanho 53</label>
                            <p><?= $model->sideP?></p>
                        <?php endif;?>
                            
                    </div>

                </div>
            </div>
        </div>
</div>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
.mt-50{
            margin-top: 85px;
        }
        .mt-9{
            margin-top: 170px;
        }
        .femural{
          width: 445px;
        }
        .mt-15{
            margin-top: 87px;
        }
        .femuralm{
            width: 445px;
            height: 486px;
        }
        .femuralb{
            width: 391px;
        }
        .mtp{
            margin-top: 182px;
        }
        .mtd{
            margin-top: 122px;
        }
        .mt-20{
            margin-top: 210px;
        }
</style>
