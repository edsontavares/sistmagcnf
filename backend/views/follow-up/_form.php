<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\QueryTepy;
use common\models\Employees;
use common\models\Patient;
use common\models\Query;
?>
<div class="follow-up-form">

    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            
            <div class="col-sm-8">
                <label for="" class="type-label" >Nome de Paciente<span class="bg-active">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Selecionar Paciente'],
                        'pluginOptions' => [
                        'maxlength' => true,
                        'allowClear' => true,
                        'class' => 'form-control  ',
                        'required' => true,
                        'data-msg' => 'Por favor Selecionar Paciente.',
                        'data-error-class' => 'u-has-error',
                        'data-success-class' => 'u-has-success',
                        ],
                    ])->label(false) ?>
                </div>          
            </div>
             
            <div class="col-sm-4">
                <label for="" class="type-label" >Tecnico Responsavel<span class="bg-active">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'id_employees')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Employees::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_employees', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Selecionar Tecnico'],
                        'pluginOptions' => [
                        'allowClear' => true,
                        'class' => 'form-control  ',
                        'maxlength' => true,
                        'required' => true,
                        'data-msg' => 'Por favor Selecionar Tecnico.',
                        'data-error-class' => 'u-has-error',
                        'data-success-class' => 'u-has-success',
                        ],
                    ])->label(false) ?>
                </div>          
            </div>
            <div class="col-sm-6">
                <label for="" class="type-label" >Actividade</label>
                <div class="form-group">
                    <?= $form->field($model, 'acticity')->textInput([
                        'maxlength' => true,
                        "class" => "form-control",
                        'placeholder' => "Introduzir Actividade",
                        'aria-label' => "Introduzir Actividade",
                        'required' => true,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Actividade",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                    
                    ])->label(false)?>
                </div>
             </div>
             <div class="col-sm-6">
                <label for="" class="type-label" >Data</label>
                <?= $form->field($model, 'date')->textInput([
                        'maxlength' => true,
                        "class" => "form-control  input_date",
                        "class" => "form-control input_date",
                        'placeholder' => "Data",
                        'aria-describedby' => "dataInicioLabel",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'data-rp-date-format' => "Y-m-d",
                        'required' => true,
                        'type' => 'date'    
                ])->label(false) ?>
            </div>
            <div class="col-sm-12">
              <label for="" class="type-label" >Observações<span class="bg-active">*</span></label>
              <?= $form->field($model, 'aboservation')->textArea(['maxlength' => true, 'id'=>'seguemento','placeholder' =>yii::t('app','') ])->label(false) ?>
              <!--div class="u-summernote-editor">
                    <textarea id="summernote" name="Query[aboservation]" rows="3" class="form-control"><!?= $model->aboservation ?></textarea>
                 </div-->
            </div>
        </div>
    <div class="col-sm-12 text-right mt-4">
        <div class="form-group">
            <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
            <!--?= Html::submitButton('Registar', ['class' => 'btn btn-indigo btn-sm rounded ']) ?-->
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
           .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>
<script type="text/javascript">

$(function () {

    $('#seguemento').summernote({
        dialogsInBody: true,
        height: 300,
        placeholder: $('#jobs-descriptionn').attr('placeholder'),
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
        ],
    });

    $('#privacidade').summernote({
        dialogsInBody: true,
        height: 300,
        placeholder: $('#jobs-descriptionn').attr('placeholder'),
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['table', ['table']],
            ['insert', ['link']],
        ],
    });

})

</script>
