<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\FollowUpSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="follow-up-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_follow_up') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'id_employees') ?>

    <?= $form->field($model, 'acticity') ?>

    <?= $form->field($model, 'aboservation') ?>

    <?php // echo $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'signature') ?>

    <?php // echo $form->field($model, 'descreption') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
