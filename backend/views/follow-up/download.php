<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style=" margin-top: -0.5cm;" >    
    <div style="float:left; width:20%;">
        <img src="./../recurso/img/Logo.svg"  width="70" height="65" style="margin-top: -0.5cm;" >
    </div>
    <div style="float:left; width:80%;">
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    </div>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->
<div class="card">
        <div class="card-body shadow-sm p-4">

            <table style="width:100%">
            <?php  foreach ($follwup as  $model) { ?>
                <tbody>
                    <tr>
                        <td class="align-middle"><b>Actividade</b></td>
                        <td class="align-middle"><b>Nome paciente</b></td>
                        <td class="align-middle"><b>Técnico</b></td>
                        <td class="align-middle"><b>Data</b></td>
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->acticity?></td>
                        <td class="text-secondary" ><?= $model->patients->name?></td>
                        <td class="text-secondary"><?= $model->id_employees?></td>
                        <td class="text-secondary"><?= $model->date?></td>
                    </tr>
                    <tr>
                        <td class="align-middle"><b>Observação</b></td>
                       
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->aboservation?></td>
                        
                    </tr>
                </tbody>
                <br> <br>
                <?php }?>  
            </table>
        </div>
    </div>

</div>



<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
 

</style>
