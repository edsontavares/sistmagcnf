<?php

use common\models\Adptacao;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<div class="employees-index">
  
    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('Adicionar Seguimento', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search.." id="myInput" onkeyup="myFunction()">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>


    <table class="table" id="myTable">
  <thead class="top">
    <tr>
      
    
      <th scope="col">Nome Paciente</th>
      <th scope="col">Técnico Responsavel</th>
      <th scope="col">Actividade</th>
      <th scope="col">data</th>
      <th scope="col">opcao</th>

    </tr>
  </thead>
  <tbody>
  <?php  foreach ($followup as  $followupa) { ?>
     
    <tr>
      
    
      <td>
        <?php if($followupa->patients):?>
          <?= $followupa->patients->name?>
        <?php endif;?>
      </td>
      <td>
      <?php if($followupa->employees):?>
        <?= $followupa->employees->name?>
        <?php endif;?>
      </td>
      <td><?= $followupa->acticity?></td>
      <td>
        <?= $followupa->date?>
      </td>
      <td> 
          <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i>', ['view', 'id'=>$followupa->id_follow_up], ['class' => '','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>
          <?= Html::a('<i class="fas fa-pencil-alt dropdown-item-icon"></i> ', ['update', 'id'=>$followupa->id_follow_up], ['class'=>"",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>
          <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id'=>$followupa->id_follow_up], [
            'class' => '',
            'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
            'data' =>
                [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
          ]) ?>
    </td>
      </tr>
    <?php }?>  
  </tbody>
</table>
<?= $this->render('../../../common/componentes/pagination', [
    'total' => @$total,
    'item_numbers' => $item_numbers,
    'prettyUrl' => 'active',
    'url' => Url::to(['/adptacao/index'])
]) ?>

</div>


<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
   
    
</style>

