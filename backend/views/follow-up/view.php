<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
\yii\web\YiiAsset::register($this);

?>
<div class="follow-up-view">
    <?php if (Yii::$app->session->hasFlash("success-create")): ?>
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            <?= Yii::$app->session->getFlash("success-create") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    <?php endif; ?>

    <div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Seguemento  de Paciente</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
                 <a class="" href="<?= Url::to(['/seguimento'])?>">Voltar</a>
                <div class="btn-group position-relative ">

                    <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                        aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                        data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                        data-unfold-type="css-animation" data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                        <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                    </a>

                    <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                        
                        <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i> Descaregar', ['baixar','id'=>$model->id_follow_up], ['class'=>"dropdown-item",'target'=>"_blank"])?>
                        
                    </div>
                </div>
            </div>
         </div>
    </div>
    <hr>
    <div class="card">
        <div class="card-body shadow-sm p-4">
            <table style="width:100%">
                <tbody>
                    <tr>
                        <td class="align-middle">Actividade</td>
                        <td class="align-middle">Nome paciente</td>
                        <td class="align-middle">Técnico</td>
                        <td class="align-middle">Data</td>
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->acticity?></td>
                        <td class="text-secondary" ><?= $model->id_patient?></td>
                        <td class="text-secondary"><?= $model->id_employees?></td>
                        <td class="text-secondary"><?= $model->date?></td>
                    </tr>
                    <tr>
                        <td class="align-middle">Observação</td>
                       
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->aboservation?></td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
