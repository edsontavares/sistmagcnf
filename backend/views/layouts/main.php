<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use common\models\Entidade;

$notificationFisio = \common\models\Notification::find()->where(['tables'=>'fisioterapia', 'titile'=>'FTP'])->all();
$notificationPayment = \common\models\Notification::find()->where(['tables'=>'Pagamento'])->all();

$notificationPaymentActiv = \common\models\Notification::find()->where(['tables'=>'Pagamento','state'=>1])->all();
$notificationEnviaPagamento = \common\models\Notification::find()->where(['tables'=>'envia-pagamento','state'=>1])->all();
$notification = count($notificationPaymentActiv) + count($notificationEnviaPagamento);

$notificationAll = \common\models\Notification::find()->all();
 $user_id = Yii::$app->user->identity->id;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	  <!-- FULL CALENDAR PLUGIN -->
   
  
</head>
<body>
<?php $this->beginBody() ?>
<!-- ========== HEADER ========== -->
  <header id="header" class="u-header u-header--navbar-bg u-header--bg-transparent-xl u-header--sticky-top-xl u-header--starter">
    <div class="u-header__section bg-yellow">
      <div id="logoAndNav" class="container-fluid">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-xl u-header__navbar u-header__navbar--no-space">
          <div class="u-header--starter__logo">
            <!-- White Logo -->
            <a class="navbar-brand u-header__navbar-brand u-header__navbar-brand-center u-header__navbar-brand-default u-header__navbar-brand-text-white" href="index.php" aria-label="Front">
              
              <span class="u-header__navbar-brand-text"><img src="../recurso/img/logob.png" class="logo" alt="" style="height: 60px"></span>
            </a>
            <!-- End White Logo -->


            <!-- Responsive Toggle Button -->
            <button type="button" class="navbar-toggler btn u-hamburger"
                    aria-label="Toggle navigation"
                    aria-expanded="false"
                    aria-controls="navBar"
                    data-toggle="collapse"
                    data-target="#navBar">
              <span id="hamburgerTrigger" class="u-hamburger__box">
                <span class="u-hamburger__inner"></span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->
          </div>

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <ul class="navbar-nav u-header__navbar-nav">
              <!-- Shopping Cart -->
              <li class="list-inline-item">
                <a id="sidebarNavToggler" class="btn btn-xs btn-icon btn-text-secondary ml-1" href="javascript:;" role="button"
                    aria-controls="sidebarContent_all"
                    aria-haspopup="true"
                    aria-expanded="false"
                    data-unfold-event="click"
                    data-unfold-hide-on-scroll="false"
                    data-unfold-target="#sidebarContent_all"
                    data-unfold-type="css-animation"
                    data-unfold-animation-in="fadeInRight"
                    data-unfold-animation-out="fadeOutRight"
                    data-unfold-duration="500">
                    <span class="fas fa-bell btn-icon__inner"></span>
                    <?php if($notification):?>
                      <span class="badge badge-sm badge-primary badge-pos rounded-circle"><?= $notification?></span>
                    <?php endif;?>
                </a>
              </li>
             

              <!-- Changelog -->
              <li class="nav-item hs-has-sub-menu u-header__nav-item"
                  data-event="hover"
                  data-animation-in="slideInUp"
                  data-animation-out="fadeOut"
                  data-position="left">
                <a id="changelogMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">
                <!--?= yii::$app->user->identity->username?-->
                <?php
                   if (!Yii::$app->user->isGuest)
                         echo Yii::$app->user->identity->username;
                   ?>
                </a>

                <ul class="hs-sub-menu u-header__sub-menu u-header__sub-menu--spacer" style="min-width: 220px;" aria-labelledby="changelogMenu">
                  <li><a class="nav-link u-header__sub-menu-nav-link" href="getting-started/changelog.html#version2_3">Latest (v2.3)</a></li>
                  <li class="dropdown-divider"></li>
                  <li><a class="nav-link u-header__sub-menu-nav-link" href="<?= Url::to(['/users/perfil', 'id'=>$user_id])?>">Perfil</a></li>
                  <li>
                    <a class="nav-link u-header__sub-menu-nav-link" href="<?= Url::to(['/site/logout'])?>" data-method="post">Sair</a>
                  </li>
                    <!--                                    <a class="btn btn-sm btn-light transition-3d-hover">-->
                    
                   
                </ul>
              </li>
              <!-- End Changelog -->

              <li class="nav-item u-header__nav-last-item">
              
              <div class="u-avatar position-relative mr-3">
                <!--img class="rounded-circle rounded-circles" src="../recurso/img/user-2.jpg"-->
                <img class="rounded-circle rounded-circles" src="../../<?= isset(Yii::$app->user->identity->imagem) ? substr(Yii::$app->user->identity->imagem, 0, 1) == '/' ? Yii::$app->user->identity->imagem : '/' . Yii::$app->user->identity->imagem : '../recurso/imgUtilizador' ?>">

                <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
              </div>
                <!--?= Html::img( Url::to(yii::$app->user->identity->imagem), ['class' => 'rounded-circle']) ?-->
              </li>
             
            </ul>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

   <!-- ========== LEFT ========== -->
  <div class="js-scrollbar hs-docs-sidebar">
    <div class="d-flex justify-content-between py-2 py-xl-0">
      <!-- Search -->
      <form class="col form-inline input-group-sm">
        <input class="js-hs-docs-search form-control w-100" type="text" placeholder="Search...">
      </form>
      <!-- End Search -->

      <!-- Responsive Toggle Button -->
      <button class="btn btn-link hs-docs-sidebar__toggle" type="button"
              data-toggle="collapse"
              data-target="#navside-accordion"
              aria-controls="navside-accordion"
              aria-expanded="false"
              aria-label="Toggle docs navigation">
        <svg xmlns="http://www.w3.org/2000/svg" viewbox="0 0 30 30" width="30" height="30" focusable="false">
          <title>Menu</title>
          <path stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-miterlimit="10" d="M4 7h22M4 15h22M4 23h22"/>
        </svg>
      </button>
      <!-- End Responsive Toggle Button -->
    </div>

    <!-- Sidebar Nav -->
    <nav id="navside-accordion" class="hs-docs-navside collapse">
      <div id="components" class="collapse show" data-parent="#navside-accordion">
        
        <span class="hs-docs-navside__title hs-docs-navside__title active">Dashboard</span>
        <ul class="hs-docs-navside__links">
        <?php if (\Yii::$app->user->can('admin')):?>
            <li><a href="<?= Url::to(['/'])?>" ><i class="fas fa-home mr-2"></i>Inicio</a></li>
          <?php endif;?>
            <li><a href="<?= Url::to(['/ortopidia'])?>"><i class="fas fa-blind mr-2"></i>Ortopidia</a></li>
            <li><a href="<?= Url::to(['/fisioterapia'])?>"><i class="fas fa-skiing-nordic mr-2"></i>Fisioterapia</a></li>
        </ul>
        <!-- registar funcionario -->
        <span class="hs-docs-navside__title">Funcionario</span>
        <ul class="hs-docs-navside__links">
            <li><a href="<?= Url::to(['/funcionario'])?>"><i class="fas fa-users-cog mr-2"></i>Funcionario</a></li>
            <li><a href="<?= Url::to(['/tipo-funcionario'])?>"><i class="fas fa-tape mr-2"></i>Categoria</a></li>
            <li><a href="<?= Url::to(['/location'])?>"><i class="fas fa-map-marker-alt mr-2"></i>Localização</a></li>
            <li><a href="<?= Url::to(['/feria'])?>"><i class="fas fa-map-marker-alt mr-2"></i>Marcação de Feria</a></li>
            
        </ul>
        <?php //if (\Yii::$app->user->can('admin')):?>
        <span class="hs-docs-navside__title">Paciente</span>
        <ul class="hs-docs-navside__links">
          <li><a href="<?= Url::to(['/paciente'])?>"><i class="fas fa-procedures mr-2"></i>Paciente</a></li>
        </ul>
        <?php //endif;?>
        <?php //if (\Yii::$app->user->can('ortopidia')):?>
        <span class="hs-docs-navside__title">Avaliação</span>
        <ul class="hs-docs-navside__links">
            <li><a href="<?= Url::to(['/consulta'])?>"><i class="fas fa-stethoscope mr-2"></i>Consulta</a></li>
            <li><a href="<?= Url::to(['/consulta-concluida'])?>"><i class="fas fa-stethoscope mr-2"></i>Consulta Concluido</a></li>
        </ul>
        <?php //endif;?>
        <?php if (\Yii::$app->user->can('admin')):?>
          <span class="hs-docs-navside__title">Pagamento</span>
          <ul class="hs-docs-navside__links">
            <li><a href="<?= Url::to(['/precarious'])?>"><i class="fas fa-ring mr-2"></i>Preçario</a></li>
            <li><a href="<?= Url::to(['/payment'])?>"><i class="fas fa-file-invoice mr-2"></i>Factura</a></li>
            
          </ul>
        <?php endif;?>
        
        
        <span class="hs-docs-navside__title">Ortópedico</span>
        <ul class="hs-docs-navside__links">
          <li><a href="<?= Url::to(['/miShortOrthosis']) ?>"><i class="fas fa-american-sign-language-interpreting mr-2"></i>Ortótese curta do mi</a>
          </li>
          <li><a href="<?= Url::to(['/Protese-femoral']) ?>">Prótese femoral</a></li>
          <li><a href="<?= Url::to(['/Protese-tibial']) ?>">Prótese tibial</a></li>
          <li><a href="<?= Url::to(['/minerva'])?>">Minerva</a></li>
          <li><a href="index.php?r=site/Utilizadores"><i class="fas fa-chair mr-2"></i>Assento</a></li>
          <li><a href="<?= Url::to(['/adaptcao'])?>"><i class="fas fa-wheelchair mr-2"></i>Adaptação</a></li>
          <li><a href="<?= Url::to(['/seguimento'])?>"><i class="fas fa-wheelchair mr-2"></i>Seguimento</a></li>
        </ul>
        <span class="hs-docs-navside__title">Agenda</span>
        <ul class="hs-docs-navside__links">
            <li><a href="<?= Url::to(['/calendar']) ?>"><i class="far fa-calendar-alt mr-2"></i> Avaliação</a></li>
            <li><a href="<?= Url::to(['/agendas']) ?>"><i class="far fa-calendar-alt mr-2"></i> Medida </a></li>
            <li><a href="<?= Url::to(['/agenda']) ?>"><i class="far fa-calendar-alt mr-2"></i> Consulta</a></li>
            <li><a href="<?= Url::to(['/lista']) ?>"><i class="far fa-calendar-alt mr-2"></i> Listas das agendas</a></li>

        </ul>
        <?php if (\Yii::$app->user->can('admin')):?>
          <ul class="hs-docs-navside__links">
            <li><a href="<?= Url::to(['/users']) ?>"><i class="fas fa-user-tag mr-2"></i>Utilizador</a></li>
          </ul>
        <?php endif;?>

        
       
      </div>
    </nav>
    <!-- End Sidebar Nav -->
  </div>
  <!-- ========== END LEFT ========== -->

  <!-- ========== MAIN ========== -->
  <main class="hs-docs-content space-2 space-top-xl-3" role="main">
  <?= Breadcrumbs::widget([
      'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
  ]) ?>
  <?= Alert::widget() ?>
  <?= $content ?>
  </main>
  <!-- ========== END MAIN ========== -->
  <!-- inicio popup -->
  <div id="modal-open" class="modal fade pt-5" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog" role="document" id="modal-size">
            <div class="modal-content card">

                <div class="modal-header card-header bg-light p-4">
                    <h3 class="h6 mb-0 modal-title" id="exampleModalCenterTitle"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div id="load-waiting" class="col-12 text-center" style="margin-top: 20px; display: none;">
                    <svg class="lds-spinner" width="100px" height="100px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" style="background: none;">
                        <g transform="rotate(0 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.9s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(36 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.8s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(72 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.7s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(108 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.6s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(144 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(180 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.4s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(216 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.3s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(252 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.2s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(288 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.1s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                        <g transform="rotate(324 50 50)">
                            <rect x="47" y="29" rx="9.4" ry="5.8" width="6" height="12" fill="#3399ff">
                                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>
                            </rect>
                        </g>
                    </svg>
                </div>

                <div class="card-body p-5" id="slim-scroll">
                    <div class="modal-conteudo"></div>
                </div>
            </div>

        </div>
    </div>
    <!-- fim de popup -->

    <!-- Shopping Cart -->
    <li class="list-inline-item">
                            <a id="sidebarNavToggler" class="btn btn-xs btn-icon btn-text-secondary ml-1" href="javascript:;" role="button"
                               aria-controls="sidebarContent_all"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="click"
                               data-unfold-hide-on-scroll="false"
                               data-unfold-target="#sidebarContent_all"
                               data-unfold-type="css-animation"
                               data-unfold-animation-in="fadeInRight"
                               data-unfold-animation-out="fadeOutRight"
                               data-unfold-duration="500">
                                <span class="fas fa-bell btn-icon__inner"></span>
                               
                                 <span class="badge badge-sm badge-primary badge-pos rounded-circle"></span>
                              
                            </a>
                          </li>

<!-- Account Sidebar Navigation -->
<aside id="sidebarContent_all" class="u-sidebar u-unfold--css-animation u-unfold--hidden" aria-labelledby="sidebarNavToggler" style="animation-duration: 500ms;">
    <div class="u-sidebar__scroller">
        <div class="u-sidebar__container">
            <div class="u-sidebar--account__footer-offset">
              <!-- Header -->
              <header class="card-header bg-light py-3 px-5">
                <div class="d-flex justify-content-between align-items-center">
                  <h3 class="h6 mb-0"><?=yii::t('app','Notificação')?></h3>

                  <button type="button" class="close"
                      aria-controls="sidebarContent_all"
                      aria-haspopup="true"
                      aria-expanded="false"
                      data-unfold-event="click"
                      data-unfold-hide-on-scroll="false"
                      data-unfold-target="#sidebarContent_all"
                      data-unfold-type="css-animation"
                      data-unfold-animation-in="fadeInRight"
                      data-unfold-animation-out="fadeOutRight"
                      data-unfold-duration="500">
                  <span aria-hidden="true">&times;</span>
                  </button>
                </div>
              </header>
              <!-- End Header -->
                <!-- Content -->
                <div class="js-scrollbar u-sidebar__body">
                  <div class="u-sidebar__content">

                  <?php if ($notificationAll  == 0): ?>
                           <!-- card notificação vazia -->
                            <div class="card-body text-center p-3">
                                <div class="mb-1"><i class="fas fa-exclamation-triangle"></i></div>
                                <div class="mb-1">
                                    <small><?= yii::t('app','Sem notificação')?></small>
                                </div>
                            </div>
                            <!-- fim de card notificação vazia -->
                        <?php else: ?>
                          <?php foreach ($notificationAll  as $key => $noticications):?>
                             <?= $this->render('notificacao', [
                                'noticications'  => $noticications,
                             ]) ?>
                           <?php endforeach; ?>
                         <?php endif; ?>
                    
                  </div>
                </div>
            </div>
        </div>
    </div>
</aside>
<!-- End Account Sidebar Navigation -->

<?php $this->endBody() ?>


<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {

         $.HSCore.components.HSStepForm.init('.js-step-form');
         $.HSCore.components.HSValidation.init('.js-validate');
         
        // initialization of autonomous popups
        $.HSCore.components.HSModalWindow.init('[data-modal-target]', '.js-modal-window', {
            autonomous: true
        });

        // initialization of datatables
        $.HSCore.components.HSDatatables.init('.js-datatable');

        // initialization of quantity counter
        //$.HSCore.components.HSQantityCounter.init('.js-quantity');

        // initialization of range datepicker
        $.HSCore.components.HSRangeDatepicker.init('.js-range-datepicker');

        $('.input_date').flatpickr();


        // initialization of horizontal progress bars
        var horizontalProgressBars = $.HSCore.components.HSProgressBar.init('.js-hr-progress', {
            direction: 'horizontal',
            indicatorSelector: '.js-hr-progress-bar'
        });

        // initialization of horizontal progress bars
        var verticalProgressBars = $.HSCore.components.HSProgressBar.init('.js-vr-progress', {
            direction: 'vertical',
            indicatorSelector: '.js-vr-progress-bar'
        });

        // initialization of text editors
        $.HSCore.components.HSSummernoteEditor.init('.js-summernote-editor');

        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');

       

        // initialization of chartist bar charts
        $.HSCore.components.HSChartistBarChart.init('.js-bar-chart');

        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));

        // initialization of chart pies
        var items = $.HSCore.components.HSChartPie.init('.js-pie');

        // initialization of focus state
        $.HSCore.components.HSFocusState.init();

        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        // initialization of malihu scrollbar
        var $scrollbar = $('.js-scrollbar');
        $.HSCore.components.HSMalihuScrollBar.init($scrollbar);
        $($scrollbar).mCustomScrollbar('scrollTo', '#components li a.active');

        // initialization of select picker
        $.HSCore.components.HSSelectPicker.init('.js-select');

    });

    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991.98,
            hideTimeOut: 0
        });

        // initialization of autocomplet
       // $.HSCore.components.HSLocalSearchAutocomplete.init('.js-hs-docs-search');

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');

    });
</script>

  <!--script src="//maps.googleapis.com/maps/api/js?key=YOURAPIKEY&callback=initMap" async defer></script---->

</body>
</html>
<?php $this->endPage() ?>
<style>
  .hs-docs-sidebar {
    background-color: #e0e0ea!important;
}
.logo{

}

#section1 {
  height: 300px;
 
  scroll-behavior: smooth;
}
.rounded-circles{
  height: 50px!important;
}


</style>
