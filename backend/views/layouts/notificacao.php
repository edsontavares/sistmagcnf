<?php 
    $notifications = \common\models\Notification::find()->where(['id'=>$noticications->id,'tables'=>'Pagamento'])->one();
    $notificationsEnviado = \common\models\Notification::find()->where(['id'=>$noticications->id,'tables'=>'envia-pagamento'])->one();
?>
<?php if($notifications):
  $payment = \common\models\Payment::findOne($notifications ->id_conteudo);                 
  $patientpayment = \common\models\Patient::findOne($payment->id_patient);   
?>
  <div class="not_pagamento">
    <div class="position-relative border-bottom pt-0 pb-1 mb-1 rounded p-2  <?=$notifications->state == 1 ? 'btn-soft-primary' :'' ?>"">
      <div class="row align-items-center">
        <div class="col-sm-11 mb-sm-0">
          <div class="d-flex">
              <div class="u-avatar mr-2">
                <?php  if($patientpayment->imagem):?>
                  <img class="img-fluid  rounded-circle mr-2" src="../../<?=$patientpayment->imagem?>" title="" style="height: 50px; width: 50px;">
                <?php else:?>
                  <img class="img-fluid avatar-img rounded-circle w-100" src="<?= yii::$app->params['urlImagem']?>" title="" style="height: 50px; width: 50px;">
                <?php endif;?>  
              </div>
              <div class="media-body">
                  <a class="text-dark" href="<?= \yii\helpers\Url::to(['/payment/view', 'id'=>$notifications->id_conteudo]) ?>">
                      <small>
                        <?= $patientpayment->name?>
                        <span class="font-weight-semi-bold"><?= yii::t('app','Fez pagamento do')?>,</span>
                        <?php if($payment->interva == 1):?>
                        <span class="">Primeiro Parcela</span>
                        <?php elseif($payment->interva == 2):?>
                          <span class="">Segundo Parcela</span>
                        <?php  elseif($payment->interva == 3):?>
                          <span class="">Terceiro Parcela</span>
                        <?php  elseif($payment->interva == 4):?>
                          <span class="">Completo</span>
                        <?php endif;?>
                      </small>
                  </a>
              </div>
            </div>
          </div>
          <?php
            $id = Yii::$app->security->generateRandomString(8);
          ?>
          <a class="delete_pagamento position-absolute top-0 right-0 mt-3" href="#" data-url="<?=\yii\helpers\Url::to(['/site/delete','id'=>$notifications->id])?>"
            data-sim="<?= Yii::t('app', 'Sim') ?>"
            data-nao="<?= Yii::t('app', 'Não') ?>"
            data-title="<?= Yii::t('app', 'Desejas eliminar este notificacao?') ?>"
            data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
            data-key=""><small class="far fa-trash-alt dropdown-item-icon"></small>
          </a>
      </div>
    </div>
  </div>
<?php elseif($notificationsEnviado):
  $patient = \common\models\Patient::findOne($notificationsEnviado->id_conteudo);   
?>
  <div class="not_envia_pagamento">
    <div class="position-relative border-bottom pt-0 pb-1 mb-1 rounded p-2  <?=$notificationsEnviado->state == 1 ? 'btn-soft-primary' :'' ?>"">
      <div class="row align-items-center">
        <div class="col-sm-11 mb-sm-0">
          <div class="d-flex">
              <div class="u-avatar mr-2">
                <?php  if($patient->imagem):?>
                  <img class="img-fluid  rounded-circle mr-2" src="../../<?=$patient->imagem?>" title="" style="height: 50px; width: 50px;">
                <?php else:?>
                  <img class="img-fluid avatar-img rounded-circle w-100" src="<?= yii::$app->params['urlImagem']?>" title="" style="height: 50px; width: 50px;">
                <?php endif;?>  
              </div>
              <div class="media-body">
                  <a class="text-dark" href="<?= \yii\helpers\Url::to(['/payment/view', 'id'=>$notificationsEnviado->id_conteudo]) ?>">
                    <?= $patient->name?>
                    <span class="font-weight-semi-bold"><?= yii::t('app','pendente ao pagamento')?>,</span>
                  </a>
              </div>
            </div>
          </div>
          <?php
            $id = Yii::$app->security->generateRandomString(8);
          ?>
          <a class="delete_Envia_pagamento position-absolute top-0 right-0 mt-3" href="#" data-url="<?=\yii\helpers\Url::to(['/site/delete','id'=>$notificationsEnviado->id])?>"
            data-sim="<?= Yii::t('app', 'Sim') ?>"
            data-nao="<?= Yii::t('app', 'Não') ?>"
            data-title="<?= Yii::t('app', 'Desejas eliminar este notificacao?') ?>"
            data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
            data-key=""><small class="far fa-trash-alt dropdown-item-icon"></small>
          </a>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php
  $script = <<<JS
  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  $(function() {
    $(document).on('click','.delete_pagamento',function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          var key = $(this).data('key');
          var url = $(this).data('url');
          var _title = $(this).data('title');
          var _message = $(this).data('message');
          var _sim = $(this).data('sim');
          var _nao = $(this).data('nao');
            var self = $(this)
          bootbox.confirm({
              title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
              message:'<p class="p-2 px-5">' +_message +'</p>',
              buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + _nao,
                    className: 'bt btn-soft-secondary btn-xs'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + _sim,
                    className: 'bt btn-primary btn-xs'
                }
              },
              callback: function (result) {
                  if(result){
                      $.ajax( {
                      method: "post",
                      url:url,
                      })
                        .done(function( respond ) {
                        data = JSON.parse(respond);
                          if (data.type === 'error'){
                              console.log('dont delete');
                          }
                          else{
                              $(self).closest(".not_pagamento").remove()

                          }
                      });
                  }
              }
          });
      });


      $(document).on('click','.delete_Envia_pagamento',function(e){
          e.preventDefault();
          e.stopImmediatePropagation();
          var key = $(this).data('key');
          var url = $(this).data('url');
          var _title = $(this).data('title');
          var _message = $(this).data('message');
          var _sim = $(this).data('sim');
          var _nao = $(this).data('nao');
            var self = $(this)
          bootbox.confirm({
              title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
              message:'<p class="p-2 px-5">' +_message +'</p>',
              buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + _nao,
                    className: 'bt btn-soft-secondary btn-xs'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + _sim,
                    className: 'bt btn-primary btn-xs'
                }
              },
              callback: function (result) {
                  if(result){
                      $.ajax( {
                      method: "post",
                      url:url,
                      })
                        .done(function( respond ) {
                        data = JSON.parse(respond);
                          if (data.type === 'error'){
                              console.log('dont delete');
                          }
                          else{
                              $(self).closest(".not_envia_pagamento").remove()

                          }
                      });
                  }
              }
          });
      });
  });
JS;

$this->registerJs($script);
?>
<style>
.modal-header {
  background-color: #f8f9fa !important;
  border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
</style>