<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-6">
            <label for="" class="type-label" >Pais <span class="text-danger">*</span></label>
                <?= $form->field($model, 'parents')->textInput([
                "class" => "form-control ",
                'placeholder' => "Introduzir nome de Pais",
                'aria-label' => "Introduzir nome de Pais",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir nome de Pais",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Ilha</label>
           
            <?php
            $sexo = ['S.Antao' => 'Ilha de S.Antao', 'S.Vicente' => 'Ilha de S.Vicente','S.Nicolau' => 'Ilha de S.Nicolau', 'Sal' => 'Ilha do Sal','Boavista' => 'Ilha de Boavista', 'Maio' => 'Ilha do Maio','Santiago' => 'Ilha de Santiago', 'Fogo' => 'Ilha do Fogo','Brava' => 'Ilha da Brava'];
            echo $form->field($model, 'island')->dropDownList(
                $sexo,
                ['prompt' => 'Selecionar o Ilha', "class" => "form-control ",
                'required' => false,
                ]
            )->label(false);
            ?>
            
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Cidade <span class="text-danger">*</span></label>
            <?= $form->field($model, 'city')->textInput([
                "class" => "form-control ",
                'placeholder' => "Introduzir Cidade",
                'aria-label' => "Introduzir Cidade",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir Cidade",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Zona<span class="text-danger">*</span></label>
                <?= $form->field($model, 'zone')->textInput([
                "class" => "form-control ",
                'placeholder' => "Introduzir Zona",
                'aria-label' => "Introduzir Zona",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir Zona",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Nome de Rua<span class="text-danger">*</span></label>
                <?= $form->field($model, 'street')->textInput([
                "class" => "form-control ",
                'placeholder' => "Introduzir Nome de Rua",
                'aria-label' => "Introduzir Nome de Rua",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir Nome de Rua",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
    </div>
    <div class="col-sm-12 text-right">
        <div class="form-group">  
        <a href="<?= Url::to(['/location'])?> " class="btn btn-sm btn-soft-secondary transition-3d-hover mr-2">Cancelar</a>  
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
