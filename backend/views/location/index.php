<?php

use common\models\Adptacao;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;


?>
<div class="employees-index">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('<i class="far fa-plus-square mr-1"></i>Registar Localização', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc search-location" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>


    <table class="table" id="#mySearch-location">
      <thead class="top">
        <tr>
          <th scope="col">Pais</th>
          <th scope="col">Ilha</th>
          <th scope="col">Cidade</th>
          <th scope="col">Zona</th>
          <th scope="col">Rua</th>
          <th scope="col">Opção</th>
        </tr>
      </thead>
      <tbody>
        <?php  foreach ($locations as  $location) { ?> 
          <tr>
            <td scope="row"><?= $location->parents?></td>
            <td><?= $location->island?></td>
            <td><?= $location->city?></td>
            <td><?= $location->zone?></td>
            <td> <?= $location->street?></td>
            <td> 
              <div class="btn-group position-relative ">
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                  <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon mr-2"></i>Ver', ['view', 'id' => $location->id_location], ['class' => 'dropdown-item','data-toggle'=>"tooltip",'data-placement'=>"top"]) ?>
                  <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i>Editar', ['update', 'id' => $location->id_location], ['class'=>"dropdown-item",'data-toggle'=>"tooltip",'data-placement'=>"top",])?>
                  <a class="delete_location dropdown-item" href="#" data-url="<?= Url::to(['/'.'location-delete/'.$location->id_location])?>"
                    data-sim="<?= Yii::t('app', 'Sim') ?>"
                    data-nao="<?= Yii::t('app', 'Não') ?>"
                    data-title="<?= Yii::t('app', 'Desejas eliminar este localização?') ?>"
                    data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                    data-key="<?= $location->id_location?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
                  </a>
                </div>
              </div>
            </td>
          </tr>
        <?php }?>  
      </tbody>
    </table>
  <?php if ($total > 12 || (@Yii::$app->request->get("page") && intval(Yii::$app->request->get("page")) > 1)): ?>
    <?= $this->render('../../../common/componentes/pagination', [
        'total' => @$total,
        'item_numbers' => $item_numbers,
        'prettyUrl' => 'active',
        'url' => Url::to(['/location/index'])
    ]) ?>
  <?php endif; ?>
</div>

<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    .top{
  background-color: #2d1582;
  box-shadow: 0 3px 6px 0 rgb(140 152 164 / 25%) !important;
}
.grid-view td:nth-child(4) {
        white-space: nowrap;
    }
    tbody tr:nth-child(odd) {
  background-color: #e0e0ea;
}
.table {
border: 1px solid #e7eaf3;
}
   
    
</style>
<?php
  $script = <<<JS


$(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  $(function() {
   
    $(document).on('click','.delete_location',function(e){
      e.preventDefault();
      var key = $(this).data('key');
      var _title = $(this).data('title');
      var url = $(this).data('url');
      var _message = $(this).data('message');
      var _sim = $(this).data('sim');
      var _nao = $(this).data('nao');
      bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-indigo btn-xs'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax( {
              method: "post",
              url:url,
            })
            .done(function( respond ) {
              data = JSON.parse(respond);
              if (data.type === 'error'){
                console.log('dont delete');
              }
              else{
                window.open('location?r=location&action=delete','_self');
              }
            });
          }
        }
      });
    });
  });
  
    $(document).ready(function(){
      $(".search-location").keyup(function(e){
        e.preventDefault();
        
          var txt = $(this).val();
          /*$('#result').html('');*/
          $.ajax({
            url:"location/ajax-search",
            method:"post",
            data:{search:txt},
            success:function(data)
            {
                $("#mySearch-location tr:not(:first)").remove();

              if(data == null || data == '') {
                  $('#mySearch-location').append('<tr colspan="5" class="text-center w-500 text-warning"><td colspan="4"> Nenhum resultado </tr></td>');
              }
              $.each(data, function(index, value) {
                $('#mySearch-location').append('<tr><td>'+ value['parents'] +'</td><td>'+ value['island'] +'</td><td>'+ value['city'] +'</td><td>'+ value['zone'] +'</td><td> <a href="location/view?id='+ value['id_location'] +'"> <i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i></a><a href="location/update?id='+ value['id_location'] +'"> <i class="fas fa-pencil-alt dropdown-item-icon"></i></a><a href="location/delete?id='+ value['id_location'] +'"> <i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i></a></td></tr>');
            });
            }

          })
      });
    });
JS;
$this->registerJs($script);
?>


