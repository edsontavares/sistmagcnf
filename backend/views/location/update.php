<?php

use yii\helpers\Html;
use yii\helpers\Url;


$this->title = 'Atualização Localização';

?>
<div class="location-create">
    <div class="row mx-gutters-2">
        <div class="col-md-10">
            <h4 class="font-weight-bold"><?= Html::encode($this->title) ?></h4>
            </div>
            <div class="col-md-2">
                <a class="float-right " href="<?= Url::to(['/location'])?>">Voltar</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="card">
         <div class="card-body shadow-sm p-4">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
     </div>

</div>