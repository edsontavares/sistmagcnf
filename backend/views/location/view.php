<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
\yii\web\YiiAsset::register($this);
?>
<div class="location-view">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Localização</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
                <a class="" href="<?= Url::to(['/location'])?>">Voltar</a>
                <div class="btn-group position-relative ">

                    <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                        aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                        data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                        data-unfold-type="css-animation" data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                        <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                    </a>

                    <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker"> 
                        <?= Html::a('<i class="far fa-plus-square mr-2"></i>Registar', ['create'], ['class' => 'dropdown-item']) ?>
                        <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i>Editar', ['update', 'id' => $model->id_location], ['class'=>"dropdown-item",])?>
                        <a class="delete_location dropdown-item" href="#" data-url="<?= Url::to(['/'.'location-delete/'.$model->id_location])?>"
                            data-sim="<?= Yii::t('app', 'Sim') ?>"
                            data-nao="<?= Yii::t('app', 'Não') ?>"
                            data-title="<?= Yii::t('app', 'Desejas eliminar este localização?') ?>"
                            data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                            data-key="<?= $model->id_location?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
                        </a>
                    </div>
                </div>
            </div>
         </div>
    </div>
    <hr>
    <div class="card shadow-lg ">
        <div class="card-body shadow-sm p-4">
            <table style="width:100%">
                <h3 class=" font-weight-bold">Dados Pessoais</h3>
                <tbody>
                    <tr>
                        <td class="align-middle"><b>Pais<b></></td>
                        <td class="align-middle"><b>Ilha</b></td>
                        <td class="align-middle"><b>Cidade</b></td>
                        <td class="align-middle"><b>Zona</b></td>
                        <td class="align-middle"><b>Rua</b></td>
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->parents?></td>
                        <td class="text-secondary" ><?= $model->island?></td>
                        <td class="text-secondary" ><?= $model->city?></td>
                        <td class="text-secondary"><?= $model->zone?></td>
                        <td class="text-secondary"><?= $model->street?></td>
                    </tr> 
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php
  $script = <<<JS

    $(function () {
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
    })

    $(function() {
    
        $(document).on('click','.delete_location',function(e){
            e.preventDefault();
            var key = $(this).data('key');
            var _title = $(this).data('title');
            var url = $(this).data('url');
            var _message = $(this).data('message');
            var _sim = $(this).data('sim');
            var _nao = $(this).data('nao');
            bootbox.confirm({
                title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
                message:'<p class="p-2 px-5">' +_message +'</p>',
                buttons: {
                cancel: {
                    label: '<i class="fa fa-times"></i> ' + _nao,
                    className: 'bt btn-soft-secondary btn-xs'
                },
                confirm: {
                    label: '<i class="fa fa-check"></i> ' + _sim,
                    className: 'bt btn-indigo btn-xs'
                }
                },
                callback: function (result) {
                if(result){
                    $.ajax( {
                    method: "post",
                    url:url,
                    })
                    .done(function( respond ) {
                    data = JSON.parse(respond);
                    if (data.type === 'error'){
                        console.log('dont delete');
                    }
                    else{
                        window.open('location?r=location&action=delete','_self');
                    }
                    });
                }
                }
            });
        });
    });
JS;

$this->registerJs($script);
?>

