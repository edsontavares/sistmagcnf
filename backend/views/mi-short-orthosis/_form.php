<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Patient;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

?>



    <?php $form = ActiveForm::begin( [
      
        'options' => [
        'class' => "js-validate js-step-form",
        'data-progress-id' => "#progressStepForm",
        'data-steps-id' => "#contentStepForm",
        'novalidate' => "novalidate",
        'enctype' => 'multipart/form-data',
     ],

    ]); ?>






    <!-- Step Form -->
<!--form class="js-validate js-step-form"
      data-progress-id="#progressStepForm"
      data-steps-id="#contentStepForm"
      novalidate="novalidate"-->
 
    <!--div class="card-header p-5">
      <nav id="progressStepForm" class="js-step-progress nav nav-icon nav-justified text-center">
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-user-circle nav-icon-action-inner"></span>
          </span>
          Select One
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-file-invoice-dollar nav-icon-action-inner"></span>
          </span>
          Select Two
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-paperclip nav-icon-action-inner"></span>
          </span>
          Select Three
        </a>
      </nav>
    </div-->

    <!-- Content Step Form -->
    <div id="contentStepForm" class="card-body p-5">
      <div id="selectStepOne" class="active">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Dados pessoais<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
         <div class="row">
               <div class="col-sm-6">
                <label for="" class="type-label" >Nome de Paciente<span class="bg-active">*</span></label>
                <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Selecionar Paciente'],
                    'pluginOptions' => [
                    'allowClear' => true,
                    'class' => 'form-control  ',
                    'required' => true,
                    'data-msg' => 'Por favor Selecionar Paciente.',
                    'data-error-class' => 'u-has-error',
                    'data-success-class' => 'u-has-success',
                    ],
                ])->label(false) ?>
              
              </div>
              <div class="col-sm-6">
              <label for="" class="type-label" >Técnico Responsavel<span class="text-danger">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'nameEmployees')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\common\models\employees::find()->all(), 'name', 'name'),
                        // 'language' => 'es',
                        'options' => [

                        'required' => true,
                        'placeholder' => yii::t('app','Técnico Responsavel'),
                        'data-msg' => yii::t('app','Por favor Técnico Responsavel'),
                        ],
                        'pluginOptions' => [
                        'tags'=>true,
                        'class' => 'form-control  ',
                        
                        'allowClear' => true,
                        'minimumInputLength' => 2,
                        ],
                    ])->label(false) ?>
                </div>  
              </div>

                <div class="col-sm-4">
                        <label class="form-label mb-1">Lado de emparelhamento: </label>
                        <?php echo $form->field($model, 'pairing_side[]')->checkboxList(
                            [ '1' => 'Esquerda', '2' => 'Direita']
                        )->label(false) ;
                        ?>
                </div>
                
                <div class="col-sm-8">
                        <label class="form-label mb-1">Número de lados: </label>
                        <?php echo $form->field($model, 'side_number[]')->checkboxList(
                            [ '1' => 'Unilateral', '2' => 'Bilateral', '3' => 'Tamanho do Sapato', '4' => 'Altura do Calcanhar']
                        )->label(false) ;
                        ?>
                </div>

            <div class="col-sm-3">
                <label for="" class="type-label mt-5" >Comprimento 3 </label>
                <?= $form->field($model, 'length_m_one')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "Retangulo 3",
                  'aria-label' => "Retangulo 3",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Retangulo 3",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                 <label for="" class="type-label" >Comprimento 4 </label>
                <?= $form->field($model, 'length_m_two')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "Retangulo 4",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Retangulo 4",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                 <label for="" class="type-label" >Comprimento 5 </label>
                <?= $form->field($model, 'length_m_three')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "Retangulo 5",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Retangulo 5",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
            </div>
            <div  class="col-sm-6">
            <img src="../../recurso/img/perna.png" alt="user">
            </div>
            <div class="col-sm-3">
            <label for="" class="type-label mt-50" >Cercunferencia 1</label>
                <?= $form->field($model, 'cercunferencia_m_one')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "cercunferencia 1",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "cercunferencia 1",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                 <label for="" class="type-label " >Cercunferencia 2</label>
                <?= $form->field($model, 'cercunferencia_m_two')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "cercunferencia 2",
                  'aria-label' => "cercunferencia 2",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "cercunferencia 1",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                 <label for="" class="type-label " >Tamanho 6</label>
                <?= $form->field($model, 'side_m')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "side_m 6",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>

            </div>
        </div>
        <div class="d-flex justify-content-end">
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepTwo">Próximo</button>
        </div>
      </div>

      <div id="selectStepTwo" style="display: none;">
      <div class="border-bottom pb-0 mb-2">
            <div class="border-bottom pb-0 mb-2">
                <h4 class="h5 font-weight-normal">Dados pessoais 2<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
            </div>
            <div class="row mt-3">
                <div class="col-sm-3">
                    <img src="../../recurso/img/peOne.png" alt="user">
                </div>
                <div class="col-sm-2">
                    <label for="" class="type-label mt-5 " >Espaçamento ML 7</label>
                    <?= $form->field($model, 'space_n_ml_one')->textInput([
                    'maxlength' => true,
                    "class" => "form-control ",
                    'placeholder' => "Espaçamento",
                    'aria-label' => "Espaçamento",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Espaçamento",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    ]) ->label(false)?>

                    <label for="" class="type-label mt-50" >Espaçamento ML 8</label>
                    <?= $form->field($model, 'space_n_ml_two')->textInput([
                    'maxlength' => true,
                    "class" => "form-control ",
                    'placeholder' => "Espaçamento",
                    'aria-label' => "Espaçamento",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Espaçamento",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    ]) ->label(false)?>

                </div>
                <div class="col-sm-2">
                <label for="" class="type-label " >Cercunferência 9</label>
                <?= $form->field($model, 'cercunferencia_c_one')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "side_m 6",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                  <label for="" class="type-label " >Espaçamento Ap 10</label>
                <?= $form->field($model, 'space_c_ap')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "side_m 6",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                  <label for="" class="type-label " >Cercunferência 11</label>
                <?= $form->field($model, 'cercunferencia_c_two')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                  <label for="" class="type-label " >Cercunferência 12</label>
                <?= $form->field($model, 'cercunferencia_c_three')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>

                </div>
                <div class="col-sm-3">
                    <img src="../../recurso/img/peTwo.png" alt="user">
                </div>
                <div class="col-sm-2">
                <label for="" class="type-label mt-70" >Comprimento 13</label>
                <?= $form->field($model, 'length_c_one')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>
                  <label for="" class="type-label mt-2" >Comprimento 14</label>
                <?= $form->field($model, 'length_c_two')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "side_m 6",
                  'aria-label' => "side_m 6",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "side_m 6",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                ]) ->label(false)?>

                </div>
            </div>

        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary transition-3d-hover mr-1 rounded" href="javascript:;" data-previous-step="#selectStepOne">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepThree">Próximo</button>
        </div>
      </div>

     
    </div>
    <div id="selectStepThree" style="display: none;">
            <div class="border-bottom pb-0 mb-2">
             <h4 class="h5 font-weight-normal">Dados pessoais3<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
            </div>
            <div class="row mt-3">
              <div class="col-sm-2">
                <label for="" class="type-label" >Comprimento 17</label>
                <?= $form->field($model, 'length_p_one')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
                <label for="" class="type-label" >Comprimento 18</label>
                <?= $form->field($model, 'length_p_two')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                <img src="../../recurso/img/tenis2.png" alt="user" class="img-pe">
                <label for="" class="type-label" >Tamanho 19</label>
                <?= $form->field($model, 'side_p')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>
              <div class="col-sm-2">
                <label for="" class="type-label" >Comprimento 15</label>
                <?= $form->field($model, 'length_p_three')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
                <label for="" class="type-label" >Comprimento 16</label>
                <?= $form->field($model, 'length_p_for')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>

              <div class="col-sm-3">
                <img src="../../recurso/img/tenis.png" alt="user">
                <label for="" class="type-label" >Calcanhar 20</label>
                <?= $form->field($model, 'heel_l')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>
              
            
            </div>
            <hr>
            <div class="row  mt-5">
              <div class="col-sm-2">
                <label for="" class="type-label" >Compensação</label>
                <?= $form->field($model, 'compensation_d')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
                <label for="" class="type-label" >Calcanhada</label>
                <?= $form->field($model, 'heel')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>
              <div class="col-sm-3">
                <img src="../../recurso/img/logoap.png" alt="user" class="logoap">
              </div>
              <div class="col-sm-2">
                <label for="" class="type-label" >Comprimento</label>
                <?= $form->field($model, 'length_d')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
              </div>
            </div>

        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary transition-3d-hover mr-1 rounded" href="javascript:;" data-previous-step="#selectStepTwo">Voltar</a>
          <?= Html::submitButton('Gurdar', ['class' => 'btn btn-sm btn-indigo rounded']) ?>
        </div>
      </div>
    <!-- End Content Step Form -->
  </div>
<!--/form-->
<!-- End Step Form -->
    <?php ActiveForm::end(); ?>
    <style>
        .mt-50{
            margin-top: 85px;
        }
        .mt-70{
            margin-top: 170px;
        }
        .img-pe{
          width: 319px;
        }
.logoap{
  width: 319px;
}
        .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
    </style>


