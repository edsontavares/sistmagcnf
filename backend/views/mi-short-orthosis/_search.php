<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MiShortOrthosisSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mi-short-orthosis-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_mi_short_orthosis') ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'length_m_one') ?>

    <?= $form->field($model, 'length_m_two') ?>

    <?php // echo $form->field($model, 'length_m_three') ?>

    <?php // echo $form->field($model, 'side_m') ?>

    <?php // echo $form->field($model, 'cercunferencia_m_one') ?>

    <?php // echo $form->field($model, 'pairing_side') ?>

    <?php // echo $form->field($model, 'left_o') ?>

    <?php // echo $form->field($model, 'right_o') ?>

    <?php // echo $form->field($model, 'side_number') ?>

    <?php // echo $form->field($model, 'one_sided') ?>

    <?php // echo $form->field($model, 'bilateral') ?>

    <?php // echo $form->field($model, 'shoe_size') ?>

    <?php // echo $form->field($model, 'heel_height') ?>

    <?php // echo $form->field($model, 'mold_manuf') ?>

    <?php // echo $form->field($model, 'space_n_ml_one') ?>

    <?php // echo $form->field($model, 'space_n_ml_two') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_one') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_two') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_three') ?>

    <?php // echo $form->field($model, 'length_c_one') ?>

    <?php // echo $form->field($model, 'length_c_two') ?>

    <?php // echo $form->field($model, 'space_c_ap') ?>

    <?php // echo $form->field($model, 'heel_l') ?>

    <?php // echo $form->field($model, 'length_p_one') ?>

    <?php // echo $form->field($model, 'length_p_two') ?>

    <?php // echo $form->field($model, 'length_p_three') ?>

    <?php // echo $form->field($model, 'length_p_for') ?>

    <?php // echo $form->field($model, 'side_p') ?>

    <?php // echo $form->field($model, 'length_d') ?>

    <?php // echo $form->field($model, 'compensation_d') ?>

    <?php // echo $form->field($model, 'heel') ?>

    <?php // echo $form->field($model, 'id_employees') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
