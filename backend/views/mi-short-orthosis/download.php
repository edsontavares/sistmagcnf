<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style=" margin-top: -0.5cm;" >    
    <div style="float:left; width:20%;">
        <img src="./../recurso/img/Logo.svg"  width="70" height="65" style="margin-top: -0.5cm;" >
    </div>
    <div style="float:left; width:80%;">
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    </div>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->
    <div class="card shadow-lg ">
        <div class="card-body shadow-sm p-4">
          
            <div class="row">
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label mt-5 font-weight-bold mt-10" >Comprimento Nº 3 </label>
                    <p><?= $model->length_m_one?></p>
                
                    <label for="" class="type-label font-weight-bold" >Comprimento Nº 4 </label>
                    <p><?= $model->length_m_two?></p>
                
                
                    <label for="" class="type-label font-weight-bold" >Comprimento Nº 5 </label>
                    <p><?= $model->length_m_three?></p>
                    
                </div>
                <div  class="" style="float:left; width:60%;" >
                <img src="./../recurso/img/perna.png" alt="user">
                </div>
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label mt-9 font-weight-bold" >Cercunferencia Nº 1</label>
                    <p><?= $model->cercunferencia_m_one?></p>
                    
                    <label for="" class="type-label  font-weight-bold" >Cercunferencia Nº 2</label>
                    <p><?= $model->cercunferencia_m_two?></p>
                
                    <label for="" class="type-label  font-weight-bold  mt-10" >Tamanho Nº 6</label>
                    <p><?= $model->side_m?></p>
                    
                </div>
            </div>
            <hr>
            <div class="row mt-5">
                <div class="" style="float:left; width:60%;" >
                    <img src="./../recurso/img/peOne.png" alt="user">
                </div>
                <div class="" style="float:left; width:40%;" >
                    <label for="" class="type-label mt-10 font-weight-bold" >Espaçamento ML 7</label>
                    <p><?= $model->space_n_ml_one?></p>
                    

                    <label for="" class="type-label mt-10 font-weight-bold" >Espaçamento ML 8</label>
                    <p><?= $model->space_n_ml_two?></p>
                </div>
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label font-weight-bold" >Cercunferência 9</label>
                    <p><?= $model->cercunferencia_c_one?></p>
                    
                    <label for="" class="type-label font-weight-bold" >Espaçamento Ap 10</label>
                    <p><?= $model->space_c_ap?></p>
                
                    <label for="" class="type-label font-weight-bold" >Cercunferência 11</label>
                    <p><?= $model->cercunferencia_c_two?></p>
                    
                    <label for="" class="type-label font-weight-bold" >Cercunferência 12</label>
                    <p><?= $model->cercunferencia_c_three?></p>
                    
                </div>
                <div class="" style="float:left; width:60%;" >
                    <img src="./../recurso/img/peTwo.png" alt="user">
                </div>
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label mt-30 font-weight-bold" >Comprimento 13</label>
                    <p><?= $model->length_c_one?></p>
            
                    <label for="" class="type-label mt-5 font-weight-bold" >Comprimento 14</label>
                    <p><?= $model->length_c_two?></p>
                
                </div>
            </div>
            <hr>
            <div class="row mt-5">
                <div class="" style="float:left; width:20%;" > 
                    <label for="" class="type-label font-weight-bold" >Comprimento17</label>
                    <p><?= $model->length_p_one?></p>
                
                    <label for="" class="type-label font-weight-bold" >Comprimento18</label>
                    <p><?= $model->length_p_two?></p>
                
                </div>
                <div class="" style="float:left; width:60%;" >
                    <img src="./../recurso/img/tenis2.png" alt="user" class="img-pe">
                    <label for="" class="type-label font-weight-bold" >Tamanho19</label>
                    <p><?= $model->side_p?></p>
                    
                </div>
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label font-weight-bold " >Comprimento15</label>
                    <p><?= $model->length_p_three?></p>
                
                    <label for="" class="type-label font-weight-bold" >Comprimento 6</label>
                    <p><?= $model->length_p_for?></p>
                    
                </div>
                <hr>

                <div class="col-sm-3">
                    <img src="./../recurso/img/tenis.png" alt="user">
                    <label for="" class="type-label font-weight-bold" >Calcanhar20</label>
                    <p><?= $model->heel_l?></p>
                    
                
                </div>
                
            </div>
            <hr>
            <div class="row  mt-5">
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label font-weight-bold mt-10" >Compensação</label>
                    <p><?= $model->compensation_d?></p>
                    <label for="" class="type-label font-weight-bold" >Calcanhada</label>
                    <p><?= $model->heel?></p>
                
                </div>
                <div class="" style="float:left; width:60%;" >
                    <img src="./../recurso/img/logoap.png" alt="user" class="logoap">
                </div>
                <div class="" style="float:left; width:20%;" >
                    <label for="" class="type-label font-weight-bold   mt-10">Comprimento</label>
                    <p><?= $model->length_d?></p>
                    
                </div>
            </div>
        </div>    
            
    </div>

</div>



<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
    .logoap{
  width: 319px;
}
.mt-14{
   
    margin-top: 9.5rem !important;

}
.mt-30{
    margin-top: 9.5rem !important;
}

.mt-50{
    margin-top: 85px;
}
.mt-70{
    margin-top: 170px;
}
.img-pe{
    width: 319px;
}
</style>
