<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

?>
<div class="employees-index">


    
    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('Adicionar Ortótese curta do mi', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>

         
        
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search.." id="myInput" onkeyup="myFunction()">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>


    <table class="table" id="myTable">
  <thead class="top">
    <tr>
     
      <th scope="col">Nome</th>
      <th scope="col">Data de Registo</th>
      <th scope="col">Técnico Responsavel</th>
      <th scope="col">Estado</th>
      <th scope="col">opcao</th>

    </tr>
  </thead>
  <tbody>
  <?php  foreach ($miShortOrthosis as $miShortOrthois) {
    $query = \common\models\Query::findOne($miShortOrthois->id_query);
    $patient = \common\models\Patient::findOne($query->id_patient); 
    
  ?>
     
    <tr>
      <td><?= $patient->name?></td>
      <td><?= $miShortOrthois->create_at?></td>
      <td><?=$miShortOrthois->nameEmployees?></td>
      <td>
        <?php if($miShortOrthois->state== 1):?>
          <span class="badge badge-success">Ativo</span>
          <?php elseif($miShortOrthois->state == -1):?>
            <span class="badge badge-danger">Desctivado</span>
            
          <?php endif;?>
        </td>
      <td> 
          <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i>', ['view', 'id' => $miShortOrthois->id_mi_short_orthosis], ['class' => '','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>
          <?= Html::a('<i class="fas fa-pencil-alt dropdown-item-icon"></i> ', ['update', 'id' => $miShortOrthois->id_mi_short_orthosis], ['class'=>"",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>
          <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id' => $miShortOrthois->id_mi_short_orthosis], [
              'class' => '',
              'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
              'data' =>
                  [
                      'confirm' => 'Are you sure you want to delete this item?',
                      'method' => 'post',
                  ],
          ]) ?>
    </td>
      </tr>
    <?php }?>  
  </tbody>
</table>
<?= $this->render('../../../common/componentes/pagination', [
    'total' => @$total,
    'item_numbers' => $item_numbers,
    'prettyUrl' => 'active',
    'url' => Url::to(['/miShortOrthosis/index'])
]) ?>


</div>
<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
   
    
</style>

