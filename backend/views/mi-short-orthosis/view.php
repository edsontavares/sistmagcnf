<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

\yii\web\YiiAsset::register($this);

?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-sm-12">
            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= Yii::$app->session->getFlash("success-create") ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação Ortótese curta do mi</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/miShortOrthosis'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i>Descaregar', ['baixar', 'id'=>$model->id_mi_short_orthosis], ['class' => 'dropdown-item transition-3d-hover' ,'target'=>"_blank"]) ?>
                     <?php if($model->state == 1): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_mi_short_orthosis], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>   
                </div>
                </div>
            </div>
         </div>
        
    </div>
   
        <hr>

        <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
               <div class="row">
                    <div class="col-sm-6">
                    <label for="" class="type-label font-weight-bold" ><b>Nome de Paciente</b></label>
                        <p>
                           <?= $model->patients->name?>
                        </p>
                       
                    </div>
                    <div class="col-sm-6">
                        <label for="" class="type-label font-weight-bold" ><b>Técnico Responsavel</b></label>

                        <p>
                           <?= $model->nameEmployees?>
                        </p>
                    </div>
                    <?php if($model->pairing_side):?>
                        <div class="col-sm-6">
                            <label for="" class="type-label font-weight-bold" ><b>Lado de emparelhamento:</b></label>
                                <p>
                                    <?php if(strpos($model->pairing_side, '1')):?>
                                        <span>Esquerda</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->pairing_side, '2')):?>
                                        <span>Direita</span>
                                    <?php endif; ?>
                                </p> 
                        </div>
                    <?php endif;?>
                    <?php if($model->side_number):?>
                        <div class="col-sm-6">
                            <label for="" class="type-label font-weight-bold" ><b>Número de lados:</b></label>
                                <p>
                                    <?php if(strpos($model->side_number, '1')):?>
                                        <span>Unilateral</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->side_number, '2')):?>
                                        <span>Bilateral</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->side_number, '3')):?>
                                        <span>Tamanho do Sapato</span>
                                    <?php endif; ?>
                                    <?php if(strpos($model->side_number, '4')):?>
                                        <span>Altura do Calcanhar</span>
                                    <?php endif; ?>
                                </p> 
                        </div>
                    <?php endif;?>
                   
                    <div class="col-sm-6">
                        <p>
                            <?= $model->create_at?>
                        </p>
                    </div>
                   
                    
               </div>
                <hr>

                <div class="row">
                   
                    <div class="col-sm-3 pt-10">
                        <label for="" class="type-label mt-5 font-weight-bold mt-10" >Comprimento Nº 3 </label>
                        <p><?= $model->length_m_one?></p>
                    
                        <label for="" class="type-label font-weight-bold" >Comprimento Nº 4 </label>
                        <p><?= $model->length_m_two?></p>
                    
                    
                        <label for="" class="type-label font-weight-bold" >Comprimento Nº 5 </label>
                        <p><?= $model->length_m_three?></p>
                        
                    </div>
                    <div  class="col-sm-6">
                    <img src="../../recurso/img/perna.png" alt="user">
                    </div>
                    <div class="col-sm-3 pt-10">
                        <label for="" class="type-label mt-9 font-weight-bold" >Cercunferencia Nº 1</label>
                        <p><?= $model->cercunferencia_m_one?></p>
                        
                        <label for="" class="type-label  font-weight-bold" >Cercunferencia Nº 2</label>
                        <p><?= $model->cercunferencia_m_two?></p>
                    
                        <label for="" class="type-label  font-weight-bold  mt-10" >Tamanho Nº 6</label>
                        <p><?= $model->side_m?></p>
                        
                    </div>
                </div>
                <hr>
                <div class="row mt-5">
                    <div class="col-sm-3">
                        <img src="../../recurso/img/peOne.png" alt="user">
                    </div>
                    <div class="col-sm-2">
                        <label for="" class="type-label mt-10 font-weight-bold" >Espaçamento ML 7</label>
                        <p><?= $model->space_n_ml_one?></p>
                        

                        <label for="" class="type-label mt-10 font-weight-bold" >Espaçamento ML 8</label>
                        <p><?= $model->space_n_ml_two?></p>
                    </div>
                    <div class="col-sm-2">
                        <label for="" class="type-label font-weight-bold" >Cercunferência 9</label>
                        <p><?= $model->cercunferencia_c_one?></p>
                        
                        <label for="" class="type-label font-weight-bold" >Espaçamento Ap 10</label>
                        <p><?= $model->space_c_ap?></p>
                    
                        <label for="" class="type-label font-weight-bold" >Cercunferência 11</label>
                        <p><?= $model->cercunferencia_c_two?></p>
                        
                        <label for="" class="type-label font-weight-bold" >Cercunferência 12</label>
                        <p><?= $model->cercunferencia_c_three?></p>
                        
                    </div>
                    <div class="col-sm-3">
                        <img src="../../recurso/img/peTwo.png" alt="user">
                    </div>
                    <div class="col-sm-2 ">
                        <label for="" class="type-label mt-30 font-weight-bold" >Comprimento 13</label>
                        <p><?= $model->length_c_one?></p>
                
                        <label for="" class="type-label mt-5 font-weight-bold" >Comprimento 14</label>
                        <p><?= $model->length_c_two?></p>
                    
                    </div>
                </div>
                <hr>
                <div class="row mt-5">
                    <div class="col-sm-2"> 
                        <label for="" class="type-label font-weight-bold" >Comprimento17</label>
                        <p><?= $model->length_p_one?></p>
                    
                        <label for="" class="type-label font-weight-bold" >Comprimento18</label>
                        <p><?= $model->length_p_two?></p>
                    
                    </div>
                    <div class="col-sm-4">
                        <img src="../../recurso/img/tenis2.png" alt="user" class="img-pe">
                        <label for="" class="type-label font-weight-bold" >Tamanho19</label>
                        <p><?= $model->side_p?></p>
                        
                    </div>
                    <div class="col-sm-2">
                        <label for="" class="type-label font-weight-bold " >Comprimento15</label>
                        <p><?= $model->length_p_three?></p>
                    
                        <label for="" class="type-label font-weight-bold" >Comprimento 6</label>
                        <p><?= $model->length_p_for?></p>
                        
                    </div>

                    <div class="col-sm-3">
                        <img src="../../recurso/img/tenis.png" alt="user">
                        <label for="" class="type-label font-weight-bold" >Calcanhar20</label>
                        <p><?= $model->heel_l?></p>
                     
                    
                    </div>
                    
                </div>
                <hr>
                <div class="row  mt-5">
                    <div class="col-sm-2">
                        <label for="" class="type-label font-weight-bold mt-10" >Compensação</label>
                        <p><?= $model->compensation_d?></p>
                        <label for="" class="type-label font-weight-bold" >Calcanhada</label>
                        <p><?= $model->heel?></p>
                    
                    </div>
                    <div class="col-sm-3">
                        <img src="../../recurso/img/logoap.png" alt="user" class="logoap">
                    </div>
                    <div class="col-sm-2">
                        <label for="" class="type-label font-weight-bold   mt-10">Comprimento</label>
                        <p><?= $model->length_d?></p>
                        
                    </div>
                </div>
            </div>    
        </div>



        
    </div>
</div>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding:
   8px;
}
.logoap{
  width: 319px;
}
.mt-14{
   
    margin-top: 9.5rem !important;

}
.mt-30{
    margin-top: 9.5rem !important;
}

.mt-50{
    margin-top: 85px;
}
.mt-70{
    margin-top: 170px;
}
.img-pe{
    width: 319px;
}
</style>
