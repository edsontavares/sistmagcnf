<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Patient;
use common\models\Query;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use yii\helpers\Url;
use yii\web\JsExpression;
   $patient = Patient::find()->one();
   
?>

<div class="minerva-form">
      
    <?php $form = ActiveForm::begin(); ?>

    <div class="row mx-gutters-2">
        <div class="col-sm-6">
            <label for="" class="type-label" >Nome de Paciente<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\patient::find()->all(), 'name', 'name'),
                    // 'language' => 'es',
                    'options' => [

                    'required' => true,
                    'placeholder' => yii::t('app','Adicionar nome de paciente'),
                    'data-msg' => yii::t('app','Por favor adicionar nome de paciente'),
                    ],
                    'pluginOptions' => [
                    'tags'=>true,
                    'class' => 'form-control  ',
                    
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    ],
                ])->label(false) ?>
            </div>        
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Técnico Responsavel<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'nameEmployees')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\employees::find()->all(), 'name', 'name'),
                    // 'language' => 'es',
                    'options' => [

                    'required' => true,
                    'placeholder' => yii::t('app','Técnico Responsavel'),
                    'data-msg' => yii::t('app','Por favor Técnico Responsavel'),
                    ],
                    'pluginOptions' => [
                    'tags'=>true,
                    'class' => 'form-control  ',
                    
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    ],
                ])->label(false) ?>
            </div>        
        </div>

        <div class="col-sm-12">
        <label for="" class="type-label" >Aparelho<span class="text-danger">*</span></label>
        <div class="form-group">
            <?=  $form->field($model, 'id_query')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Query::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_query', 'appliance'),
                'language' => 'en',
                'options' => ['placeholder' => 'Selecionar Paciente'],
                'pluginOptions' => [
                'allowClear' => true,
                'class' => 'form-control  ',
                'required' => true,
                'data-msg' => 'Por favor Selecionar Paciente.',
                'data-error-class' => 'u-has-error',
                'data-success-class' => 'u-has-success',
                ],
            ])->label(false) ?>
        </div>
        </div>
       
        <div  class="col-sm-3">
            <label for="" class="type-label mt-5" >Cercunferência Nº 1</label>
            <?= $form->field($model, 'cercunferencia_m_one')->textInput([
                'maxlength' => true,
                "class" => "form-control ",
                'placeholder' => "",
                'aria-label' => "",
                'required' => false,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => " ",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                'type' => 'number'
            ]) ->label(false)?>
        </div>
        <div  class="col-sm-6">
            <img src="../../recurso/img/minirava.jpg" alt="user">
        </div>
        <div  class="col-sm-3">
        
            <label for="" class="type-label" >Cercunferência Nº 2</label>
            <?= $form->field($model, 'cercunferencia_m_two')->textInput([
                'maxlength' => true,
                "class" => "form-control",
                'placeholder' => "",
                'aria-label' => "",
                'required' => false,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => " ",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                'type' => 'number'
            ]) ->label(false)?>

            <label for="" class="type-label mt-4" >Cercunferência Nº 3</label>
            <?= $form->field($model, 'cercunferencia_m_three')->textInput([
                'maxlength' => true,
                "class" => "form-control",
                'placeholder' => "",
                'aria-label' => "",
                'required' => false,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => " ",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                'type' => 'number'
            ])->label(false) ?>

            <label for="" class="type-label ml-2" >Cercunferência Nº 4</label>
            <?= $form->field($model, 'cercunferencia_c_one')->textInput([
                'maxlength' => true,
                "class" => "form-control ml-2",
                'placeholder' => "",
                'aria-label' => "",
                'required' => false,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => " ",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                'type' => 'number'
            ])->label(false) ?>

        </div>
      <div class="col-sm-12">
        <label for="" class="type-label" >Observação</label>
        <?= $form->field($model, 'observation')->textarea(['placeholder' => "Observação", 'class' => 'form-control','rows'=>3])->label(false) ?>
             
        <!-- Text Editor Input -->
        <!--?=  $form->field($model, 'description')->widget(CKEditor::className(), [
            'options' => ['rows' => 7],
            'preset' => 'advanced'
        ])  ?-->
        <!-- End Text Editor Input -->
      </div>
    </div>

<div class="row">
    <div class="col-sm-12 text-right">
        <div class="form-group">
            <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
            
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>

        </div>
    </div>
</div>
  

    <?php ActiveForm::end(); ?>

</div>
<style>
          .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
</style>
<script type="text/javascript">
    $(function () {
        $('#jobs-description').summernote({
            dialogsInBody: true,
            height: 300,
            placeholder: $('#jobs-description').attr('placeholder'),
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                // ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link']],
            ],
        });
    });
</script>