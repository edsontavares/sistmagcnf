<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MinervaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="minerva-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_minerva') ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'cercunferencia_m_one') ?>

    <?= $form->field($model, 'cercunferencia_m_two') ?>

    <?php // echo $form->field($model, 'cercunferencia_m_three') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_one') ?>

    <?php // echo $form->field($model, 'observation') ?>

    <?php // echo $form->field($model, 'id_employees') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'side') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
