<?php
    use \frontend\models\PerfirenciaLanguagen;
    use frontend\models\Competencias;
    use frontend\models\Defaults;
    $query= \common\models\Query::findOne($model->id_query);
    $patient = \common\models\Patient::findOne($query->id_patient); 
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style=" margin-top: -0.5cm;" >    
    <div style="float:left; width:20%;">
        <img src="./../recurso/img/Logo.svg"  width="70" height="65" style="margin-top: -0.5cm;" >
    </div>
    <div style="float:left; width:80%;">
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    </div>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->

          
    <div class="row mx-gutters-2" style="margin-top: 0.5cm;">
        <div class="col-sm-12"> 
            <table style="width:100%">
                <tbody>
                    <tr>
                        <td class="font-weight-bold font-weight-bold">Nome de Paciente</td>
                        <td class="font-weight-bold font-weight-bold">Técnico Responsavel</td>
                        <td class="font-weight-bold font-weight-bold">Aparelho</td>
                        
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $patient->name?></td>
                        <td class="text-secondary" ><?= $model->nameEmployees?></td>
                        <td class="text-secondary"><?= $model->querys->appliance?></td>
                        
                    </tr>
                    
                </tbody>
            </table>
        </div>
    </div>
    <hr>
    <div class="row mx-gutters-2" style="margin-top: 0.5cm;">
        <div  class="" style="float:left; width:20%;" >
            <label for="" class="type-label mt-14 font-weight-bold"  style="margin-top: 5cm;" >Cercunferência Nº 1</label>
            <p><?= $model->cercunferencia_m_one?></p>
        </div>
        <div  class="" style="float:left; width:60%;">
            <img src="./../recurso/img/minirava.jpg" alt="user">
        </div>
        <div  class="" style="float:left; width:20%;">
        
            <label for="" class="type-label font-weight-bold" >Cercunferência Nº 2</label>
            <p><?= $model->cercunferencia_m_two?></p>
            
            <label for="" class="type-label mt-10 font-weight-bold" >Cercunferência Nº 3</label>
            <p><?= $model->cercunferencia_m_three?></p>
        
            <label for="" class="type-label mt-4 font-weight-bold" >Cercunferência Nº 4</label>
            <p class=" ml-2 "><?= $model->cercunferencia_c_one?></p>
        </div>
         <hr>
         <div class="col-sm-12">
            <?php if($model->nameEmployees):?>
                <h5 class="mt-4 font-weight-bold font-weight-bold">Técnico Responsável</h5>
                <p><?= $model->nameEmployees?></p>
            <?php endif;?>
        </div>
        <div class="col-sm-12">
            <?php if($model->observation):?>
                <h5 class="mt-4 font-weight-bold font-weight-bold">Observação</h5>
                <p><?= $model->observation?></p>
            <?php endif;?>
        </div>
    </div>
</div>
<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
</style>
