<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
$query= \common\models\Query::findOne($model->id_query);
$patient = \common\models\Patient::findOne($query->id_patient); 

\yii\web\YiiAsset::register($this);

?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-sm-12">
            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= Yii::$app->session->getFlash("success-create") ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Minerava</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/minerva'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i>Descaregar', ['baixar', 'id'=>$model->id_minerva], ['class' => 'dropdown-item transition-3d-hover' ,'target'=>"_blank"]) ?>   
                    <?php if($model->state == 1): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_minerva], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>
                </div>
                </div>
            </div>
         </div>
        
    </div>
   
    <hr>

    <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
            <div class="row mx-gutters-2">
                <div class="col-sm-4">
                    <label for="" class="type-label font-weight-bold" ><b>Nome de Paciente</b></label>
                    <p><?= $patient->name?></p>
                </div>
                <div class="col-sm-4">
                    <label for="" class="type-label font-weight-bold" ><b>Técnico Responsavel</b></label>
                    <p><?= $model->nameEmployees?></p>
                </div>
                <div class="col-sm-4">
                    <label for="" class="type-label font-weight-bold" ><b>Aparelho</b></label>
                    <p><?= $model->querys->appliance?></p>
                </div>
            </div>
            <hr style="color: blue; ">
            <div class="row mx-gutters-2">
                <div  class="col-sm-3 mt-4">
                    <label for="" class="type-label mt-14 font-weight-bold" >Cercunferência Nº 1</label>
                    <p><?= $model->cercunferencia_m_one?></p>
                </div>
                <div  class="col-sm-6">
                    <img src="../../recurso/img/minirava.jpg" alt="user">
                </div>
                <div  class="col-sm-3">
                
                    <label for="" class="type-label font-weight-bold" >Cercunferência Nº 2</label>
                    <p><?= $model->cercunferencia_m_two?></p>
                    
                    <label for="" class="type-label mt-10 font-weight-bold" >Cercunferência Nº 3</label>
                    <p><?= $model->cercunferencia_m_three?></p>
                
                    <label for="" class="type-label mt-4 font-weight-bold" >Cercunferência Nº 4</label>
                    <p class=" ml-2 "><?= $model->cercunferencia_c_one?></p>
                </div>
                <hr>
            <div class="col-sm-12">
               <?php if($model->observation):?>
                    <h5 class="mt-4 font-weight-bold font-weight-bold">Observação</h5>
                    <p><?= $model->observation?></p>
                <?php endif;?>
              
            </div>
            </div>
               
               
            </div>
        </div>
</div>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding:
   8px;
}

.mt-14{
   
    margin-top: 9.5rem !important;

}
</style>
