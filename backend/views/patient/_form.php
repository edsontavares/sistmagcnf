<?php

use common\models\Patient;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\Location;
$language = [];
?>
<?php $form = \yii\widgets\ActiveForm::begin([

    'options' => [
        'class' => "js-validate js-step-form",
        'data-progress-id' => "#progressStepForm",
        'data-steps-id' => "#contentStepForm",
        'novalidate' => "novalidate",
        'enctype' => 'multipart/form-data',
    ],
]) ?>

<!-- Step Form -->



  <!--div class="card"-->
    <!--div class="card-header p-5">
      <nav id="progressStepForm" class="js-step-progress nav nav-icon nav-justified text-center">
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-user-circle nav-icon-action-inner"></span>
          </span>
          Select One
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-file-invoice-dollar nav-icon-action-inner"></span>
          </span>
          Select Two
        </a>
        <a href="javascript:;" class="nav-item">
          <span class="nav-icon-action">
            <span class="fas fa-paperclip nav-icon-action-inner"></span>
          </span>
          Select Three
        </a>
      </nav>
    </div--->

    <!-- Content Step Form -->
    <div id="contentStepForm" class="card-body p-5">
      <div id="selectStepOne" class="active">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Dados pessoais<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
         <div class="row">
           <div class="col-sm-6">
              <label for="" class="type-label" >Nome completo <span class="text-danger">*</span></label>
               <?= $form->field($model, 'name')->textInput([
                  "class" => "form-control ",
                  'placeholder' => "Introduzir nome de paciente",
                  'aria-label' => "Introduzir nome de paciente",
                  'required' => true,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Introduzir nome de paciente",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                 ]) ->label(false)?>
           </div>
           <div class="col-sm-3">
                <label for="" class="type-label" >Alcunha </label>
                <?= $form->field($model, 'nickname')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "Introduzir alcunha",
                  'aria-label' => "Introduzir alcunha",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Introduzir alcunha",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                  ]) ->label(false)?>
           </div>
           <div class="col-sm-3">
               <label for="" class="type-label" >Morada</label>
                  <div class="form-group">
                    <?=  $form->field($model, 'id_location')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Location::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_location', 'zone'),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Selecionar morada'],
                            'pluginOptions' => [
                            'allowClear' => true,
                            'class' => 'form-control form-control-sm ',
                            'required' => false,
                            'data-msg' => 'Por favor Selecionar Serviço.',
                            'data-error-class' => 'u-has-error',
                            'data-success-class' => 'u-has-success',
                            ],
                    ])->label(false) ?>
                </div>
              </div>
           <div class="col-sm-4">
                <label for="" class="type-label" >Data nascimento<span class="text-danger">*</span></label>
                <?= $form->field($model, 'birth_date')->textInput([
                      "class" => "form-control input_date",
                      'placeholder' => "Data nascimento",
                      'aria-describedby' => "dataInicioLabel",
                      'data-error-class' => "u-has-error",
                      'data-success-class' => "u-has-success",
                      'data-rp-date-format' => "Y-m-d",
                      'type' => 'date'
                ])->label(false) ?>
           </div>
           <div class="col-sm-4">
                <label for="" class="type-label" >Nacionalidade<span class="text-danger">*</span></label>
                <?= $form->field($model, 'nationality')->textInput([
                  'maxlength' => true,
                  "class" => "form-control",
                  'placeholder' => "Introduzir nacionalidade",
                  'aria-label' => "Introduzir nacionalidade",
                  'required' => true,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Introduzir nacionalidade",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                  ]) ->label(false)?>

                  
           </div>
           <div class="col-sm-4">
                <label for="" class="type-label" >Naturalidade</label>
                <?= $form->field($model, 'naturalness')->textInput([
                  'maxlength' => true,
                  "class" => "form-control ",
                  'placeholder' => "Introduzir naturalidade",
                  'aria-label' => "Introduzir naturalidade",
                  'required' => false,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Introduzir naturalidade",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
                  ]) ->label(false)?>
           </div>
           <div class="col-sm-4">
              <label for="" class="type-label" >Telefone</label>
               <?= $form->field($model, 'telephone')->textInput([
                   'maxlength' => true,
                   "class" => "form-control",
                    'placeholder' => "Introduzir telefone",
                    'aria-label' => "Introduzir telefone",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir telefone",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                 ]) ->label(false)?>
           </div>
           <div class="col-sm-4">
               <label for="" class="type-label" >Telemovel</label>
               <?= $form->field($model, 'mobile_phone')->textInput([
                   'maxlength' => true,
                   "class" => "form-control",
                    'placeholder' => "Introduzir telemovel",
                    'aria-label' => "Introduzir telemovel",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir telemovel",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                 ]) ->label(false)?>
           </div>
           <div class="col-sm-4">
               <label for="" class="type-label" >E-mail</label>
               <?= $form->field($model, 'email')->textInput([
                   'maxlength' => true,
                   "class" => "form-control",
                    'placeholder' => "exemplo@gmail.com",
                    'aria-label' => "Introduzir e-mail",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir e-mail",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    //'type' => 'email'
                 ]) ->label(false)?>
           </div>
           <div class="col-sm-4">
              <label for="" class="type-label" >Cni/Bi<span class="text-danger">*</span></label>
               <?= $form->field($model, 'cni')->textInput([
                   'maxlength' => true,
                   "class" => "form-control",
                    'placeholder' => "Introduzir cni/bi",
                    'aria-label' => "Introduzir cni/bi",
                    'required' => true,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir cni/bi",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                 ]) ->label(false)?>
           </div>
           <div class="col-sm-4">
               <label for="" class="type-label" >Sexo<span class="text-danger">*</span></label>
           
               <?php
                  $sexo = ['M' => 'Masculino', 'F' => 'Feminino'];
                  echo $form->field($model, 'sex')->dropDownList(
                      $sexo,
                      ['prompt' => 'Selecionar o sexo', "class" => "form-control ",
                      'required' => true,
                      ]
                  )->label(false);
                ?>
           </div>
           <div class="col-sm-4">
               <label for="" class="type-label" >Nivel de escolaridade</label>
                <?= $form->field($model, 'educational_level')->textInput([
                'maxlength' => true,
                "class" => "form-control",
                'placeholder' => "Introduzir nivel de escolaridade",
                'aria-label' => "Introduzir Nivel de Escolaridade",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir Nivel de Escolaridade",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                ]) ->label(false)?>

           </div>
         </div>
        
          <div class="d-flex justify-content-end mt-5">
              <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepTwo">Próximo</button>
              </div>
          </div>
      </div>

      <div id="selectStepTwo" style="display: none;">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Situação Sócio-Familiar<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
            <div class="row">
              <div class="col-sm-8">
                  <label for="" class="type-label" >Estado civil<span class="text-danger">*</span></label>
                 <?php
                      echo $form->field($model, 'marital_status')->dropDownList(Patient::$ESTADO_CIVIL, [
                              'prompt' => 'Escolha o teu estado civil','required' => true,
              ])->label(false) ?>
              </div>
              <div class="col-sm-4">
                 <label for="" class="type-label" >Quantidade de filho</label>
                  <?= $form->field($model, 'qtd_son')->textInput([
                     'maxlength' => true,
                     "class" => "form-control",
                       'placeholder' => "Introduzir Nº de filho",
                       'aria-label' => "Introduzir Nº de filho",
                       'required' => false,
                       'aria-describedby' => "dataInicioLabel",
                       'data-msg' => "Introduzir Nº de filho",
                       'data-error-class' => "u-has-error",
                       'data-success-class' => "u-has-success",
                       'type' => 'number'
                  ])  ->label(false)?>
              </div>
            </div>
            <h4>Composição agregado familiar(Em Número)</h4>
            <div class="row">
             
              <div class="col-sm-4">
              <label for="" class="type-label" >Total da familia<span class="text-danger">*</span></label>
               <?= $form->field($model, 'qtd_family')->textInput([
                   'maxlength' => true,
                   "class" => "form-control",
                    'placeholder' => "Introduzir total da familia",
                    'aria-label' => "Introduzir total da familia",
                    'required' => true,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir total da familia",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                 ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                <label for="" class="type-label" >Maior de idade</label>
                <?= $form->field($model, 'adult')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                      'placeholder' => "Introduzir maior de idade",
                      'aria-label' => "Introduzir maior de idade",
                      'required' => false,
                      'aria-describedby' => "dataInicioLabel",
                      'data-msg' => "Introduzir maior de idade",
                      'data-error-class' => "u-has-error",
                      'data-success-class' => "u-has-success",
                      'type' => 'number'
                ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                  <label for="" class="type-label" >Menor de idade</label>
                  <?= $form->field($model, 'minor')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir menor de idade",
                        'aria-label' => "Introduzir menor de idade",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir menor de idade",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                 <label for="" class="type-label" >Pré-escolar</label>
                  <?= $form->field($model, 'preschool')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir pré-escolar",
                        'aria-label' => "Introduzir pré-escolar",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir Pré-escolar",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                  <label for="" class="type-label" >E.B.O</label>
                  <?= $form->field($model, 'ebo')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir e.b.o",
                        'aria-label' => "Introduzir e.b.o",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir e.b.o",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                  <label for="" class="type-label" >Secundário</label>
                  <?= $form->field($model, 'secondary')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir secundário",
                        'aria-label' => "Introduzir secundário",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir secundário",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                  <label for="" class="type-label" >Universidade</label>
                  <?= $form->field($model, 'college_student')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir universidade",
                        'aria-label' => "Introduzir universidade",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir universidade",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                   <label for="" class="type-label" >Trabalhador</label>
                  <?= $form->field($model, 'worker')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir trabalhador",
                        'aria-label' => "Introduzir trabalhador",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir trabalhador",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
              <div class="col-sm-4">
                  <label for="" class="type-label" >Desempregado</label>
                  <?= $form->field($model, 'Unemployed')->textInput([
                      'maxlength' => true,
                      "class" => "form-control",
                        'placeholder' => "Introduzir desempregado",
                        'aria-label' => "Introduzir desempregado",
                        'required' => false,
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => "Introduzir desempregado",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                  ]) ->label(false)?>
              </div>
            </div>
        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary rounded mr-1" href="javascript:;" data-previous-step="#selectStepOne">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepThree">Próximo</button>
        </div>
      </div>

    
      <div id="selectStepThree" style="display: none;">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Situação Profissional<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
        <div class="card ">
          <div class="card-body p-4"> 
            <h4>Exerce alguma actividade profissional? </h4>
            <div class="row mt-2">
              <div class="col-sm-4 mb-1">
                  <label for="" class="type-label" >Qual?</label>
                    <?= $form->field($model, 'profession')->textInput([
                      'maxlength' => true,
                      "class" => "form-control ",
                      'placeholder' => "Nome de actividade profissional",
                      'aria-label' => "Introduzir nome de actividade profissional",
                      'required' => false,
                      'aria-describedby' => "dataInicioLabel",
                      'data-msg' => "Introduzir nome de actividade profissional",
                      'data-error-class' => "u-has-error",
                      'data-success-class' => "u-has-success",
                      ]) ->label(false)?>
              </div>
              <div class="col-sm-8  mb-3">
                  <label for="" class="type-label" >Onde?</label>
                  <?php
                      $type = ['F' => 'Função Pública', 'E' => 'Empresa Privada' , 'P'=> 'Conta Própria'];
                      echo $form->field($model, 'institution_type')->dropDownList(
                          $type,
                          ['prompt' => 'Selecionar tipo de actividade profissional', "class" => "form-control ",]
                      )->label(false);
                    ?>
              </div>
              </div>
            </div>
          </div>
          <div class="card mt-2">
            <div class="card-body p-4"> 
              <h4>Se não trabalha qual é a situação? </h4>
              <div class="row">
                <div class="col-sm-12 mb-3">
                      <label for="" class="type-label" >Estuda?  onde?</label>
                      <?= $form->field($model, 'student')->textInput([ 
                          'maxlength' => true,
                          "class" => "form-control",
                          'placeholder' => "Nome da escola",
                          'aria-label' => "Nome da escola",
                          'required' => false,
                          'aria-describedby' => "dataInicioLabel",
                          'data-msg' => "nome da escola",
                          'data-error-class' => "u-has-error",
                          'data-success-class' => "u-has-success",
                        ]) ->label(false)?>
                </div>
              </div>
            </div>
           </div>
           <div class="card mt-2">
              <div class="card-body p-4"> 
                <h4>Segurado? </h4>
                <div class="row mt-2">
                  <div class="col-sm-6 mb-3">
                  <label for="" class="type-label" >Onde?</label>
                        <?php
                          $type = ['G' => 'Garantia' , 'I'=> 'Impar','S' => 'INPS',];
                          echo $form->field($model, 'insurance_institution')->dropDownList(
                              $type,
                              ['prompt' => 'Selecionar nome de seguradora', "class" => "form-control ",]
                          )->label(false);
                        ?>
                  </div>
                  <div class="col-sm-6 mb-3">
                    <label for="" class="type-label" >Outros</label>
                    <?= $form->field($model, 'others')->textInput([ 
                      'maxlength' => true,
                      "class" => "form-control",
                      'placeholder' => "Outros",
                      'aria-label' => "Outros",
                      'required' => false,
                      'aria-describedby' => "dataInicioLabel",
                      'data-msg' => "Outros",
                      'data-error-class' => "u-has-error",
                      'data-success-class' => "u-has-success",
                    ]) ->label(false)?>
                  </div>
              </div>
            </div>
          </div>
        <div class="d-flex justify-content-end mt-2">
          <a class="btn btn-sm btn-soft-secondary rounded mr-1" href="javascript:;" data-previous-step="#selectStepTwo">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepFour">Próximo</button>
        </div>
      </div>

      <div id="selectStepFour" style="display: none;">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Se menor de idade ou na dependência de Outro<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
        <!--    grau parentesco -->
        <div class="row">
          <div class="col-md-6">
            <label for="" class="type-label" >Nome parentesco</label>
            <?= $form->field($model, 'name_parents')->textInput([
              'maxlength' => true,
              "class" => "form-control ",
              'placeholder' => "Introduzir nome parentesco",
              'aria-label' => "Introduzir nome parentesco",
              'required' => false,
              'aria-describedby' => "dataInicioLabel",
              'data-msg' => "Introduzir nome parentesco",
              'data-error-class' => "u-has-error",
              'data-success-class' => "u-has-success",
            ]) ->label(false)?>
          </div>
          <div class="col-md-3">
            <label for="" class="type-label" >Morada de parentesco</label>
            <?= $form->field($model, 'address')->textInput([
              'maxlength' => true,
              "class" => "form-control ",
              'placeholder' => "Introduzir morada de Parentesco",
              'aria-label' => "Introduzir morada de Parentesco",
              'required' => false,
              'aria-describedby' => "dataInicioLabel",
              'data-msg' => "Introduzir morada de Parentesco",
              'data-error-class' => "u-has-error",
              'data-success-class' => "u-has-success",
            ]) ->label(false)?>
          </div>

          <div class="col-md-3">
            <label for="" class="type-label" >Grau de arentesco</label>
              <?= $form->field($model, 'degree')->textInput([
                'maxlength' => true,
                "class" => "form-control",
                'placeholder' => "Introduzir grau de Parentesco",
                'aria-label' => "Introduzir grau de Parentesco",
                'required' => false,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => "Introduzir grau de Parentesco",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
              ]) ->label(false)?>
          </div>
        </div>
        <div class="d-flex justify-content-end mt-3">
          <a class="btn btn-sm btn-soft-secondary rounded mr-1" href="javascript:;" data-previous-step="#selectStepThree">Voltar</a>
          <button type="button" class="btn btn-sm btn-indigo rounded" data-next-step="#selectStepFive">Próximo</button>
        </div>
      </div>
      <div id="selectStepFive" style="display: none;">
        <div class="border-bottom pb-0 mb-2">
            <h4 class="h5 font-weight-normal">Foto<a href="" class="float-right"><i class="fa fa-times"></i></a></h4>
        </div>
        <div class="row">
          
          <div class="col-md-6">
          <?php if (@$model->imagem ==''): ?>
            <!-- File Attachment Input -->
            <label id="nameLabel" class="form-label">
                Foto da Paciente
                <span class="text-danger">*</span>
            </label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Carregar capa</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label"
                              data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="Patient[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '/' . $model->imagem?>" style="height: 200px; width: 100%;display: none;"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                      class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                      style="width: 100%; display: none;" for="floorplanAttachmentInput">
                    Mudar Capa
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                          style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar Foto de Perfil
                    </label>
                <?php endif; ?>
            </div>
          <?php else:?>
            <!-- File Attachment Input -->
            <label id="nameLabel" class="form-label">
                Foto da Paciente
                <span class="text-danger">*</span>
            </label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Carregar capa</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label"
                              data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="Patient[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '/' . $model->imagem ?>" style="height: 200px; width: 100%;"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                      class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                      style="width: 100%; display: none;" for="floorplanAttachmentInput">
                    Mudar Capa
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-perfil" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                          style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar Foto de Perfil
                    </label>
                <?php endif; ?>
            </div>
          <?php endif; ?>
          </div>
        </div>
     
        <div class="d-flex justify-content-end">
          <a class="btn btn-sm btn-soft-secondary rounded mr-1" href="javascript:;" data-previous-step="#selectStepFour">Voltar</a>
          <!--= Html::submitButton('Gurdar', ['class' => 'btn btn-indigo btn-x']) ?-->
          <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>
      </div>
    </div>
    <!-- End Content Step Form -->
  <!--/div-->

<!-- End Step Form -->
<!-- End Step Form -->
<?php ActiveForm::end(); ?>
<style media="screen">
  .m{
    margin-top: 150px;
  }
  .mr-7{
    margin-right: 7.5rem !important;
  }
  .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
.flatpickr-current-month {
      padding: 0px!important;
  }
  .flatpickr-months {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    /* display: flex; */
    position: relative;
    background-color: #2d1582;
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
    padding: .75rem;
  }
  span.cur-month {
    color: #fff!important;
  }
  .numInputWrapper {
      color: #fff!important;
  }
  svg {
    fill: rgba(255, 255, 255, 0.7);
  }
  .flatpickr-day.today {
      border-color: #959ea9;
  }
  .flatpickr-day.today {
      border-color: #2d1582!important;
  }
</style>



 

<!-- End Step Form -->

<?php
    $scipt = <<<JS
    $(document).ready(function () {
        $('#floorplanAttachmentInput').change(function () {

            var reader = new FileReader()

            reader.onload = function (e) {

                $('#capa_preview').attr('src', e.target.result)
                $('#image-capa-btn-attach').css('display', 'block')
                $('#change').css('display', 'none')
                $('#capa-continer').css('display', 'none')
                $('#capa_preview').css('display', 'block')
            }
            reader.readAsDataURL(this.files[0])
        })


        /// Language
        var dynamic_form_language = $("#dynamic_form_language").dynamicForm("#dynamic_form_language", "#plus_language", "#minus_language", {
            limit: 20,
            formPrefix: "Language",
            normalizeFullForm: false
          })
        ;

        var string_lingua_data = $('#language_list').val()
        string_lingua_data = string_lingua_data.split('@')
        var languages = string_lingua_data.map(function (value) {
          value = value.split('-')
          return {
              idioma: value[0],
              nivel: value[1]
          }
        })
        dynamic_form_language.inject(languages);
    })








JS;
    $this->registerJS($scipt);
?>