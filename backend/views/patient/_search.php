<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\patientSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'nickname') ?>

    <?= $form->field($model, 'birth_date') ?>

    <?= $form->field($model, 'nationality') ?>

    <?php // echo $form->field($model, 'naturalness') ?>

    <?php // echo $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'mobile_phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'cni') ?>

    <?php // echo $form->field($model, 'educational_level') ?>

    <?php // echo $form->field($model, 'sex') ?>

    <?php // echo $form->field($model, 'marital_status') ?>

    <?php // echo $form->field($model, 'qtd_son') ?>

    <?php // echo $form->field($model, 'qtd_family') ?>

    <?php // echo $form->field($model, 'adult') ?>

    <?php // echo $form->field($model, 'minor') ?>

    <?php // echo $form->field($model, 'preschool') ?>

    <?php // echo $form->field($model, 'college_student') ?>

    <?php // echo $form->field($model, 'secondary') ?>

    <?php // echo $form->field($model, 'worker') ?>

    <?php // echo $form->field($model, 'Unemployed') ?>

    <?php // echo $form->field($model, 'ebo') ?>

    <?php // echo $form->field($model, 'profession') ?>

    <?php // echo $form->field($model, 'institution_type') ?>

    <?php // echo $form->field($model, 'institution_name') ?>

    <?php // echo $form->field($model, 'student') ?>

    <?php // echo $form->field($model, 'insurance_institution') ?>

    <?php // echo $form->field($model, 'others') ?>

    <?php // echo $form->field($model, 'name_parents') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'degree') ?>

    <?php // echo $form->field($model, 'image') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'zone') ?>

    <?php // echo $form->field($model, 'island') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
