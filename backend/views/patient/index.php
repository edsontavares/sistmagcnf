<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="employees-index">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert alert-info  alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('<i class="far fa-plus-square mr-1"></i>Registar paciente', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc"  name="search-text" id="search-text" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>
    <table class="table">
  <thead class="top">
    <tr>
      
      <th scope="col">Nome</th>
      <th scope="col">Alcunha</th>
      <th scope="col">CNI/BI</th>
      <th scope="col">Tipo Consulta</th>
      <th scope="col">Estado</th>
      <th scope="col">opção</th>

    </tr>
  </thead>
  <tbody id="result">
  <?php  foreach ($patient as $patientState) {
          $query = \common\models\Query::findOne($patientState->id_query);
          $patients = \common\models\Patient::findOne($patientState->id_patient);
          
    ?>
    <tr >
      <td class="align-middle text-secondary">
        <span><?= $patients->name ?></span>
      </td>
      
      <td><?= $patients->nickname?></td>
      <td><?= $patients->cni?></td>
      <td>
        <?php if($query):?>
          <?php if($query->tepy_query == 'FTP'):?>
              Fisioterapia
          <?php elseif($query->tepy_query == 'OTP'):?>
              Ortopidia
          <?php endif;?>
        <?php endif;?>
      </td>
      <td>
        <?php if($patientState->state == 1):?>
          <span class="badge badge-danger">Pendente</span>
            <!--span class="badge badge-primary">Agurada agendamento para AV</!--span-->
          <?php elseif($patientState->state == 2):?>
            <span class="badge badge-secondary">Agendado AV</span>
          <?php elseif($patientState->state == 3):?>
            <span class="badge badge-danger">Pendente Pagamento</span>
          <?php elseif($patientState->state == 16):?>
            <span class="badge badge-danger">Pendente Pagamento</span>
          <?php elseif($patientState->state == 4):?>
          <span class="badge badge-danger">Cancelado</span>
          <?php elseif($patientState->state == 5):?>
            <span class="badge badge-info">Enviado Para Ortópidia</span>
          <?php elseif($patientState->state == 17):?>
          <span class="badge badge-dark">Enviado para Médico</span>
          <?php elseif($patientState->state == 6):?>
            <span class="badge badge-dark">Enviado Para fisioterapia</span>
          <?php elseif($patientState->state == 7):?>
            <span class="badge badge-warning">Pago! Aguarda Agendamento Medidas</span>
          <?php elseif($patientState->state == 8 && $query->tepy_query == 'FTP'):?>
           <span class="badge badge-warning">Pago! Aguarda Agendamento Consulta</span>
          <?php elseif($patientState->state == 9):?>
            <span class="badge badge-danger">Agendado Consulta</span>
          <?php elseif($patientState->state == 10):?>
           <span class="badge badge-danger">Agendado Medida</span>
          <?php elseif($patientState->state == 11):?>
           <span class="badge badge-danger">Em Tratamento</span>
           <!-- em tratamento otopidia--->  
          <?php elseif($patientState->state == 12):?>
           <span class="badge badge-danger">Em Tratamento</span>  
          <?php elseif($patientState->state == 14):?>
            <span class="badge badge-success">Tratamento Concluido</span>
          <?php elseif($patientState->state == 21):?>
              <span class="badge badge-info">Em Avaliação</span>
          <?php endif;?>
        </td>
      <td> 
      <div class="btn-group position-relative ">
            <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                data-unfold-type="css-animation" data-unfold-duration="300"
                data-unfold-delay="300"
                data-unfold-animation-in="slideInUp"
                data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                <span class="fas fa-ellipsis-v btn-icon__inner"></span>
            </a>

            <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
              
              <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i> Ver', ['view', 'id' => $patients->id_patient], ['class' => 'dropdown-item']) ?>
              <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i> Editar', ['update', 'id' => $patients->id_patient], ['class'=>"dropdown-item"])?>
              <?php if($patientState->state == 1):?>
                <a href="#" class="open-modal dropdown-item" id="anucio_automatico" data-modal-size="modal-lg"  data-url="<?= Url::to(['calendar/create/', 'tipo'=>'avaliação']) ?>" data-title="<?= Yii::t('app', 'Agendar avaliação') ?>">
                  <i class="far fa-edit btn-soft-primary mr-2"></i>
                  <?= Yii::t('app', 'Agendar avaliação') ?>
                </a>
              <?php endif;?>
              
              <a class="delete_patient dropdown-item" href="#" data-url="<?= Url::to(['/'.'patient/delete/'.$patients->id_patient])?>"
                data-sim="<?= Yii::t('app', 'Sim') ?>"
                data-nao="<?= Yii::t('app', 'Não') ?>"
                data-title="<?= Yii::t('app', 'Deseja  eliminar o paciente '.$patients->name.'?') ?>"
                data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                data-key="<?= $patients->id_patient?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
              </a>
            </div>
          </div>
    </td>



      </tr>
    <?php }?>  
  </tbody>
</table>
<?php if ($total > 12 || (@Yii::$app->request->get("page") && intval(Yii::$app->request->get("page")) > 1)): ?>
<?= $this->render('../../../common/componentes/pagination', [
    'total' => @$total,
    'item_numbers' => $item_numbers,
    'prettyUrl' => 'active',
    'url' => Url::to(['/patient/index'])
]) ?>
<?php endif; ?>
<!--div class="pagination">
  <!?= LinkPager::widget(['pagination'=> $pagination])?>
</div-->


</div>

<?php
  $script = <<<JS

  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  
  $(document).on('click','.delete_patient',function(e){
      e.preventDefault();
      var key = $(this).data('key');
      var _title = $(this).data('title');
      var url = $(this).data('url');
      var _message = $(this).data('message');
      var _sim = $(this).data('sim');
      var _nao = $(this).data('nao');
      bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-indigo btn-xs'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax( {
              method: "post",
              url:url,
            })
            .done(function( respond ) {
              data = JSON.parse(respond);
              if (data.type === 'error'){
                console.log('dont delete');
              }
              else{
                window.open('paciente?r=paciente&action=delete','_self');
              }
            });
          }
        }
      });
    });
  
JS;

$this->registerJs($script);
?>
<style>
.modal-header {
  background-color: #f8f9fa !important;
  border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
</style>

<style>

    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    .text {
  white-space: nowrap; 
  color: white;
  font-size: 20px;
  position: absolute;
  overflow: hidden;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #008CBA;
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;
}

th{
  color: #fff;
}
.grid-view td:nth-child(4) {
        white-space: nowrap;
    }
    tbody tr:nth-child(odd) {
      background-color: #e0e0ea;
    }
    th {
        color: #fff;
    }
  .top{
    background-color: #2d1582;
    box-shadow: 0 3px 6px 0 rgb(140 152 164 / 25%) !important;
  }
  .table {
  border: 1px solid #e7eaf3;
  }
    
    
</style>
