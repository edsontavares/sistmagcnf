<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;
use yii\helpers\Url;
?>
<div class="employees-index">

    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('Adicionar Paciente', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc"  name="search-text" id="search-text" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>
    <table class="table">
  <thead class="top">
    <tr>
      
      <th scope="col">Nome</th>
      <th scope="col">Alcunha</th>
      <th scope="col">CNI/BI</th>
      <th scope="col">Estado</th>
      <th scope="col">opção</th>

    </tr>
  </thead>
  <tbody id="result">
  <?php  foreach ($patient as $patientState) {
          $query = \common\models\Query::findOne($patientState->id_query);
          $patients = \common\models\Patient::findOne($patientState->id_patient);
          
    ?>
    <tr >
      <td class="align-middle text-secondary">
        <span><?= $patients->name ?></span>
      </td>
      
      <td><?= $patients->nickname?></td>
      <td><?= $patients->cni?></td>
      <td>
        <?php if($patientState->state == 1):?>
            <span class="badge badge-primary">Agurada agendamento AV</span>
          <?php elseif($patientState->state == 2):?>
            <span class="badge badge-secondary">Agendado AV</span>
          <?php elseif($patientState->state == 3):?>
            <span class="badge badge-danger">Pendente Pagamento</span>
          <?php elseif($patientState->state == 4 && $query->tepy_query == 'OTP'):?>
            <span class="badge badge-warning">Pago! Aguarda Agendamento Consulta</span>
          <?php elseif($patientState->state == 4 && $query->tepy_query == 'FTP'):?>
          <span class="badge badge-warning">Pago! Aguarda Agendamento Medidas</span>
          <?php elseif($patientState->state == 5):?>
            <span class="badge badge-info">Agendado Consulta</span>
          <?php elseif($patientState->state == 6):?>
            <span class="badge badge-dark">Em Tratamento</span>
          <?php elseif($patientState->state == 7):?>
            <span class="badge badge-success">Tratamento Concluido</span>
          <?php elseif($patientState->state == 8):?>
            <span class="badge badge-danger">Agendado Medida</span>
          <?php endif;?>
        </td>
      <td> 
      <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i>', ['view', 'id' => $patients->id_patient], ['class' => '','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver",]) ?>
      <?= Html::a('<i class="fas fa-pencil-alt dropdown-item-icon"></i> ', ['update', 'id' => $patients->id_patient], ['class'=>"",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>
      <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id' => $patients->id_patient], [
        'class' => '',
        'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
        'data' =>
        [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
        ],
      ]) ?>
      <a class="remover-patient btn-soft-danger dropdown-item-icon" href="#" data-url=""
          data-sim="<?= Yii::t('app', 'Sim') ?>"
          data-nao="<?= Yii::t('app', 'Não') ?>"
          data-title="<?= Yii::t('app', 'Deseja eliminar este anexo?') ?>"
          data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
          data-key="<?=  $patients->id_patient?>" ><i class="fas fa-trash-alt "aria-hidden="true"></i>
        </a>
  
    </td>



      </tr>
    <?php }?>  
  </tbody>
</table>
<?= $this->render('../../../common/componentes/pagination', [
    'total' => @$total,
    'item_numbers' => $item_numbers,
    'prettyUrl' => 'active',
    'url' => Url::to(['/patient/index'])
]) ?>
<!--div class="pagination">
  <!?= LinkPager::widget(['pagination'=> $pagination])?>
</div-->


</div>

<script>
  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

  $(function() {
    $(document).on('click','.delete_company',function(e){
      e.preventDefault();
      var key = $(this).data('key');
      var _title = $(this).data('title');
      var _message = $(this).data('message');
      var _sim = $(this).data('sim');
      var _nao = $(this).data('nao');
      bootbox.confirm({
        title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
        message:'<p class="p-2 px-5">' +_message +'</p>',
        buttons: {
          cancel: {
            label: '<i class="fa fa-times"></i> ' + _nao,
            className: 'bt btn-soft-secondary btn-xs'
          },
          confirm: {
            label: '<i class="fa fa-check"></i> ' + _sim,
            className: 'bt btn-primary btn-xs'
          }
        },
        callback: function (result) {
          if(result){
            $.ajax( {
              method: "post",
              url:'/admin?r=company%2Fdelete&id='+key,
            })
            .done(function( respond ) {
              data = JSON.parse(respond);
              if (data.type === 'error'){
                console.log('dont delete');
              }
              else{
                window.open('index.php?r=company%2Findex&action=delete','_self');
              }
            });
          }
        }
      });
    });
  });
</script>
<style>
.modal-header {
  background-color: #f8f9fa !important;
  border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
}
</style>

<style>

    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    .text {
  white-space: nowrap; 
  color: white;
  font-size: 20px;
  position: absolute;
  overflow: hidden;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: #008CBA;
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;
}
.top{
  background-color: #2d1582;
}
th{
  color: #fff;
}
    
</style>
