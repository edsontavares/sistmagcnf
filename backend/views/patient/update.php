<?php

use yii\helpers\Url;

use yii\helpers\Html;
$this->title = 'Atualizar Paciente';
?>
<div class="Atualizar Paciente">
<div class="row mx-gutters-2">
        <div class="col-md-10">
        <h4 class="font-weight-bold"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="col-md-2">
            <a class="float-right " href="<?= Url::to(['/paciente'])?>">Voltar</a>
        </div>
    </div>
    <hr>
    <div class="card">
         <div class="card-body shadow-sm p-4">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
     </div>

</div>