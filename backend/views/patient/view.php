<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use common\models\Location;
use yii\widgets\ActiveForm;
use common\models\PatientState;

$locaton = Location::findOne($model->id_location);

\yii\web\YiiAsset::register($this);
?>
<div class="patient-view">
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert alert-info  alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash("success-ficheiro")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-ficheiro") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash("success-active")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-active") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<?php if (Yii::$app->session->hasFlash("success-desable")): ?>
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-desable") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
            <?php endif; ?>
<div class="row mx-gutters-2">
    <div class="col-lg-6">
        <h1 class="h5 font-weight-bold">Informação de Paciente</h1>
    </div>
    <div class="col-lg-6">
        <div class="ml-lg-auto float-right">
            <a class=" " href="<?= Url::to(['/paciente'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>
                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">    
                    <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i> Descaregar', ['patient/download', 'id'=>$model->id_patient], ['class'=>"dropdown-item",'target'=>"_blank"])?>
                    <?= Html::a('<i class="far fa-plus-square mr-2"></i>Registar', ['create'], ['class' => 'dropdown-item']) ?>
                    <?= Html::a('<i class="far fa-edit dropdown-item-icon"></i> Editar', ['update', 'id' => $model->id_patient], ['class'=>"dropdown-item"])?>
                    <?php 
                     $patientState = PatientState::find()->where(['id_patient'=>$model->id_patient ])->one();
                    if($patientState->state == 1):?>
                        <a href="#" class="open-modal dropdown-item" id="anucio_automatico" data-modal-size="modal-lg"  data-url="<?= Url::to(['calendar/create/', 'tipo'=>'avaliação']) ?>" data-title="<?= Yii::t('app', 'Agendar avaliação') ?>">
                            <i class="far fa-edit btn-soft-primary mr-2"></i>
                            <?= Yii::t('app', 'Agendar avaliação') ?>
                        </a>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="top-card">
<!-- start informacao pessoais -->
<div class="card">
    <div class="card-body shadow-sm p-4">
       <h4 class="h6 font-weight-bold"><b>Dados pessoais</b></h4>
        <div class="row">
            <div class="col-sm-4">
                <?php if($model->imagem):?>
                    <div class="card p-1 mb-4 card-user">
                        <div class="card-body text-center">
                            <div class="mb-3 zoom" >
                                 <img class="u-lg-avatar rounded-circle " onmouseover="bigImg(this)" onmouseout="normalImg(this)" src="../../<?= @$model->imagem ?>">
                            </div>
                            <div class="mb-3">
                                  <h1 class="h6 font-weight-medium mb-0"><?=$model->name?></h1>
                                 <small class="d-block text-muted"><?=$model->email?></small>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
            <div class="col-sm-8">
                <table style="width:100%">
                    <tbody>
                        <tr>
                            <td class="align-middle"><b>Alcunha</b></></td>
                            <td class="align-middle"><b>Residencia</b></td>
                            <td class="align-middle"><b>CNI/BI</b></td>
                        </tr>
                        <tr>
                            <td class="text-secondary"><?= $model->nickname?></td>
                            <td class="text-secondary" ><?= @$model->location->zone?></td>
                            <td class="text-secondary"><b><?= $model->cni?></b></td>
                        </tr>
                        <tr class="mt-4">
                            <td class="align-middle"><b>Nacionalidade</b></td>
                            <td class="align-middle"><b>Naturalidade</b></td>
                            <td class="align-middle"><b>Data Nascimento</b></td>
                        </tr>
                        <tr>
                            <td class="text-secondary"><?= $model->nationality?></td>
                            <td class="text-secondary"><?= $model->naturalness?></td>
                            <td class="text-secondary"><?= $model->birth_date?></td>
                        </tr>
                        <tr class="mt-4">
                            <td class="align-middle"><b>Telefone </b></td>
                            <td class="align-middle"><b>Telemovel</b></></td>
                            <td class="align-middle"><b>Sexo</b></td>
                        </tr>
                        <tr class="mt-4">
                            <td class="text-secondary"><?= $model->telephone?></td>
                            <td class="text-secondary"><?= $model->mobile_phone?></td>
                            <td class="text-secondary">
                            <?php if($model->sex == 'M') :?>
                                Masculino
                            <?php elseif($model->sex == 'F') :?> 
                                Fiminino
                            <?php endif;?>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end informacao pessoais -->
<div class="row mt-5">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-body shadow-sm p-4">
                <h4 class="h6 font-weight-bold"><b>Situação Sócio-Familiar</b></h4>
                <table style="width:100%">
                    <tbody>
                        <?php if($model->marital_status):?>
                            <tr>
                                <td><b>Estado Civil</td><td class="text-secondary">
                                <?php if($model->marital_status == 0):?>
                                Solteira(o)
                                <?php elseif($model->marital_status == 1):?>
                                Casada(o)
                                <?php elseif($model->marital_status == 2):?>
                                Viúva(o)
                                <?php elseif($model->marital_status == 3):?>
                                Devorciada(o)
                                <?php elseif($model->marital_status == 0):?>
                                União de factos
                                <?php endif;?>
                                </td>  
                            </tr>
                        <?php endif;?>
                        <?php if($model->qtd_son):?>
                            <tr>
                                <td class="align-middle"><b>Quantidade de filho</b></td><td class="text-secondary"><?= $model->qtd_son?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->qtd_family):?>
                            <tr>
                                <td class="align-middle"><b>Total da Familia</b></td><td class="text-secondary"><?= $model->qtd_family?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->adult):?>
                            <tr>
                                <td class="align-middle"><b>Maior da Idade</b></td><td class="text-secondary"><?= $model->adult?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->minor):?>
                            <tr>
                                <td class="align-middle"><b>Menor da idade</b></td><td class="text-secondary"><?= $model->minor?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->preschool):?>
                            <tr>
                                <td class="align-middle"><b>Pré-escolar</b></td><td class="text-secondary"><?= $model->preschool?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->ebo):?>
                            <tr>
                                <td class="align-middle"><b>E.B.O</b></td><td class="text-secondary"><?= $model->ebo?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->secondary):?>
                        <tr>
                             <td class="align-middle"><b>Secundário</b></td><td class="text-secondary"><?= $model->secondary?></td>
                        </tr>
                        <?php endif;?>
                        <?php if($model->college_student):?>
                            <tr>
                                <td class="align-middle"><b>Universidade</b></td><td class="text-secondary"><?= $model->college_student?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->worker):?>
                            <tr>
                                <td class="align-middle"><b>Trabalhador</b></td><td class="text-secondary"><?= $model->worker?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->Unemployed):?>
                            <tr>
                                <td class="align-middle"><b>Desempregado</b></td><td class="text-secondary"><?= $model->Unemployed?></td>
                            </tr>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-body shadow-sm p-4">
                <h4 class="h6 font-weight-bold"><b>Situação Profissional</b></h4>
                <table style="width:100%">
                    <tbody>
                        <?php if($model->profession):?>
                            <tr>
                                <td class="align-middle"><b>Nome de Actividade Profissional</b></td><td class="text-secondary"><?= $model->profession?></td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->institution_type):?>
                            <tr>
                                <td class="align-middle"><b>Tipo de Actividade Profissional</b></td><td class="text-secondary">
                                <?php if($model->institution_type == 'F'):?>
                                Função Pública
                                <?php elseif($model->institution_type == 'E'):?>
                                Empresa Privada
                                <?php elseif($model->institution_type == 'P'):?>
                                Conta Própria
                                <?php endif;?>
                                </td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->student):?>
                            <tr>
                                <td class="align-middle"><b>Nome da Escola</b></td><td class="text-secondary"><?= $model->student?>
                                </td>
                            </tr>
                        <?php endif;?>
                        <?php if($model->student):?>
                            <tr>
                                <td class="align-middle"><b>Nome de Seguradora</td><td class="text-secondary"><?= $model->insurance_institution?>
                                <?php if($model->insurance_institution == 'G'):?>
                                    Garantia
                                <?php elseif($model->insurance_institution == 'I'):?>
                                    Impar
                                <?php elseif($model->insurance_institution == 'S'):?>
                                    INPS
                                <?php endif;?>
                                </td>
                            </tr>
                        <?php endif;?>
                    </tbody>

                </table>
                <?php
                $dependente = json_decode($model->name_parents);

                ?>
                <?php // if (count($dependente ) > 0 && $model->name_parents !='[""]' && $model->name_parents !='' && $model->name_parents != '[{"name_parents":"","address":"","degree":""}]'): ?>

                <h4 class="h6 font-weight-bold pt-5"><b>Dependência de Outro</b></h4>

            </div>
        </div>
        <!-- jhjjhjgjj--->
    </div>
</div>     
<div class="card shadow-lg mb-4 mt-2"> <div class="card-header pt-4 pb-3 px-0 mx-4" style="border-bottom: none;">
    <h2 class="h6 mb-0"><span class="fas fa-paperclip text-primary mr-2" aria-hidden="true"></span>Documentos de paciente</h2>
    </div>
    <hr class="mt-0 mb-0">
    <div class="card-body p-4">
        <!--ANEXOS DE INSCRICAO-->
        <div class="row">
            <div class="col-sm-12">
                <div class="anexos-file">
                <?= $this->render('_anexos_lista', ['model' => $model]) ?>
                </div>
            </div>
        </div>
        <!--FIM ANEXOS INSCRICAO-->
        <div class="row">
            <!-- adicionar documentos -->
            <div class="col-sm-12">
                <a class="text-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-plus mr-2"></i>Anexa os documentos
                </a>
                <div class="collapse mt-3 ID_DA_DIV" id="collapseExample">
                    <?php $form = ActiveForm::begin([
                    'id' => 'form-formador',
                    'options' => ['enctype' => 'multipart/form-data'],
                    //'action' => \yii\helpers\Url::to(['/user/formador/create']),
                ]) ?>
                    <?= $form->field($model, 'name')->hiddenInput(['maxlength' => true])->label(false) ?>
                    <div class="d-flex load-form">
                        <!-- File Attachment Button -->
                        <label class="btn btn-sm btn-soft-primary file-attachment-btn" for="">
                        <i class="fas fa-file-pdf mr-2"></i>caregar documento
                        <input id="inputArquivo" name="image-assinatura" type="file" class="file-attachment-btn__label nomeCertificacao" value=" " autocomplete="off">
                            </label>
                        <label class="botaoArquivo" id="datatable_users_filter"></label>
                        <!-- End File Attachment Button -->
                        <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Adcionar', ['class' => $model->isNewRecord ? 'btn btn-sm btn-primary save buton_save ml-2' : 'btn btn-sm btn-primary save buton_save ml-2 ','id'=>'btn-save']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
                </div>
            </div>
            <!-- fim de adicionar documentos -->
</div>
<style>




    .jumbotron {
    padding: 2rem 2rem!important;
}
.top-card .card{
   
    border: 0px solid #e7eaf3!important;
        
   
}
.card-user{
    border: 1px solid #e7eaf3!important;
}
</style>
<script>
     $(function () {
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
    })
function bigImg(x) {
  x.style.height = "300px";
  x.style.width = "300px";
}

function normalImg(x) {
  x.style.height = "100px";
  x.style.width = "100px";
}

var div = document.getElementsByClassName("botaoArquivo")[0];
    var input = document.getElementById("inputArquivo");

    div.addEventListener("click", function(){
        input.click();
    });
    input.addEventListener("change", function(){
        var nome = "Não há arquivo selecionado. Selecionar arquivo...";
        if(input.files.length > 0) nome = input.files[0].name;
        div.innerHTML = nome;
    });
</script>
 


  

