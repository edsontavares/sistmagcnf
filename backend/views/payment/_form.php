<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\select2\Select2;
    use yii\helpers\ArrayHelper;
    use common\models\Patient;
    use yii\helpers\Url;
?>
<div class="payment-form">
    <?php $form = ActiveForm::begin(
         [
            //'action'=>'/sistmagcnf/admin/payment/create'
        ]
    ); ?>
    <div class="row">
        <div class="col-sm-6">
            <label for="" class="type-label">Nome de paciente<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                    'language' => 'en',
                    'options' => ['placeholder' => 'Selecionar Paciente'],
                    'pluginOptions' => [
                    'allowClear' => true,
                    'class' => 'form-control  ',
                    'required' => true,
                    'data-msg' => 'Por favor Selecionar Paciente.',
                    'data-error-class' => 'u-has-error',
                    'data-success-class' => 'u-has-success',
                    ],
                ])->label(false) ?>
            </div>
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label">type<span class="text-danger">*</span></label>
            <?= $form->field($model, 'type')->dropDownList(
                  [
                      'Recibo' => 'Recibo',
                      'Proforma' => 'Proforma',
                  ],
                  [
                      'class' => 'form-control custom-select',
                      'data-msg' => 'Selecionar typy',
                      'require' =>true,
                      'data-error-class' => 'u-has-error',
                      'data-success-class' => 'u-has-success',
                      'prompt' => 'Selecionar forma de pagamento'
                  ]
            )->label(false) ?>

        </div>
        <div class="col-sm-4">
            <label for="" class="type-label">Nº de Prestações<span class="text-danger">*</span></label>
            <?= $form->field($model, 'interva')->dropDownList(
                  [
                      '1' => 'Primeiro Parcela',
                      '2' => 'Segundo Parcela',
                      '3' => 'Terceiro Parcela',
                      '4' => 'Completo',

                  ],
                  [
                      'class' => 'form-control custom-select',
                      
                      'data-msg' => 'Selecionar modo de pagamento',
                      'data-error-class' => 'u-has-error',
                      'data-success-class' => 'u-has-success',
                      'prompt' => 'Selecionar modo de pagamento'
                  ]
              )->label(false) ?>
        </div>
        <div class="col-sm-4">
            <label for="" class="type-label">Pag. Referente a:<span class="text-danger">*</span></label>
            <?= $form->field($model, 'payment_type')->dropDownList(
                  [
                      'Dinheiro' => 'Dinheiro',
                      'Vinte 24' => 'Vinte 24',
                      'Cheque' => 'Cheque',
                      'Transferencia' => 'Tranferencia',
                      'Financimento' =>'Financiamento',
                  ],
                  [
                      'class' => 'form-control custom-select',
                      
                      'data-msg' => 'Selecionar forma de Pagamento',
                      'data-error-class' => 'u-has-error',
                      'data-success-class' => 'u-has-success',
                      'prompt' => 'Selecionar forma de pagamento'
                  ]
              )->label(false) ?>

        </div>
        <div class="col-sm-4">
            <label for="" class="type-label">Nº Ord. Trans</label>
            <?= $form->field($model, 'N_tranf')->textInput([
                        "class" => "form-control",
                        'placeholder' => "Introduzir Nº Ord. Trans",
                        'aria-label' => "Introduzir Nº Ord. Trans",
                        'aria-describedby' => "dataInicioLabel",
                        'data-msg' => " Introduzir Nº Ord. Trans",
                        'data-error-class' => "u-has-error",
                        'data-success-class' => "u-has-success",
                        'type' => 'number'
                ])->label(false) ?>
        </div>
    </div>
    <div class="col-sm-12 text-right">
        <div class="form-group">
            <a href="<?= Url::to(['/payment'])?> " class="btn btn-sm btn-soft-secondary rounded">Cancelar</a>

            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
<style>
.type-label {
    display: block;
    /* text-transform: lowercase; */
    font-size: 80%;
    font-weight: 600;

}

.select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px !important;
}

.select2-container--krajee .select2-selection--single {
    height: 50px !important;
    padding: 14px 24px 6px 16px !important;
}
</style>


<script>
$(document).on('click', '.saves', function(e) {
    e.preventDefault()
    e.stopImmediatePropagation();
    alert('ok');
    save($('#formConching'), $('#formConching')[0], 'create');
    $('input').val(" ");
    $("#datatable_users_filter label").css("display", "none");

});
</script>