<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PaymentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="payment-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_payment') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'value') ?>

    <?= $form->field($model, 'amount') ?>

    <?= $form->field($model, 'coin') ?>

    <?php // echo $form->field($model, 'payment_type') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'id_query') ?>

    <?php // echo $form->field($model, 'interva') ?>

    <?php // echo $form->field($model, 'manage_many_times') ?>

    <?php // echo $form->field($model, 'experation_date') ?>

    <?php // echo $form->field($model, 'id_precarious') ?>

    <?php // echo $form->field($model, 'unit_price') ?>

    <?php // echo $form->field($model, 'total_price') ?>

    <?php // echo $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'qt') ?>

    <?php // echo $form->field($model, 'description') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
