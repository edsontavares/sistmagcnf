<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
?>
<div class="payment-index">

    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('criar recibo', ['create'], ['class' => 'btn btn-nr btn-indigo font-weight-bold']) ?>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search.." id="myInput" onkeyup="myFunction()">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>

   
    <table class="table" id="myTable">
      <thead class="top">
        <tr>
          <th scope="col">Numero</th>
          <th scope="col">Nome de paciente</th>
          <th scope="col">Total de Preço</th>
          <th scope="col">Tipo Pagamento</th>
          <th scope="col">Intervalo</th>
          <th scope="col">Type</th>
          <th scope="col">Opção</th>

        </tr>
      </thead>
      <tbody>
        <?php  foreach ($payment as $payments) {
        ?>
          <tr>
            <td scope="row"><?= $payments->id_payment?></td>
            <td>
            
               <?php if($payments->patients):?>
                   <?= $payments->patients->name?>
               <?php endif;?>
            </td>
            <td><?= $payments->value?> </td>
            <td><?= $payments->payment_type?></td>
            <td>
              <?php if($payments->interva == 1):?>
                <span class="badge badge-success">Primeiro Parcela</span>
              <?php elseif($payments->interva == 2):?>
                  <span class="badge badge-success">Segundo Parcela</span>
              <?php elseif($payments->interva == 3):?>
                  <span class="badge badge-success">Terceiro Parcela</span>
              <?php elseif($payments->interva == 4):?>
                  <span class="badge badge-success">Completo</span>
              <?php endif;?>
            </td>
            <td>
              <?= $payments->type?> </td>
            </td>
            <td> 
              <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i>', ['view', 'id' => $payments->id_payment], ['class' => '','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>
              <?= Html::a('<i class="fas fa-pencil-alt dropdown-item-icon"></i> ', ['update', 'id' => $payments->id_payment], ['class'=>"",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar",])?>
              <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id' => $payments->id_payment], [
                  'class' => '',
                  'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
                  'data' =>
                      [
                          'confirm' => 'Are you sure you want to delete this item?',
                          'method' => 'post',
                      ],
              ]) ?>
            </td>
          </tr>
        <?php }?>  
      </tbody>
    </table>
    <?= $this->render('../../../common/componentes/pagination', [
      'total' => @$total,
      'item_numbers' => $item_numbers,
      'prettyUrl' => 'active',
      'url' => Url::to(['/payment/index'])
    ]) ?>
</div>
<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
</style>




