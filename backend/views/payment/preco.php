<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\select2\Select2;
    use yii\helpers\ArrayHelper;
    use common\models\Patient;
    use yii\helpers\Url;
?>
    <?php $form = ActiveForm::begin([
        'id' => 'form-formador',
        'options' => ['enctype' => 'multipart/form-data'],
            'action' => \yii\helpers\Url::to(['/payment/adcionar']),
        ]) ?>
    <div class="row">
        <div class="col-sm-4">
            <label for="" class="type-label">Quantidade<span class="text-danger">*</span></label>
            <?= $form->field($model, 'qt')->textInput([
                "class" => "form-control",
                'placeholder' => "Introduzir qt",
                'aria-label' => "Introduzir qt",
                'required' => true,
                'aria-describedby' => "dataInicioLabel",
                'data-msg' => " Introduzir qt",
                'data-error-class' => "u-has-error",
                'data-success-class' => "u-has-success",
                'type' => 'number'
            ]
            )->label(false)  ?>
        </div>
        <div class="col-sm-4">
            <label for="" class="type-label">Codigo<span class="text-danger">*</span></label>
            <?= $form->field($model, 'id_precarious')->dropDownList(ArrayHelper::map(\common\models\Precarious::find()->all(), 'id_precarious', 'code'), ['prompt'=>'','id'=>'id_precarious'] )->label(false); ?>
        </div>
        <div class="col-sm-4">
            <label for="" class="type-label">preco<span class="text-danger">*</span></label>
            <?= $form->field($model, 'qt')->textInput([
                    "class" => "form-control",
                    'placeholder' => "Introduzir qt",
                    'aria-label' => "Introduzir qt",
                    'required' => true,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " Introduzir qt",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]
                )->label(false)  ?>
        </div>
        <div class="col-sm-12 mt-2 text-right">
            <?= Html::submitButton('<i class="far fa-save mr-2"></i>Adcionar', ['class' => 'btn btn-sm btn-primary save buton_save ']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
