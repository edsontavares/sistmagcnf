<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
\yii\web\YiiAsset::register($this);
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">

<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold"><?= $model->type?></h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/payment/index'])?>">Voltar</a>
            <div class="btn-group position-relative ">

                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                    <?= Html::a('<i class="fas fa-briefcase mr-1"></i>Imprimir', ['imprimirr'], ['class' => 'dropdown-item transition-3d-hover open-modal' ,'target'=>"_blank"]) ?>   
                </div>
                </div>
            </div>
         </div>
    </div>
   
    <hr>

  
        <div class="card bg-img-hero" style="background-image: url(../../biblioteca/svg/components/bg-elements-6.svg);">
          <div class="card-body p-4 p-md-7 p-lg-11">
            <div class="row justify-content-lg-between align-items-sm-center ">
              <div class="col-sm-6 col-lg-4 order-sm-2 text-sm-right  mb-sm-0">
                <h1 class="h2 font-weight-medium mb-0 mt-20">
                    <?php if($model->type == 'Recibo'):?>
                      Fatura#<br>
                       Recibo
                    <?php elseif($model->type == 'Proforma'):?>
                      Proforma
                    <?php endif;?>
                </h1>
              </div>

              <div class="col-sm-5 order-sm-1">
                <!-- Logo -->
                <div class="media align-items-center d-block mb-3">
                    <span class="u-sm-avatar">
                        <img class=" rounded" src="../../recurso/img/Logo.jpg" alt="Image Description" style="height: 80px; width: 100px;">
                    </span>
                    <span>CENORF.- CP 26-C</span>
                </div>
                <!-- End Logo -->
                <!-- Address -->
                
                <span class="d-block"> <small class="mr-2 font-weight-bold font-weight-normal"><b>Sede:</b></small><small class=" text-muted">Praia, A.S. Filipe - Cabo Verde</small></span>
                <span class="d-block"><small class="mr-2 font-weight-bold font-weight-normal"><b>Contacto:</b></small><small class="text-muted">264 76 9 - 5157145</small></span>
                <span class="d-block"><small class="mr-2 font-weight-bold font-weight-normal"><b>Email:</b></small><small class="text-muted">cenorf@sapo.cv</small></span>
                <span class="d-block"><small class="mr-2 font-weight-bold font-weight-normal"><b>NIF:</b></small><small class="text-muted">93423246</small></span>
                <span class="d-block"><small class="mr-2 font-weight-bold font-weight-normal"><b>Nº Conta(BCA):</b></small><small class="text-muted">74589703102</small></span>
                <!-- End Address -->
              </div>
              
            </div>
            <hr style="border:1px solid #e7eaf3!important;" class="mb-4">
            

            <!-- Bill To -->
            <div class="row justify-content-md-between mb-7">
              <div class="col-sm-12">
                <?php if($model->patients):?>
                <span class="font-weight-normal text-uppercase"><?=$model->patients->name?></span>
                <?php endif;?>
              </div>
              <div class="col-sm-7">
                <span class="d-block "><small class="mr-2 font-weight-normal "><?= yii::t('app', 'Data') ?>:</small><small class="font-weight-normal "><?= $model->create_at?></small><span>
              </div>

              <div class="col-md-7 col-lg-4 text-right">
                <span class="d-block"><small class="mr-2 font-weight-normal "><?= yii::t('app', 'Nº') ?></small><small class="font-weight-normal "><?=$model->id_payment?>/<?=  Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');?></small><span>
              </div>
              <div class="col-sm-6">
                <span class="d-block"><small class="mr-2 font-weight-normal "><?= yii::t('app', 'Nif') ?>:</small><small class="font-weight-normal ">87676655545</small><span>
              </div>

              <div class="col-sm-6 text-right">
                <span class="d-block"><small class="mr-2 font-weight-normal "><?= yii::t('app', 'pgt') ?></small><small class="font-weight-normal ">imprisão</small><span>
              </div>
              
            </div>
            <!-- End Bill To -->

            <!-- Table -->
            <table class="table table-heighlighted font-size-1 mb-9">
              <thead class="top">
                <tr class="text-secondary">
                  <th scope="col" class="font-weight-bold"><?= yii::t('app', 'Qtd') ?></th>
                  <th scope="col" class="font-weight-bold"><?= yii::t('app', 'Descrição') ?></th>
                  <th scope="col" class="font-weight-bold"><?= yii::t('app', 'Preço') ?></th>
                  <th scope="col" class="font-weight-bold text-right"><?= yii::t('app', 'Total') ?></th>
                </tr>
              </thead>
              <tbody>

                <tr>
                  <td scope="row" class="font-weight-normal">10 </td>
                  <td>fgdhg dhjdfgjkg dmdjfjgbdmfjhjf dnhfdjf dhhdf </td>
                  <td>20</td>
                  <td class="text-right">30000Esc</td>
                </tr>

              </tbody>
              <tfoot>
                <tr class="h6">
                  <td colspan="2" class="text-right font-weight-bold">Subtotal</td>
                  <td colspan="3" class="text-right">30000Esc</td>
                </tr>
                <tr class="h6">
                  <td colspan="2" class="text-right font-weight-bold">Desconto</td>
                  <td colspan="3" class="text-right">0Esc</td>
                </tr>

                <tr class="h6">
                  <td colspan="2" class="text-right font-weight-bold">Total</td>
                  <td colspan="3" class="text-right">30000Esc</td>
                </tr>

              </tfoot>
            </table><br><br>
            
            <div class="row">
              <div class="col-md-12 col-lg-12 order-md-1 align-self-end text-center">
              <span>_________________________</span>
                <h3 class=" mb-0 font-weight-bold">Assinatura</h3>
                
              </div>
            </div><br><br><br><br>
            <!-- End Table -->
            <hr style="border:1px solid #e7eaf3!important;" class="">
            <!-- Contacts -->
            <div class="row">
              <div class="col-md-12 col-lg-12 order-md-1 align-self-end text-center">
                <p class="small text-muted mb-0">&copy; Documento processado por computador</p>
              </div>
            </div>
            <!-- End Contacts -->
          </div>
        </div>
        </div>
        <?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
    
<style>

.foter {
    position: absolute;
    bottom: -20px;
    width: 100%;
    background: white;
}

  .mt-20 {
    margin-top: 11rem !important;
}
.table-heighlighted tfoot td {
    border-top: 2px solid #e7eaf3!important; 
}



</style>
