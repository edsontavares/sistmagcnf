<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Precarious */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="precarious-form">

    <?php $form = ActiveForm::begin(
       
    ); ?>
    <div class="row">
        <div class="col-sm-4">
            <label for="" class="type-label" >Codigo <span class="bg-active">*</span></label>
             <?= $form->field($model, 'code')->textInput([
                   "class" => "form-control ",
                   'placeholder' => "Introduzir codigo",
                   'aria-label' => "Introduzir codigo",
                   'required' => true,
                   'aria-describedby' => "dataInicioLabel",
                   'data-msg' => "Introduzir codigo",
                   'data-error-class' => "u-has-error",
                   'data-success-class' => "u-has-success",
             ])->label(false)?>
        </div>
        <div class="col-sm-8">
            <label for="" class="type-label" >Designação <span class="bg-active">*</span></label>
            <?= $form->field($model, 'designation')->textInput([
                  "class" => "form-control ",
                  'placeholder' => "Introduzir Designação",
                  'aria-label' => "Introduzir Designação",
                  'required' => true,
                  'aria-describedby' => "dataInicioLabel",
                  'data-msg' => "Introduzir Designação",
                  'data-error-class' => "u-has-error",
                  'data-success-class' => "u-has-success",
            ])->label(false)?>
        </div>
        <div class="col-sm-4">
             <label for="" class="type-label" >Material <span class="bg-active">*</span></label>
             <?= $form->field($model, 'material')->textInput([
                 "class" => "form-control ",
                 'placeholder' => "Introduzir Material",
                 'aria-label' => "Introduzir Material",
                 'required' => true,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => "Introduzir Material",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-4">
             <label for="" class="type-label" >Preço <span class="bg-active">*</span></label>
             <?= $form->field($model, 'price')->textInput([
                 "class" => "form-control ",
                 'placeholder' => "Introduzir Preço",
                 'aria-label' => "Introduzir Preço",
                 'required' => true,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => "Introduzir Preço",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-4">
            <label for="" class="type-label" >Vida Util <span class="bg-active">*</span></label>
             <?= $form->field($model, 'lifespan')->textInput([
                 "class" => "form-control ",
                 'placeholder' => "Introduzir Vida Util",
                 'aria-label' => "Introduzir Vida Util",
                 'required' => true,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => "Introduzir Vida Util",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
            ]) ->label(false)?>
        </div>
        <div class="col-sm-12">
                <label for="" class="type-label" >Observação<span class="bg-active">*</span></label>
                <?= $form->field($model, 'description')->textarea(['placeholder' => "Antecedentes Pessoais", 'class' => 'form-control','rows'=>3])->label(false) ?>
        </div>
    </div>

    <div class="col-sm-12 text-right">
            <div class="form-group">
                <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
                <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
            </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>
