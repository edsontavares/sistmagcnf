<?php
use yii\helpers\Html;
$this->title = 'Create Precarious';
?>
<div class="precarious-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
