<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\Url;




?>
<div class="precarious-index">

 

    <p>
        <!--?= Html::a('Create Precarious', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>
    <div class="row">
    <div class="col-sm-6">
        <a href="#" class="btn btn-nr btn-indigo font-weight-bold open-modal" id="anucio_automatico" data-modal-size="modal-lg"  data-url="<?= Url::to(['precarious/create']) ?>" data-title="<?= Yii::t('app', 'Preçario') ?>">
        <i class="far fa-plus-square mr-1"></i>
        <?= Yii::t('app', 'Registar preçario') ?>
        </a>
      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>
    <table class="table mt-4" id="myTable">
  <thead class="top">
    <tr>
      <th scope="col">Codigo</th>
      <th scope="col">Designação</th>
      <th scope="col">Material</th>
      <th scope="col">Preço</th>
      <th scope="col">Vida Util</th>
      <th scope="col">opcao</th>

    </tr>
  </thead>
  <tbody>
  <?php  foreach ($precarious as $precariou) { ?>
    <tr>
      <td><?= $precariou->code?></td>
      <td><?= $precariou->designation?></td>
      <td><?= $precariou->material?></td>
      <td>
      <?= $precariou->price?>
  </td>
  <td><?= $precariou->lifespan?></td>
      <td> 
    
         
          


          <div class="btn-group position-relative ">
            <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                data-unfold-type="css-animation" data-unfold-duration="300"
                data-unfold-delay="300"
                data-unfold-animation-in="slideInUp"
                data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                <span class="fas fa-ellipsis-v btn-icon__inner"></span>
            </a>

            <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
              
               <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i> Ver', ['view', 'id' =>  $precariou->id], ['class' => 'dropdown-item','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>

               <a href="#" class="open-modal dropdown-item"  data-toggle="tooltip",data-placement="top", title="Atualizar", data-modal-size="modal-lg" data-toggle="modal" data-url="<?= Url::to(['update', 'id'=> $precariou->id]) ?>" data-title="<?= Yii::t('app', 'Preçario') ?> " >
                <i class="far fa-edit btn-soft-primary mr-2"></i> Editar
              </a>

              <a class="delete_precario dropdown-item" href="#" data-url="<?= Url::to(['/'.'categoria-delete/'. $precariou->id])?>"
                data-sim="<?= Yii::t('app', 'Sim') ?>"
                data-nao="<?= Yii::t('app', 'Não') ?>"
                data-title="<?= Yii::t('app', 'Desejas eliminar este categoria?') ?>"
                data-message="<?= Yii::t('app', 'Esta ação não pode ser desfeita.') ?>"
                data-key="<?=  $precariou->id?>"><i class="fas fa-trash-alt btn-soft-danger mr-2"></i>Eliminar
              </a>
              <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id' => $precariou->id], [
                        'class' => 'dropdown-item',
                        'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"",
                        'data' =>
                            [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                    ]) ?>

            </div>
          </div>
    </td>

      </tr>
    <?php }?>  
  </tbody>
</table>
  

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'designation',
            'material',
            'price',
            //'lifespan',
            //'description',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?-->


</div>

  <!-- Modal -->
  <div id="precarious" class="js-modal-window u-modal-window" style="width: 800px;">
        <div class="card">
            <!-- Header -->
            <header class="card-header bg-light py-3 px-5">
                <div class="d-flex justify-content-between align-items-center">
                    <h3 class="h6 mb-0">Preçarios</h3>

                    <button type="button" class="close" aria-label="Close" onclick="Custombox.modal.close();">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
            </header>
            <!-- End Header -->
                    <!-- Body -->
            <div class="card-body p-5">
                    <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
            <!-- End Body -->

            <!-- Footer -->
           
            <!-- End Footer -->
        </div>
    </div>
    <!-- End Modal -->
    <style>
        .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
    
    .grid-view td:nth-child(4) {
        white-space: nowrap;
    }
    tbody tr:nth-child(odd) {
      background-color: #e0e0ea;
    }
    .top {
      box-shadow: 0 3px 6px 0 rgb(140 152 164 / 25%) !important;
    }
    .table {
    border: 1px solid #e7eaf3;
   }
    </style>
<?php if (Yii::$app->request->isAjax): ?>
  <script>
      $('.dropdown-toggle').dropdown();
  </script>
<?php endif; ?>
    <?php
  $script = <<<JS
  $(function () {
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  })

    $(function() {
   
   $(document).on('click','.delete_precario',function(e){
     e.preventDefault();
     var key = $(this).data('key');
     var _title = $(this).data('title');
     var url = $(this).data('url');
     var _message = $(this).data('message');
     var _sim = $(this).data('sim');
     var _nao = $(this).data('nao');
     bootbox.confirm({
       title:'<h3 class="h6 mb-0 modal-title px-2">'+ _title +'</h3>',
       message:'<p class="p-2 px-5">' +_message +'</p>',
       buttons: {
         cancel: {
           label: '<i class="fa fa-times"></i> ' + _nao,
           className: 'bt btn-soft-secondary btn-xs'
         },
         confirm: {
           label: '<i class="fa fa-check"></i> ' + _sim,
           className: 'bt btn-indigo btn-xs'
         }
       },
       callback: function (result) {
         if(result){
           $.ajax( {
             method: "post",
             url:url,
           })
           .done(function( respond ) {
             data = JSON.parse(respond);
             if (data.type === 'error'){
               console.log('dont delete');
             }
             else{
               window.open('precarious?r=precarious&action=delete','_self');
             }
           });
         }
       }
     });
   });



  
 });

JS;

$this->registerJs($script);
?>
