<?php
use yii\helpers\Html;
$this->title = 'Update Precarious: ' . $model->id;
?>
<div class="precarious-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
