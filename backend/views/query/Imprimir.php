<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

\yii\web\YiiAsset::register($this);
?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Avaliação ortopédico</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <div class="btn-group position-relative ">

                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fas fa-briefcase mr-1"></i>Imprimir', ['imprimir', 'id' => $model->id_query], ['class' => 'dropdown-item transition-3d-hover open-modal' ,'target'=>"_blank"]) ?>   
                </div>
                </div>
            </div>
         </div>
        
    </div>
   
    <hr>

    <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
                <table style="width:100%">
                    <h3 class=" font-weight-bold">Dados de Paciente </h3>
                    <tbody>
                        <tr>
                            <td class="align-middle"><b>Nome de Paciente<b></></td>
                            <td class="text-secondary"><?= $model->patients->name?></td>
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Altura<b></></td>
                            <td class="text-secondary" ><?= $model->height?></td>
                        </tr> 
                      
                        <tr>
                            <td class="align-middle"><b>Lado</b></td> <td class="text-secondary" >
                             <?php if(strpos($model->side , '1')):?>
                                    Direita 
                             <?php endif;?>
                             <?php if(strpos($model->side  , '2')):?>
                                Esquerda
                             <?php endif;?>
                             <?php if(strpos($model->side , '3')):?>
                                BIL
                             <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Ortoprotesista</b></td>       <td class="text-secondary"><?= $model->orthoprotectionist?></td>
                        </tr>
                        <tr>
                        <td class="align-middle"><b>Aparelho</b></td> <td class="text-secondary"><?= $model->appliance?></td>
                        </tr>

                    </tbody>
                </table>
                <?php if($model->pathology):?>
                    <h5 class="mt-4 font-weight-bold">Diagnóstico/Patologia:</h5>
                    <p><?= $model->pathology?></p>
                <?php endif;?>
                
                <?php if($model->etiology):?>
                    <h5 class="mt-4 font-weight-bold">Etiologia:</h5>
                    <p><?= $model->etiology?></p>
                <?php endif;?>

                <?php if($model->historic):?>
                    <h5 class="mt-4 font-weight-bold">Histórico:</h5>
                    <p><?= $model->historic?></p>
                <?php endif;?>

                <?php if($model->associates):?>
                    <h5 class="mt-4 font-weight-bold">Problema Associados:</h5>
                    <p><?= $model->associates?></p>
                <?php endif;?>

                <?php if($model->medication):?>
                    <h5 class="mt-4 font-weight-bold">Modeficação atual:</h5>
                    <p><?= $model->medication?></p>
                <?php endif;?>

                <?php if($model->macha):?>
                    <h5 class="mt-4 font-weight-bold">March:</h5>
                    <p><?= $model->macha?></p>
                <?php endif;?>

                <?php if($model->vertibral):?>
                    <h5 class="mt-4 font-weight-bold">C. Vertebal:</h5>
                    <p><?= $model->vertibral?></p>
                <?php endif;?>
               
            
               
            </div>
        </div>
</div>
<?php if( @$type == 'preview' ): ?>
<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
</style>
