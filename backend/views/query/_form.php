<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\models\QueryTepy;
use common\models\Patient;
    
    $countries=Patient::find()->all();
    $listData=ArrayHelper::map($countries,'id_patient','name');
?>

<div class="query-form">

    <?php $form = ActiveForm::begin(); ?>
        
        <div class="row">
        <div class="col-sm-12">
       
                <label for="" class="type-label" >Nome de Paciente<span class="text-danger">*</span></label>
                <div class="form-group">
                    <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Selecionar Paciente'],
                        'pluginOptions' => [
                        'maxlength' => true,
                        'class' => 'form-control  ',
                        'required' => true,
                        'data-msg' => 'Por favor Selecionar Paciente.',
                        'data-error-class' => 'u-has-error',
                        'data-success-class' => 'u-has-success',
                        ],
                    ])->label(false) ?>
                </div>
           
     
            </div>
            <div class="col-sm-4">
                <label for="" class="type-label" >Altura</label>
                
                <?= $form->field($model, 'height')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir altura",
                    'aria-label' => "Introduzir altura",
                    'required' => true,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir altura",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                    ])->label(false)?>
            </div>
            <div class="col-sm-4">
                    <label class="form-label mb-5">Lado</label>
                    <?php echo $form->field($model, 'side[]')->checkboxList(
                         [ 1 => 'Direita', 2 => 'Esquerda', 3 => 'BIL']
                       )->label(false) ;
                    ?>

            </div>
            <div class="col-sm-4">
                <label for="" class="type-label" >Peso</label>
                <?= $form->field($model, 'height')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir altura",
                    'aria-label' => "Introduzir altura",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir altura",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                    ])->label(false)?>
            </div>
            <div class="col-sm-6">
                <label for="" class="type-label" >Ortoprotesista</label>
                <?= $form->field($model, 'orthoprotectionist')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Ortoprotesista",
                    'aria-label' => "Introduzir Ortoprotesista",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Ortoprotesista",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                    ])->label(false)?>
            </div>
            <div class="col-sm-6">
                <label for="" class="type-label" >Aparelho</label>
                <?= $form->field($model, 'appliance')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir Aparelho",
                    'aria-label' => "Introduzir Aparelho",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir Aparelho",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                   
                    ])->label(false)?>
            </div>
            <div class="col-sm-12">
              <label for="" class="type-label" >Diagnóstico/Patologia:</label>
              <?= $form->field($model, 'pathology')->textarea(['placeholder' => "Histórico da doença", 'class' => 'form-control','rows'=>3])->label(false) ?>
            </div>
          
           
            
            <div class="col-sm-12">
                <label for="" class="type-label" >Etiologia</label>
                <?= $form->field($model, 'etiology')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "Introduzir etiologia",
                    'aria-label' => "Introduzir etiologia",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => "Introduzir etiologia",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    ])->label(false)?>
            </div>
           
            
            <div class="col-sm-6">
              <label for="" class="type-label" >Histórico</label>
              <?= $form->field($model, 'historic')->textarea(['placeholder' => "Histórico da doença", 'class' => 'form-control','rows'=>3])->label(false) ?>
            </div>
            <div class="col-sm-6">
              <label for="" class="type-label" >Problema Associados</label>
              <?= $form->field($model, 'associates')->textarea(['placeholder' => "Antecedentes Pessoais", 'class' => 'form-control','rows'=>3])->label(false) ?>
            </div>
            <div class="col-sm-6">
              <label for="" class="type-label" >Modeficação atual</label>
              <?= $form->field($model, 'medication')->textarea(['placeholder' => "Modeficação atuala", 'class' => 'form-control','rows'=>3])->label(false) ?>
            </div>
           
            
            <div class="col-sm-6">
                <label for="" class="type-label" >March</label>
                <!--?= $form->field($model, 'macha')->textarea(['rows' => 6]) ->label(false) ?-->
                <?= $form->field($model, 'macha')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>

            </div>
            <div class="col-sm-6">

                <label for="" class="type-label" >C.Vertebral</label>
                <!--?= $form->field($model, 'vertibral')->textarea(['rows' => 6])->label(false)  ?-->
                <?= $form->field($model, 'vertibral')->textarea(['placeholder' => "", 'class' => 'form-control','rows'=>3])->label(false) ?>
            </div>
           
        </div>
        <div class="col-sm-12 text-right mt-4">
            <div class="form-group">
               <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
                <!--?= Html::submitButton('Guardar', ['class' => 'btn btn-indigo btn-sm rounded']) ?-->
                <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Atualizar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded ' : 'btn btn-indigo btn-sm rounded ','id'=>'btn-save']) ?>
            </div>
        </div>
    

   
    <?php ActiveForm::end(); ?>

</div>
<style>
        .type-label{
        display: block;
        /* text-transform: lowercase; */
        font-size: 80%;
        font-weight: 600;

        }  
        .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
.flatpickr-current-month {
      padding: 0px!important;
  }
  .flatpickr-months {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    /* display: flex; */
    position: relative;
    background-color: #2d1582;
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
    padding: .75rem;
  }
  span.cur-month {
    color: #fff!important;
  }
  .numInputWrapper {
      color: #fff!important;
  }
  svg {
    fill: rgba(255, 255, 255, 0.7);
  }
.flatpickr-day.today {
      border-color: #959ea9;
  }
  .flatpickr-day.today {
      border-color: #2d1582!important;
  }
</style>
