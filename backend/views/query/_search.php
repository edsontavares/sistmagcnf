<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\QuerySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="query-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'id_query_tepy') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'height') ?>

    <?= $form->field($model, 'side') ?>

    <?php // echo $form->field($model, 'orthoprotectionist') ?>

    <?php // echo $form->field($model, 'appliance') ?>

    <?php // echo $form->field($model, 'pathology') ?>

    <?php // echo $form->field($model, 'etiology') ?>

    <?php // echo $form->field($model, 'historic') ?>

    <?php // echo $form->field($model, 'associates') ?>

    <?php // echo $form->field($model, 'medication') ?>

    <?php // echo $form->field($model, 'macha') ?>

    <?php // echo $form->field($model, 'vertibral') ?>

    <?php // echo $form->field($model, 'Weight') ?>

    <?php // echo $form->field($model, 'physical_activity') ?>

    <?php // echo $form->field($model, 'name_physical_activity') ?>

    <?php // echo $form->field($model, 'frequency') ?>

    <?php // echo $form->field($model, 'duration') ?>

    <?php // echo $form->field($model, 'session') ?>

    <?php // echo $form->field($model, 'attended1') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'anticident') ?>

    <?php // echo $form->field($model, 'tobacco') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'food_examination') ?>

    <?php // echo $form->field($model, 'physical_exploration') ?>

    <?php // echo $form->field($model, 'moscular_evaluation') ?>

    <?php // echo $form->field($model, 'joint_evaluation') ?>

    <?php // echo $form->field($model, 'neurological_evaluation') ?>

    <?php // echo $form->field($model, 'orthopedic_test') ?>

    <?php // echo $form->field($model, 'treatment') ?>

    <?php // echo $form->field($model, 'hit') ?>

    <?php // echo $form->field($model, 'recommendation') ?>

    <?php // echo $form->field($model, 'weith') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
