<?php

use yii\helpers\Html;

use yii\helpers\Url;
$this->title = 'Avaliação da ortopedista';
?>
<div class="query-create">
<?php if (Yii::$app->session->hasFlash("error-create")): ?>
        <div class="alert-success alert alert-dismissible fade show" role="alert">
            <?= Yii::$app->session->getFlash("success-create") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
    <div class="row mx-gutters-2">
        <div class="col-md-10">
        <h4 class="font-weight-bold"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="col-md-2">
            <a class="float-right " href="<?= Url::to(['/consulta-ortopidia'])?>">Voltar</a>
        </div>
    </div>
    <hr>
    <div class="card">
         <div class="card-body shadow-sm p-4">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
     </div>
</div>
