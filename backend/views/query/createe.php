<?php

use yii\helpers\Html;

use yii\helpers\Url;
$this->title = 'Consulta Fisioterapia';
?>
<div class="query-create">
    <div class="row mx-gutters-2">
        <div class="col-md-10">
        <h4 class="font-weight-bold"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="col-md-2">
            <a class="float-right " href="<?= Url::to(['/consulta-fisioterapia'])?>">Voltar</a>
        </div>
    </div>
    <hr>
    <div class="card">
         <div class="card-body shadow-sm p-4">
            <?= $this->render('_formm', [
                'model' => $model,
            ]) ?>
        </div>
     </div>

</div>
