<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;


?>
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert">
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
<?= $this->render('/auxiliar/menu',[

]) ?>
<h1>ghghgh</h1>

<div class="employees-index mt-6">

    <div class="row mb-3">
      <div class="col-sm-6">
         <?= Html::a('Adicionar Consulta', ['createe'], ['class' => 'btn btn-nr btn-indigo font-weight-bold ']) ?>

      </div>
      <div class="col-sm-6">
          <form role="search" class="app-search">
              <div class="form-group mb-0">
                  <input type="text" class="form-control input-searc" placeholder="Search..">
                  <button type="submit" class="buton-search"><i class="fa fa-search icon-seach"></i></button>
              </div>
          </form>
      </div>
    </div>
    <table class="table">
  <thead class="top">
    <tr>
      
     
      <th scope="col">Paciente</th>
      <th scope="col">Data de Inicio</th>
      <th scope="col">Data Fim</th>
      <th scope="col">Estado</th>
      <th scope="col">Opção</th>

    </tr>
  </thead>
  <tbody>
  <?php  foreach ($fisioterapia as $fisioterapias) {  ?>
    <?php if($fisioterapias->state == 10 || $fisioterapias->state == 11 || $fisioterapias->state == 12): ?>
      <tr>
        <td>
        <?php if($fisioterapias->patients):?>
           <?= 
                 $fisioterapias->patients->name?>
        <?php endif;?>
        </td>
        <td><?= $fisioterapias->start_date?></td>
        <td>
            <?= $fisioterapias->end_date?>
        
          </td>
          <td>
          <?php if($fisioterapias->state == 11):?>
            <span class="badge badge-primary">Em Tratamento</span>
          <?php elseif($fisioterapias->state == 12):?>
            <span class="badge badge-warning">Agendado paraa iniciar tratamento</span>
          <?php elseif($fisioterapias->state == 10):?>
            <span class="badge badge-info">Em Avaliação</span>
          <?php elseif($fisioterapias->state == 33):?>
            <span class="badge badge-warning">Enviado Ao Medico</span>
          <?php endif;?>
          </td>
          <td> 
              <?= Html::a('<i class="fas fa-eye btn-soft-secondary dropdown-item-icon"></i>', ['views', 'id' => $fisioterapias->id_query], ['class' => '','data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Ver"]) ?>
            
                <?= Html::a('<i class="fas fa-pencil-alt dropdown-item-icon"></i> ', ['query/updates', 'id' => $fisioterapias->id_query], ['class'=>"",'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Atualizar"])?>
            
              
              <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger dropdown-item-icon"></i>', ['delete', 'id' => $fisioterapias->id_query], [
                  'class' => '',
                  'data-toggle'=>"tooltip",'data-placement'=>"top", 'title'=>"Eliminar",
                  'data' =>
                      [
                          'confirm' => 'Are you sure you want to delete this item?',
                          'method' => 'post',
                      ],
              ]) ?>
        
        
            </td>
        </tr>
      <?php endif;?>  
    <?php }?>  
  </tbody>
</table>
<div class="pagination">
  <?= LinkPager::widget(['pagination'=> $pagination])?>
</div>


</div>
<style>
    .btn-nr{
      padding: 0.625rem 1.125rem;
    font-size: 0.875rem;
    line-height: 1.5;
    }

    .input-searc{
      border: 1px solid #e0e0ea;
    font-size: 13px;
    height: 34px;
    padding-left: 18px;
    padding-right: 40px;
    margin-right: 16px;
    background: #e0e0ea;
    -webkit-box-shadow: none;
    box-shadow: none;
    border-radius: 30px;
    
    }
    .buton-search{
      position: absolute;
    top: 10px;
    right: 26px;
    display: block;
    color: #9ca8b3;
    font-size: 11px;
    border: none;
    background-color: transparent;
    }
    .btn-nr {
      color: #fff;
     
    font-size: 0.875rem;
    }
   
    
</style>
