<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;
?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style="text-align: center; margin-top: -0.5cm;" >    
    <!--h6><img src="./recurso/img/Logo.jpg" style="margin-top: -0.5cm;text-align: center;width: 50px; height: 50px;" alt="user"></h6-->
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    
                    </thead>


                    <tbody>
                    <tr>
                        <td class="font-weight-bold"><b>Nome</b></td>
                        <td><?= $model->patients->name?></td>
                    
                    </tr>
                    <tr>
                        <td class="font-weight-bold"><b>Profissão</b></td>
                        <td><?= $model->patients->profession?></td>
                        
                    </tr>
                    <tr>
                        <td class="font-weight-bold"><b>Contacto</b></td>
                        <td><?= $model->patients->telephone?></td>
                        
                    </tr>
                    <tr>
                        <td class="font-weight-bold"><b>Endereço</b></td>
                        <td><?= $model->patients->naturalness?></td>
                        
                    </tr>
                    <tr>
                        <td class="font-weight-bold"><b>Sexo</b></td>
                        <td><?= $model->patients->sex?></td>
                        
                    </tr>
                    
                    </tbody>
                </table>

            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->




</div>
<?php if($model->historic): ?>
    <h4 class="font-weight-bold">historico da doença </h4>
    <p><?= $model->historic?></p>
<?php endif; ?>
<?php if($model->associates): ?>
    <h4 class="font-weight-bold">Antecedentes Pessoais </h4>
    <p><?= $model->associates?></p>
<?php endif; ?>
<?php if($model->medication): ?>
    <h4 class="font-weight-bold">Modeficação atual </h4>
    <p><?= $model->medication?></p>
<?php endif; ?>
<?php if($model->food_examination): ?>
    <h4 class="font-weight-bold">Exames Complimentaris</h4>
    <p><?= $model->food_examination?></p>
<?php endif; ?>
<?php if($model->physical_exploration): ?>
    <h4 class="font-weight-bold">Exploração Fisica:</h4>
    <p><?= $model->physical_exploration ?></p>
<?php endif; ?>
<?php if($model->measures): ?>
    <h4 class="font-weight-bold">Medidas: </h4>
    <p><?= $model->measures?></p>
<?php endif; ?>
<?php if($model->moscular_evaluation): ?>
    <h4 class="font-weight-bold">Avaliação Moscular: </h4>
    <p><?= $model->moscular_evaluation ?></p>
<?php endif; ?>
<?php if($model->neurological_evaluation): ?>
    <h4 class="font-weight-bold">Avaliação Neurológica: </h4>
    <p><?= $model->neurological_evaluation ?></p>
<?php endif; ?>
<?php if($model->sensitivity): ?>
    <h4 class="font-weight-bold">Sensibilidade: </h4>
    <p><?= $model->sensitivity ?></p>
<?php endif; ?>
<?php if($model->reflections): ?>
    <h4 class="font-weight-bold">Reflexos:</h4>
    <p><?= $model->reflections ?></p>
<?php endif; ?>
<?php if($model->orthopedic_test): ?>
    <h4 class="font-weight-bold">Teste Ortopidia e outros: </h4>
    <p><?= $model->orthopedic_test ?></p>
<?php endif; ?>
<?php if($model->treatment ): ?>
    <h4 class="font-weight-bold">Objectivo de Tratamento: </h4>
    <p><?= $model->treatment  ?></p>
<?php endif; ?>
<?php if($model->recommendation ): ?>
    <h4 class="font-weight-bold">Resultado de Tratamento: </h4>
    <p><?= $model->recommendation  ?></p>
<?php endif; ?>


<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<div class="form-group modal-action">
    <a href="#" class="btn btn-primary cancel close-modal" style="margin-right: 15px;"><i class="fa fa-times"></i> Fechar</a>
    <a href="#" class="btn btn-primary bt" style="margin-right: 15px;" onclick="print()"><i class="fa fa-print"></i> Imprimir</a>
    <a href="/u/curriculum/baixar" class="btn btn-primary bt" style="margin-right: 15px;"><i class="fa fa-download"></i> Baixar</a>
</div>


<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
</style>
