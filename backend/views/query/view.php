<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

\yii\web\YiiAsset::register($this);

?>
<?php if (Yii::$app->session->hasFlash("create-sucesso")): ?>
    <div class="alert-success alert alert-dismissible fade show" role="alert">
        <?= Yii::$app->session->getFlash("create-sucesso") ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Avaliação ortopedista</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/consulta-ortopidia'])?>">Voltar</a>
            <div class="btn-group position-relative ">

                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="far fa-edit btn-soft-primary mr-2"></i> Editar', ['update', 'id' => $model->id_query], ['class'=>"dropdown-item"])?>
                     <?= Html::a('<i class="fas fa-briefcase mr-1"></i>Gerar Relatorio', ['baixar', 'id' => $model->id_query], ['class' => 'dropdown-item transition-3d-hover ' ,'target'=>"_blank"]) ?>
                    <?php if($model->state == 11): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_query], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>
                    <?php if($model->state == 1 ): ?>
                        
                        <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Envia Pagamento', ['query/pagament', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                        <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Cancelado ', ['query/cancelar', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                        <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviado Fisioterapia ', ['query/fisioterapia', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                        <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviado Medico', ['query/medico', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                    
                    <?php endif;?>
                    <?php if($model->state == 12 ): ?>
                    <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviar Tratamento ', ['query/updated', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                    <?php endif;?>
                </div>
            </div>
            </div>
         </div>
        
    </div>
   
    <hr>

    <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
                <table style="width:100%">
                    <h3 class=" font-weight-bold">Dados de Paciente </h3>
                    <tbody>
                        <tr>
                            <td class="align-middle"><b>Nome de Paciente<b></></td>
                            <td class="text-secondary"><?= $model->patients->name?></td>
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Altura<b></></td>
                            <td class="text-secondary" ><?= $model->height?></td>
                        </tr> 
                      
                        <tr>
                            <td class="align-middle"><b>Lado</b></td> <td class="text-secondary" >
                             <?php if(strpos($model->side , '1')):?>
                                    Direita 
                             <?php endif;?>
                             <?php if(strpos($model->side  , '2')):?>
                                Esquerda
                             <?php endif;?>
                             <?php if(strpos($model->side , '3')):?>
                                BIL
                             <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Ortoprotesista</b></td>       <td class="text-secondary"><?= $model->orthoprotectionist?></td>
                        </tr>
                        <tr>
                        <td class="align-middle"><b>Aparelho</b></td> <td class="text-secondary"><?= $model->appliance?></td>
                        </tr>

                    </tbody>
                </table>
                <?php if($model->pathology):?>
                    <h5 class="mt-4 font-weight-bold">Diagnóstico/Patologia:</h5>
                    <p><?= $model->pathology?></p>
                <?php endif;?>
                
                <?php if($model->etiology):?>
                    <h5 class="mt-4 font-weight-bold">Etiologia:</h5>
                    <p><?= $model->etiology?></p>
                <?php endif;?>

                <?php if($model->historic):?>
                    <h5 class="mt-4 font-weight-bold">Histórico:</h5>
                    <p><?= $model->historic?></p>
                <?php endif;?>

                <?php if($model->associates):?>
                    <h5 class="mt-4 font-weight-bold">Problema Associados:</h5>
                    <p><?= $model->associates?></p>
                <?php endif;?>

                <?php if($model->medication):?>
                    <h5 class="mt-4 font-weight-bold">Modeficação atual:</h5>
                    <p><?= $model->medication?></p>
                <?php endif;?>

                <?php if($model->macha):?>
                    <h5 class="mt-4 font-weight-bold">March:</h5>
                    <p><?= $model->macha?></p>
                <?php endif;?>

                <?php if($model->vertibral):?>
                    <h5 class="mt-4 font-weight-bold">C. Vertebal:</h5>
                    <p><?= $model->vertibral?></p>
                <?php endif;?>
               
            
               
            </div>
        </div>
</div>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
</style>
