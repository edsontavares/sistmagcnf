<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
\yii\web\YiiAsset::register($this);


?>
  <div class="row">
        <div class="col-sm-12 text-right">
        <a class="" href="<?= Url::to(['/consulta-fisioterapia'])?>">Voltar</a>
        <div class="btn-group position-relative ">

            <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                data-unfold-type="css-animation" data-unfold-duration="300"
                data-unfold-delay="300"
                data-unfold-animation-in="slideInUp"
                data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                <span class="fas fa-ellipsis-v btn-icon__inner"></span>
            </a>

            <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                <?= Html::a('<i class="fas fa-briefcase mr-1"></i>Relatorio', ['baixar', 'id' => $model->id_query], ['class' => 'dropdown-item transition-3d-hover open-modal' ,'target'=>"_blank"]) ?>
                <?php if($model->state == 11): ?>
                     <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_query], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                <?php endif;?>
                <?php if($model->state == 10 ): ?>
                    
                    <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Envia Pagamento', ['query/pagamento', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover"])?>
                    <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Cancelado ', ['query/cancelar', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                    <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviado Ortópidia ', ['query/ortopidia', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover"])?>
                    <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviado Medico', ['query/medico', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                
                <?php endif;?>
                <?php if($model->state == 12 ): ?>
                  <?= Html::a('<i class="fas fa-paper-plane dropdown-item-icon"></i>Enviar Tratamento ', ['query/updated', 'id' => $model->id_query], ['class'=>"dropdown-item transition-3d-hover "])?>
                <?php endif;?>
            </div>
            </div>
        </div>
    </div>
    <hr style="border:1px solid #e7eaf3!important;">

      <!-- end row -->
      <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
                <table style="width:100%">
                    <h3 class=" font-weight-bold">Dados de Paciente </h3>
                    <tbody>
                        <tr>
                            <td class="align-middle"><b>Nome de Paciente<b></></td>
                            <td class="text-secondary"><?= $model->patients->name?></td>
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Inicio de TTO<b></></td>
                            <td class="text-secondary" ><?= $model->start_date?></td>
                        </tr> 
                      
                        <tr>
                            <td class="align-middle"><b>Fim de TTO</b></td> 
                            <td class="text-secondary" ><?= $model->end_date?></td>
                           
                        </tr>
                        <tr>
                            <td class="align-middle"><b>Nº Sessões</b></td>       <td class="text-secondary"><?= $model->session?></td>
                        </tr>
                        <tr>
                        <td class="align-middle"><b>Frequência</b></td> <td class="text-secondary"><?= $model->frequency?></td>
                        </tr>
                        <tr>
                        <td class="align-middle"><b>Idade</b></td> <td class="text-secondary"><?= $model->age?></td>
                        </tr>

                    </tbody>
                </table>
                <?php if($model->diagnosis):?>
                    <h5 class="mt-4 font-weight-bold">Diagonóstico:</h5>
                    <p><?= $model->diagnosis ?></p>
                <?php endif;?>

                <?php if($model->historic):?>
                    <h5 class="mt-4 font-weight-bold">Histórico:</h5>
                    <p><?= $model->historic?></p>
                <?php endif;?>

                <?php if($model->associates):?>
                    <h5 class="mt-4 font-weight-bold">Antecedentes Pessoais:</h5>
                    <p><?= $model->associates?></p>
                <?php endif;?>

                <?php if($model->medication):?>
                    <h5 class="mt-4 font-weight-bold">Modeficação atual:</h5>
                    <p><?= $model->medication?></p>
                <?php endif;?>
                
                <?php if($model->food_examination):?>
                    <h5 class="mt-4 font-weight-bold">Exames Complimentaris:</h5>
                    <p><?= $model->food_examination?></p>
                <?php endif;?>

                <?php if($model->physical_exploration):?>
                    <h5 class="mt-4 font-weight-bold">Exploração Fisica:</h5>
                    <p><?= $model->physical_exploration?></p>
                <?php endif;?>

                <?php if($model->measures):?>
                    <h5 class="mt-4 font-weight-bold">Medidas:</h5>
                    <p><?= $model->measures?></p>
                <?php endif;?>

                <?php if($model->moscular_evaluation):?>
                    <h5 class="mt-4 font-weight-bold">Avaliação Moscular:</h5>
                    <p><?= $model->moscular_evaluation?></p>
                <?php endif;?>

                <?php if($model->joint_evaluation):?>
                    <h5 class="mt-4 font-weight-bold">Avaliação Articular:</h5>
                    <p><?= $model->joint_evaluation?></p>
                <?php endif;?>
                <?php if($model->neurological_evaluation):?>
                    <h5 class="mt-4 font-weight-bold">Avaliação Neurológica:</h5>
                    <p><?= $model->neurological_evaluation?></p>
                <?php endif;?>
                <?php if($model->sensitivity):?>
                    <h5 class="mt-4 font-weight-bold">Sensibilidade:</h5>
                    <p><?= $model->sensitivity?></p>
                <?php endif;?>
                <?php if($model->reflections):?>
                    <h5 class="mt-4 font-weight-bold">Reflexos:</h5>
                    <p><?= $model->reflections?></p>
                <?php endif;?>
                <?php if($model->orthopedic_test):?>
                    <h5 class="mt-4 font-weight-bold">Teste Ortopidia e outros:</h5>
                    <p><?= $model->orthopedic_test?></p>
                <?php endif;?>
                <?php if($model->treatment):?>
                    <h5 class="mt-4 font-weight-bold">Objectivo de Tratamento::</h5>
                    <p><?= $model->treatment?></p>
                <?php endif;?>
                <?php if($model->recommendation):?>
                    <h5 class="mt-4 font-weight-bold">Resultado de Tratamento:</h5>
                    <p><?= $model->recommendation?></p>
                <?php endif;?>
              
               
            
               
            </div>
        </div>

        <style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
</style>


