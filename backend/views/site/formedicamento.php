<div class="border-bottom mb-2 pb-1">
  <div class="row">

    <div class="col-md-4">
      <h1 class="font-weight-bold h5">Adicionar Medicamentos</h1>
    </div>

  </div>
</div>

<div class="card card-body p-4">
	<div class="row mx-gutters-2">
		<div class="col-lg-6 mb-4">
		    <label for="exampleInputEmail1">Nome medicamento<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="Nome medicamento">
		</div>

		<div class="col-lg-3 mb-3">
		    <label for="exampleInputEmail1">Nome comercial<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="Nome comercial">
		</div>
		
		<div class="col-lg-3 mb-4">
		    <label for="exampleInputEmail1">Tipo<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="Tipo">
		</div>
		
		<div class="col-lg-4 mb-4">
		    <label for="exampleInputEmail1">Categoria<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="Categoria">
		</div>
		
		<div class="col-lg-2 mb-4">
		    <label for="exampleInputEmail1">preço<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="preço">
		</div>
		
		<div class="col-lg-3 mb-4">
		    <label for="exampleInputEmail1">codigo de barra<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="codigo de barra">
		</div>

		<div class="col-lg-3 mb-4">
		    <label for="exampleInputEmail1">validade<span class="text-danger">*</span></label>
		    <input type="text" class="form-control form-control-sm" aria-describedby="emailHelp" placeholder="validade">
		</div>
	</div>

	<div class="row mx-gutters-2">
		<div class="col-lg-2">
			<form>
			  <!-- File Attachment Input -->
			  <label class="file-attachment-input" for="fileAttachmentInput">
			    <small>Carregar Foto</small>
			    <input id="fileAttachmentInput" name="file-attachment" type="file" class="file-attachment-input__label">
			  </label>
			  <!-- End File Attachment Input -->
			</form>	
		</div>

		<div class="col-lg-10">	
			<div class="form-group">
			    <label for="exampleFormControlTextarea1">descrição do medicamento</label>
			    <textarea class="form-control form-control-sm" id="exampleFormControlTextarea1" rows="3"></textarea>
			</div>
		</div>
	</div>

	<div class="form-group text-sm-right mt-6">
		<a href="" class="btn btn-soft-secondary btn-sm rounded">Cancelar</a>
	    <a href="" class="btn btn-warning btn-sm rounded">Criar</a>
	</div>

</div>