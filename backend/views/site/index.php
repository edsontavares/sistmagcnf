<?php
$this->title = 'Cenorf';
?>
    <div class="border-bottom pb-2 mb-4">
      <div class="row mx-gutters-2">
        <div class="col-md-4">
          <h4 class="font-weight-bold"><?=yii::t('app','Administração de cenorf')?></h4>
        </div>

        <div class="col-md-8">
            <?php
              $year = [
                  '' => yii::t('app','Resultado por ano')
              ];
              for ($i = date('Y'); $i >= 2015; $i--) {
                  $year[$i] = $i;
              }
          ?>
          <?= yii\helpers\Html::dropDownList('name', @$_GET['year'] ? $_GET['year'] : null, $year, [
              'id' => 'select-year',
              'class' => 'js-select selectpicker dropdown-select float-right border',
              'data-style' => 'btn-sm btn-white'
          ]) ?>
        </div>

      </div>
    </div>
<div class="site-index">
    <div class="body-content">
    <div class="row mx-gutters-2 mb-3"><!-- card-deck d-block d-lg-flex card-lg-gutters-3 -->
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $patient,
                'title' => 'Total de paciente'
            ]) ?>
        </div>
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $employees,
                'title' => 'Total de funcionario'
            ]) ?>
        </div>
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $query,
                'title' => 'Total de consulta feito'
            ]) ?>
        </div>
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $query_otp,
                'title' => 'Total consulta ortopidia'
            ]) ?>
        </div>
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $query_ftp,
                'title' => 'Total consulta fisioterapia'
            ]) ?>
        </div>
        <div class="col-lg-4 mb-3">
            <?= $this->render('dashboard/cardNumber.php', [
                'iconClass' => 'fas fa-chalkboard',
                 'number' => $total_session,
                'title' => 'Total de Sessão'
            ]) ?>
        </div>
        <div class="col-lg-6 mt-3">
            <div class="card-body shadow">
                <canvas id="myChartCat" height="200"></canvas>
            </div>
        </div>
        <!-- grafico sessao -->
        <div class="col-lg-6 mt-3">
            <div class="card-body shadow">
                <canvas id="myChartSesstion" height="200"></canvas>
            </div>
        </div>
        <div class="col-12 mt-3">
            <div class="card-body shadow">
                <canvas id="myChart" height="100"></canvas>
            </div>
        </div>
        
        <div class="col-12 mt-3">
            <div class="card-body shadow">
                <canvas id="myChartCandidatura" height="100"></canvas>
            </div>
        </div>
        <div class="col-12 mt-3">
            <div class="card-body shadow">
                <canvas id="myChartConsulta" height="100"></canvas>
            </div>
        </div>
    </div>
    </div>
</div>
<?php 
    $localizacao = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'paciente por ilha'));
    $localizacaoLabel = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Paciente'));

    $anuciosMes = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Paciente por mês'));
    $anuciosMesLabel = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Paciente'));

    $candidatura = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Sessão por mês'));
    $candidaturaLabel = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Sessão'));

    $consulta = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Total de avaliacão  por mês'));
    $consultaLabel = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Avaliacão'));

    $sesionMes = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Total de sessão por mês'));
    $sesionMesLabel = \yii\helpers\Json::htmlEncode(  \Yii::t('app', 'Sessão'));

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
<script>
    var textos = <?php echo $localizacao ?>;
    var anucioMes = <?php echo $anuciosMes ?>;
    var candidaturas = <?php echo $candidatura?>;
    var consulta  = <?php echo $consulta ?>;
    var sessionMes = <?php echo $sesionMes ?>;

    var textosLabel = <?php echo $localizacaoLabel ?>;
    var anucioMesLabel = <?php echo $anuciosMesLabel ?>;
    var candidaturasLabel = <?php echo $candidaturaLabel?>;
    var consultaLabel = <?php echo $consultaLabel?>;
    var sessionMesLabel = <?php echo $sesionMesLabel?>;

    let ctxCat = document.getElementById('myChartCat').getContext('2d');
    let ctx = document.getElementById('myChart').getContext('2d');
    let ctxCandidatura = document.getElementById('myChartCandidatura').getContext('2d');
    let ctxConsulta = document.getElementById('myChartConsulta').getContext('2d');
    let ctxsessetion = document.getElementById('myChartSesstion').getContext('2d');

      // Global Options
      Chart.defaults.global.defaultFontFamily = 'Lato';
      Chart.defaults.global.defaultFontSize = 18;
      Chart.defaults.global.defaultFontColor = '#777';

        // patient por Ilhas
        let labels = "<?= implode(',', $patientByIsland['label']) ?>".split(',');
        let data = "<?= implode(',', $patientByIsland['data']) ?>".split(',');
        new Chart(ctxCat, {
            type: 'radar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data: {
                labels: labels,
                datasets: [{
                    label: textosLabel,
                    data: data,
                    //backgroundColor:'green',
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 0,
                    borderColor: '#777',
                    hoverBorderWidth: 1,
                    hoverBorderColor: '#000'
                }]
            },
            options: {
                title: {
                    display: true,
                    text: textos,
                    fontSize: 20
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontColor: '#000',
                        fontSize: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                tooltips: {
                    enabled: true
                }
            }
        });

      // paciente por mes
      labels = "<?= implode(',', $paciente['label']) ?>".split(',');
        data = "<?= implode(',', $paciente['data']) ?>".split(',');
        new Chart(myChart, {
            type: 'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data: {
                labels: labels,
                datasets: [{
                    label: anucioMesLabel,
                    data: data,
                    //backgroundColor:'green',
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 0,
                    borderColor: '#777',
                    hoverBorderWidth: 1,
                    hoverBorderColor: '#000'
                }]
            },
            options: {
                title: {
                    display: true,
                    text: anucioMes,
                    fontSize: 20
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontColor: '#000',
                        fontSize: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                tooltips: {
                    enabled: true
                }
            }
        });

        // sessao por mes
        labels = "<?= implode(',', $sessao['label']) ?>".split(',');
        data = "<?= implode(',', $sessao['data']) ?>".split(',');
        new Chart(ctxCandidatura, {
            type: 'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data: {
                labels: labels,
                datasets: [{
                    label: candidaturasLabel,
                    data: data,
                    //backgroundColor:'green',
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 0,
                    borderColor: '#777',
                    hoverBorderWidth: 1,
                    hoverBorderColor: '#000'
                }]
            },
            options: {
                title: {
                    display: true,
                    text: candidaturas,
                    fontSize: 20
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontColor: '#000',
                        fontSize: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                tooltips: {
                    enabled: true
                }
            }
        });


         // consulta por mes otp
         labels = "<?= implode(',', $tepy_query_otp['label']) ?>".split(',');
        data = "<?= implode(',', $tepy_query_otp['data']) ?>".split(',');
        new Chart(ctxConsulta, {
            type: 'line', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data: {
                labels: labels,
                datasets: [{
                    label: consultaLabel,
                    data: data,
                    //backgroundColor:'green',
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 0,
                    borderColor: '#777',
                    hoverBorderWidth: 1,
                    hoverBorderColor: '#000'
                }]
            },
            options: {
                title: {
                    display: true,
                    text: consulta,
                    fontSize: 20
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontColor: '#000',
                        fontSize: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                tooltips: {
                    enabled: true
                }
            }
        });

          // sesetion
        labels = "<?= implode(',', $setion['label']) ?>".split(',');
        data = "<?= implode(',', $setion['data']) ?>".split(',');
        new Chart(ctxsessetion, {
            type: 'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
            data: {
                labels: labels,
                datasets: [{
                    label: sessionMesLabel,
                    data: data,
                    //backgroundColor:'green',
                    backgroundColor: [
                        'rgba(153, 102, 255, 0.6)',
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                        'rgba(75, 192, 192, 0.6)',
                        'rgba(255, 159, 64, 0.6)',
                        'rgba(255, 99, 132, 0.6)'
                    ],
                    borderWidth: 0,
                    borderColor: '#777',
                    hoverBorderWidth: 1,
                    hoverBorderColor: '#000'
                }]
            },
            options: {
                title: {
                    display: true,
                    text: sessionMes,
                    fontSize: 20
                },
                legend: {
                    display: false,
                    position: 'right',
                    labels: {
                        fontColor: '#000',
                        fontSize: 12
                    }
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }
                },
                tooltips: {
                    enabled: true
                }
            }
        });

</script>
<?php
    $script = <<<JS
    $(function() {
      $('#select-year').change(function() {
        location.href = location.origin + '/sistmagcnf/admin/?year=' + $(this).val();
      })
    })
JS;

    $this->registerJS($script);
?>