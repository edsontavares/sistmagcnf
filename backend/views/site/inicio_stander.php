<div class="alert alert-warning" role="alert">
  <h4 class="h5 alert-heading">Painel administração de "nome farmacia"</h4>
</div>

<div class="row mx-gutters-2">
	<div class="col-lg-3">
		<!-- Card -->
	  	<div class="card mb-3 mb-lg-0">
		    <div class="card-body p-5">
		      <div class="media align-items-center">
		        <span class="btn btn-lg btn-icon btn-soft-primary rounded-circle mr-4">
		          <span class="fas fa-dollar-sign btn-icon__inner"></span>
		        </span>
		        <div class="media-body">
		          <span class="d-block font-size-3">$45.99</span>
		          <h2 class="h6 text-secondary font-weight-normal mb-0">Available balance</h2>
		        </div>
		      </div>
		    </div>
	  	</div>
	  <!-- End Card -->
	</div>

	<div class="col-lg-3">
		<!-- Card -->
	  	<div class="card mb-3 mb-lg-0">
		    <div class="card-body p-5">
		      <div class="media align-items-center">
		        <span class="btn btn-lg btn-icon btn-soft-primary rounded-circle mr-4">
		          <span class="fas fa-dollar-sign btn-icon__inner"></span>
		        </span>
		        <div class="media-body">
		          <span class="d-block font-size-3">$45.99</span>
		          <h2 class="h6 text-secondary font-weight-normal mb-0">Available balance</h2>
		        </div>
		      </div>
		    </div>
	  	</div>
	  <!-- End Card -->
	</div>

	<div class="col-lg-3">
		<!-- Card -->
	  	<div class="card mb-3 mb-lg-0">
		    <div class="card-body p-5">
		      <div class="media align-items-center">
		        <span class="btn btn-lg btn-icon btn-soft-primary rounded-circle mr-4">
		          <span class="fas fa-dollar-sign btn-icon__inner"></span>
		        </span>
		        <div class="media-body">
		          <span class="d-block font-size-3">$45.99</span>
		          <h2 class="h6 text-secondary font-weight-normal mb-0">Available balance</h2>
		        </div>
		      </div>
		    </div>
	  	</div>
	  <!-- End Card -->
	</div>

	<div class="col-lg-3">
		<!-- Card -->
	  	<div class="card mb-3 mb-lg-0">
		    <div class="card-body p-5">
		      <div class="media align-items-center">
		        <span class="btn btn-lg btn-icon btn-soft-primary rounded-circle mr-4">
		          <span class="fas fa-dollar-sign btn-icon__inner"></span>
		        </span>
		        <div class="media-body">
		          <span class="d-block font-size-3">$45.99</span>
		          <h2 class="h6 text-secondary font-weight-normal mb-0">Available balance</h2>
		        </div>
		      </div>
		    </div>
	  	</div>
	  <!-- End Card -->
	</div>
</div>

<div class="row mx-gutters-2 mt-4">
	<div class="col-lg-6">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-4 px-4 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Receita</h4>

		      <!-- Settings Dropdown -->
		      	<div class="position-relative">
		        	<a id="balanceSettingsDropdownInvoker" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="javascript:;" role="button"
		           aria-controls="balanceSettingsDropdown"
		           aria-haspopup="true"
		           aria-expanded="false"
		           data-unfold-event="click"
		           data-unfold-target="#balanceSettingsDropdown"
		           data-unfold-type="css-animation"
		           data-unfold-duration="300"
		           data-unfold-delay="300"
		           data-unfold-hide-on-scroll="true"
		           data-unfold-animation-in="slideInUp"
		           data-unfold-animation-out="fadeOut">
		          <span class="fas fa-ellipsis-h btn-icon__inner"></span>
		        	</a>

		        	<div id="balanceSettingsDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="balanceSettingsDropdownInvoker" style="min-width: 190px;">
		          		<a class="dropdown-item" href="#">
		            		<small class="fas fa-layer-group dropdown-item-icon"></small>
				            Ano 2020
				          </a>
				          <a class="dropdown-item" href="#">
				            <small class="fas fa-layer-group dropdown-item-icon"></small>
				            Ano 2019
				          </a>
				          <a class="dropdown-item" href="#">
				            <small class="fas fa-layer-group dropdown-item-icon"></small>
				            Ano 2018
				          </a>
		        	</div>
		      	</div>
		      <!-- End Settings Dropdown -->
		    </div>
		    <!-- End Title & Settings -->

		    <hr class="mt-3 mb-4">

		    <div class="row">
		    	<?php
					foreach (range(1,12) as $ordem)
					{
				?>	
		    	<div class="col-1">
			        <div class="js-vr-progress progress-vertical rounded mb-2">
			          <div class="js-vr-progress-bar bg-warning rounded-bottom" role="progressbar" style="height: 35%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
			        </div>
			        <div class="text-center">
			          <h4 class="small mb-0">J</h4>
			        </div>
		      	</div>
		      	<?php
					}
				?>
		    </div>
		    <!-- End Balance Info -->
		  </div>
		</div>
		<!-- End Stats -->
	</div>

	<div class="col-lg-3">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-4 px-4 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Top 5 Categoria mais Vendido</h4>
		    </div>
		    <!-- End Title & Settings -->
		    <hr class="mt-3 mb-4">

		    <ul class="list-group list-group-borderless">
	    	<?php
				foreach (range(1,5) as $ordem)
				{
			?>
			  <li class="list-group-item p-2">
			    Anti-inflamatorio
			  </li>
			<?php
				}
			?>
			</ul>

		  </div>
		</div>
		<!-- End Stats -->
	</div>

	<div class="col-lg-3">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-4 px-4 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Top 5 Remedio mais Vendido</h4>
		    </div>
		    <!-- End Title & Settings -->
		    <hr class="mt-3 mb-4">

		    <ul class="list-group list-group-borderless">
	    	<?php
				foreach (range(1,5) as $ordem)
				{
			?>
			  <li class="list-group-item p-2">Mascara</li>
			<?php
				}
			?>
			</ul>

		  </div>
		</div>
		<!-- End Stats -->
	</div>
	
</div>

<div class="row mx-gutters-2 mt-4">
	<div class="col-lg-6">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-4 px-4 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Estatisticas de Vendas online</h4>

		      <!-- Settings Dropdown -->
		      <div class="position-relative">
		        <a id="balanceSettingsDropdownInvoker" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="javascript:;" role="button"
		           aria-controls="balanceSettingsDropdown"
		           aria-haspopup="true"
		           aria-expanded="false"
		           data-unfold-event="click"
		           data-unfold-target="#balanceSettingsDropdown"
		           data-unfold-type="css-animation"
		           data-unfold-duration="300"
		           data-unfold-delay="300"
		           data-unfold-hide-on-scroll="true"
		           data-unfold-animation-in="slideInUp"
		           data-unfold-animation-out="fadeOut">
		          <span class="fas fa-ellipsis-h btn-icon__inner"></span>
		        </a>

		        <div id="balanceSettingsDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="balanceSettingsDropdownInvoker" style="min-width: 190px;">
		          <a class="dropdown-item" href="#">
		            <small class="fas fa-cogs dropdown-item-icon"></small>
		            Settings
		          </a>
		          <a class="dropdown-item" href="#">
		            <small class="fas fa-cloud-download-alt dropdown-item-icon"></small>
		            Download
		          </a>
		          <a class="dropdown-item" href="#">
		            <small class="fas fa-archive dropdown-item-icon"></small>
		            Archive
		          </a>
		        </div>
		      </div>
		      <!-- End Settings Dropdown -->
		    </div>
		    <!-- End Title & Settings -->

		    <hr class="mt-3 mb-4">

		    <!-- Balance Info -->
		    <div class="row align-items-center mb-4">
		      <div class="col-6 u-ver-divider">
		        <label class="d-block small text-muted mb-0">Available:</label>
		        <span class="font-weight-medium">$45.99</span>
		      </div>

		      <div class="col-6">
		        <label class="d-block small text-muted mb-0">Pending:</label>
		        <span class="font-weight-medium">$0.00</span>
		      </div>
		    </div>

		    <div class="row">
		      <div class="col-3">
		        <div class="js-vr-progress progress-vertical rounded mb-2">
		          <div class="js-vr-progress-bar bg-warning rounded-bottom" role="progressbar" style="height: 45%;" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100"></div>
		        </div>
		        <div class="text-center">
		          <h4 class="small mb-0">May</h4>
		        </div>
		      </div>
		      <div class="col-3">
		        <div class="js-vr-progress progress-vertical rounded mb-2">
		          <div class="js-vr-progress-bar bg-warning rounded-bottom" role="progressbar" style="height: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
		        </div>
		        <div class="text-center">
		          <h4 class="small mb-0">Jun</h4>
		        </div>
		      </div>
		      <div class="col-3">
		        <div class="js-vr-progress progress-vertical rounded mb-2">
		          <div class="js-vr-progress-bar bg-warning rounded-bottom" role="progressbar" style="height: 23%;" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100"></div>
		        </div>
		        <div class="text-center">
		          <h4 class="small mb-0">Jul</h4>
		        </div>
		      </div>
		      <div class="col-3">
		        <div class="js-vr-progress progress-vertical rounded mb-2">
		          <div class="js-vr-progress-bar bg-warning rounded-bottom" role="progressbar" style="height: 39%;" aria-valuenow="39" aria-valuemin="0" aria-valuemax="100"></div>
		        </div>
		        <div class="text-center">
		          <h4 class="small mb-0">Aug</h4>
		        </div>
		      </div>
		    </div>
		    <!-- End Balance Info -->
		  </div>
		</div>
		<!-- End Stats -->
	</div>

	<div class="col-lg-3">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-4 px-4 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Top 5 Categoria mais Vendido</h4>
		    </div>
		    <!-- End Title & Settings -->
		    <hr class="mt-3 mb-4">
		  </div>
		</div>
		<!-- End Stats -->
	</div>

	<div class="col-lg-3">
		<!-- Stats -->
		<div class="card mb-7 mb-lg-0">
		  <div class="card-body pt-4 pb-5 px-5 mb-3 mb-md-0">
		    <!-- Title & Settings -->
		    <div class="d-flex justify-content-between align-items-center">
		      <h4 class="h6 mb-0">Top 5 Remedio mais Vendido</h4>
		    </div>
		    <!-- End Title & Settings -->
		    <hr class="mt-3 mb-4">

		  </div>
		</div>
		<!-- End Stats -->
	</div>
	
</div>