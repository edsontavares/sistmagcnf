<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


?>
<div class="d-flex align-items-center position-relative height-lg-100vh">
  <div class="col-md-8 col-lg-7 col-xl-10 offset-md-2 offset-lg-2 offset-xl-1 space-3 space-lg-0">
    <form class="js-validate w-md-75 w-lg-40 mx-md-auto card-body card p-6 shadow-sm" method="post">

    <div  class="col-sm-12 text-center">
            <img src="../recurso/img/Logo.jpg" class="img-logo">
        </div>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <label class="label-form">Utilizador <span class="text-danger">*</span></label>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
                <label class="label-form">palavra passe <span class="text-danger">*</span></label>
                <!--?= $form->field($model, 'password')->passwordInput()->label(false) ?-->
                <?= $this->render("@common/componentes/password-input", [
                  'form' => $form,
                  'model' => $model,
                  'attribute' => 'password',
                  'label' => false
              ]) ?>
              

                <div class="row align-items-center mb-5">
                    <div class="col-6">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="LoginForm[rememberMe]" value="<?= $model->rememberMe ?>"
                                    class="custom-control-input" id="stylishCheckbox">
                            <label class="custom-control-label" for="stylishCheckbox">
                                Lembrar-me
                            </label>
                        </div>
                    </div>


                    <div class="col-6 text-right">
                       
                        <?= Html::submitButton('<i class="fas fa-sign-in-alt"></i> Entrar', ['class' => 'btn btn-indigo transition-3d-hover', 'name' => 'login-button']) ?>
                    </div>
                </div>
               
                <a href="<?= Url::to(['/signup'])?>">Registar</a>
                

            <?php ActiveForm::end(); ?>
        
    </form>
  </div>
</div>
<style>
    .img-logo{
        height: 91px;
    width: 140px;
    }
</style>
