<div class="border-bottom mb-2 pb-1">
  <div class="row">

    <div class="col-md-4">
      <h1 class="font-weight-bold h5">Medicaments</h1>
    </div>

    <div class="col-md-8">
      <a href="index.php?r=site/formedicamento" class="btn btn-sm float-right btn-soft-warning">Adicionar Medicamento</a>
    </div>

  </div>
</div>

<div class="card">

  <div class="card-body bg-light">
      <!-- Search -->
      <div class="row mx-gutters-2">
        <div class="col-1">
          <input class="form-control form-control-sm" type="text" placeholder="id" aria-label="pesquisar">
        </div>

        <div class="col-4">
          <input class="form-control form-control-sm" type="text" placeholder="Nome" aria-label="pesquisar">
        </div>

        <div class="col-3">
          <select class="form-control form-control-sm">
            <option selected>Categoria</option>
            <option value="location1">categ1</option>
            <option value="location2">categ2</option>
            <option value="location2">categ3</option>
          </select>
        </div>

        <div class="col-2">
          <input class="form-control form-control-sm" type="text" placeholder="preço" aria-label="pesquisar">
        </div>

        <div class="col-2">
          <select class="form-control form-control-sm">
            <option selected>Estado</option>
            <option value="location1">Dinheiro, US</option>
            <option value="location2">Vinte4</option>
            <option value="location2">Visa</option>
          </select>
        </div>
        
      </div>
      <!-- End Search -->
  </div>

  <div class="table-responsive-md u-datatable">
    <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5"
           data-dt-info="#datatableWithPaginationInfo"
           data-dt-page-length="4"
           data-dt-is-show-paging="true"

           data-dt-pagination="datatablePagination"
           data-dt-pagination-classes="pagination mb-0"
           data-dt-pagination-items-classes="page-item"
           data-dt-pagination-links-classes="page-link"

           data-dt-pagination-next-classes="page-item"
           data-dt-pagination-next-link-classes="page-link"
           data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

           data-dt-pagination-prev-classes="page-item"
           data-dt-pagination-prev-link-classes="page-link"
           data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
      <thead>
        <tr class="text-uppercase font-size-1">
          <th scope="col" class="font-weight-medium">
            <div class="d-flex justify-content-between align-items-center">
              ID
              <div class="ml-2">
                <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                <span class="fas fa-angle-down u-datatable__thead-icon"></span>
              </div>
            </div>
          </th>
          <th scope="col" class="font-weight-medium">
            <div class="d-flex justify-content-between align-items-center">
              Nome
              <div class="ml-2">
                <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                <span class="fas fa-angle-down u-datatable__thead-icon"></span>
              </div>
            </div>
          </th>
          <th scope="col" class="font-weight-medium">
            <div class="d-flex justify-content-between align-items-center">
              Categoria
              <div class="ml-2">
                <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                <span class="fas fa-angle-down u-datatable__thead-icon"></span>
              </div>
            </div>
          </th>
          <th scope="col" class="font-weight-medium">
            <div class="d-flex justify-content-between align-items-center">
              Preço
              <div class="ml-2">
                <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                <span class="fas fa-angle-down u-datatable__thead-icon"></span>
              </div>
            </div>
          </th>
          <th scope="col" class="font-weight-medium">
            <div class="d-flex justify-content-between align-items-center">
              Estado
              <div class="ml-2">
                <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                <span class="fas fa-angle-down u-datatable__thead-icon"></span>
              </div>
            </div>
          </th>
        </tr>
      </thead>
      <tbody class="font-size-1">
        <?php
          foreach (range(1,5) as $ordem)
          {
            ?>
            <tr>
              <td>
                <span>#01</span>
              </td>
              <td class="align-middle text-secondary">Antinflamatorio</td>
              <td class="align-middle text-secondary">Sem receita</td>
              <td class="align-middle">500ECV</td>
              <td class="align-middle text-danger">Disponivél</td>
              <td>
                <!-- Settings -->
                <div class="position-relative">
                  <a id="settingsDropdown1InvokerExample1" class="btn btn-sm float-right btn-icon btn-soft-secondary btn-bg-transparent rounded" href="javascript:;" role="button"
                     aria-controls="settingsDropdown1"
                     aria-haspopup="true"
                     aria-expanded="false"
                     data-unfold-event="click"
                     data-unfold-target="#settingsDropdown1"
                     data-unfold-type="css-animation"
                     data-unfold-duration="300"
                     data-unfold-delay="300"
                     data-unfold-hide-on-scroll="true"
                     data-unfold-animation-in="slideInUp"
                     data-unfold-animation-out="fadeOut">
                    <span class="fas fa-ellipsis-h btn-icon__inner"></span>
                  </a>

                  <div id="settingsDropdown1" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="settingsDropdown1InvokerExample1" style="min-width: 160px;">
                    <a class="dropdown-item" href="index.php?r=site/view_farmacia">
                      <small class="fas fa-eye dropdown-item-icon"></small>
                      ver
                    </a>
                    <a class="dropdown-item" href="#">
                      <small class="fas fa-pencil-alt dropdown-item-icon"></small>
                      Editar
                    </a>
                    <a class="dropdown-item" href="#">
                      <small class="fas fa-trash-alt dropdown-item-icon"></small>
                      eliminar
                    </a>
                    <a class="dropdown-item" href="#">
                      <small class="fas fa-minus dropdown-item-icon"></small>
                      Duplicar
                    </a>
                  </div>
                </div>
                <!-- End Settings -->
              </td>
            </tr> 
            <?php
          }
        ?>       
      </tbody>
    </table>
  </div>

  <!-- Pagination -->
  <div class="d-flex justify-content-between align-items-center mb-4">
    <nav id="datatablePagination" aria-label="Activity pagination"></nav>

    <small id="datatableInfo" class="text-secondary"></small>
  </div>
  <!-- End Pagination -->
</div>