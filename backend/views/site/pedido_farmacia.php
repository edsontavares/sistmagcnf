<div class="border-bottom mb-2 pb-1">
  <div class="row">

    <div class="col-md-4">
      <h1 class="font-weight-bold h5">Pedidos</h1>
    </div>

  </div>
</div>

<div class="card">
  <div class="card-header py-4 px-0 mx-4">
    <!-- Activity Menu -->
    <div class="row mx-gutters-2 justify-content-sm-between align-items-sm-center">
      <div class="col-md-3">
        <input class="form-control form-control-sm" type="text" placeholder="Nome" aria-label="pesquisar">
      </div>
      <div class="col-md-3 mb-2 mb-md-0">
        <!-- Datepicker -->
        <div id="datepickerWrapper" class="js-focus-state u-datepicker w-auto input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text">
              <span class="fas fa-calendar"></span>
            </span>
          </div>
          <input type="text" class="js-range-datepicker form-control bg-white rounded-right"
                 data-rp-wrapper="#datepickerWrapper"
                 data-rp-type="range"
                 data-rp-date-format="d M Y"
                 data-rp-default-date='["05 Jul 2018", "19 Jul 2018"]'
                 data-rp-is-disable-future-dates="true">
        </div>
        <!-- End Datepicker -->
      </div>

      <div class="col-lg-2">
          <input class="form-control form-control-sm" type="text" placeholder="Montante" aria-label="pesquisar">
      </div>

      <div class="col-md-2">
        <select class="form-control form-control-sm">
          <option selected>Tipo Pagamento</option>
          <option value="location1">Dinheiro, US</option>
          <option value="location2">Vinte4</option>
          <option value="location2">Visa</option>
        </select>
      </div>

      <div class="col-md-2">
        <select class="form-control form-control-sm">
          <option selected>Estado</option>
          <option value="location1">Pendente</option>
          <option value="location2">Entregue</option>
          <option value="location2">Cancelado</option>
        </select>
      </div>

    </div>
    <!-- End Activity Menu -->
  </div>

  <div class="card-body p-4">
    <!-- Activity Table -->
    <div class="table-responsive-md u-datatable">
      <table class="js-datatable table table-borderless u-datatable__striped u-datatable__content u-datatable__trigger mb-5"
             data-dt-info="#datatableInfo"
             data-dt-search="#datatableSearch"
             data-dt-entries="#datatableEntries"
             data-dt-page-length="12"
             data-dt-is-responsive="false"
             data-dt-is-show-paging="true"
             data-dt-details-invoker=".js-datatabale-details"
             data-dt-select-all-control="#invoiceToggleAllCheckbox"

             data-dt-pagination="datatablePagination"
             data-dt-pagination-classes="pagination mb-0"
             data-dt-pagination-items-classes="page-item"
             data-dt-pagination-links-classes="page-link"

             data-dt-pagination-next-classes="page-item"
             data-dt-pagination-next-link-classes="page-link"
             data-dt-pagination-next-link-markup='<span aria-hidden="true">»</span>'

             data-dt-pagination-prev-classes="page-item"
             data-dt-pagination-prev-link-classes="page-link"
             data-dt-pagination-prev-link-markup='<span aria-hidden="true">«</span>'>
        <thead>
          <tr class="text-uppercase font-size-1">
            <th scope="col">
              <div class="custom-control custom-checkbox d-flex align-items-center">
                <input type="checkbox" class="custom-control-input" id="invoiceToggleAllCheckbox">
                <label class="custom-control-label" for="invoiceToggleAllCheckbox">
                  <span class="text-hide">Checkbox</span>
                </label>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Pedido
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Cliente
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Preço
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Data
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Estado
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
            <th scope="col" class="font-weight-medium">
              <div class="d-flex justify-content-between align-items-center">
                Acção
                <div class="ml-2">
                  <span class="fas fa-angle-up u-datatable__thead-icon"></span>
                  <span class="fas fa-angle-down u-datatable__thead-icon"></span>
                </div>
              </div>
            </th>
          </tr>
        </thead>
        <tbody class="font-size-1">
          <?php
            foreach (range(1,5) as $ordem)
            {
          ?>
          <tr class="js-datatabale-details" data-details='
            <div class="border rounded p-5">
              <h4 class="h3">Fatura</h4>

              <div class="row mb-6">
                <div class="col-3">
                  <span class="text-secondary">Date:</span>
                  <span class="font-weight-medium">04 Maio, 2020</span>
                </div>
                <div class="col-3">
                  <span class="text-secondary">Comerciante:</span>
                  <span class="font-weight-medium">Farmácia universal</span>
                </div>
                <div class="col-6">
                  <span class="text-secondary">Numero de fatura:</span>
                  <span class="font-weight-medium">128746739419</span>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6 mb-3 mb-sm-0">
                  <h5 class="text-dark font-size-1 text-uppercase">Endereço de Cobrança:</h5>
                  <address class="text-secondary">
                    <h6 class="h5 text-dark">Farmácia universal</h6>
                    Palmarejo, Avenida Santiago, entrada cidadela
                  </address>
                </div>

                <div class="col-sm-6">
                  <h5 class="text-dark font-size-1 text-uppercase">Informação de Cliente:</h5>
                  <ul class="list-unstyled mb-0">
                    <li class="mb-2">
                      <span class="text-secondary">Nome:</span>
                      <span class="font-weight-medium">Jailson</span>
                    </li>
                    <li class="mb-2">
                      <span class="text-secondary">Apelido:</span>
                      <span class="font-weight-medium">Silva</span>
                    </li>
                    <li class="mb-2">
                      <span class="text-secondary">País:</span>
                      <span class="font-weight-medium">Cabo Verde</span>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="row justify-content-end mb-4">
                <div class="col-sm-6">
                  <hr class="my-4">

                  <h5 class="text-dark font-size-1 text-uppercase">Detalhes da Transaction:</h5>
                  <ul class="list-unstyled mb-0">
                    <li class="d-flex justify-content-between align-items-center mb-2">
                      <span class="text-secondary">Valor da transação</span>
                      <span class="font-weight-medium">9.00$</span>
                    </li>
                    <li class="d-flex justify-content-between align-items-center mb-2">
                      <span class="text-secondary">Taxa</span>
                      <span class="font-weight-medium">0.50$</span>
                    </li>
                    <li class="d-flex justify-content-between align-items-center mb-2">
                      <span class="text-secondary">Valor total</span>
                      <span class="text-warning font-weight-medium">9.37$</span>
                    </li>
                  </ul>
                </div>
              </div>

              <ul class="list-inline mb-0">
                <li class="list-inline-item u-ver-divider pr-3 mr-3">
                  <a href="#" class="text-warning">
                    <span class="fas fa-file-word text-secondary mr-1"></span>
                    Baixar fatura
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#" class="text-warning">
                    <span class="fas fa-print text-secondary mr-1"></span>
                    Imprimir Detalhes
                  </a>
                </li>
              </ul>
            </div>'>
            <td class="align-middle">
              <div class="custom-control custom-checkbox d-flex align-items-center">
                <input type="checkbox" class="custom-control-input" id="invoiceCheckbox01">
                <label class="custom-control-label" for="invoiceCheckbox01">
                  <span class="text-hide">Checkbox</span>
                </label>
              </div>
            </td>
            <td class="align-middle text-secondary font-weight-normal u-datatable__trigger-icon">Paracetamol</td>
            <td class="align-middle">
              <div class="media align-items-center">
                <span>Jailson Silva</span>
              </div>
            </td>
            <td class="align-middle text-warning">9.00$</td>
            <td class="align-middle text-secondary">05 May 2020</td>
            <td class="align-middle text-danger">Pendente</td>
            <td class="align-middle">
              <!-- Settings -->
              <div class="position-relative">
                <a id="settingsDropdown1InvokerExample1" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded" href="javascript:;" role="button"
                   aria-controls="settingsDropdown1"
                   aria-haspopup="true"
                   aria-expanded="false"
                   data-unfold-event="click"
                   data-unfold-target="#settingsDropdown1"
                   data-unfold-type="css-animation"
                   data-unfold-duration="300"
                   data-unfold-delay="300"
                   data-unfold-hide-on-scroll="true"
                   data-unfold-animation-in="slideInUp"
                   data-unfold-animation-out="fadeOut">
                  <span class="fas fa-ellipsis-h btn-icon__inner"></span>
                </a>

                <div id="settingsDropdown1" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="settingsDropdown1InvokerExample1" style="min-width: 160px;">
                  <a class="dropdown-item" href="#">
                    <small class="fas fa-cogs dropdown-item-icon"></small>
                    Validar pedido
                  </a>
                  <a class="dropdown-item" href="#">
                    <small class="fas fa-code-branch dropdown-item-icon"></small>
                    Repository
                  </a>
                </div>
              </div>
              <!-- End Settings -->
            </td>
          </tr>
          <?php
            }
          ?>

        </tbody>
      </table>
    </div>
    <!-- End Activity Table -->

    <!-- Pagination -->
    <div class="d-flex justify-content-between align-items-center">
      <nav id="datatablePagination" aria-label="Activity pagination"></nav>

      <small id="datatableInfo" class="text-secondary"></small>
    </div>
    <!-- End Pagination -->
  </div>
</div>