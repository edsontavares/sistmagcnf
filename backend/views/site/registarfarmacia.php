<div class="border-bottom mb-2 pb-1">
  <div class="row">

    <div class="col-md-4">
      <h1 class="font-weight-bold h5">Registar Farmacia</h1>
    </div>

  </div>
</div>

<div class="card">
  <div class="card-body">
    <div class="row mx-gutters-2">
      <div class="col-lg-6">
        <label for="exampleInputEmail1">Nome da farmacia<span class="text-danger">*</span></label>
        <input type="email" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Nome da farmacia">
      </div>
      <div class="col-lg-3">
        <label for="exampleInputEmail1">email<span class="text-danger">*</span></label>
        <input type="email" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="email">
      </div>
      <div class="col-lg-3">
        <label for="exampleInputEmail1">Telefone<span class="text-danger">*</span></label>
        <input type="email" class="form-control form-control-sm" id="exampleInputEmail1" placeholder="Telefone">
      </div>
      <div class="col-lg-12 mt-3">
        <label for="exampleInputEmail1">Localização<span class="text-danger">*</span></label>
        <!--div id="GMapMultipleMarkers" class="js-g-map embed-responsive embed-responsive-21by9"
           data-type="terrain"
           data-lat="-33.92"
           data-lng="151.25"
           data-zoom="10"
           data-multiple-markers="true"
           data-markers-locations='[
             ["Bondi Beach", -33.890542, 151.274856, 4],
             ["Coogee Beach", -33.923036, 151.259052, 5],
             ["Cronulla Beach", -34.028249, 151.157507, 3],
             ["Manly Beach", -33.80010128657071, 151.28747820854187, 2],
             ["Maroubra Beach", -33.950198, 151.259302, 1]
           ]'>
        </div-->
      </div>

      <div class="col-lg-4">
        <!-- File Attachment Input -->
        <label class="file-attachment-input mt-3" for="imagesAttachmentInput">
          <i class="far fa-image"></i>
          <span class="d-block mb-2">carega a foto de capa</span>
          <small class="d-block text-muted">Tamanh Maximo 2MB</small>
          <input id="imagesAttachmentInput" name="images-attachment" type="file" class="js-custom-file-attach file-attachment-input__label"
                 data-result-text-target="#fileUploadText">
          <span id="fileUploadText"></span>
        </label>
        <!-- End File Attachment Input -->
      </div>

      <div class="col-lg-8">
        <!-- File Attachment Input -->
        <label class="file-attachment-input mt-3" for="imagesAttachmentInput">
          <i class="far fa-image"></i>
          <span class="d-block mb-2">carega aqui o banner</span>
          <small class="d-block text-muted">Tamanh Maximo 5MB</small>
          <input id="imagesAttachmentInput" name="images-attachment" type="file" class="js-custom-file-attach file-attachment-input__label"
                 data-result-text-target="#fileUploadText">
          <span id="fileUploadText"></span>
        </label>
        <!-- End File Attachment Input -->
      </div>
    </div>

    <div class="mt-3">
      <label for="exampleInputEmail1">Breve descrição da farmacia<span class="text-danger">*</span></label>
      <div class="input-group">
        <textarea class="form-control" rows="4" name="text" placeholder="breve descrição da farmacia ..." aria-label="Hi there, I would like to ..." required
          data-msg="Please enter a reason."
          data-error-class="u-has-error"
          data-success-class="u-has-success">
        </textarea>
      </div>
    </div>

    <div class="form-group mt-6">
      <a type="submit" class="btn btn-sm rounded bg-yellow">Criar</a>
    </div>

  </div>
</div>