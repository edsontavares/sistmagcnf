<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

?>
<div class="d-flex align-items-center position-relative height-lg-100vh">

  <div class="col-md-8 col-lg-7 col-xl-10 offset-md-2 offset-lg-2 offset-xl-1 space-3 space-lg-0 ">
    <form class="js-validate w-md-75 w-lg-40 mx-md-auto card-body card p-6 shadow-sm" method="post">
    
            <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

               <label class="label-form">Nome de utilizador <span class="text-danger">*</span></label>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false) ?>
                <label class="label-form">Email <span class="text-danger">*</span></label>
                <?= $form->field($model, 'email') ->label(false)?>
                <label class="label-form">Palavra passe <span class="text-danger">*</span></label>
                <?= $form->field($model, 'password')->passwordInput() ->label(false)?>
                
                
                <div class="col-12 text-right">
                    <div class="form-group">
                        <?= Html::submitButton('Registar', ['class' => 'btn btn-indigo transition-3d-hove', 'name' => 'signup-button']) ?>
                    </div>
                </div>
                <a href="<?= Url::to(['/login'])?>">Login</a>

            <?php ActiveForm::end(); ?>
        </form>
      
  </div>
  
</div>
<style>
    .img-logo{
        height: 91px;
    width: 140px;
    }
</style>
