<div class="border-bottom mb-2 pb-1">
  <div class="row">

    <div class="col-md-4">
      <h1 class="font-weight-bold h5">Farmácias Africana</h1>
    </div>

    <div class="col-md-8">
      <a href="index.php?r=site/registarfarmacia" class="btn btn-sm rounded float-right btn-soft-warning">
      	<i class="far fa-edit mr-2"></i>Editar</a>
    </div>

  </div>
</div>

<!-- Hero Section -->
<div class="bg-light p-4">
  <div class="row">

  	<div class="col-lg-4">
  		<div class="card card-body">
  			<div class="mb-3 mx-auto">
                <img class="img-fluid rounded-circle" src="../../biblioteca/img/160x160/img3.jpg">
            </div>
  		</div>
  	</div>

  	<div class="col-lg-8">
  		<h1 class="font-weight-bold h5">Farmácias Africana</h1>
  		<p>
  			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
  	</div>

  </div>
</div>