<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Patient;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

?>

<div class="minerva-form">
      
    <?php $form = ActiveForm::begin(); ?>
    <div class="row mx-gutters-2">
        <div class="col-sm-6">
            <label for="" class="type-label" >Nome de Paciente<span class="bg-active">*</span></label>
            <?=  $form->field($model, 'id_patient')->widget(Select2::classname(), [
                'data' => ArrayHelper::map(Patient::find()->orderBy(['create_at' => SORT_DESC,])->all(), 'id_patient', 'name'),
                'language' => 'en',
                'options' => ['placeholder' => 'Selecionar Paciente'],
                'pluginOptions' => [
                'allowClear' => true,
                'class' => 'form-control  ',
                'required' => true,
                'data-msg' => 'Por favor Selecionar Paciente.',
                'data-error-class' => 'u-has-error',
                'data-success-class' => 'u-has-success',
                ],
            ])->label(false) ?> 
        </div>
        <div class="col-sm-6">
            <label for="" class="type-label" >Técnico Responsavel<span class="text-danger">*</span></label>
            <div class="form-group">
                <?=  $form->field($model, 'nameEmployees')->widget(Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\employees::find()->all(), 'name', 'name'),
                    // 'language' => 'es',
                    'options' => [

                    'required' => true,
                    'placeholder' => yii::t('app','Técnico Responsavel'),
                    'data-msg' => yii::t('app','Por favor Técnico Responsavel'),
                    ],
                    'pluginOptions' => [
                    'tags'=>true,
                    'class' => 'form-control  ',
                    
                    'allowClear' => true,
                    'minimumInputLength' => 2,
                    ],
                ])->label(false) ?>
            </div>        
        </div>
        <div class="col-sm-6">
            <label class="type-label ml-3" >Tipo de Encaixe: </label>
            <?php echo $form->field($model, 'fit_type[]')->checkboxList(
                [ 1 => 'PTB', 2 => 'PTB SC',3 => 'PTB SC SP', 4 => 'Com Coxa'],
                ['class'=>'ml-3']
            )->label(false) ;
            ?>
        </div>
        <div class="col-sm-8">
        <div class="row">
            <div  class="col-sm-4">
                <label for="" class="type-label mt-3" >Cercunferênçia Nº 1</label>
                <?= $form->field($model, 'cercunferencia_m_one')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
                <label for="" class="type-label mt-3" >Cercunferênçia Nº 2</label>
                <?= $form->field($model, 'cercunferencia_m_two')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
            </div>
            <div  class="col-sm-5">
                <img src="../../recurso/img/petriball.png" alt="user">
            </div>
            <div  class="col-sm-3">
            <label for="" class="type-label pt-5" >Altura Nº 12</label>
                <?= $form->field($model, 'height_m')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="row pt-4">
            <div  class="col-sm-6 mt-5">
                <img src="../../recurso/img/altura.png" alt="user">
            </div>
            <div  class="col-sm-6">
            <label for="" class="type-label mt-3">Altura do Calcanhar</label>
                <?= $form->field($model, 'length_heel')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
            </div>
        </div>
        <hr>
        <div class="row pt-2">
            <div  class="col-sm-6">
                <img src="../../recurso/img/app.png" alt="user">
            </div>
            <div  class="col-sm-6">
            <label for="" class="type-label mt-3" >Espaçamentos AP</label>
                <?= $form->field($model, 'spacing_l_one')->textInput([
                    'maxlength' => true,
                    "class" => "form-control",
                    'placeholder' => "",
                    'aria-label' => "",
                    'required' => false,
                    'aria-describedby' => "dataInicioLabel",
                    'data-msg' => " ",
                    'data-error-class' => "u-has-error",
                    'data-success-class' => "u-has-success",
                    'type' => 'number'
                ]) ->label(false)?>
            </div>
        </div>
    </div>
    <hr>
    <div class="row pt-5">
        <div  class="col-sm-2">
            <label for="" class="type-label mt-3" >Circunferência Nº 3</label>
            <?= $form->field($model, 'cercunferencia_c_one')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 4</label>
            <?= $form->field($model, 'cercunferencia_c_two')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-labecercunferencia_c_threel' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 5</label>
            <?= $form->field($model, 'cercunferencia_c_three')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 6</label>
            <?= $form->field($model, 'cercunferencia_c_for')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 7</label>
            <?= $form->field($model, 'cercunferencia_c_five')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 8</label>
            <?= $form->field($model, 'cercunferencia_c_sex')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 9</label>
            <?= $form->field($model, 'cercunferencia_c_seven')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 10</label>
            <?= $form->field($model, 'cercunferencia_c_eigth')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Circunferência Nº 11</label>
            <?= $form->field($model, 'cercunferencia_c_nene')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
        </div>
        <div  class="col-sm-5">
            <img src="../../recurso/img/petribal.png" class="petribal" alt="user">
        </div>
        <div  class="col-sm-2 petriballateral">
            <label for="" class="type-label mt-3 petriballateral" >Espaçamentos Nº 15</label>
            <?= $form->field($model, 'spacing_c_one')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
             <label for="" class="type-label mt-3" >Espaçamentos Nº 14</label>
            <?= $form->field($model, 'spacing_c_two')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-3" >Altura Nº 13</label>
            <?= $form->field($model, 'length')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
        </div>
        <div class="col-sm-3">
            <h3>Ângulo de Coto</h3>
        <label for="" class="type-label mt-3" >Flexãoº</label>
            <?= $form->field($model, 'flexion')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-3" >Extensãoº</label>
            <?= $form->field($model, 'extension')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-3" >Abduçãoº</label>
            <?= $form->field($model, 'abduction')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            <label for="" class="type-label mt-3" >Abduçãoº</label>
            <?= $form->field($model, 'abduction1')->textInput([
                 'maxlength' => true,
                 "class" => "form-control",
                 'placeholder' => "",
                 'aria-label' => "",
                 'required' => false,
                 'aria-describedby' => "dataInicioLabel",
                 'data-msg' => " ",
                 'data-error-class' => "u-has-error",
                 'data-success-class' => "u-has-success",
                 'type' => 'number'
            ]) ->label(false)?>
            
        </div>
        <div class="col-sm-12">
                <label for="" class="type-label" >Observação</label>
                <!--?= $form->field($model, 'vertibral')->textarea(['rows' => 6])->label(false)  ?-->
                <div class="u-summernote-editor">
                <textarea id="summernote" name="Curso[observation]" rows="3" class="form-control"><?= $model->observation?></textarea>
        </div>
        <div class="col-sm-12 text-right mt-3">
        <div class="form-group">
            <?= Html::resetButton(Yii::t('app', 'Cancelar'), ['class' => 'btn btn-sm btn-soft-secondary rounded']) ?>
            <?= Html::submitButton('Gurdar', ['class' => 'btn btn-sm  btn-indigo rounded']) ?>
        </div>
    </div>

    </div>
    

<div class="row mt-3">
    
</div>
  
<?php ActiveForm::end(); ?>

</div>
<style>
          .select2-container--krajee .select2-selection--single .select2-selection__arrow {
    height: 48px!important;
}
.select2-container--krajee .select2-selection--single {
    height: 50px!important;
    padding: 14px 24px 6px 16px!important;
}
.petribal{
    height: 500px;
    margin-top: 466px;
}
.petriballateral{
    margin-top: 200px;
}
</style>
