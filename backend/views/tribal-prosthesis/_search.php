<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TribalProsthesisSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tribal-prosthesis-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_tribal_prosthesis') ?>

    <?= $form->field($model, 'id_query') ?>

    <?= $form->field($model, 'id_patient') ?>

    <?= $form->field($model, 'cercunferencia_m_one') ?>

    <?= $form->field($model, 'cercunferencia_m_two') ?>

    <?php // echo $form->field($model, 'height_m') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_one') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_two') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_three') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_for') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_five') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_sex') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_seven') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_eigth') ?>

    <?php // echo $form->field($model, 'cercunferencia_c_nene') ?>

    <?php // echo $form->field($model, 'spacing_c_one') ?>

    <?php // echo $form->field($model, 'spacing_c_two') ?>

    <?php // echo $form->field($model, 'length') ?>

    <?php // echo $form->field($model, 'spacing_l_one') ?>

    <?php // echo $form->field($model, 'length_heel') ?>

    <?php // echo $form->field($model, 'observation') ?>

    <?php // echo $form->field($model, 'with_thigh') ?>

    <?php // echo $form->field($model, 'ptb_sc_ptbsc_sp') ?>

    <?php // echo $form->field($model, 'ptb_sc') ?>

    <?php // echo $form->field($model, 'ptb') ?>

    <?php // echo $form->field($model, 'id_employees') ?>

    <?php // echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'create_at') ?>

    <?php // echo $form->field($model, 'state') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
