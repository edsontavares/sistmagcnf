<?php
use \frontend\models\PerfirenciaLanguagen;
use frontend\models\Competencias;
use frontend\models\Defaults;

?>
<div id="print" style=" font-family: 'Open Sans', sans-serif; size: 18cm 18cm;">
<header style="text-align: center; margin-top: -0.5cm;" >    
    <!--h6><img src="./recurso/img/Logo.jpg" style="margin-top: -0.5cm;text-align: center;width: 50px; height: 50px;" alt="user"></h6-->
    <h3  style="color:#2d1582; ">Centro Nacional Ortopédico e de Reeducação Funcional</h3>
    <h6 style="margin-top: -0.5cm;">A.S. Filipe – Praia</h6>
    <hr style="color:#070403; margin-top: -0.5cm; width: 800px; height: 2px;">
</header>
<!-- end row -->
<div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
            <div class="row pt-">
                <div  class="col-sm-4 pt-10">
                    <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 1</label>
                    <p><?=$model->cercunferencia_m_one?></p>
                    <label for="" class="type-label mt-3 font-weight-bold" >Cercunferênçia Nº 2</label>
                    <p><?=$model->cercunferencia_m_two?></p>
                </div>
                <div  class="col-sm-5">
                    <img src="../../recurso/img/petriball.png" alt="user">
                </div>
                <div  class="col-sm-3 pt-10">
                    <label for="" class="type-label mt-10 font-weight-bold" >Altura Nº 12</label>
                    <p><?=$model->height_m?></p>
                </div>
               
                <div class="col-sm-12">
                    <hr>
                    <div class="row pt-4">
                        <div  class="col-sm-3 mt-5">
                            <img src="../../recurso/img/altura.png" alt="user">
                        </div>
                        <div  class="col-sm-3">
                            <label for="" class="type-label mt-3 font-weight-bold">Altura do Calcanhar</label>
                            <p><?=$model->length_heel?></p>   
                        </div>
                   
                        <div  class="col-sm-3">
                            <img src="../../recurso/img/app.png" alt="user">
                        </div>
                        <div  class="col-sm-3">
                        <label for="" class="type-label mt-3 font-weight-bold" >Espaçamentos AP</label>
                            <p><?=$model->spacing_l_one?></p>   
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row pt-5">
                <div  class="col-sm-3">
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 3</label>
                    <p><?=$model->spacing_l_one?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 4</label>
                    <p><?=$model->cercunferencia_c_two?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 5</label>
                    <p><?=$model->cercunferencia_c_three?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 6</label>
                    <p><?=$model->cercunferencia_c_for?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 7</label>
                    <p><?=$model->cercunferencia_c_five?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 8</label>
                    <p><?=$model->cercunferencia_c_sex?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 9</label>
                    <p><?=$model->cercunferencia_c_seven?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 10</label>
                    <p><?=$model->cercunferencia_c_eigth?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 11</label>
                    <p><?=$model->cercunferencia_c_nene?></p> 
                    
                </div>
                <div  class="col-sm-6">
                    <img src="../../recurso/img/petribal.png" class="petribal" alt="user">
                </div>
                <div  class="col-sm-3 petriballateral pt-10">
                    <label for="" class="type-label mt-3 petriballateral font-weight-bold" >Espaçamentos Nº 15</label>
                    <p><?=$model->spacing_c_one?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Espaçamentos Nº 14</label>
                    <p><?=$model->spacing_c_two?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold mt-10" >Altura Nº 13</label>
                    <p><?=$model->length?></p> 
                </div>
                
            </div>
            <hr>
            <table style="width:100%">
                <h3 class=" font-weight-bold">Ângulo de Coto</h3>
                 <tbody>
                    <tr>
                    
                        <td class="align-middle"><b>Flexãoº<b></></td>
                        <td class="align-middle"><b>Extensãoº</b></td>
                        <td class="align-middle"><b>Abduçãoº</b></td>
                        <td class="align-middle"><b>Abduçãoº</b></td>
                        
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->flexion?></td>
                        <td class="text-secondary" ><?= $model->extension?></td>
                        <td class="text-secondary" ><?= $model->abduction?></td>
                        <td class="text-secondary"><?= $model->abduction1?></td>
                       
                    </tr> 
                 </tbody>
            </table>
</div>

</div>



<footer style="text-align: center;" >
    <hr>
    ACD-CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
</footer>


<?php if( @$type == 'preview' ): ?>

<script src="/js/jQuery.print.js"></script>
<script>
    function print() {
        //Print ele4 with custom options
        $("#print").print({
            //Use Global styles
            globalStyles: false,
            //Add link with attrbute media=print
            mediaPrint: false,
            //Custom stylesheet
            stylesheet: "https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i",
            // bootstrap: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css",
            // fontawesome: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css",
            //Print in a hidden iframe
            iframe: true,
            //Don't print this
            // noPrintSelector: ".avoid-this",
            //Add this at top
            prepend: "EMPREGOSCV",
            //Add this on bottom
            append: "GERADO EM EMPREGOS.CV",
            //Log to console when printing is done via a deffered callback
            deferred: $.Deferred().done(function () {
                console.log('Printing done', arguments);
            })
        });
    }
</script>

<?php endif; ?>
<style>
    footer 
    {
        position: fixed;
        bottom: 0cm;
        left: 0cm;
        right: 0cm;
        height: 1cm;
        content: counter(page);
    }
    .logoap{
  width: 319px;
}
.mt-14{
   
    margin-top: 9.5rem !important;

}
.mt-30{
    margin-top: 9.5rem !important;
}

.mt-50{
    margin-top: 85px;
}
.mt-70{
    margin-top: 170px;
}
.img-pe{
    width: 319px;
}
</style>
