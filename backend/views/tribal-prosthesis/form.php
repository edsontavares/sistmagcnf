<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TribalProsthesis */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tribal-prosthesis-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_tribal_prosthesis')->textInput() ?>

    <?= $form->field($model, 'id_query')->textInput() ?>

    <?= $form->field($model, 'id_patient')->textInput() ?>

    <?= $form->field($model, 'cercunferencia_m_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_m_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'height_m')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_three')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_for')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_five')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_sex')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_seven')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_eigth')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cercunferencia_c_nene')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spacing_c_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spacing_c_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'length')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spacing_l_one')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'length_heel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'observation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'with_thigh')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ptb_sc_ptbsc_sp')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ptb_sc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ptb')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_employees')->textInput() ?>

    <?= $form->field($model, 'update_at')->textInput() ?>

    <?= $form->field($model, 'create_at')->textInput() ?>

    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
