<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\TribalProsthesis */

$this->title = 'Atualizar Prótese tibial: ' . $model->id_tribal_prosthesis;

?>
<div class="tribal-prosthesis-update">

    <div class="row mx-gutters-2">
        <div class="col-md-10">
        <h4 class="font-weight-bold"><?= Html::encode($this->title) ?></h4>
        </div>
        <div class="col-md-2">
            <a class="float-right " href="<?= Url::to(['/Protese-femoral'])?>">Voltar</a>
        </div>
    </div>
    <hr>

    <div class="card">
        <div class="card-body shadow-sm p-4">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
     </div>

</div>
