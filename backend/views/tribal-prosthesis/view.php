<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
$query = \common\models\Query::findOne($model->id_query);
$patient = \common\models\Patient::findOne($query->id_patient);
\yii\web\YiiAsset::register($this);
?>
<div class="query-view">

<div class="row mx-gutters-2">
        <div class="col-sm-12">
            <?php if (Yii::$app->session->hasFlash("success-create")): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <?= Yii::$app->session->getFlash("success-create") ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php endif; ?>
        </div>
        <div class="col-lg-6">
            <h1 class="h5 font-weight-bold">Informação de Prótese tibial</h1>
        </div>
        <div class="col-lg-6">
            <div class="ml-lg-auto float-right">
            <a class="" href="<?= Url::to(['/Protese-tibial'])?>">Voltar</a>
            <div class="btn-group position-relative ">
                
                <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                    aria-controls="#id" aria-haspopup="true" aria-expanded="false"
                    data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#"
                    data-unfold-type="css-animation" data-unfold-duration="300"
                    data-unfold-delay="300"
                    data-unfold-animation-in="slideInUp"
                    data-unfold-animation-out="fadeOut" style="width: 2.6rem; height: 2.6rem;">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>

                <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="id-invoker">
                     <?= Html::a('<i class="fa fa-download dropdown-item-icon"></i>Descaregar', ['baixar', 'id'=>$model->id_tribal_prosthesis], ['class' => 'dropdown-item transition-3d-hover' ,'target'=>"_blank"]) ?>   
                     <?php if($model->state == 1): ?>
                        <?= Html::a('<i class="fas fa-briefcase mr-1"></i> Concluido', ['concluded', 'id' => $model->id_tribal_prosthesis], ['class' => 'dropdown-item transition-3d-hover ']) ?>
                    <?php endif;?>   
               </div>
                </div>
            </div>
         </div>
        
    </div>
   
    <hr>

    <div class="card shadow-lg ">
            <div class="card-body shadow-sm p-4">
            <div class="row mx-gutters-2">
                <div class="col-sm-6">
                    <label for="" class="type-label font-weight-bold" ><b>Nome de Paciente</b></label>
                    <p><?= $patient->name?></p>
                </div>
                <div class="col-sm-6">
                    <label for="" class="type-label font-weight-bold" ><b>Técnico Responsável</b></label>
                    <p><?= $model->nameEmployees?></p>
                </div>
                <?php if($model->fit_type):?>
                    <div class="col-sm-12">
                        <label for="" class="type-label font-weight-bold" ><b>Tipo de Encaixe:</b></label>
                            <p>
                                <?php if(strpos($model->fit_type, '1')):?>
                                    <span>PTB</span>
                                <?php endif; ?>
                                <?php if(strpos($model->fit_type, '2')):?>
                                    <span>PTB SC</span>
                                <?php endif; ?>
                                <?php if(strpos($model->fit_type, '3')):?>
                                    <span>PTB SC SP</span>
                                <?php endif; ?>
                                <?php if(strpos($model->fit_type, '4')):?>
                                    <span>Com Coxa</span>
                                <?php endif; ?>
                            </p> 
                    </div>
                <?php endif;?>
            </div>
            <hr>
            <div class="row pt-">
                <div  class="col-sm-4 pt-10">
                    <label for="" class="type-label mt-9 font-weight-bold" >Cercunferênçia Nº 1</label>
                    <p><?=$model->cercunferencia_m_one?></p>
                    <label for="" class="type-label mt-3 font-weight-bold" >Cercunferênçia Nº 2</label>
                    <p><?=$model->cercunferencia_m_two?></p>
                </div>
                <div  class="col-sm-5">
                    <img src="../../recurso/img/petriball.png" alt="user">
                </div>
                <div  class="col-sm-3 pt-10">
                    <label for="" class="type-label mt-10 font-weight-bold" >Altura Nº 12</label>
                    <p><?=$model->height_m?></p>
                </div>
               
                <div class="col-sm-12">
                    <hr>
                    <div class="row pt-4">
                        <div  class="col-sm-3 mt-5">
                            <img src="../../recurso/img/altura.png" alt="user">
                        </div>
                        <div  class="col-sm-3">
                            <label for="" class="type-label mt-3 font-weight-bold">Altura do Calcanhar</label>
                            <p><?=$model->length_heel?></p>   
                        </div>
                   
                        <div  class="col-sm-3">
                            <img src="../../recurso/img/app.png" alt="user">
                        </div>
                        <div  class="col-sm-3">
                        <label for="" class="type-label mt-3 font-weight-bold" >Espaçamentos AP</label>
                            <p><?=$model->spacing_l_one?></p>   
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row pt-5">
                <div  class="col-sm-3">
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 3</label>
                    <p><?=$model->spacing_l_one?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 4</label>
                    <p><?=$model->cercunferencia_c_two?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 5</label>
                    <p><?=$model->cercunferencia_c_three?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 6</label>
                    <p><?=$model->cercunferencia_c_for?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 7</label>
                    <p><?=$model->cercunferencia_c_five?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 8</label>
                    <p><?=$model->cercunferencia_c_sex?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 9</label>
                    <p><?=$model->cercunferencia_c_seven?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 10</label>
                    <p><?=$model->cercunferencia_c_eigth?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold" >Circunferência Nº 11</label>
                    <p><?=$model->cercunferencia_c_nene?></p> 
                    
                </div>
                <div  class="col-sm-6">
                    <img src="../../recurso/img/petribal.png" class="petribal" alt="user">
                </div>
                <div  class="col-sm-3 petriballateral pt-10">
                    <label for="" class="type-label mt-3 petriballateral font-weight-bold" >Espaçamentos Nº 15</label>
                    <p><?=$model->spacing_c_one?></p> 
                    
                    <label for="" class="type-label mt-3 font-weight-bold" >Espaçamentos Nº 14</label>
                    <p><?=$model->spacing_c_two?></p> 
                
                    <label for="" class="type-label mt-3 font-weight-bold mt-10" >Altura Nº 13</label>
                    <p><?=$model->length?></p> 
                </div>
                
            </div>
            <hr>
            <table style="width:100%">
                <h3 class=" font-weight-bold">Ângulo de Coto</h3>
                 <tbody>
                    <tr>
                    
                        <td class="align-middle"><b>Flexãoº<b></></td>
                        <td class="align-middle"><b>Extensãoº</b></td>
                        <td class="align-middle"><b>Abduçãoº</b></td>
                        <td class="align-middle"><b>Abduçãoº</b></td>
                        
                    </tr>
                    <tr>
                        <td class="text-secondary"><?= $model->flexion?></td>
                        <td class="text-secondary" ><?= $model->extension?></td>
                        <td class="text-secondary" ><?= $model->abduction?></td>
                        <td class="text-secondary"><?= $model->abduction1?></td>
                       
                    </tr> 
                 </tbody>
            </table>
</div>
<style>
    td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
.petribal{
    height: 580px;
   
}
td, th {
    border: none; 
    
}
</style>
