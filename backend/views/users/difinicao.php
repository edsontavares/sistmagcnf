<?php
// use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('app','Definições');
//$this->params['breadcrumbs'][] = $this->title;  ?r=site%2Fdeleteuseraccount
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<section class="space-top-1 pb-6">
    <div class="container space-1">
        <div class="row justify-content-md-center">

            <div class="col-md-8 col-md-offset-2">
                <!-- card dados pessoais -->
                <div class="card shadow-soft mb-4" style="height: auto;">
                    <div class="card-header pb-3 px-0 mx-4" style="border-bottom: none;">
                        <h2 class="h6 mb-0"><span class="fas fa-cogs text-primary mr-2"></span><?=yii::t('app','Definições')?>
                            <div class="position-relative float-right">
                                <a id="settingsDropdown1InvokerExample1_definicion" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded" href="javascript:;" role="button"
                                   aria-controls="settingsDropdown1_definicion"
                                   aria-haspopup="true"
                                   aria-expanded="false"
                                   data-unfold-event="click"
                                   data-unfold-target="#settingsDropdown1_definicion"
                                   data-unfold-type="css-animation"
                                   data-unfold-duration="300"
                                   data-unfold-delay="300"
                                   data-unfold-hide-on-scroll="true"
                                   data-unfold-animation-in="slideInUp"
                                   data-unfold-animation-out="fadeOut">
                                    <span class="fas fa-ellipsis-h btn-icon__inner"></span>
                                </a>

                                <div id="settingsDropdown1_definicion" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="settingsDropdown1InvokerExample1_definicion" style="min-width: 160px;">
                                    <a class="dropdown-item open-modal" href="javascript:void(0)"
                                        data-url="<?= Url::to(['/users/mudar'])?>" data-toggle="modal" data-modal-size="modal-md" data-title="Ediatr palavra-passe">
                                        <small class="fas fa-key dropdown-item-icon"></small>
                                        Editar Palavra passe
                                    </a>
                                </div>
                            </div>
                        </h2>
                    </div>
                    <hr class="mt-0 mb-0">
                    <div class="card-body p-4">

                        <?php if (Yii::$app->session->hasFlash('success-change')) : ?>
                            <div class="alert alert-success" id="alertSuccess" role="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <?= Yii::$app->session->getFlash('success-change') ?>
                            </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('error-change')) : ?>
                            <div class="alert alert-danger" id="alertError" role="alert">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <?= Yii::$app->session->getFlash('error-change') ?>
                            </div>
                        <?php endif; ?>

                        <?php if($modelUser): ?>
                        <div class="mb-4 border-bottom pb-2 mb-2">
                            <h3 class="h6 mb-1 font-weight-bold pt-4"><?=yii::t('app','Nome de utilizador')?> </h3>
                            <p><?= $modelUser->username ?></p>
                        </div>

                        <div class="mb-4">
                            <h3 class="h6 mb-1 font-weight-bold"><?=yii::t('app','Palavra passe')?></h3>
                            <input type="password" name="password" class="text bradius" value="<?= $modelUser->password_hash ?>">
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- fim de card dados pessoais -->
            </div>

        </div>
    </div>
</section>

<?php $this->registerJsFile('@web/biblioteca/vendor/jquery/dist/jquery.min.js'); ?>
<style>
    .text.bradius {
  border: none;
}
</style>