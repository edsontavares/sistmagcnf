<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;


?>
<div class="user-form">

    <?php $form = ActiveForm::begin([
        'options' => [
           
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
  
    
    <div class="row mb-5">
        <div class="col-sm-12">
            <label class="label-form">Palavra passe <span class="text-danger">*</span></label>
            <?= $form->field($model, 'password_hash')->passwordInput()->label(false) ?>
        </div>
    </div>
    <div class="row foter mt-2">
        <div class="col-sm-12 text-right">
            <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover" data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
            <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Guardar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded mr-5' : 'btn btn-indigo btn-sm rounded  mr-5','id'=>'btn-save']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

</div>
<style media="screen">
  
  .foter {
        position: absolute;
        bottom: 1px;
        width: 105%;
        background: white;
    }
</style>
