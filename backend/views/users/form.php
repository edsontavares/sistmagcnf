<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

?>
<div class="user-form">
    <?php $form = ActiveForm::begin([
        'options' => [
           
            'enctype' => 'multipart/form-data',
        ],
    ]); ?>
  <div class="row">
    <!--div class="col-sm-6">
     <label for="" class="type-label" >Nome</label>
     <?php /*= $form->field($model, 'username')->textInput([
            "class" => "form-control ",
            'placeholder' => "Introduzir morada",
            'aria-label' => "Introduzir morada",
            'required' => false,
            'aria-describedby' => "dataInicioLabel",
            'data-error-class' => "u-has-error",
            'data-msg' => "Por favor Introduzir morada curso.",
            'data-success-class' => "u-has-success",
            'data-rp-date-format' => "Y.m.d-H.i.s",   
        ])->label(false) */?>
    </div-->
    <div class="col-sm-6">
         <label for="" class="type-label" >Email <span class="text-danger">*</span></label>
         <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ->label(false)?>
     </div>
    <div class="col-sm-6">
        <label for="" class="type-label" >Contacto</label>
        <?= $form->field($model, 'contacto')->textInput([
            "class" => "form-control ",
            'placeholder' => "Introduzir contacto",
            'aria-label' => "Introduzir contacto",
            'required' => false,
            'aria-describedby' => "dataInicioLabel",
            'data-error-class' => "u-has-error",
            'data-msg' => "Por favor Introduzir morada curso.",
            'data-success-class' => "u-has-success",
            'data-rp-date-format' => "Y.m.d-H.i.s",   
        ])->label(false) ?>
    </div>
    
     <div class="col-sm-12">
        <label for="" class="type-label" >Localização </label>
        <?= $form->field($model, 'morada')->textInput([
            "class" => "form-control ",
            'placeholder' => "Introduzir morada",
            'aria-label' => "Introduzir morada",
            'required' => false,
            'aria-describedby' => "dataInicioLabel",
            'data-error-class' => "u-has-error",
            'data-msg' => "Por favor Introduzir morada curso.",
            'data-success-class' => "u-has-success",
            'data-rp-date-format' => "Y.m.d-H.i.s",   
        ])->label(false) ?>
     </div>
     <div class="col-sm-6">
         <!-- File Attachment Input -->
         <?php if($model->imagem == ''):?>
            <label for="" class="type-label" >Foto <span class="text-danger">*</span></label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Carregar capa</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-user" type="file" class="js-custom-file-attach file-attachment-input__label"
                                data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="user[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '/' . $model->imagem ?>" style="height: 200px; width: 100%; display:none"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                        class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                        style="width: 100%; display: none;" for="floorplanAttachmentInput">
                        Mudar Foto de Utilizador
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-user" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                            style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar Foto de Utilizador
                    </label>
                <?php endif; ?>
            </div>
          <?php else: ?>
                <label id="nameLabel">
                Foto utilizador
                <span class="text-danger">*</span>
            </label>
            <?php if (@$model->imagem == ''): ?>
                <div id="capa-continer">
                    <label class="file-attachment-input py-8" for="floorplanAttachmentInput">
                        <span class="d-block mb-2">Carregar capa</span>
                        <small class="d-block text-muted">Tamanho Maximo 3MB</small>
                        <input id="floorplanAttachmentInput" name="image-user" type="file" class="js-custom-file-attach file-attachment-input__label"
                                data-result-text-target="#floorplanFileUploadText">
                        <span id="floorplanFileUploadText"></span>
                        <input type="hidden" name="user[imagem]" value="<?= $model->imagem ?>">
                    </label>
                </div>
            <?php endif; ?>
            <img id="capa_preview" class="img-fluid" alt="" src="<?= '../../' . $model->imagem ?>" style="height: 200px; width: 100%;"/>

            <div class="media-body">
                <label id="image-capa-btn-attach"
                        class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1"
                        style="width: 100%; display: none;" for="floorplanAttachmentInput">
                        Mudar Foto de Utilizador
                </label>
                <?php if (@$model->imagem != ''): ?>
                    <input id="floorplanAttachmentInput" name="image-user" type="file" class="js-custom-file-attach file-attachment-input__label" data-result-text-target="#floorplanFileUploadText">
                    <label id="change" class="btn btn-block btn-secondary btn-sm transition-3d-hover file-attachment-btn mb-1 mb-sm-0 mr-1 mt-1"
                            style="width: 100%; display: block;" for="floorplanAttachmentInput">
                        Mudar Foto de Utilizador
                    </label>
                <?php endif; ?>
            </div>
          <?php endif; ?>
     </div>
  </div>
  
  <div class="row foter mt-2">
    <div class="col-sm-12 text-right">
        <button type="button" class="btn btn-sm rounded btn-soft-secondary transition-3d-hover" data-dismiss="modal" style="margin-right: 5px;"><i class="fa fa-times mr-1"></i><?= Yii::t('app', 'Cancelar') ?></button>
        <?= Html::submitButton($model->isNewRecord ? '<i class="far fa-save mr-2"></i>Guardar' : '<i class="far fa-save mr-2"></i>Guardar', ['class' => $model->isNewRecord ? 'btn btn-indigo btn-sm rounded mr-5' : 'btn btn-indigo btn-sm rounded  mr-5','id'=>'btn-save']) ?>
    </div>
  </div>
<?php ActiveForm::end(); ?>

</div>
<?php
    $scipt = <<<JS
    $(document).ready(function () {
        $('#floorplanAttachmentInput').change(function () {

            var reader = new FileReader()

            reader.onload = function (e) {

                $('#capa_preview').attr('src', e.target.result)
                $('#image-capa-btn-attach').css('display', 'block')
                $('#change').css('display', 'none')
                $('#capa-continer').css('display', 'none')
                $('#capa_preview').css('display', 'block')
            }
            reader.readAsDataURL(this.files[0])
        })
    })
JS;
    $this->registerJS($scipt);
?>
<style media="screen">
  
  .foter {
        position: absolute;
        bottom: 1px;
        width: 105%;
        background: white;
    }
</style>
