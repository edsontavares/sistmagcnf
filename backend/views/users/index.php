<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
?>
<div class="user-index">
    <div class="row mx-gutters-2">

        <?php if (Yii::$app->session->hasFlash("success-create")): ?>
        <div class="alert-success alert alert-dismissible fade show" role="alert" style="width:100%">
            <?= Yii::$app->session->getFlash("success-create") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash("error")): ?>
        <div class="alert-error alert alert-dismissible fade show" role="alert">
            <?= Yii::$app->session->getFlash("error") ?>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>

        <div class="col-md-10">
            <h4 class="font-weight-bold">Lista dos Utilizadores</h4>
        </div>
        <div class="col-md-2">
            <!--a class="float-right " href="<!?= Url::to(['/users/index'])?>">Voltar</a-->
        </div>
    </div>
    <hr>

    <table class="table" id="#mySearch-location">
        <thead class="top">
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Email</th>
                <th scope="col">Tipo de utilizador</th>
                <th scope="col">Opção</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($user as $users):?>
            <tr>
                <td><?= $users->username?></td>
                <td><?= $users->email?></td>
                <td>
                    <?php if($users->type_user == 'admin'):?>
                    Administrador
                    <?php elseif($users->type_user == 'dr'):?>
                    Director
                    <?php elseif($users->type_user == 'Ot'):?>
                    Ortopedista
                    <?php elseif($users->type_user == 'Ft'):?>
                    Fisioterapeuta
                    <?php elseif($users->type_user == 'rp'):?>
                    Recepcionistas
                    <?php elseif($users->type_user == 'as'):?>
                    Asistente social
                    <?php endif;?>

                </td>
                <td>
                    <div class="btn-group position-relative ">
                        <a id="" class="btn btn-sm btn-icon rounded " href="javascript:;" role="button"
                            aria-controls="#id" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown"
                            data-unfold-event="click" data-unfold-target="#" data-unfold-type="css-animation"
                            data-unfold-duration="300" data-unfold-delay="300" data-unfold-animation-in="slideInUp"
                            data-unfold-animation-out="fadeOut" style="width: 2.6rem;">
                            <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                        </a>

                        <div id="" class="dropdown-menu dropdown-unfold dropdown-menu-right"
                            aria-labelledby="id-invoker">

                            <?= Html::a('<i class="fas fa-eye  mr-2"></i> Ver', ['users/view', 'id'=>$users->id], ['class'=>"dropdown-item"])?>
                            <a href="#" class="dropdown-item open-modal" data-modal-size="modal-lg" data-toggle="modal"
                                data-url="<?= Url::to(['users/update', 'id'=>$users->id]) ?>"
                                data-title="<?= Yii::t('app', 'Editar utilizador') ?>">
                                <span class="far fa-edit btn-soft-primary mr-2"></span><?= Yii::t('app', 'Editar') ?>
                            </a>
                            <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger mr-2"></i> Eliminar', ['users/delete', 'id'=>$users->id], ['class'=>"dropdown-item"])?>
                        </div>
                    </div>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
</div>