<?php
use \yii\helpers\Url;
?>
<?php
    $this->title = $users->username;
?>
<?php if (Yii::$app->session->hasFlash("success-create")): ?>
<div class="alert-success alert alert-dismissible fade show" role="alert" style="width:100%" >
    <?= Yii::$app->session->getFlash("success-create") ?>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<?php endif; ?>
  <div class="bg-light border shadow-soft rounded p-6 mb-4">
      <div class="media">

          <div class="u-lg-avatar mb-3 mx-auto">
            <?php if (@$users->imagem == '') : ?>
              <img class="w-100 rounded shadow-soft" src="" title="" alt="">
            <?php else : ?>
              <img class="w-100 rounded shadow-soft" src="../../<?= $users->imagem ?>" title="" alt="">
            <?php endif; ?>
          </div>

          <div class="media-body ml-4">
            <div class="mb-3">
                <h4 class=""><?= $users->username ?></h4>
            </div>

              <div class="pb-1 mb-2">
                  <div id="all">
                     
                  </div>

                  
              </div>

              <div class="d-md-flex align-items-md-center">
                      <!-- Location -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h2 class="small text-secondary mb-0">Morada</h2>
                          <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                          <span class="align-middle"><?=$users->morada?></span>
                      </div>
                      <!-- End Location -->

                      <!-- Posted -->
                      <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                          <h3 class="small text-secondary mb-0">Contacto</h3>
                          <small class="fas fa-phone text-secondary align-middle mr-1"></small>
                          <span class="align-middle"><?=$users->contacto?></span>
                      </div>
                      <!-- End Posted -->
                      <!-- Posted -->
                      <div class="mb-3 mb-md-0">
                          <h5 class="small text-secondary mb-0">Email</h5>
                          <small class="fas fa-envelope text-secondary align-middle mr-1"></small>
                          <span class="align-middle"><?=$users->email?></span>
                      </div>
                      <!-- End Posted -->
                  </div>
          </div>

         
             <!-- End Settings -->

              <div class="mb-0">

              </div>
              <div class="media align-items-center text-right">
                <div class="ml-md-auto">
                  <!-- Settings -->
                  <div class="position-relative mb-3">
                    <a id="settingsDropdown1InvokerExample1" class="btn btn-sm btn-soft-secondary rounded" href="javascript:;" role="button"
                        aria-controls="settingsDropdown1"
                        aria-haspopup="true"
                        aria-expanded="false"
                        data-unfold-event="click"
                        data-unfold-target="#settingsDropdown1"
                        data-unfold-type="css-animation"
                        data-unfold-duration="300"
                        data-unfold-delay="300"
                        data-unfold-hide-on-scroll="true"
                        data-unfold-animation-in="slideInUp"
                        data-unfold-animation-out="fadeOut">
                      Editar<span class="fas fa-sort-down ml-2"></span>
                    </a>

                    <div id="settingsDropdown1" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="settingsDropdown1InvokerExample1" style="min-width: 160px;">
                      <a class="dropdown-item open-modal" href="javascript:void(0)"
                          data-url="<?= Url::to(['/users/updates', 'id'=>$users->id])?>" data-toggle="modal" data-modal-size="modal-lg" data-title="Ediatr perfil">
                          <small class="fas fa-user-tag dropdown-item-icon"></small>
                          Perfil
                      </a>
                      <a class="dropdown-item " href="<?= Url::to(['/users/difinicao'])?>"
                           >
                          <small class="fas fa-key dropdown-item-icon"></small>
                          Palavra passe
                      </a>

                    </div>
                  </div>
                </div>
              </div>
      </div>
</div>

<script>
  $(function(){
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
  });
</script>
<?php

$script = <<<JS

        $(document).on('click', '.all', function (e) {
            e.preventDefault();
            $('#all').attr('style', 'display: none;');
            $('#less').attr('style', 'display: block;');
        });

        $(document).on('click', '.less', function (e) {
            e.preventDefault();
            $('#all').attr('style', 'display: block;');
            $('#less').attr('style', 'display: none;');
        });
JS;
$this->registerJS($script);
?>
