<?php

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */

    /* @var $model \frontend\models\ResetPasswordForm */

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    $this->title = 'Mudar Palavra-passe';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div id="error"></div>
<div class="site-reset-password">
    <div class="mb-4">
        <h2 class="h5 text-primary font-weight-normal">Alterar sua <span class="font-weight-semi-bold">palavra-passe</span></h2>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'reset-password-form',
        'options' => [
            'class' => 'js-validate'
        ]
    ]); ?>

    <div class="js-form-message js-focus-state">
        <div class="form-group field-changepasswordform-password">
            <input
                type="password"
                id="changepasswordform-password"
                class="form-control"
                name="ChangePasswordForm[password]"
                placeholder="Introduza palavra-passe atual"
                aria-label="Introduza palavra-passe"
                required
                aria-describedby="passwordLabel changepasswordform-password-error"
                data-msg="Por favor introduza palavra-passe."
                data-error-class="u-has-error"
                data-success-class="u-has-success"
                aria-required="true"
                aria-invalid="true">
        </div>
    </div>

    <div class="js-form-message js-focus-state">
        <div class="form-group field-changepasswordform-password_change required">
            <input
                type="password"
                id="changepasswordform-password_change"
                class="form-control"
                name="ChangePasswordForm[password_change]"
                placeholder="Introduza a nova palavra-passe"
                aria-label="Introduza a nova palavra-passe"
                required
                min="6"
                aria-describedby="passwordLabel changepasswordform-password_change-error"
                data-msg="Por favor introduza a nova palavra-passe."
                data-error-class="u-has-error"
                data-success-class="u-has-success"
                aria-required="true"
                aria-invalid="true">
        </div>
    </div>

    <div class="js-form-message js-focus-state">
        <div class="form-group field-changepasswordform-password_change_repeat required">
            <input type="password" id="changepasswordform-password_change_repeat" class="form-control" name="ChangePasswordForm[password_change_repeat]" placeholder="Confirme a nova palavra-passe" aria-label="Confirme a nova palavra-passe" required aria-describedby="passwordLabel" data-msg="Por favor confirme a nova palavra-passe." data-error-class="u-has-error" data-success-class="u-has-success" aria-required="true">
        </div>
    </div>


    <div class="row foter">
        <div class="col-6">
            <button type="button" class="btn btn-sm btn-soft-secondary transition-3d-hover" data-dismiss="modal">Cancelar
            </button>
        </div>
        <div class="col-6">
            <button type="button" id="change-password" class="btn btn-sm btn-primary transition-3d-hover mr-1" style="float: right">Adicionar
            </button>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script !src="">
    //alert-success
    $("#change-password").click(function (e) {
        e.preventDefault();
        $('#error').empty();
        let password = $("#changepasswordform-password").val();
        let password_change = $("#changepasswordform-password_change").val();
        let password_change_repeat = $("#changepasswordform-password_change_repeat").val();

        if (password.length === 0) {
            set_error("Palavra-passe não pode ser nulo.");
            return;
        }

        if (password_change.length === 0) {
            set_error("Nova palavra-passe não pode ser nulo.");
            return;
        }

        if (password_change.length < 6 ) {
            set_error("Palavra-passe tem de ter pelo menus 6 caracteres.");
            return;
        }

        if (password_change !== password_change_repeat) {
            set_error("Palavra-passe não confere");
            return;
        }

        const body = {
            'ChangePasswordForm[password]': password,
            'ChangePasswordForm[password_change]': password_change,
            'ChangePasswordForm[password_change_repeat]': password_change_repeat
        };

        $.post(`${location.origin}/mudar`, body, function (data) {
            data = JSON.parse(data);
            if (data.status === 'success') {
                $('#modal-open').modal('hide');
                toastr.success("Palavra-passe atualizada com successo", "Atualizar palavra-passe");
            } else {
                set_error(data.message)
            }
        })
    });

    function set_error(message, type = 'alert-danger') {
        $('#error').append(
            '<div class="alert ' + type + '" id="' + ((type === "alert-danger") ? "alertError" : "alertSuccess") + '" role="alert">\n' +
            '    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>\n' +
            message +
            '</div>'
        );
    }
</script>
