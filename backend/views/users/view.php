<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
$this->title = $model->username;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">
     <div class="row">
         <div class="col-sm-6 ">
            <h1><?= Html::encode($this->title) ?></h1>
         </div>
         <div class="col-sm-6 text-right">
             <a class="" href="<?= Url::to(['/users'])?>">Voltar</a>
         </div>
     </div>
    <hr>
    <div class="card mb-2">
        <div class="card-body">
            <!-- Header -->
            <div class="media align-items-center mb-4">
                <!-- Avatar -->
                <div class="u-xl-avatar mr-4">
                    <img class="w-100 h-100 shadow-soft rounded-circle mx-auto" src="../../<?= @$model->imagem?>">
                </div>
                <!-- End Avatar -->
                <div class="media-body">
                    <h1 class="font-weight-bold h4">
                        <?= $model->username?>
                     </h1>

                    <p><?= $model->email?></p>
                    <!--small class="text-secondary"></small-->

                    <div class="card-footer border-top-0 pt-3 px-0 pb-4">
                        <div class="d-sm-flex align-items-sm-center">
                            <?php if($model->created_at):?>
                                <div class="u-ver-divider u-ver-divider--none-sm pr-4 mr-4 mb-3 mb-sm-0">
                                    <h2 class="small text-secondary mb-0">Data</h2>
                                    <small class="fas fa-calendar-alt text-secondary mr-1" aria-hidden="true"></small>
                                    <span class="align-middle font-size-1 font-weight-medium"><?= $model->created_at?> </span>
                                </div>
                            <?php endif;?>
                            <?php if($model->contacto):?>
                                <div class="u-ver-divider u-ver-divider--none-sm pr-4 mr-4 mb-3 mb-sm-0">
                                    <h2 class="small text-secondary mb-0">Contacto</h2>
                                    <small class="fas fa-calendar-alt text-secondary mr-1" aria-hidden="true"></small>
                                    <span class="align-middle font-size-1 font-weight-medium"><?= $model->contacto?> </span>
                                </div>
                            <?php endif;?>
                            <?php if($model->type_user):?>
                                <div class="u-ver-divider u-ver-divider--none-sm pr-4 mr-4 mb-3 mb-sm-0">
                                    <h2 class="small text-secondary mb-0">Tipo de utilizador</h2>
                                    <small class="fas fa-calendar-alt text-secondary mr-1" aria-hidden="true"></small>
                                    <span class="align-middle font-size-1 font-weight-medium">
                                        <?php if($model->type_user == 'admin'):?>
                                        Administrador
                                        <?php elseif($model->type_user == 'dr'):?>
                                        Director
                                        <?php elseif($model->type_user == 'Ot'):?>
                                        Ortopedista
                                        <?php elseif($model->type_user == 'Ft'):?>
                                        Fisioterapeuta
                                        <?php elseif($model->type_user == 'rp'):?>
                                        Recepcionistas
                                        <?php elseif($model->type_user == 'as'):?>
                                        Asistente social
                                        <?php endif;?>
                                    </span>
                                </div>
                            <?php endif;?>
                      
                            <!-- Review -->
                            <div class="ml-md-auto">
                                <a href="#" class="btn btn-soft-primary rounded btn-sm open-modal" data-modal-size="modal-lg" data-toggle="modal"
                                    data-url="<?= Url::to(['users/update', 'id'=>$model->id]) ?>"
                                    data-title="<?= Yii::t('app', 'Editar utilizador') ?>">
                                    <span class="fas fa-pencil-alt dropdown-item-icon mr-2"></span><?= Yii::t('app', 'Editar') ?>
                                </a>
                                <?= Html::a('<i class="fas fa-trash-alt btn-soft-danger mr-2"></i> Eliminar', ['users/delete', 'id'=>$model->id], ['class'=>"btn btn-sm btn-outline-danger rounded"])?>
                            </div>
                            <!-- End Review -->

                        </div>
                    </div>

                </div>
            </div>
            <!-- End Header -->
        </div>

    </div>



</div>