<?php

namespace common\componentes;

use Yii;
use Mpdf\Mpdf;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\Defaults;
# extenção usada : https://github.com/PHPOffice/PHPWord



/* @var $model common\models\Assignment */

class Create_doc {

    private $FICHEIRO_LOCAL = "/";
    private $model;
    private $type;
    private $doc;
    private $ObjetoDocumento;
    private $secsao;
    private $category_product = [];
    private $name;
    private $download;
    private $email_send;
    private $prefix;
    function __construct($model, $download = TRUE, $email_send = FALSE, $prefix = NULL) {

        Yii::info('Create_doc: Iniciar processo de exportação');
        //$download = TRUE;
        $this->model = $model;
        $this->download = $download;
        $this->email_send = $email_send;
        $this->name = "#" . $model->id_patient. " - Subscrição no pacote ";//. preg_replace('/(\/)/', '', $model->object_name);
        $this->creat_render();
        $this->prefix = $prefix;

        Yii::info('Create_doc: Render terminado');

        Yii::info('Create_doc: ficheiro: ' . $this->caminhoFicheiro());
    }

    private function NomeFicheiro() {
        return $this->model->id_patient. '.pdf';
    }

    public function caminhoFicheiro() {

        return $this->FICHEIRO_LOCAL . $this->NomeFicheiro();
    }

    private function fatura_head_render() {
        $head = '
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                table#t01, tr#t01, td#t01 { border-collapse: collapse; border: 1px solid black; }
                body {
                    margin-top: 2.5cm;
                    margin-left: 0cm;
                    margin-right: 0cm;
                    margin-bottom: 0.5cm;
                }
                footer {
                    position: fixed;
                    bottom: 0cm;
                    left: 0cm;
                    right: 0cm;
                    height: 1cm;
                    content: counter(page);
                }
                .pagenum:before {
                    content: counter(page);
                }
                header {
                    position: fixed;
                    top: 0cm;
                    left: 0cm;
                    right: 0cm;
                    height: 2.5cm;
                }
                table {
                    page-break-inside:avoid;
                }
                td {
                  vertical-align: top;
                }
                .title{
                  font-style: italic;
                  font-size: 20px;
                }
            </style>
        </head>
        <header>
            <table style="width:100%">
                  <tbody><tr>
                    <td><img src="./../recurso/img/Logo.svg" style="width:250px;"></td>
                    <td>
                      <small>
                          <table>
                              <tbody>
                                <tr>
                                  <td>Data de Fatura:</td>
                                  <td>data</td>
                                </tr>
                                <tr>
                                  <td>Fatura Nr.:</td>
                                  <td>'.$this->model->id_patient.'</td>
                                </tr>
                              </tbody>
                          </table>
                      </small>
                    </td>
                  </tr>
            </tbody>
          </table>
        </header>
        <br>
        <br>
        <br>
        <br>
        <footer>
            <hr>
            ACD -CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
        </footer>
        <br />
        ';
        return $head;
    }

    private function fatura_title_info_render() {
        $info_cliente_prestador = '
        <hr>
        <table style="width:100%">
          <tbody>
          <tr>
            <td class="title"><b>Dados pessoais:</b></td>
          </tr>
          <tr>
            <td><b>Nome</b></td>
            <td><b>Alcunha</></td>
            <td><b>Residencia</b></td>

          </tr>
          <tr>
            <td>'.$this->model->name.'</td>
            <td>'.$this->model->nickname.'</td>
            <td>'.$this->model->naturalness.'</td>

          </tr>
          <tr>
            <td><b>Nacionalidade</b></td>
            <td><b>Morada</b></td>
            <td><b>Data Nascimento</b></td>
          </tr>
          <tr>
            <td>'.$this->model->nationality.'</td>
            <td>'.$this->model->id_location.'</td>
            <td>'.$this->model->birth_date.'</td>
          </tr>

          <tr>
            <td><b>Email:</b></td>
            <td><b>Telefone</b></td>
            <td><b>Telemovel</b></td>
          </tr>
          <tr>
            <td>'.$this->model->email.'</td>
            <td>'.$this->model->mobile_phone.'</td>
            <td>'.$this->model->telephone.'</td>

          </tr>
          <tr>
            <td><b>Cni/Bi</b></td>
            <td><b>Sexo</b></td>
            <td><b>Nivel de Escolaridade<b></td>

          </tr>
          <tr>
            <td>'.$this->model->cni.'</td>
            <td>'.$this->model->sex.'</td>
            <td>'.$this->model->educational_level.'</td>
          </tr>
         

        </tbody>

        </table>
    <br>
    <hr>
    <br>
    ';
        return $info_cliente_prestador;
    }

    private function fatura_servicos_render() {
        $servicos_info = '
        <table style="width:100%">
          <tbody>
            <tr>
              <td class="title">
                <b>Situação Sócio-Familiar:</b>
              </td>
            </tr>
            <tr>
              <td><b>Estado Civil</b></td>
              <td>'.$this->model->marital_status.'</td>
            </tr>
            <tr>
              <td><b>Quantidade de filho</b></td>
              <td>'.$this->model->qtd_son.'</td>
            </tr>
            <tr>
              <td><b>Total da Familia</b></td>
              <td>'.$this->model->qtd_family.'</td>
            </tr>
            <tr>
              <td><b>Maior da Idade</b></td>
              <td>'.$this->model->adult.'</td>
            </tr>
            <tr>
              <td><b>Menor da idade</b>
              </td>
              <td>'.$this->model->minor.'</td>
            </tr>
            <tr>
              <td><b>Pré-escolar</b></td>
              <td>'.$this->model->preschool.'</td>
            </tr>
            <tr>
              <td><b>E.B.O</b></td>
              <td>'.$this->model->ebo.'</td>
            </tr>
            <tr>
              <td><b>Secundário</b></td>
              <td>'.$this->model->secondary.'</td>
            </tr>
            <tr>
              <td><b>Universidade</b></td>
              <td>'.$this->model->college_student.'</td>
            </tr>
            <tr>
              <td><b>Trabalhador</b></td>
              <td>'.$this->model->worker.'</td>
            </tr>
            <tr>
              <td><b>Desempregado</b></td>
              <td>'.$this->model->Unemployed.'</td>
            </tr>
            <hr>
            <tr>
              <td class="title"><b>Situação Profissional:</b></td>
            </tr>
            <tr>
              <td><b>Nome de Actividade Profissional</b></td>
              <td><b>Tipo de Actividade Profissional</b></td>
            </tr>
            <tr>
              <td>'.$this->model->profession.'</td>
              <td>'.$this->model->institution_type.'</td>
              
            </tr>
            <tr>
          
              <td><b>Nome da Escola<b></td>
              <td><b>Nome de Seguradora<b></td>

            </tr>
            <tr>
              <td>'.$this->model->student.'</td>
              <td>'.$this->model->insurance_institution.'</td>
            </tr>
            <hr>
            <tr>
              <td class="title"><b>Dependência dos Outros:</b></td>
            </tr>
            <tr>
              <td ><b>Nome Parentesco<b></td>
              <td>'.$this->model->name_parents.'</td>
            </tr>
            <tr>

              <td><b>Morada de Parentesco<b></td>
              <td>'.$this->model->address.'</td>
            </tr>
            <tr>
              <td><b>Grau de Parentesco<b></td>
              <td>'.$this->model->degree.'</td>
            </tr>
        </tbody>
      </table><br><br>
        ';
        return $servicos_info;
    }
    private function fatura_end_render() {
        $end = '
            </body>
            </html>
        ';
        return $end;
    }


    private function creat_render() {

        $html = '';
        $html .= $this->fatura_head_render();
        $html .= $this->fatura_title_info_render();
        $html .= $this->fatura_servicos_render();
       
        $html .= $this->fatura_end_render();
//        $html .= $this->assignment_task_effort_render();
//        $html .= $this->assignment_material_info_render();
//        $html .= $this->assignment_end_render();


        // $mpdf = new Mpdf();
        // $mpdf->load_html($html);
        // $mpdf->setPaper('A4', 'portrait');
        // $mpdf->render();
        //
        // $mpdf->getCanvas()->page_text(505, 793.5, "Pagina {PAGE_NUM} / {PAGE_COUNT}", "", 12, array(0,0,0));
        // file_put_contents($this->caminhoFicheiro(), $mpdf->output());
        // if(!$this->email_send){
        //     if ($this->download) {
        //         $mpdf->stream($this->NomeFicheiro());
        //         $this->download($html);
        //     }else{
        //         $mpdf->stream($this->NomeFicheiro(),array("Attachment"=>0));
        //     }
        // }
        $mpdf = new mPDF();
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = 'UTF-8';
        $mpdf->WriteHTML($html);
        $mpdf->Output('meu-pdf', 'I');
        $mpdf->getCanvas()->page_text(505, 793.5, "Pagina {PAGE_NUM} / {PAGE_COUNT}", "", 12, array(0,0,0));
        file_put_contents($this->caminhoFicheiro(), $mpdf->output());
        if(!$this->email_send){
            if ($this->download) {
                $mpdf->stream($this->NomeFicheiro());
                $this->download($html);
            }else{
                $mpdf->stream($this->NomeFicheiro(),array("Attachment"=>0));
            }
        }

    }

    private function download($html) {

        header("Content-type:application/pdf");
        header("Content-Disposition: attachment; filename='" . $this->NomeFicheiro() . "'");
        readfile($this->caminhoFicheiro());
        $this->remove();
    }

    public function remove() {
        unlink($this->caminhoFicheiro());
    }

    // private function font_AssignmentName() {

    //     $fonte = new Font();

    //     $fonte->setBold(true)
    //         ->setName('Times New Roman')
    //         ->setSize(18);

    //     return $fonte;
    // }

    // private function font_title() {

    //     $fonte = new Font();

    //     $fonte->setBold(true)
    //         ->setSize(12)
    //         ->setName('Times New Roman');

    //     return $fonte;
    // }

    // private function font_title_underline() {

    //     $fonte = new Font();

    //     $fonte->setName('Times New Roman')
    //         ->setSize(12)
    //         ->setUnderline()
    //         ->setBold();

    //     return $fonte;
    // }

}
