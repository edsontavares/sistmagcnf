<?php

namespace common\componentes;

use Yii;
use Mpdf\Mpdf;
use yii\helpers\Url;
use yii\widgets\DetailView;
use common\models\Defaults;
# extenção usada : https://github.com/PHPOffice/PHPWord



/* @var $model common\models\Assignment */

class Create_docad {

    private $FICHEIRO_LOCAL = "/";
    private $model;
    private $type;
    private $doc;
    private $ObjetoDocumento;
    private $secsao;
    private $category_product = [];
    private $name;
    private $download;
    private $email_send;
    private $prefix;
    function __construct($model, $download = TRUE, $email_send = FALSE, $prefix = NULL) {

        Yii::info('Create_doc: Iniciar processo de exportação');
        //$download = TRUE;
        $this->model = $model;
        $this->download = $download;
        $this->email_send = $email_send;
        $this->pillow = "#" . $model->id_adptacao. " - Subscrição no pacote ";//. preg_replace('/(\/)/', '', $model->object_name);
        $this->creat_render();
        $this->prefix = $prefix;

        Yii::info('Create_doc: Render terminado');

        Yii::info('Create_doc: ficheiro: ' . $this->caminhoFicheiro());
    }

    private function NomeFicheiro() {
        return $this->model->id_adptacao. '.pdf';
    }

    public function caminhoFicheiro() {

        return $this->FICHEIRO_LOCAL . $this->NomeFicheiro();
    }

    private function fatura_head_render() {
        $head = '
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                table#t01, tr#t01, td#t01 { border-collapse: collapse; border: 1px solid black; }
                body {
                    margin-top: 2.5cm;
                    margin-left: 0cm;
                    margin-right: 0cm;
                    margin-bottom: 0.5cm;
                }
                footer {
                    position: fixed;
                    bottom: 0cm;
                    left: 0cm;
                    right: 0cm;
                    height: 1cm;
                    content: counter(page);
                }
                .pagenum:before {
                    content: counter(page);
                }
                header {
                    position: fixed;
                    top: 0cm;
                    left: 0cm;
                    right: 0cm;
                    height: 2.5cm;
                }
                table {
                    page-break-inside:avoid;
                }
                td {
                  vertical-align: top;
                }
                .title{
                  font-style: italic;
                  font-size: 20px;
                }
            </style>
        </head>
        <header>
            <table style="width:100%">
                  <tbody><tr>
                    <td><img src="./../recurso/img/Logo.svg" style="width:250px;"></td>
                    <td>
                      <small>
                          <table>
                              <tbody>
                                <tr>
                                  <td>Data de Fatura:</td>
                                  <td>data</td>
                                </tr>
                                <tr>
                                  <td>Fatura Nr.:</td>
                                  <td>'.$this->model->id_adptacao.'</td>
                                </tr>
                              </tbody>
                          </table>
                      </small>
                    </td>
                  </tr>
            </tbody>
          </table>
        </header>
        <br>
        <br>
        <br>
        <br>
        <footer>
            <hr>
            ACD -CENORF.- CP 26-C  | Praia, A.S. Filipe  - Cabo Verde | cenorf@sapo.cv  264 76 9 
        </footer>
        <br />
        ';
        return $head;
    }

    private function fatura_title_info_render() {
        $info_cliente_prestador = '
        <hr>
        <table style="width:100%">
          <tbody>
          <tr>
            <td class="title"><b>Dados pessoais:</b></td>
          </tr>
          <tr>
            <td><b>Nome de Paciente</b></td>
            <td><b>Almofada</></td>
            <td><b>Colete</b></td>

          </tr>
          <tr>
            <td>'.$this->model->id_patient.'</td>
            <td>'.$this->model->pillow.'</td>
            <td>'.$this->model->vest.'</td>

          </tr>
          <tr>
            <td><b>Cinto Seguramça</b></td>
            <td><b>Apoio Cabeça</></td>
            <td><b>Proteção de Rodas</b></td>

          </tr>
          <tr>
            <td>'.$this->model->seat_belt.'</td>
            <td>'.$this->model->head_support.'</td>
            <td>'.$this->model->wheel_protection.'</td>

          </tr>
          <tr>
            <td><b>Enchimento Lateral</b></td>
            <td><b>Enchimento Posterior</></td>
            <td><b>Cinto Proteção Pês</b></td>

          </tr>
          <tr>
            <td>'.$this->model->side_filling.'</td>
            <td>'.$this->model->head_support.'</td>
            <td>'.$this->model->backfill.'</td>

          </tr>
          <tr>
          <td><b>Cinto Proteção Perna</b></td>
          <td><b>Cinto Proteção Pês</></td>
          <td><b>Cinto Proteção Pêss</b></td>

        </tr>
        <tr>
          <td>'.$this->model->foot_support_climb.'</td>
          <td>'.$this->model->foot_protection_belt.'</td>
          <td>'.$this->model->nest_separator.'</td>

        </tr>
        <tr>
          <td><b>Outros</b></td>
          <td><b>Data Criação</></td>
          <td><b>Técnico Responsável</></td>

        </tr>
        <tr>
          <td>'.$this->model->others.'</td>
          <td>'.$this->model->create_at.'</td>
          <td>'.$this->model->nameEmployees.'</td>

        </tr>
        </tbody>

        

        </table>
        <p>
        '.$this->model->description.'
        </p>
    <br>
    <hr>
    <br>
    ';
        return $info_cliente_prestador;
    }

   
    private function fatura_end_render() {
        $end = '
            </body>
            </html>
        ';
        return $end;
    }


    private function creat_render() {

        $html = '';
        $html .= $this->fatura_head_render();
        $html .= $this->fatura_title_info_render(); 
        $html .= $this->fatura_end_render();

        $mpdf = new mPDF();
        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = 'UTF-8';
        $mpdf->WriteHTML($html);
        $mpdf->Output('meu-pdf', 'I');
        $mpdf->getCanvas()->page_text(505, 793.5, "Pagina {PAGE_NUM} / {PAGE_COUNT}", "", 12, array(0,0,0));
        file_put_contents($this->caminhoFicheiro(), $mpdf->output());
        if(!$this->email_send){
            if ($this->download) {
                $mpdf->stream($this->NomeFicheiro());
                $this->download($html);
            }else{
                $mpdf->stream($this->NomeFicheiro(),array("Attachment"=>0));
            }
        }

    }

    private function download($html) {

        header("Content-type:application/pdf");
        header("Content-Disposition: attachment; filename='" . $this->NomeFicheiro() . "'");
        readfile($this->caminhoFicheiro());
        $this->remove();
    }

    public function remove() {
        unlink($this->caminhoFicheiro());
    }
}
