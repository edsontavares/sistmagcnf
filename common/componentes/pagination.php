<?php
    function getParamsArray($params)
    {
        return array_map(
            function ($v, $k) {
                if (is_array($v)) {
                    return $k . '[]=' . implode('&' . $k . '[]=', $v);
                } else {
                    return $k . '=' . $v;
                }
            },
            $params,
            array_keys($params));

    }

    // Table qith model is type (yii\data\ActiveDataProvider) have per-page
    // another views work with item_numbers to paginate
    if (@$item_numbers) {
        $perPage = $item_numbers;
    } else {
        $perPage = @Yii::$app->request->get('per-page') ? Yii::$app->request->get('per-page') : 20;
    }
    // calculate total page number
    $totalPage = ceil(intval($total) / intval($perPage));

    // get current page number
    $page = Yii::$app->request->get("page");
    $page = $page ? intval($page) : 1;

    // get view url that render this component
    $simbolo = @$prettyUrl && $prettyUrl == 'active' ? '?' : '&';
    $url = $url ? $url . $simbolo : '';

    // Total page is displayed on pagination with more that 3 item
    $totalItemShow = 3;

    // Get the first and last item displayed
    $firstItemNumber = $lastItemNumber = 1;
    if ($totalPage > 3) {
        if ($page == 1) {
            $firstItemNumber = 1;
            $lastItemNumber = $totalItemShow;
        } else if ($page == $totalPage) {
            $firstItemNumber = $page - $totalItemShow;
            $lastItemNumber = $page;
        } else {
            $firstItemNumber = $page - 1;
            $lastItemNumber = $page + 1;
        }
    } else {
        $firstItemNumber = 1;
        $lastItemNumber = $totalPage;
    }

    // Get URL PARAMS
    $params = Yii::$app->request->get();

    $params['page'] = 1;
    $params['per-page'] = $perPage;
    $params['item_numbers'] = $perPage;

    // maque query
    $stringParams = implode('&', getParamsArray($params));
    $firstPageLink = $url . $stringParams;

    $params['page'] = $totalPage;
    $stringParams = implode('&', getParamsArray($params));

    $lastPageLink = $url . $stringParams;
?>
    <div class="d-flex justify-content-between align-items-center">
        <nav id="datatablePagination" aria-label="Activity pagination">
            <ul class="pagination mb-0">
                <li class="page-item">
                    <a id="datatablePaginationPrev"
                       class="page-link"
                       href="javascript:void(0)"
                       aria-label="Previous">
                        <span aria-hidden="true">«</span>
                    </a>
                </li>

                <?php if ($firstItemNumber != 1): ?>
                    <li class="page-item">
                        <a id="datatablePaginationPrev"
                           class="page-link"
                           href="<?= $firstPageLink ?>"
                           aria-label="Previous">
                            <span aria-hidden="true">Primeiro</span>
                        </a>
                    </li>
                <?php endif; ?>


                <?php for ($i = $firstItemNumber; $i <= $lastItemNumber; $i++) : ?>
                    <?php
                        $params['page'] = $i;
                        $stringParams = implode('&', getParamsArray($params));

                        $link = $url . $stringParams;
                    ?>
                    <li class="page-item <?= $page === $i ? 'active' : '' ?>">
                        <a id="datatablePaginationPage<?= $i ?>"
                           class="page-link"
                           href="<?= $link ?>"
                           data-dt-page-to="0"><?= $i ?></a></li>
                <?php endfor; ?>

                <?php if ($lastItemNumber != $totalPage): ?>
                    <li class="page-item">
                        <a id="datatablePaginationPrev"
                           class="page-link"
                           href="<?= $lastPageLink ?>"
                           aria-label="Previous">
                            <span aria-hidden="true">Ultimo</span>
                        </a>
                    </li>
                <?php endif; ?>

                <li class="page-item">
                    <a id="datatablePaginationNext"
                       class="page-link"
                       href="javascript:void(0)"
                       aria-label="Next">
                        <span aria-hidden="true">»</span>
                    </a>
                </li>
            </ul>
        </nav>

        <small id="datatableInfo" class="text-secondary"></small>
    </div>

<?php
    //        $simbolo = @$prettyUrl && $prettyUrl == 'active' ? '?' : '&';

    $script = <<<JS


        $(function() {
            $('#datatablePaginationPrev').click(function() {
              changeCurrentPage(-1)
            })

             $('#datatablePaginationNext').click(function() {
              changeCurrentPage(+1)
            })

            const urlParams = new URLSearchParams(window.location.search);

            function changeCurrentPage(page = 1) {

                const currentPage = $page
                const totalPages = $totalPage
                const perPage = $perPage
                const queryLink = location.origin + '$url'
                const goToPage = currentPage + page

                urlParams.set('page', goToPage)
                urlParams.set('per-page', perPage)
                urlParams.set('item_numbers', perPage)

                console.log(urlParams.toString(), goToPage, totalPages)

                if (goToPage > 0 && goToPage <= totalPages) {
                   window.location.href = queryLink  + urlParams.toString()
                }
            }
        })
JS;

    $this->registerJS($script);

?>
