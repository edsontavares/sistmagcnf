<?= $form->field(
    $model,
    $attribute,
    [
        'inputOptions' => [
            'class' => 'form-control __passwordInput',
            'aria-describedby' => "__toogleInputType",
            'placeholder' => @$placeholder ? $placeholder : '',
        ],
        'inputTemplate' => "
            <div class='input-group'>
                {input}
                <div class='input-group-append'>
                    <button class='btn __toogleInputType' id='__toogleInputType' type='button' style='border: 1px solid #ccc;'>
                        <span id='item' class='fas fa-eye-slash'></span>
                    </button>
                </div>
            </div>
        "
    ]
)->label(@$label ? true : false)->passwordInput() ?>

<?php
    
    $script = <<<JS
        $('.__passwordInput').blur(function() {
          $(this)
            .closest('div.input-group')
            .children('.input-group-append')
            .children('.btn')
            .attr('style', "border: 1px solid #ccc;")
        })
        
         $('.__passwordInput').focus(function() {
          $(this)
            .closest('div.input-group')
            .children('.input-group-append')
            .children('.btn')
            .attr('style', "border: 1px solid #3399ff;")

        })
        
        var currentTypeIsPassword = true;
        $('.__toogleInputType').click(function() {
            
            if (currentTypeIsPassword) {
                $(this)
                    .closest('div.input-group')
                    .children('.__passwordInput')
                    .attr('type', "text")
                    
                    $(this).html("<span id='item' class='fas fa-eye'></span>")
            }  else {
                $(this)
                    .closest('div.input-group')
                    .children('.__passwordInput')
                    .attr('type', "password")
                 
                    $(this).html("<span id='item' class='fas fa-eye-slash'></span>")
            }
            currentTypeIsPassword =!currentTypeIsPassword
        })
JS;
    $this->registerJs($script);

?>

