<div>
    <div style="text-align:center;background-color:#f8f8f8">
        <div style="display:inline-block;max-width:880px;width:100%">
            <table border="0" cellpadding="0" cellspacing="0" id="m_-2057454157133139132tbl" width="100%">
                <tbody>
                <tr>
                    <td align="center" style="padding:45px 0px 0px 0px;">
                        <a href="<?=Yii::$app->params['baseUrl'] ?>/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.shyftplan.com&amp;source=gmail&amp;ust=1529512541094000&amp;usg=AFQjCNFsT1B3xLWysfD9lJlIGy0qrGMuXw">
                            <img width="100" src="<?= Yii::$app->params['baseUrl'] ?>/biblioteca/svg/logos/empregoscv.png" class="CToWUd" alt="Empregos.cv" title="Empregos.cv" style="width:45%;">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="color:#b5b5b5;padding-bottom:15px;font-size:14px"></td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#f8f8f8" valign="top">
                        <table bgcolor="#FFF" border="0" cellspacing="0" width="90%">
                            <tbody>
                            <tr>
                                <td align="center" style="padding:20px 0px;border-radius:3px;border:solid 1px #eee">
                                    <table border="0" cellpadding="0" cellspacing="0" width="90%">
                                        <tbody>
                                            <tr>
                                                <td style="line-height:1.2em;font-size:16px" valign="top">
                                                    <?= $conteudo ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        <table bgcolor="#F8F8F8" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td align="center" style="line-height:1.2em" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="80%">
                                        <tbody>
                                        <tr>
                                            <td align="center" style="color:#b5b5b5;padding:20px 0 15px 0;font-size:14px">
                                                Praia | Palmarejo | Avenida Santiago | Prédio Ordem dos Engenheiros, 5º andar, Direito
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="color:#b5b5b5;padding:15px 0;font-size:14px;border-top:1px solid #eee">

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" style="font-size:12px;color:#ccc;padding:10px 0;text-align:center">
                                                Copyright © <?= date('Y') ?> <a href="https://www.cenorf.cv" target="_blank" style="color:#3399ff;">Cenorf</a><?= yii::t('app',' Todos os direitos reservados.')?>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="yj6qo"></div>
            <div class="adL"></div></div>
            <div class="adL"></div>
          </div>
      <div class="adL"></div>
</div>
