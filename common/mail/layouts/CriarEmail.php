<?php

namespace common\mail\layouts;
use Yii;
use yii\web\Controller;
use app\models\User;
use frontend\models\Files;

/**
 * Description of Emails
 *
 * @author jandrade
 */
class CriarEmail  extends Controller{

    private $titulo;
    private $conteudo;
    public  $layout;
    private $email;

    public function __construct($titulo, $conteudo, $mailer = 'mailer', $layout = 'html') {
        $this->titulo = $titulo;
        $this->conteudo = $conteudo;
        $this->layout = $layout;
        $this->email = $mailer;
    }


    public function send($para, $resposta='info@waoempleos.com', $de = ['info@waoempleos.com'=>'waoempleos.com'], $Cc = [], $anexoConteodo = [], $anexoFicheiro = []) {
       Yii::$app->mailqueue->htmlLayout = false;
        if($this->email == 'mailer'){
            $this->email = Yii::$app->mailqueue->compose('@common/mail/layouts/'.$this->layout, ['titulo' => $this->titulo,
                    'conteudo' => $this->conteudo,
                        ]
                );
        }  elseif ($this->email == 'mailerb') {
            $this->email = Yii::$app->mailqueueb->compose('@common/mail/layouts/'.$this->layout, ['titulo' => $this->titulo,
                    'conteudo' => $this->conteudo,
                        ]
                );
        }  elseif ($this->email == 'mailerc') {
            $this->email = Yii::$app->mailqueuec->compose('@common/mail/layouts/'.$this->layout, ['titulo' => $this->titulo,
                    'conteudo' => $this->conteudo,
                        ]
                );
        }  elseif ($this->email == 'mailerd') {
            $this->email = Yii::$app->mailqueued->compose('@common/mail/layouts/'.$this->layout, ['titulo' => $this->titulo,
                    'conteudo' => $this->conteudo,
                        ]
                );
        }

                $this->email->setFrom($de)
                ->setTo($para)
                ->setCc($Cc)
                ->setReplyTo($resposta)
                ->setSubject($this->titulo);
        foreach ($anexoConteodo as $value) {
            $this->email->attachContent($value['conteudo'], ['fileName' => $value['nome'], 'contentType' => $value['tipo']]);
        }
        foreach ($anexoFicheiro as $value) {
          $file = Files::find()->where(['full_path'=>$value])->one();
           // $this->email->attach($value);
          // $this->email->attachContent(file_get_contents($value), ['fileName' => basename($value), 'contentType' => "png"]);
           $this->email->attachContent(file_get_contents($value), ['fileName' => $file->nome_original, 'contentType' => filetype($value)]);
        }

        if($this->email->send()){
            return 0;
        }  else {
            return $this->email->queue();
        }

    }
}
