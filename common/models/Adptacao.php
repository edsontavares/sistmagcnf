<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "adptacao".
 *
 * @property int $id_adptacao
 * @property int $id_query
 * @property int $id_patient
 * @property string $entry_date
 * @property string $description
 * @property string $pillow
 * @property string $nameEmployees
 * @property string $vest
 * @property string $seat_belt
 * @property string $head_support
 * @property string $wheel_protection
 * @property string $side_filling
 * @property string $backfill
 * @property string $nest_separator
 * @property string $protection_belt
 * @property string $foot_protection_belt
 * @property string $foot_support_climb
 * @property string $others
 * @property string $update_at
 * @property string $measure_right
 * @property int $id_employees
 */
class Adptacao extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adptacao';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_adptacao'], 'required'],
            [['id_adptacao', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['entry_date'], 'string', 'max' => 11],
            [['description', 'pillow', 'head_support', 'wheel_protection', 'side_filling', 'backfill', 'nest_separator', 'protection_belt', 'foot_protection_belt', 'foot_support_climb', 'measure_right'], 'string', 'max' => 200],
            [['vest','nameEmployees',], 'string','max' => 255],
            [['seat_belt'], 'string', 'max' => 300],
            [['others'], 'string', 'max' => 400],
            [['id_adptacao'], 'unique'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_adptacao' => 'Id Adptacao',
            'id_query' => 'Id Query',
            'id_patient' => 'Id Patient',
            'entry_date' => 'Entry Date',
            'description' => 'Description',
            'pillow' => 'Pillow',
            'vest' => 'Vest',
            'seat_belt' => 'Seat Belt',
            'head_support' => 'Head Support',
            'wheel_protection' => 'Wheel Protection',
            'side_filling' => 'Side Filling',
            'backfill' => 'Backfill',
            'nest_separator' => 'Nest Separator',
            'protection_belt' => 'Protection Belt',
            'foot_protection_belt' => 'Foot Protection Belt',
            'foot_support_climb' => 'Foot Support Climb',
            'others' => 'Others',
            'measure_right' => 'Measure Right',
            'id_employees' => 'Id Employees',
        ];
    }

     public function getQuerys()
    {
        
        return $this->hasOne(Query::className(), ['id_query' => 'id_query']);
    }
}
