<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Adptacao;

/**
 * AdptacaoSearch represents the model behind the search form of `common\models\Adptacao`.
 */
class AdptacaoSearch extends Adptacao
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_adptacao', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['entry_date', 'description', 'pillow', 'vest', 'seat_belt', 'head_support', 'wheel_protection', 'side_filling', 'backfill', 'nest_separator', 'protection_belt', 'foot_protection_belt', 'foot_support_climb', 'others', 'measure_right'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adptacao::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_adptacao' => $this->id_adptacao,
            'id_query' => $this->id_query,
            'id_patient' => $this->id_patient,
            'id_employees' => $this->id_employees,
        ]);

        $query->andFilterWhere(['like', 'entry_date', $this->entry_date])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'pillow', $this->pillow])
            ->andFilterWhere(['like', 'vest', $this->vest])
            ->andFilterWhere(['like', 'seat_belt', $this->seat_belt])
            ->andFilterWhere(['like', 'head_support', $this->head_support])
            ->andFilterWhere(['like', 'wheel_protection', $this->wheel_protection])
            ->andFilterWhere(['like', 'side_filling', $this->side_filling])
            ->andFilterWhere(['like', 'backfill', $this->backfill])
            ->andFilterWhere(['like', 'nest_separator', $this->nest_separator])
            ->andFilterWhere(['like', 'protection_belt', $this->protection_belt])
            ->andFilterWhere(['like', 'foot_protection_belt', $this->foot_protection_belt])
            ->andFilterWhere(['like', 'foot_support_climb', $this->foot_support_climb])
            ->andFilterWhere(['like', 'others', $this->others])
            ->andFilterWhere(['like', 'measure_right', $this->measure_right]);

        return $dataProvider;
    }
}
