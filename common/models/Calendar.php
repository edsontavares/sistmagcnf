<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calendar".
 *
 * @property int $id_calendar
 * @property int|null $id_patient
 * @property int $id_employees
 * @property int id_query
 * @property string $update_at
 * @property string $data
 * @property string $created_at
 * @property string $name_patient
 * @property string $type
 * @property string $state
 * @property string $name
 * @property string $hora_inicio
 * @property string $hora_fim
 */
class Calendar extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_employees','id_query'], 'integer'],
            //[['id_employees', 'update_at', 'date', 'created_at', 'state', 'name'], 'required'],
            [['update_at', 'data', 'created_at', 'state'], 'safe'],
            [['name','hora_inicio','hora_fim','name_patient','type'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_calendar' => 'Id Calendar',
            'id_patient' => 'Id Patient',
            'id_employees' => 'Id Employees',
            'update_at' => 'Update At',
            'data' => 'Date',
            'created_at' => 'Created At',
            'state' => 'State',
            'name' => 'Name',
        ];
    }
    public function getPatients()
    {
        
        return $this->hasOne(Patient::className(), ['id_patient' => 'id_patient']);
    }

    public function getEmployes()
    {
        
        return $this->hasOne(Employees::className(), ['id_employees' => 'id_employees']);
    }

    
}
