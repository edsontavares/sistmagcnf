<?php

namespace common\models;
use borales\extensions\phoneInput\PhoneInputValidator;

use Yii;

/**
 * This is the model class for table "employees".
 *
 * @property int $id_employees
 * @property int $id_employees_type
 * @property int $id_location
 * @property string $name
 * @property string|null $telephone
 * @property string|null $mobilephone
 * @property string bi_ficheiro
 * @property string nif_ficheiro
 * @property string|null $email
 * @property string|null $cni
 * @property int|null $bi
 * @property int|null $nif
 * @property int|null $nib
 * @property int|null $insurance_number
 * @property int|null $accountnumber
 * @property string|null $date_start
 * @property string $date_hurt_end
 * @property string|null $date_end
 * @property string|null $renewal_date
 * @property string $hiring
 * @property int $insurance_numberaccontnumber
 * @property int $bank_name
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 * @property string $until_now
 * * @property string $imagem
 * @property EmployeesType $employeesType 
  * @property Location $location
 */

class Employees extends \yii\db\ActiveRecord
{

    public static $NOME_BANCO = [
        'BAI(Banco africano de investimento)',
        'BCA(Banco comercial do atlântico)',
        'BCN(Banco caboverdiano de negócios)',
        'BI(Banco Interatlantico)',
        'Caixa económica',
        'Ecobank',
        'iib Cabo Verde'
    ];

    public static $CONTRATO = [
        'Tempo inteiro',
        'Temporário',
        'Prestação serviço',
        'Part-Time',
        'Voluntariado',
        'Estagio',
    ];
  
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_employees_type', 'id_location','date_start','name'], 'required'],
            [['id_employees_type', 'id_location','bi', 'nif', 'nib', 'insurance_number', 'accountnumber', 'insurance_numberaccontnumber', 'bank_name'], 'integer'],
            [['date_start', 'date_end', 'renewal_date', 'update_at', 'create_at','date_hurt_end'], 'safe'],
            [['name', 'telephone', 'email','cni'], 'string', 'max' => 200],
            //[['mobilephone'], 'string', 'max' => 15],
            [['hiring','bi_ficheiro','nif_ficheiro','imagem'], 'string', 'max' => 255],
            [['state'], 'string', 'max' => 11],
            [['email'], 'email'],
            [['name'], 'required', 'message' =>'Introduzir nome'],
            [['date_end'],'required', 'when' => function($model){ return $model->until_now == '0' || $model->until_now == null; }, 'whenClient' => "function (attribute, value) { return $('#formacao-until_now').val() == '0'; }", 'message'=>'Indica a data ou tempo de contratção'],
            [['until_now'], 'string', 'max' => 1],
            [['mobilephone'], 'string'],
            [['mobilephone'], PhoneInputValidator::className()],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_employees' => 'Id Employees',
            'id_employees_type' => 'Id Employees Type',
            'id_location' => 'Id Location',
            'name' => 'Name',
            'telephone' => 'Telephone',
            'mobilephone' => 'Mobilephone',
            'email' => 'Email',
            'cni' => 'Cni',
            'bi' => 'Bi',
            'nif' => 'Nif',
            'nib' => 'Nib',
            'insurance_number' => 'Insurance Number',
            'accountnumber' => 'Accountnumber',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'renewal_date' => 'Renewal Date',
            'hiring' => 'Hiring',
            'insurance_numberaccontnumber' => 'Insurance Numberaccontnumber',
            'bank_name' => 'Bank Name',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }

    public function getEmployeesTypes()
    {
        
        return $this->hasOne(EmployeesType::className(), ['id_employees_type' => 'id_employees_type']);
    }

    public function getLocation()
    {
        
        return $this->hasOne(Location::className(), ['id_location' => 'id_location']);
    }

}
