<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Employees;

/**
 * EmployeesSearch represents the model behind the search form of `common\models\Employees`.
 */
class EmployeesSearch extends Employees
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_employees', 'id_employees_type', 'id_location', 'cni', 'bi', 'nif', 'nib', 'insurance_number', 'accountnumber', 'insurance_numberaccontnumber', 'bank_name'], 'integer'],
            [['name', 'telephone', 'mobilephone', 'email', 'date_start', 'date_end', 'renewal_date', 'hiring', 'update_at', 'create_at', 'state'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employees::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_employees' => $this->id_employees,
            'id_employees_type' => $this->id_employees_type,
            'id_location' => $this->id_location,
            'cni' => $this->cni,
            'bi' => $this->bi,
            'nif' => $this->nif,
            'nib' => $this->nib,
            'insurance_number' => $this->insurance_number,
            'accountnumber' => $this->accountnumber,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'renewal_date' => $this->renewal_date,
            'insurance_numberaccontnumber' => $this->insurance_numberaccontnumber,
            'bank_name' => $this->bank_name,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'mobilephone', $this->mobilephone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'hiring', $this->hiring])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
