<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employees_type".
 *
 * @property int $id_employees_type
 * @property string $name
 * @property string|null $description
 * @property string $create_at
 * @property string $update_at
 */
class EmployeesType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'update_at'], 'required'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_employees_type' => 'Id Employees Type',
            'name' => 'Name',
            'description' => 'Description',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
