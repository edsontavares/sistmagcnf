<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fair".
 *
 * @property int $id_fair
 * @property string $date_start
 * @property string $date_end
 * @property string $state
 * @property string $name_employees
 * @property int $id_employees
 * @property string $create_at
 * @property string $update_at 
 */
class Fair extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fair';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date_start', 'date_end', 'id_employees', ], 'required'],
            [['date_start', 'date_end', 'create_at', 'update_at'], 'safe'],
            [['id_employees'], 'integer'],
            [['state','name_employees'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_fair' => 'Id Fair',
            'date_start' => 'Date Start',
            'date_end' => 'Date End',
            'state' => 'State',
            'id_employees' => 'Id Employees',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }


    public function getEmployeess()
    {
        
        return $this->hasOne(Employees::className(), ['id_employees' => 'id_employees']);
    }
}
