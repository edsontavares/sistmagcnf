<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "fermural_prosthesis".
 *
 * @property int $id_fermural_Prosthesis
 * @property int $id_patient
 * @property int $id_query
 * @property string $orthopedist
 * @property string $help
 * @property string $angular_frame
 * @property string $river
 * @property string $fit_type
 * @property string $suspension
 * @property string $nameEmployees
 * @property string $amputation_pass
 * @property string $circumference_m_one
 * @property string $circumference_m_two
 * @property string $circumference_m_three
 * @property string $height_m
 * @property string $angled_m
 * @property string $length_m
 * @property string $angulation_m
 * @property string $cercunferencia_n_one
 * @property string $cercunferencia_n_two
 * @property string $cercunferencia_n_three
 * @property string $cercunferencia_n_four
 * @property string $cercunferencia_n_five
 * @property string $cercunferencia_n_six
 * @property string $cercunferencia_n_seven
 * @property string $cercunferencia_n_eight
 * @property string $suspension
 * @property string $length_a
 * @property string $circumference_c_one
 * @property string $circumference_c_two
 * @property string $length_c_one
 * @property string $length_c_two
 * @property string $length
 * @property string $spacing_ap_one
 * @property string $spacing_ap_two
 * @property string $spacing_ml_one
 * @property string $spacing_ml_two
 * @property string $circumference_p_one
 * @property string $circumference_p_two
 * @property string $sideP
 * @property string $heel
 * @property int $id_employees
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 */
class FermuralProsthesis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fermural_prosthesis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_fermural_Prosthesis', 'id_patient',], 'required'],
            [['id_fermural_Prosthesis', 'id_patient', 'id_query', 'id_employees'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['orthopedist', 'angular_frame', 'river', 'amputation_pass', 'circumference_m_one', 'circumference_m_two', 'circumference_m_three', 'height_m', 'angled_m', 'length_m', 'angulation_m', 'cercunferencia_n_one', 'cercunferencia_n_two', 'cercunferencia_n_three', 'cercunferencia_n_four', 'cercunferencia_n_five', 'cercunferencia_n_six', 'cercunferencia_n_seven', 'cercunferencia_n_eight', 'length_a', 'circumference_c_one', 'circumference_c_two', 'length_c_one', 'length_c_two', 'length', 'spacing_ap_two', 'spacing_ml_one', 'spacing_ml_two', 'circumference_p_one', 'circumference_p_two', 'sideP', 'heel', 'state'], 'string', 'max' => 11],
            [['help','fit_type','nameEmployees','amputation_pass','suspension'], 'string', 'max' => 255],
            [['spacing_ap_one'], 'string', 'max' => 20],
            [['id_fermural_Prosthesis'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_fermural_Prosthesis' => 'Id Fermural Prosthesis',
            'id_patient' => 'Id Patient',
            'id_query' => 'Id Query',
            'orthopedist' => 'Orthopedist',
            'help' => 'Help',
            'angular_frame' => 'Angular Frame',
            'river' => 'River',
            'amputation_pass' => 'Amputation Pass',
            'circumference_m_one' => 'Circumference M One',
            'circumference_m_two' => 'Circumference M Two',
            'circumference_m_three' => 'Circumference M Three',
            'height_m' => 'Height M',
            'angled_m' => 'Angled M',
            'length_m' => 'Length M',
            'angulation_m' => 'Angulation M',
            'cercunferencia_n_one' => 'Cercunferencia N One',
            'cercunferencia_n_two' => 'Cercunferencia N Two',
            'cercunferencia_n_three' => 'Cercunferencia N Three',
            'cercunferencia_n_four' => 'Cercunferencia N Four',
            'cercunferencia_n_five' => 'Cercunferencia N Five',
            'cercunferencia_n_six' => 'Cercunferencia N Six',
            'cercunferencia_n_seven' => 'Cercunferencia N Seven',
            'cercunferencia_n_eight' => 'Cercunferencia N Eight',
            'length_a' => 'Length A',
            'circumference_c_one' => 'Circumference C One',
            'circumference_c_two' => 'Circumference C Two',
            'length_c_one' => 'Length C One',
            'length_c_two' => 'Length C Two',
            'length' => 'Length',
            'spacing_ap_one' => 'Spacing Ap One',
            'spacing_ap_two' => 'Spacing Ap Two',
            'spacing_ml_one' => 'Spacing Ml One',
            'spacing_ml_two' => 'Spacing Ml Two',
            'circumference_p_one' => 'Circumference P One',
            'circumference_p_two' => 'Circumference P Two',
            'sideP' => 'Side P',
            'heel' => 'Heel',
            'id_employees' => 'Id Employees',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }
}
