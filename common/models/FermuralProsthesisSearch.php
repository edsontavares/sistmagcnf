<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FermuralProsthesis;

/**
 * FermuralProsthesisSearch represents the model behind the search form of `common\models\FermuralProsthesis`.
 */
class FermuralProsthesisSearch extends FermuralProsthesis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_fermural_Prosthesis', 'id_patient', 'id_query', 'id_employees'], 'integer'],
            [['orthopedist', 'help', 'angular_frame', 'river', 'amputation_pass', 'circumference_m_one', 'circumference_m_two', 'circumference_m_three', 'height_m', 'angled_m', 'length_m', 'angulation_m', 'cercunferencia_n_one', 'cercunferencia_n_two', 'cercunferencia_n_three', 'cercunferencia_n_four', 'cercunferencia_n_five', 'cercunferencia_n_six', 'cercunferencia_n_seven', 'cercunferencia_n_eight', 'length_a', 'circumference_c_one', 'circumference_c_two', 'length_c_one', 'length_c_two', 'length', 'spacing_ap_one', 'spacing_ap_two', 'spacing_ml_one', 'spacing_ml_two', 'circumference_p_one', 'circumference_p_two', 'sideP', 'heel', 'update_at', 'create_at', 'state'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FermuralProsthesis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_fermural_Prosthesis' => $this->id_fermural_Prosthesis,
            'id_patient' => $this->id_patient,
            'id_query' => $this->id_query,
            'id_employees' => $this->id_employees,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'orthopedist', $this->orthopedist])
            ->andFilterWhere(['like', 'help', $this->help])
            ->andFilterWhere(['like', 'angular_frame', $this->angular_frame])
            ->andFilterWhere(['like', 'river', $this->river])
            ->andFilterWhere(['like', 'amputation_pass', $this->amputation_pass])
            ->andFilterWhere(['like', 'circumference_m_one', $this->circumference_m_one])
            ->andFilterWhere(['like', 'circumference_m_two', $this->circumference_m_two])
            ->andFilterWhere(['like', 'circumference_m_three', $this->circumference_m_three])
            ->andFilterWhere(['like', 'height_m', $this->height_m])
            ->andFilterWhere(['like', 'angled_m', $this->angled_m])
            ->andFilterWhere(['like', 'length_m', $this->length_m])
            ->andFilterWhere(['like', 'angulation_m', $this->angulation_m])
            ->andFilterWhere(['like', 'cercunferencia_n_one', $this->cercunferencia_n_one])
            ->andFilterWhere(['like', 'cercunferencia_n_two', $this->cercunferencia_n_two])
            ->andFilterWhere(['like', 'cercunferencia_n_three', $this->cercunferencia_n_three])
            ->andFilterWhere(['like', 'cercunferencia_n_four', $this->cercunferencia_n_four])
            ->andFilterWhere(['like', 'cercunferencia_n_five', $this->cercunferencia_n_five])
            ->andFilterWhere(['like', 'cercunferencia_n_six', $this->cercunferencia_n_six])
            ->andFilterWhere(['like', 'cercunferencia_n_seven', $this->cercunferencia_n_seven])
            ->andFilterWhere(['like', 'cercunferencia_n_eight', $this->cercunferencia_n_eight])
            ->andFilterWhere(['like', 'length_a', $this->length_a])
            ->andFilterWhere(['like', 'circumference_c_one', $this->circumference_c_one])
            ->andFilterWhere(['like', 'circumference_c_two', $this->circumference_c_two])
            ->andFilterWhere(['like', 'length_c_one', $this->length_c_one])
            ->andFilterWhere(['like', 'length_c_two', $this->length_c_two])
            ->andFilterWhere(['like', 'length', $this->length])
            ->andFilterWhere(['like', 'spacing_ap_one', $this->spacing_ap_one])
            ->andFilterWhere(['like', 'spacing_ap_two', $this->spacing_ap_two])
            ->andFilterWhere(['like', 'spacing_ml_one', $this->spacing_ml_one])
            ->andFilterWhere(['like', 'spacing_ml_two', $this->spacing_ml_two])
            ->andFilterWhere(['like', 'circumference_p_one', $this->circumference_p_one])
            ->andFilterWhere(['like', 'circumference_p_two', $this->circumference_p_two])
            ->andFilterWhere(['like', 'sideP', $this->sideP])
            ->andFilterWhere(['like', 'heel', $this->heel])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
