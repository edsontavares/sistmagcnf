<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id_files
 * @property string $nome
 * @property string $nome_original
 * @property string $full_path
 * @property string $mim_type
 * @property string $medio_type
 * @property string $descicao
 * @property int $model_id
 * @property int $model
 * @property string $create_at
 * @property int $publico
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'nome_original', 'full_path', 'mim_type', 'medio_type', 'descicao', 'model_id', 'create_at', 'publico'], 'required'],
            [['model_id', 'publico'], 'integer'],
            [['create_at'], 'safe'],
            [['nome', 'nome_original', 'full_path', 'mim_type', 'medio_type', 'descicao','model'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_files' => 'Id Files',
            'nome' => 'Nome',
            'nome_original' => 'Nome Original',
            'full_path' => 'Full Path',
            'mim_type' => 'Mim Type',
            'medio_type' => 'Medio Type',
            'descicao' => 'Descicao',
            'model_id' => 'Model ID',
            'create_at' => 'Create At',
            'publico' => 'Publico',
        ];
    }

    public function getFileType(){
        $fileType = [
            'pdf'=>'fa-file-pdf-o',
            'docx'=>'file-word-o',
            'doc'=>'file-word-o',
            'pptx'=>'file-powerpoint-o',
            'xls'=>'file-excel-o',
            'xlsx'=>'file-excel-o',
            'zip' => 'file-archive-o',
            'rar' => 'file-archive-o',
            'jpg' => 'file-image-o',
            'png' => 'file-image-o',
            'pub' => 'file-powerpoint-o',
            'txt' => 'fa-file-o',
            'xps' => 'fa-file-o',
        ];
        return $fileType;
    }
}
