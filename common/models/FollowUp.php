<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "follow_up".
 *
 * @property int $id_follow_up
 * @property int $id_patient
 * @property int $id_employees
 * @property string $acticity
 * @property string $aboservation
 * @property string $date
 * @property string $signature
 * @property string $descreption
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 */
class FollowUp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'follow_up';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_follow_up', 'id_patient', 'id_employees', 'acticity', 'aboservation', 'date'], 'required'],
            [['id_follow_up', 'id_patient', 'id_employees'], 'integer'],
            [['date', 'update_at', 'create_at'], 'safe'],
            [['descreption'], 'string'],
            [['acticity', 'signature'], 'string', 'max' => 255],
            [['aboservation'], 'string', 'max' => 500],
            [['state'], 'string', 'max' => 11],
            [['id_follow_up'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_follow_up' => 'Id Follow Up',
            'id_patient' => 'Id Patient',
            'id_employees' => 'Id Employees',
            'acticity' => 'Acticity',
            'aboservation' => 'Aboservation',
            'date' => 'Date',
            'signature' => 'Signature',
            'descreption' => 'Descreption',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }

    public function getPatients()
    {
        
        return $this->hasOne(Patient::className(), ['id_patient' => 'id_patient']);
    }

    public function getEmployees()
    {
        
        return $this->hasOne(Employees::className(), ['id_employees' => 'id_employees']);
    }
}
