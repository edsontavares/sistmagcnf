<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\FollowUp;

/**
 * FollowUpSearch represents the model behind the search form of `common\models\FollowUp`.
 */
class FollowUpSearch extends FollowUp
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_follow_up', 'id_patient', 'id_employees'], 'integer'],
            [['acticity', 'aboservation', 'date', 'signature', 'descreption', 'update_at', 'create_at', 'state'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FollowUp::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_follow_up' => $this->id_follow_up,
            'id_patient' => $this->id_patient,
            'id_employees' => $this->id_employees,
            'date' => $this->date,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'acticity', $this->acticity])
            ->andFilterWhere(['like', 'aboservation', $this->aboservation])
            ->andFilterWhere(['like', 'signature', $this->signature])
            ->andFilterWhere(['like', 'descreption', $this->descreption])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
