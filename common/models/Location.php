<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "location".
 *
 * @property int $id_location
 * @property string $parents
 * @property string|null $city
 * @property string|null $zone
 * @property string|null $street
 * @property string $update_at
 * @property string $island
 * @property string $create_at
 * @property string $state
 */
class Location extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parents', 'update_at', 'create_at', 'state'], 'required'],
            [['update_at', 'create_at'], 'safe'],
            [['parents', 'city'], 'string', 'max' => 20],
            [['zone', 'street'], 'string', 'max' => 15],
            [['state'], 'string', 'max' => 11],
            [['island'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_location' => 'Id Location',
            'parents' => 'Parents',
            'city' => 'City',
            'zone' => 'Zone',
            'street' => 'Street',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }

   
}
