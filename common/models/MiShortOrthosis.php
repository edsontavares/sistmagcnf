<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mi_short_orthosis".
 *
 * @property int $id_mi_short_orthosis
 * @property int $id_query
 * @property int $id_patient
 * @property string $length_m_one
 * @property string $length_m_two
 * @property string $length_m_three
 * @property string $side_m
 * @property string $cercunferencia_m_one
 * @property string $cercunferencia_m_two
 * @property string $pairing_side
 * @property string $left_o
 * @property string $right_o
 * @property string $side_number
 * @property string $one_sided
 * @property string $bilateral
 * @property string $shoe_size
 * @property string $heel_height
 * @property string $mold_manuf
 * @property string $space_n_ml_one
 * @property string $space_n_ml_two
 * @property string $cercunferencia_c_one
 * @property string $cercunferencia_c_two
 * @property string $cercunferencia_c_three
 * @property string $length_c_one
 * @property string $length_c_two
 * @property string $space_c_ap
 * @property string $heel_l
 * @property string $length_p_one
 * @property string $length_p_two
 * @property string $length_p_three
 * @property string $length_p_for
 * @property string $side_p
 * @property string $nameEmployees
 * @property string $length_d
 * @property string $compensation_d
 * @property string $heel
 * @property int $id_employees
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 */
class MiShortOrthosis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mi_short_orthosis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mi_short_orthosis'], 'required'],
            [['id_mi_short_orthosis', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['length_m_one', 'length_m_two', 'length_m_three', 'side_m', 'cercunferencia_m_one', 'pairing_side', 'left_o', 'right_o', 'side_number', 'one_sided', 'bilateral', 'shoe_size', 'heel_height', 'space_n_ml_one', 'space_n_ml_two', 'cercunferencia_c_one', 'cercunferencia_c_two', 'cercunferencia_c_three', 'length_c_one', 'length_c_two', 'space_c_ap', 'heel_l', 'length_p_one', 'length_p_two', 'length_p_three', 'length_p_for', 'side_p', 'length_d', 'compensation_d', 'heel', 'state'], 'string', 'max' => 11],
            [['mold_manuf','nameEmployees'], 'string', 'max' => 255],
            [['id_mi_short_orthosis'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mi_short_orthosis' => 'Id Mi Short Orthosis',
            'id_query' => 'Id Query',
            'id_patient' => 'Id Patient',
            'length_m_one' => 'Length M One',
            'length_m_two' => 'Length M Two',
            'length_m_three' => 'Length M Three',
            'side_m' => 'Side M',
            'cercunferencia_m_one' => 'Cercunferencia M One',
            'cercunferencia_m_two' => 'Cercunferencia M two',
            'pairing_side' => 'Pairing Side',

            'space_n_ml_one' => 'Space N Ml One',
            'space_n_ml_two' => 'Space N Ml Two',


            'left_o' => 'Left O',
            'right_o' => 'Right O',
            'side_number' => 'Side Number',
            'one_sided' => 'One Sided',
            'bilateral' => 'Bilateral',
            'shoe_size' => 'Shoe Size',
            'heel_height' => 'Heel Height',
            'mold_manuf' => 'Mold Manuf',
           
            'cercunferencia_c_one' => 'Cercunferencia C One',
            'cercunferencia_c_two' => 'Cercunferencia C Two',
            'cercunferencia_c_three' => 'Cercunferencia C Three',
            'space_c_ap' => 'Space C Ap',
            'length_c_one' => 'Length C One',
            'length_c_two' => 'Length C Two',
           
            'heel_l' => 'Heel L',

            'length_p_one' => 'Length P One',
            'length_p_two' => 'Length P Two',
            'length_p_three' => 'Length P Three',
            'length_p_for' => 'Length P For',
            'side_p' => 'Side P',

            'length_d' => 'Length D',
            'compensation_d' => 'Compensation D',
            'heel' => 'Heel',

            'id_employees' => 'Id Employees',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }

    public function getPatients()
    {
        
        return $this->hasOne(\common\models\Patient::className(), ['id_patient' => 'id_patient']);
    }
}
