<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MiShortOrthosis;

/**
 * MiShortOrthosisSearch represents the model behind the search form of `common\models\MiShortOrthosis`.
 */
class MiShortOrthosisSearch extends MiShortOrthosis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mi_short_orthosis', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['length_m_one', 'length_m_two', 'length_m_three', 'side_m', 'cercunferencia_m_one', 'pairing_side', 'left_o', 'right_o', 'side_number', 'one_sided', 'bilateral', 'shoe_size', 'heel_height', 'mold_manuf', 'space_n_ml_one', 'space_n_ml_two', 'cercunferencia_c_one', 'cercunferencia_c_two', 'cercunferencia_c_three', 'length_c_one', 'length_c_two', 'space_c_ap', 'heel_l', 'length_p_one', 'length_p_two', 'length_p_three', 'length_p_for', 'side_p', 'length_d', 'compensation_d', 'heel', 'update_at', 'create_at', 'state'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MiShortOrthosis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_mi_short_orthosis' => $this->id_mi_short_orthosis,
            'id_query' => $this->id_query,
            'id_patient' => $this->id_patient,
            'id_employees' => $this->id_employees,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'length_m_one', $this->length_m_one])
            ->andFilterWhere(['like', 'length_m_two', $this->length_m_two])
            ->andFilterWhere(['like', 'length_m_three', $this->length_m_three])
            ->andFilterWhere(['like', 'side_m', $this->side_m])
            ->andFilterWhere(['like', 'cercunferencia_m_one', $this->cercunferencia_m_one])
            ->andFilterWhere(['like', 'pairing_side', $this->pairing_side])
            ->andFilterWhere(['like', 'left_o', $this->left_o])
            ->andFilterWhere(['like', 'right_o', $this->right_o])
            ->andFilterWhere(['like', 'side_number', $this->side_number])
            ->andFilterWhere(['like', 'one_sided', $this->one_sided])
            ->andFilterWhere(['like', 'bilateral', $this->bilateral])
            ->andFilterWhere(['like', 'shoe_size', $this->shoe_size])
            ->andFilterWhere(['like', 'heel_height', $this->heel_height])
            ->andFilterWhere(['like', 'mold_manuf', $this->mold_manuf])
            ->andFilterWhere(['like', 'space_n_ml_one', $this->space_n_ml_one])
            ->andFilterWhere(['like', 'space_n_ml_two', $this->space_n_ml_two])
            ->andFilterWhere(['like', 'cercunferencia_c_one', $this->cercunferencia_c_one])
            ->andFilterWhere(['like', 'cercunferencia_c_two', $this->cercunferencia_c_two])
            ->andFilterWhere(['like', 'cercunferencia_c_three', $this->cercunferencia_c_three])
            ->andFilterWhere(['like', 'length_c_one', $this->length_c_one])
            ->andFilterWhere(['like', 'length_c_two', $this->length_c_two])
            ->andFilterWhere(['like', 'space_c_ap', $this->space_c_ap])
            ->andFilterWhere(['like', 'heel_l', $this->heel_l])
            ->andFilterWhere(['like', 'length_p_one', $this->length_p_one])
            ->andFilterWhere(['like', 'length_p_two', $this->length_p_two])
            ->andFilterWhere(['like', 'length_p_three', $this->length_p_three])
            ->andFilterWhere(['like', 'length_p_for', $this->length_p_for])
            ->andFilterWhere(['like', 'side_p', $this->side_p])
            ->andFilterWhere(['like', 'length_d', $this->length_d])
            ->andFilterWhere(['like', 'compensation_d', $this->compensation_d])
            ->andFilterWhere(['like', 'heel', $this->heel])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
