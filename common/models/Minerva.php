<?php

namespace common\models;
use \yii\web\Response;

use Yii;

/**
 * This is the model class for table "minerva".
 *
 * @property int $id_minerva
 * @property int $id_query
 * @property string $cercunferencia_m_one
 * @property string $cercunferencia_m_two
 * @property string $cercunferencia_m_three
 * @property string $cercunferencia_c_one
 * @property string $observation
 * @property string $id_patient
 * @property string $nameEmployees
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 * @property string $side
 * 
 * 
 * @property querys $query
 */
class Minerva extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'minerva';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_minerva'], 'required'],
            [['id_minerva', 'id_query'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['cercunferencia_m_one','nameEmployees', 'cercunferencia_m_two', 'cercunferencia_m_three', 'cercunferencia_c_one', 'observation', 'state'], 'string', 'max' => 11],
            [['side','id_patient'], 'string', 'max' => 255],
            [['id_minerva'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_minerva' => 'Id Minerva',
            'id_query' => 'Id Query',
            'cercunferencia_m_one' => 'Cercunferencia M One',
            'cercunferencia_m_two' => 'Cercunferencia M Two',
            'cercunferencia_m_three' => 'Cercunferencia M Three',
            'cercunferencia_c_one' => 'Cercunferencia C One',
            'observation' => 'Observation',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
            'side' => 'Side',
        ];
    }

  
    public function getQuerys()
    {
        
        return $this->hasOne(Query::className(), ['id_query' => 'id_query']);
    }

    public function actionGetCompetencia($q = null)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $out = [];
        if (!is_null($q)) {
            $query = new Query();
            $query->select('comp.id_patient as id_patient, comp.name')
                ->from('patient as comp')
                ->where(['like', 'comp.name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        return $out;
    }
}
