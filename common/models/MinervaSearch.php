<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Minerva;

/**
 * MinervaSearch represents the model behind the search form of `common\models\Minerva`.
 */
class MinervaSearch extends Minerva
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_minerva', 'id_query'], 'integer'],
            [['cercunferencia_m_one', 'cercunferencia_m_two', 'cercunferencia_m_three', 'cercunferencia_c_one', 'observation', 'update_at', 'create_at', 'state', 'side'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Minerva::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_minerva' => $this->id_minerva,
            'id_query' => $this->id_query,
  
           
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'cercunferencia_m_one', $this->cercunferencia_m_one])
            ->andFilterWhere(['like', 'cercunferencia_m_two', $this->cercunferencia_m_two])
            ->andFilterWhere(['like', 'cercunferencia_m_three', $this->cercunferencia_m_three])
            ->andFilterWhere(['like', 'cercunferencia_c_one', $this->cercunferencia_c_one])
            ->andFilterWhere(['like', 'observation', $this->observation])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'side', $this->side]);

        return $dataProvider;
    }
}
