<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property string $titile
 * @property string $state
 * @property string $create_at
 * @property string $update_at
 * @property int $tables
 * @property int $id_conteudo
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titile', 'tables', 'id_conteudo'], 'required'],
            [['tables', 'id_conteudo'], 'integer'],
            [['titile'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titile' => 'Titile',
            'tables' => 'Tables',
            'id_conteudo' => 'Id Conteudo',
        ];
    }
}
