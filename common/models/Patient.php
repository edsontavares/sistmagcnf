<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property int $id_patient
 * @property string $name
 * @property string|null $nickname
 * @property int $id_location
 * @property string|null $birth_date
 * @property string|null $nationality
 * @property string|null $naturalness
 * @property string|null $telephone
 * @property string|null $mobile_phone
 * @property string|null $email
 * @property int|null $cni
 * @property string|null $educational_level
 * @property string|null $sex
 * @property string|null $marital_status
 * @property int|null $qtd_son
 * @property int|null $qtd_family
 * @property int|null $adult
 * @property int|null $minor
 * @property int|null $preschool
 * @property int|null $college_student
 * @property int|null $secondary
 * @property int|null $worker
 * @property int|null $Unemployed
 * @property int|null $ebo
 * @property string|null $profession
 * @property string|null $institution_type
 * @property string|null $institution_name
 * @property string|null $student
 * @property string|null $insurance_institution
 * @property string|null $others
 * @property string|null $name_parents
 * @property string|null $address
 * @property string|null $degree
 * @property string $imagem
 * @property string $update_at
 * @property string $create_at
 * @property string $bi_ficheiro
 * @property string $state
 * @property string $city
 * @property string $zone
 * @property string $island
 * @property string|null $name_parentsp
 * @property string|null $addressp
 * @property string|null $degreep
 */
class Patient extends \yii\db\ActiveRecord
{

    public static $ESTADO_CIVIL = [
        'Solteira(o)',
        'Casada(o)',
        'Viúva(o)',
        'Devorciada(o)',
        'União de facto',
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['birth_date', 'update_at', 'create_at'], 'safe'],
            [['cni', 'id_location','qtd_son', 'qtd_family', 'adult', 'minor', 'preschool', 'college_student', 'secondary', 'worker', 'Unemployed', 'ebo'], 'integer'],
            [['name'], 'string', 'max' => 35],
            [['nickname', 'nationality', 'naturalness', 'telephone', 'mobile_phone', 'sex', 'marital_status', 'profession', 'institution_type', 'institution_name', 'student', 'insurance_institution', 'others', 'address', 'degree'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 30],
            [['imagem','name_parents','educational_level','bi_ficheiro'], 'string', 'max' => 500],
            [['state'], 'string', 'max' => 11],
            [['island'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_patient' => 'Id Patient',
            'name' => 'Name',
            'nickname' => 'Nickname',
            'birth_date' => 'Birth Date',
            'nationality' => 'Nationality',
            'naturalness' => 'Naturalness',
            'telephone' => 'Telephone',
            'mobile_phone' => 'Mobile Phone',
            'email' => 'Email',
            'cni' => 'Cni',
            'educational_level' => 'Educational Level',
            'sex' => 'Sex',
            'marital_status' => 'Marital Status',
            'qtd_son' => 'Qtd Son',
            'qtd_family' => 'Qtd Family',
            'adult' => 'Adult',
            'minor' => 'Minor',
            'preschool' => 'Preschool',
            'college_student' => 'College Student',
            'secondary' => 'Secondary',
            'worker' => 'Worker',
            'Unemployed' => 'Unemployed',
            'ebo' => 'Ebo',
            'profession' => 'Profession',
            'institution_type' => 'Institution Type',
            'institution_name' => 'Institution Name',
            'student' => 'Student',
            'insurance_institution' => 'Insurance Institution',
            'others' => 'Others',
            'name_parents' => 'Name Parents',
            'address' => 'Address',
            'degree' => 'Degree',
            'imagem' => 'imagem',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
            'city' => 'City',
            'zone' => 'Zone',
            'island' => 'Island',
        ];
    }
    public function getLocation()
    {
        return $this->hasOne(Location::className(), ['id_location' => 'id_location']);
    }
}
