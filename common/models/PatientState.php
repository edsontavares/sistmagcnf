<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "patient_state".
 *
 * @property int $id
 * @property int|null $id_patient
 * @property int $id_query
 * @property int $id_payment
 * @property int $id_minerva
 * @property int $id_mi_short_orthosis
 * @property int $id_fermural_Prosthesis
 * @property int $id_adptacao
 * @property int $id_follow_up
 * @property int $id_tribal_prosthesis
 * @property int $id_calendar
 * @property int $id_seat
 * @property string $state
 * @property string $create_at
 * @property string $update_at
 * @property Adptacao $adptacao
 * @property Calendar $calendar
 * @property Minerva $minerva
 * @property TribalProsthesis $tribalProsthesis
 * @property FollowUp $followUp
 * @property MiShortOrthosis $miShortOrthosis
 * @property Seat $seat
 * @property Payment $payment
 */
class PatientState extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patient_state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_query', 'id_payment', 'id_minerva', 'id_mi_short_orthosis', 'id_fermural_Prosthesis', 'id_adptacao', 'id_follow_up', 'id_tribal_prosthesis', 'id_calendar', 'id_seat'], 'integer'],
            //[['id_patient'], 'required'],
            [['id_patient', 'id_query', 'id_payment', 'id_minerva', 'id_mi_short_orthosis', 'id_fermural_Prosthesis', 'id_adptacao', 'id_follow_up', 'id_tribal_prosthesis', 'id_calendar', 'id_seat'], 'number'],
            [['create_at', 'update_at'], 'safe'],
            [['state'], 'string', 'max' => 20],
            [['id_adptacao'], 'exist', 'skipOnError' => true, 'targetClass' => Adptacao::className(), 'targetAttribute' => ['id_adptacao' => 'id_adptacao']],
            [['id_calendar'], 'exist', 'skipOnError' => true, 'targetClass' => Calendar::className(), 'targetAttribute' => ['id_calendar' => 'id_calendar']],
            [['id_minerva'], 'exist', 'skipOnError' => true, 'targetClass' => Minerva::className(), 'targetAttribute' => ['id_minerva' => 'id_minerva']],
            [['id_tribal_prosthesis'], 'exist', 'skipOnError' => true, 'targetClass' => TribalProsthesis::className(), 'targetAttribute' => ['id_tribal_prosthesis' => 'id_tribal_prosthesis']],
            [['id_follow_up'], 'exist', 'skipOnError' => true, 'targetClass' => FollowUp::className(), 'targetAttribute' => ['id_follow_up' => 'id_follow_up']],
            [['id_mi_short_orthosis'], 'exist', 'skipOnError' => true, 'targetClass' => MiShortOrthosis::className(), 'targetAttribute' => ['id_mi_short_orthosis' => 'id_mi_short_orthosis']],
            //[['id_seat'], 'exist', 'skipOnError' => true, 'targetClass' => Seat::className(), 'targetAttribute' => ['id_seat' => 'id_seat']],
            [['id_payment'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['id_payment' => 'id_payment']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_patient' => 'Id Patient',
            'id_query' => 'Id Query',
            'id_payment' => 'Id Payment',
            'id_minerva' => 'Id Minerva',
            'id_mi_short_orthosis' => 'Id Mi Short Orthosis',
            'id_fermural_Prosthesis' => 'Id Fermural Prosthesis',
            'id_adptacao' => 'Id Adptacao',
            'id_follow_up' => 'Id Follow Up',
            'id_tribal_prosthesis' => 'Id Tribal Prosthesis',
            'id_calendar' => 'Id Calendar',
            'id_seat' => 'Id Seat',
            'state' => 'State',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    /**
     * Gets query for [[Adptacao]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdptacao()
    {
        return $this->hasOne(Adptacao::className(), ['id_adptacao' => 'id_adptacao']);
    }

    /**
     * Gets query for [[Calendar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalendar()
    {
        return $this->hasOne(Calendar::className(), ['id_calendar' => 'id_calendar']);
    }

    /**
     * Gets query for [[Minerva]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMinerva()
    {
        return $this->hasOne(Minerva::className(), ['id_minerva' => 'id_minerva']);
    }

    /**
     * Gets query for [[TribalProsthesis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTribalProsthesis()
    {
        return $this->hasOne(TribalProsthesis::className(), ['id_tribal_prosthesis' => 'id_tribal_prosthesis']);
    }

    /**
     * Gets query for [[FollowUp]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFollowUp()
    {
        return $this->hasOne(FollowUp::className(), ['id_follow_up' => 'id_follow_up']);
    }

    /**
     * Gets query for [[MiShortOrthosis]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMiShortOrthosis()
    {
        return $this->hasOne(MiShortOrthosis::className(), ['id_mi_short_orthosis' => 'id_mi_short_orthosis']);
    }

    /**
     * Gets query for [[Seat]].
     *
     * @return \yii\db\ActiveQuery
     */
   

    /**
     * Gets query for [[Payment]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id_payment' => 'id_payment']);
    }

    public function getList($list = 'all', $likeFilter = [], $whereFilter = [], $limitFilter = 0)
    {
        
        $model = Patient::find()->where(['id_patient' => $this->id]);
        switch ($list):
            case 'draft':
                $model = $model->andWhere(['state' => 1]);
                break;

            case 'published':
                $model = $model->andWhere(['state' => 9]);
                break;
            case 'archive':
                $model = $model->andWhere(['state' => 3]);
                break;

            case 'deleted':
                $model = $model->andWhere(['state' => 4]);
                break;

            case 'pendent':
                $model = $model->andWhere(['state' => 5]);
                break;
        endswitch;

        if ($limitFilter != 0 && $limitFilter != null)
            $model->limit($limitFilter);

        return $model->andWhere($likeFilter)->andWhere($whereFilter)->orderBy('create_at DESC')->all();
    }

}
