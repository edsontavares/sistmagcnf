<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment".
 *
 * @property int $id_payment
 * @property int|null $id_patient
 * @property int $	N_tranf
 * @property float $value
 * @property float $amount
 * @property string $coin
 * @property string $type
 * @property string $payment_type
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 * @property int|null $id_query
 * @property string $interva
 * @property string $manage_many_times
 * @property string $experation_date
 * @property int|null $id_precarious
 * @property float $unit_price
 * @property float $total_price
 * @property string $code
 * @property int $qt
 * @property string $description
 *
 * @property Precarious $precarious
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_patient', 'id_query', 'id_precarious', 'qt'], 'integer'],
            [['value', 'amount', 'coin', 'payment_type', 'update_at', 'create_at', 'state', 'interva', 'manage_many_times', 'experation_date', 'unit_price', 'total_price','description'], 'required'],
            [['value', 'amount', 'unit_price', 'total_price'], 'number'],
            [['update_at', 'create_at'], 'safe'],
            [['coin', 'payment_type', 'manage_many_times', 'experation_date', 'code'], 'string', 'max' => 255],
            [['state'], 'string', 'max' => 11],
            [['interva'], 'string', 'max' => 30],
            [['description','type'], 'string', 'max' => 655],
            [['id_precarious'], 'exist', 'skipOnError' => true, 'targetClass' => Precarious::className(), 'targetAttribute' => ['id_precarious' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_payment' => 'Id Payment',
            'id_patient' => 'Id Patient',
            'value' => 'Value',
            'amount' => 'Amount',
            'coin' => 'Coin',
            'payment_type' => 'Payment Type',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
            'id_query' => 'Id Query',
            'interva' => 'Interva',
            'manage_many_times' => 'Manage Many Times',
            'experation_date' => 'Experation Date',
            'id_precarious' => 'Id Precarious',
            'unit_price' => 'Unit Price',
            'total_price' => 'Total Price',
            'code' => 'Code',
            'qt' => 'Qt',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Precarious]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrecarious()
    {
        return $this->hasOne(Precarious::className(), ['id' => 'id_precarious']);
    }
    public function getPatients()
    {
        return $this->hasOne(Patient::className(), ['id_patient' => 'id_patient']);
    }
}
