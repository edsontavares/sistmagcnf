<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Payment;

/**
 * PaymentSearch represents the model behind the search form of `common\models\Payment`.
 */
class PaymentSearch extends Payment
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_payment', 'id_patient', 'id_query', 'id_precarious', 'qt'], 'integer'],
            [['value', 'amount', 'unit_price', 'total_price'], 'number'],
            [['coin', 'payment_type', 'update_at', 'create_at', 'state', 'interva', 'manage_many_times', 'experation_date', 'code', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_payment' => $this->id_payment,
            'id_patient' => $this->id_patient,
            'value' => $this->value,
            'amount' => $this->amount,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
            'id_query' => $this->id_query,
            'id_precarious' => $this->id_precarious,
            'unit_price' => $this->unit_price,
            'total_price' => $this->total_price,
            'qt' => $this->qt,
        ]);

        $query->andFilterWhere(['like', 'coin', $this->coin])
            ->andFilterWhere(['like', 'payment_type', $this->payment_type])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'interva', $this->interva])
            ->andFilterWhere(['like', 'manage_many_times', $this->manage_many_times])
            ->andFilterWhere(['like', 'experation_date', $this->experation_date])
            ->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
