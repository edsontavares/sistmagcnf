<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "payment_state".
 *
 * @property int $id_payment_state
 * @property int|null $id_payment
 * @property int $preco
 * @property int $qt
 * @property string $state
 * @property string $codigo
 * @property string $create_at
 * @property string $update_at
 */
class PaymentState extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_state';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_payment_state', 'state', 'create_at', 'update_at'], 'required'],
            [['id_payment_state', 'id_payment','preco', 'qt'], 'integer'],
            [['create_at', 'update_at'], 'safe'],
            [['state','codigo'], 'string', 'max' => 80],
            [['id_payment_state'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_payment_state' => 'Id Payment State',
            'id_payment' => 'Id Payment',
            'state' => 'State',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }
}
