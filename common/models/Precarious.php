<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "precarious".
 *
 * @property int $id
 * @property string $code
 * @property string $designation
 * @property string $material
 * @property string $price
 * @property string $lifespan
 * @property string $description
 *
 * @property Payment[] $payments
 */
class Precarious extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'precarious';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'designation', 'material', 'price', 'lifespan', 'description'], 'required'],
            [['price'], 'integer'],
            [['code', 'lifespan'], 'string', 'max' => 200],
            [['designation', 'material'], 'string', 'max' => 600],
            [['description'], 'string', 'max' => 800],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'designation' => 'Designation',
            'material' => 'Material',
            'price' => 'Price',
            'lifespan' => 'Lifespan',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Payments]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['id_precarious' => 'id']);
    }
}
