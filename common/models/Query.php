<?php

namespace common\models;


use Yii;

/**
 * This is the model class for table "query".
 *
 * @property int $id_query
 * @property string tepy_query
 * @property int $id_patient
 * @property int $age
 * @property string $height
 * @property int $side
 *@property string diagnosis 
 * @property string $orthoprotectionist
 * @property string $measures 
 * @property string $sensitivity 
 * @property string $appliance
 * @property string $pathology
 * @property string $etiology
 * @property string $historic
 * @property string $associates
 * @property string $medication
 * @property string $macha
 * @property string $reflections 
 * @property string $vertibral
 * @property string $Weight
 * @property string $physical_activity
 * @property string $name_physical_activity
 * @property string $frequency
 * @property string $duration
 * @property int $session
 * @property string $attended1
 * @property string $start_date
 * @property string $end_date
 * @property string $anticident
 * @property string $tobacco
 * @property string $description
 * @property string $food_examination
 * @property string $physical_exploration
 * @property string $moscular_evaluation
 * @property string $joint_evaluation
 * @property string $neurological_evaluation
 * @property string $orthopedic_test
 * @property string $treatment
 * @property string $hit
 * @property string $recommendation
 * @property float $weith
 * @property string $state
 * @property string $update_at
 * @property string $create_at
 * @property patients $patient
 */

class Query extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'query';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_query','id_patient'], 'required'],
            [['id_patient', 'session'], 'integer'],
            [['macha', 'vertibral', 'description', 'food_examination', 'physical_exploration', 'moscular_evaluation', 'joint_evaluation', 'neurological_evaluation', 'treatment', 'hit', 'recommendation'], 'string'],
            [[ 'update_at', 'create_at'], 'safe'],
            [['weith','side'], 'number'],
            [['height','Weight'], 'string', 'max' => 10],
            [['orthoprotectionist', 'appliance', 'pathology', 'frequency', 'duration', 'attended1', 'anticident', 'tobacco'], 'string', 'max' => 60],
            [['etiology', 'orthopedic_test','tepy_query','start_date', 'end_date',], 'string', 'max' => 100],
            [['historic','sensitivity','diagnosis','reflections','measures'], 'string', 'max' => 400],
            [['associates', 'medication'], 'string', 'max' => 200],
            [['physical_activity', 'name_physical_activity'], 'string', 'max' => 40],
            [['state'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_query' => 'Id Query',
            'id_patient' => 'Id Patient',
            'height' => 'Height',
            'side' => 'Side',
            'orthoprotectionist' => 'Orthoprotectionist',
            'appliance' => 'Appliance',
            'pathology' => 'Pathology',
            'etiology' => 'Etiology',
            'historic' => 'Historic',
            'associates' => 'Associates',
            'medication' => 'Medication',
            'macha' => 'Macha',
            'vertibral' => 'Vertibral',
            'Weight' => 'Weight',
            'physical_activity' => 'Physical Activity',
            'name_physical_activity' => 'Name Physical Activity',
            'frequency' => 'Frequency',
            'duration' => 'Duration',
            'session' => 'Session',
            'attended1' => 'Attended1',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'anticident' => 'Anticident',
            'tobacco' => 'Tobacco',
            'description' => 'Description',
            'food_examination' => 'Food Examination',
            'physical_exploration' => 'Physical Exploration',
            'moscular_evaluation' => 'Moscular Evaluation',
            'joint_evaluation' => 'Joint Evaluation',
            'neurological_evaluation' => 'Neurological Evaluation',
            'orthopedic_test' => 'Orthopedic Test',
            'treatment' => 'Treatment',
            'hit' => 'Hit',
            'recommendation' => 'Recommendation',
            'weith' => 'Weith',
            'state' => 'State',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
        ];
    }
   

    public function getPatients()
    {
        
        return $this->hasOne(Patient::className(), ['id_patient' => 'id_patient']);
    }

   

    // public function getQueryTepys()
    // {
        
    //     return $this->hasOne(QueryTepy::className(), ['id_query_tepy' => 'id_query_tepy']);
    // }
}
