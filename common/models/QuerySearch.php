<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Query;

/**
 * QuerySearch represents the model behind the search form of `common\models\Query`.
 */
class QuerySearch extends Query
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_query',  'id_patient', 'session'], 'integer'],
            [['height', 'side', 'orthoprotectionist', 'appliance', 'pathology', 'etiology', 'historic', 'associates', 'medication', 'macha', 'vertibral', 'Weight', 'physical_activity', 'name_physical_activity', 'frequency', 'duration', 'attended1', 'start_date', 'end_date', 'anticident', 'tobacco', 'description', 'food_examination', 'physical_exploration', 'moscular_evaluation', 'joint_evaluation', 'neurological_evaluation', 'orthopedic_test', 'treatment', 'hit', 'recommendation', 'state', 'update_at', 'create_at'], 'safe'],
            [['weith'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Query::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_query' => $this->id_query,
          
            'id_patient' => $this->id_patient,
            'session' => $this->session,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'weith' => $this->weith,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'height', $this->height])
            ->andFilterWhere(['like', 'side', $this->side])
            ->andFilterWhere(['like', 'orthoprotectionist', $this->orthoprotectionist])
            ->andFilterWhere(['like', 'appliance', $this->appliance])
            ->andFilterWhere(['like', 'pathology', $this->pathology])
            ->andFilterWhere(['like', 'etiology', $this->etiology])
            ->andFilterWhere(['like', 'historic', $this->historic])
            ->andFilterWhere(['like', 'associates', $this->associates])
            ->andFilterWhere(['like', 'medication', $this->medication])
            ->andFilterWhere(['like', 'macha', $this->macha])
            ->andFilterWhere(['like', 'vertibral', $this->vertibral])
            ->andFilterWhere(['like', 'Weight', $this->Weight])
            ->andFilterWhere(['like', 'physical_activity', $this->physical_activity])
            ->andFilterWhere(['like', 'name_physical_activity', $this->name_physical_activity])
            ->andFilterWhere(['like', 'frequency', $this->frequency])
            ->andFilterWhere(['like', 'duration', $this->duration])
            ->andFilterWhere(['like', 'attended1', $this->attended1])
            ->andFilterWhere(['like', 'anticident', $this->anticident])
            ->andFilterWhere(['like', 'tobacco', $this->tobacco])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'food_examination', $this->food_examination])
            ->andFilterWhere(['like', 'physical_exploration', $this->physical_exploration])
            ->andFilterWhere(['like', 'moscular_evaluation', $this->moscular_evaluation])
            ->andFilterWhere(['like', 'joint_evaluation', $this->joint_evaluation])
            ->andFilterWhere(['like', 'neurological_evaluation', $this->neurological_evaluation])
            ->andFilterWhere(['like', 'orthopedic_test', $this->orthopedic_test])
            ->andFilterWhere(['like', 'treatment', $this->treatment])
            ->andFilterWhere(['like', 'hit', $this->hit])
            ->andFilterWhere(['like', 'recommendation', $this->recommendation])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
