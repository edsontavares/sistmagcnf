<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "query_tepy".
 *
 * @property int $id_query_tepy
 * @property string $name
 * @property string|null $description
 * @property string $update_at
 * @property string $create_at
 * @property string $state
 */
class QueryTepy extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'query_tepy';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'update_at', 'create_at', 'state'], 'required'],
            [['update_at', 'create_at'], 'safe'],
            [['name'], 'string', 'max' => 20],
            [['description'], 'string', 'max' => 100],
            [['state'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_query_tepy' => 'Id Query Tepy',
            'name' => 'Name',
            'description' => 'Description',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }
}
