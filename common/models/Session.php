<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "session".
 *
 * @property int $id
 * @property int $id_query
 * @property string $creat_at
 * @property int $estado
 * @property int $numero
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_query', 'creat_at', 'estado', 'numero'], 'required'],
            [['id_query', 'estado', 'numero'], 'integer'],
            [['creat_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_query' => 'Id Query',
            'creat_at' => 'Creat At',
            'estado' => 'Estado',
            'numero' => 'Numero',
        ];
    }
}
