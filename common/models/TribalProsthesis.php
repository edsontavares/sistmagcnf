<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tribal_prosthesis".
 *
 * @property int $id_tribal_prosthesis
 * @property int $id_query
 * @property int $id_patient
 * @property string $cercunferencia_m_one
 * @property string $cercunferencia_m_two
 * @property string $height_m
 * @property string $nameEmployees
 * @property string $cercunferencia_c_one
 * @property string $cercunferencia_c_two
 * @property string $cercunferencia_c_three
 * @property string $cercunferencia_c_for
 * @property string $cercunferencia_c_five
 * @property string $cercunferencia_c_sex
 * @property string $cercunferencia_c_seven
 * @property string $cercunferencia_c_eigth
 * @property string $cercunferencia_c_nene
 * @property string $spacing_c_one
 * @property string $spacing_c_two
 * @property string $length
 * @property string $spacing_l_one
 * @property string $length_heel
 * @property string $observation
 * @property string $with_thigh
 * @property string $ptb_sc_ptbsc_sp
 * @property string $ptb_sc
 * @property string $ptb
 * @property int $id_employees
 * @property string $update_at
 * @property string $flexion
 * @property string $extension
 * @property string $abduction
 * @property string $abduction1
 * @property string $create_at
 * @property string $fit_type
 * @property string $state
 */
class TribalProsthesis extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tribal_prosthesis';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tribal_prosthesis'], 'required'],
            [['id_tribal_prosthesis', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['update_at', 'create_at'], 'safe'],
            [['cercunferencia_m_one', 'cercunferencia_m_two', 'height_m', 'cercunferencia_c_one', 'cercunferencia_c_two', 'cercunferencia_c_three', 'cercunferencia_c_for', 'cercunferencia_c_five', 'cercunferencia_c_sex', 'cercunferencia_c_seven', 'cercunferencia_c_eigth', 'cercunferencia_c_nene', 'spacing_c_one', 'spacing_c_two', 'length', 'spacing_l_one', 'length_heel', 'with_thigh', 'ptb_sc_ptbsc_sp', 'ptb_sc', 'ptb', 'state'], 'string', 'max' => 11],
            [['observation','nameEmployees','fit_type','flexion','extension','abduction','abduction1'], 'string', 'max' => 200],
            [['id_tribal_prosthesis'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tribal_prosthesis' => 'Id Tribal Prosthesis',
            'id_query' => 'Id Query',
            'id_patient' => 'Id Patient',
            'cercunferencia_m_one' => 'Cercunferencia M One',
            'cercunferencia_m_two' => 'Cercunferencia M Two',
            'height_m' => 'Height M',
            'cercunferencia_c_one' => 'Cercunferencia C One',
            'cercunferencia_c_two' => 'Cercunferencia C Two',
            'cercunferencia_c_three' => 'Cercunferencia C Three',
            'cercunferencia_c_for' => 'Cercunferencia C For',
            'cercunferencia_c_five' => 'Cercunferencia C Five',
            'cercunferencia_c_sex' => 'Cercunferencia C Sex',
            'cercunferencia_c_seven' => 'Cercunferencia C Seven',
            'cercunferencia_c_eigth' => 'Cercunferencia C Eigth',
            'cercunferencia_c_nene' => 'Cercunferencia C Nene',
            'spacing_c_one' => 'Spacing C One',
            'spacing_c_two' => 'Spacing C Two',
            'length' => 'Length',
            'spacing_l_one' => 'Spacing L One',
            'length_heel' => 'Length Heel',
            'observation' => 'Observation',
            'with_thigh' => 'With Thigh',
            'ptb_sc_ptbsc_sp' => 'Ptb Sc Ptbsc Sp',
            'ptb_sc' => 'Ptb Sc',
            'ptb' => 'Ptb',
            'id_employees' => 'Id Employees',
            'update_at' => 'Update At',
            'create_at' => 'Create At',
            'state' => 'State',
        ];
    }
}
