<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TribalProsthesis;

/**
 * TribalProsthesisSearch represents the model behind the search form of `common\models\TribalProsthesis`.
 */
class TribalProsthesisSearch extends TribalProsthesis
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tribal_prosthesis', 'id_query', 'id_patient', 'id_employees'], 'integer'],
            [['cercunferencia_m_one', 'cercunferencia_m_two', 'height_m', 'cercunferencia_c_one', 'cercunferencia_c_two', 'cercunferencia_c_three', 'cercunferencia_c_for', 'cercunferencia_c_five', 'cercunferencia_c_sex', 'cercunferencia_c_seven', 'cercunferencia_c_eigth', 'cercunferencia_c_nene', 'spacing_c_one', 'spacing_c_two', 'length', 'spacing_l_one', 'length_heel', 'observation', 'with_thigh', 'ptb_sc_ptbsc_sp', 'ptb_sc', 'ptb', 'update_at', 'create_at', 'state'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TribalProsthesis::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tribal_prosthesis' => $this->id_tribal_prosthesis,
            'id_query' => $this->id_query,
            'id_patient' => $this->id_patient,
            'id_employees' => $this->id_employees,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'cercunferencia_m_one', $this->cercunferencia_m_one])
            ->andFilterWhere(['like', 'cercunferencia_m_two', $this->cercunferencia_m_two])
            ->andFilterWhere(['like', 'height_m', $this->height_m])
            ->andFilterWhere(['like', 'cercunferencia_c_one', $this->cercunferencia_c_one])
            ->andFilterWhere(['like', 'cercunferencia_c_two', $this->cercunferencia_c_two])
            ->andFilterWhere(['like', 'cercunferencia_c_three', $this->cercunferencia_c_three])
            ->andFilterWhere(['like', 'cercunferencia_c_for', $this->cercunferencia_c_for])
            ->andFilterWhere(['like', 'cercunferencia_c_five', $this->cercunferencia_c_five])
            ->andFilterWhere(['like', 'cercunferencia_c_sex', $this->cercunferencia_c_sex])
            ->andFilterWhere(['like', 'cercunferencia_c_seven', $this->cercunferencia_c_seven])
            ->andFilterWhere(['like', 'cercunferencia_c_eigth', $this->cercunferencia_c_eigth])
            ->andFilterWhere(['like', 'cercunferencia_c_nene', $this->cercunferencia_c_nene])
            ->andFilterWhere(['like', 'spacing_c_one', $this->spacing_c_one])
            ->andFilterWhere(['like', 'spacing_c_two', $this->spacing_c_two])
            ->andFilterWhere(['like', 'length', $this->length])
            ->andFilterWhere(['like', 'spacing_l_one', $this->spacing_l_one])
            ->andFilterWhere(['like', 'length_heel', $this->length_heel])
            ->andFilterWhere(['like', 'observation', $this->observation])
            ->andFilterWhere(['like', 'with_thigh', $this->with_thigh])
            ->andFilterWhere(['like', 'ptb_sc_ptbsc_sp', $this->ptb_sc_ptbsc_sp])
            ->andFilterWhere(['like', 'ptb_sc', $this->ptb_sc])
            ->andFilterWhere(['like', 'ptb', $this->ptb])
            ->andFilterWhere(['like', 'state', $this->state]);

        return $dataProvider;
    }
}
