<?php

    namespace common\models;

    use yii\base\ErrorException;
    use yii\base\Exception;
    use yii\base\InvalidArgumentException;
    use yii\base\Model;
    use yii\web\BadRequestHttpException;
    use yii\web\UploadedFile;
    use Yii;




    
    class UploadHandler extends Model
    {
        public $file;

        public function __construct($fileName, $init = true, $config = [])
        {
            if ($init) {
                $_file = $_FILES[$fileName];

                if (empty($_file)) {
                    throw new InvalidArgumentException("Nenhum foto foi enviado.");
                }


                $this->file = new UploadedFile([
                    'name' => $_file['name'],
                    'tempName' => $_file['tmp_name'],
                    'type' => $_file['type'],
                    'size' => $_file['size'],
                    'error' => $_file['error'],
                ]);

            }

            parent::__construct($config);

        }

        public function rules()
        {
            return [
                [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, zip, pdf, pptx, gif, jpeg '],
            ];
        }

        protected function get_file_path($model)
        {
            $rootFolder = 'recurso';
            switch ($model) {
                case 'user':
                    $model = 'imgUtilizador';
                    break;
            }

            return $rootFolder . '/' . $model;
        }

        public function upload($model = 'user', $from_admin = false)
        {
            try {
                if ($this->validate()) {
                    $name = Yii::$app->security->generateRandomString();
                    $full_path = $this->get_file_path($model) . '/' . $name . '.' . $this->file->extension;

                    $this->file->saveAs($from_admin ? '../' . $full_path : $full_path);
                    return [
                        'original_name' => $this->file->name,
                        'name' => $name . '.' . $this->file->extension,
                        'full_path' => $full_path,
                        'type' => $this->file->type,
                        'size' => $this->file->size,
                        'extension' => $this->file->extension,
                    ];
                } else {
                    throw new BadRequestHttpException("Error ao processar o arquivo. Arquivo desconhecido.");
                }
            } catch (Exception $e) {
                throw new \yii\db\Exception("Error ao processar o arquivo.");
            }
        }

        public function delete($file, $from_admin = false)
        {
            try {
                if ($from_admin) $file = '../' . $file;

                if ($file && $file != '/img/avatar_forma.png')
                    return unlink($file);
                else
                    return false;
            } catch (Exception $e) {
                throw new BadRequestHttpException($e->getMessage());
            }

        }

        public static function remove($file, $from_admin = false)
        {
            try {
                if ($from_admin) $file = '../' . $file;

                if ($file)
                    return unlink($file);
                else
                    return false;
            } catch (Exception $e) {
                throw new BadRequestHttpException($e->getMessage());
            }

        }
    }
