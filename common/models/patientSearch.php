<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\patient;

/**
 * patientSearch represents the model behind the search form of `common\models\patient`.
 */
class patientSearch extends patient
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_patient', 'cni', 'educational_level', 'qtd_son', 'qtd_family', 'adult', 'minor', 'preschool', 'college_student', 'secondary', 'worker', 'Unemployed', 'ebo'], 'integer'],
            [['name', 'nickname', 'birth_date', 'nationality', 'naturalness', 'telephone', 'mobile_phone', 'email', 'sex', 'marital_status', 'profession', 'institution_type', 'institution_name', 'student', 'insurance_institution', 'others', 'name_parents', 'address', 'degree', 'imagem', 'update_at', 'create_at', 'state','island'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = patient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_patient' => $this->id_patient,
            'birth_date' => $this->birth_date,
            'cni' => $this->cni,
            'educational_level' => $this->educational_level,
            'qtd_son' => $this->qtd_son,
            'qtd_family' => $this->qtd_family,
            'adult' => $this->adult,
            'minor' => $this->minor,
            'preschool' => $this->preschool,
            'college_student' => $this->college_student,
            'secondary' => $this->secondary,
            'worker' => $this->worker,
            'Unemployed' => $this->Unemployed,
            'ebo' => $this->ebo,
            'update_at' => $this->update_at,
            'create_at' => $this->create_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'nickname', $this->nickname])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'naturalness', $this->naturalness])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'sex', $this->sex])
            ->andFilterWhere(['like', 'marital_status', $this->marital_status])
            ->andFilterWhere(['like', 'profession', $this->profession])
            ->andFilterWhere(['like', 'institution_type', $this->institution_type])
            ->andFilterWhere(['like', 'institution_name', $this->institution_name])
            ->andFilterWhere(['like', 'student', $this->student])
            ->andFilterWhere(['like', 'insurance_institution', $this->insurance_institution])
            ->andFilterWhere(['like', 'others', $this->others])
            ->andFilterWhere(['like', 'name_parents', $this->name_parents])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'degree', $this->degree])
            ->andFilterWhere(['like', 'imagem', $this->imagem])
            ->andFilterWhere(['like', 'state', $this->state])
         
            ->andFilterWhere(['like', 'island', $this->island]);

        return $dataProvider;
    }
}
