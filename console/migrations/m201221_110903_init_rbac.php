<?php

use yii\db\Migration;

/**
 * Class m201221_110903_init_rbac
 */
class m201221_110903_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201221_110903_init_rbac cannot be reverted.\n";

        return false;
    }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $auth = Yii::$app->authManager;

        // add "createPost" permission
        $create = $auth->createPermission('create');
        $create->description = 'Create a post';
        $auth->add($create);

        // add "updatePost" permission
        $update = $auth->createPermission('update');
        $update->description = 'Update post';
        $auth->add($update);

        //delete
        $delete = $auth->createPermission('delete');
        $delete->description = 'delete post';
        $auth->add($delete);

         //view
         $view = $auth->createPermission('view');
         $view->description = 'view post';
         $auth->add($view);

        // add "author" role and give this role the "createPost" permission
        $ortopidia = $auth->createRole('ortopidia');
        $auth->add($ortopidia);
        $auth->addChild($ortopidia , $create, $update, $view);

        // add "author" role and give this role the "createPost" permission
        $fisioterapia = $auth->createRole('fisioterapia');
        $auth->add($fisioterapia);
        $auth->addChild($fisioterapia , $create, $update, $view);

         // add "author" role and give this role the "createPost" permission
         $utilizador = $auth->createRole('utilizador');
         $auth->add( $utilizador);
         $auth->addChild($utilizador , $create, $update, $view);

        $exterior = $auth->createRole('exterior');
        $auth->add($exterior);
        $auth->addChild($exterior, $create, $update, $view);

        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $delete);
        $auth->addChild($admin, $ortopidia);
        $auth->addChild($admin, $fisioterapia);
        $auth->addChild($admin, $utilizador);
        $auth->addChild($admin, $exterior);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($ortopidia, 5);
        $auth->assign($fisioterapia, 4);
        $auth->assign($utilizador, 3);
        $auth->assign($exterior, 2);
        $auth->assign($admin, 1);

    }

    public function down()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();
    }
    
}
