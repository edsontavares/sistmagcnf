<?php

    namespace frontend\models;

    use Exception;
    use Yii;
    use yii\base\Model;
    use common\models\User;
    use yii\web\BadRequestHttpException;

    /**
     * Password reset form
     */
    class ChangePasswordForm extends Model
    {
        public $password;
        public $password_change;
        public $password_change_repeat;

        private $_user;

        /**
         * {@inheritdoc}
         */
        public function rules()
        {
            return [
                [['password', 'password_change', 'password_change_repeat'], 'required'],
                [['password_change', 'password_change_repeat'], 'string', 'min' => 6],
                ['password', 'validatePassword'],
                ['password_change', 'compare']
            ];
        }

        public function attributeLabels()
        {
            return [
                'password' => 'Palavra-passe',
                'password_change' => 'Nova palavra-passe',
                'password_change_repeat' => 'Confirme nova palavra-passe',
            ];
        }

        public function validatePassword($attribute, $params)
        {
            if (!$this->hasErrors()) {
                $user = $this->getUser();
                if (!$user || !$user->validatePassword($this->password)) {
                    $this->addError($attribute, 'Palavra-passe esta errada.');
                }
            }
        }

        protected function getUser()
        {
            if ($this->_user === null) {
                $this->_user = User::findOne(\Yii::$app->user->identity->getId());
            }

            return $this->_user;
        }

        public function changePassword()
        {
            try {
                $user = $this->getUser();
                $user->setPassword($this->password_change);

                if ($user->validate() && $user->save())
                    return true;
                else
                    return false;
            } catch (Exception $e) {
                throw new BadRequestHttpException($e->getMessage());
            }
        }

    }
