    <!-- Breadcrumb Section -->
    <div class="bg-yellow">
      <div class="container mt-0 space-top-3 pb-3">
        <div class="row">
          <div class="col-lg-5 order-lg-2 text-lg-right mb-4 mb-lg-0">
            <div class="d-flex d-lg-inline-block justify-content-between justify-content-lg-end align-items-center align-items-lg-start">
              <!-- Breadcrumb -->
              <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
                <li class="breadcrumb-item"><a class="breadcrumb-link" href="index.php">Pagina inicial</a></li> 
                <li class="breadcrumb-item active" aria-current="page">Comida</li>
              </ol>
              <!-- End Breadcrumb -->
            </div>
          </div>

          <div class="col-lg-7 order-lg-1">
            <!-- User Info -->
            <div class="media d-block d-sm-flex align-items-sm-center">
              <div class="u-avatar position-relative bg-white p-1 rounded-circle mb-3 mb-sm-0 mr-3">
                <img class="img-fluid rounded-circle" src="../../biblioteca/img/icons_principal/animals.png">
                <!--span class="badge badge-md badge-outline-success badge-pos badge-pos--bottom-right rounded-circle">
                  <span class="fas fa-check"></span>
                </span-->
              </div>
              <div class="media-body">
                <h1 class="h3 text-white font-weight-medium mb-1">Comida</h1>
                <!--span class="d-block text-white">natalie.curtis@gmail.com</span-->
              </div>
            </div>
            <!-- End User Info -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Breadcrumb Section -->

    <div class="border-bottom space-bottom-1">
      <div class="nav_categ_ali shadow border-bottom">
        <div class="container space-0 space-top-0">
          <!-- Nav Classic -->
          <ul id="SVGnavIcons" class="svg-preloader nav nav-classic nav-justified border-bottom-0" role="tablist">
            <li class="nav-item">
              <a class="nav-link font-weight-medium active" id="pills-one-example2-tab" data-toggle="pill" href="#pills-one-example2" role="tab" aria-controls="pills-one-example2" aria-selected="true">
                <div class="d-md-flex justify-content-md-center align-items-md-center">
                  <figure class="ie-height-40 d-none d-md-block w-100 max-width-6 mr-3">
                    <img class="js-svg-injector" src="../../biblioteca/img/restaurante/food.svg"
                         data-parent="#SVGnavIcons">
                  </figure>
                  Restaurantes
                </div>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link font-weight-medium" id="pills-two-example2-tab" data-toggle="pill" href="#pills-two-example2" role="tab" aria-controls="pills-two-example2" aria-selected="false">
                <div class="d-md-flex justify-content-md-center align-items-md-center">
                  <figure class="ie-height-40 d-none d-md-block w-100 max-width-6 mr-3">
                    <img class="js-svg-injector" src="../../biblioteca/img/restaurante/fast-food.svg" alt="SVG"
                         data-parent="#SVGnavIcons">
                  </figure>
                  Fast Food
                </div>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link font-weight-medium" id="pills-three-example2-tab" data-toggle="pill" href="#pills-three-example2" role="tab" aria-controls="pills-three-example2" aria-selected="false">
                <div class="d-md-flex justify-content-md-center align-items-md-center">
                  <figure class="ie-height-40 d-none d-md-block w-100 max-width-6 mr-3">
                    <img class="js-svg-injector" src="../../biblioteca/img/restaurante/nutrition.svg" alt="SVG"
                         data-parent="#SVGnavIcons">
                  </figure>
                  Pequeno Almoço e Lanche
                </div>
              </a>
            </li>
          </ul>
          <!-- End Nav Classic -->
        </div>
      </div>

      <div class="container">
          <!-- Tab Content -->
          <div class="tab-content">
            <div class="tab-pane fade pt-4 show active" id="pills-one-example2" role="tabpanel" aria-labelledby="pills-one-example2-tab">

              <div class="bg-light border p-2 mb-2">
                <div class="row mx-gutters-2">
                  <div class="col-lg mb-0 mb-lg-0">
                    <input type="text" class="form-control form-control-sm" name="text" placeholder="Pesquisa aqui ...">
                  </div>

                  <div class="col-lg-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Escolhe o tipo de restaurante</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                  <div class="col-sm-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Por preço</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                </div>
              </div>

              <div class="row mx-gutters-2">
                <?php
                  foreach (range(1,9) as $ordem)
                  {
                ?>
                <div class="col-lg-4">
                  <div class="card shadow mb-3">
                    <div class="card-img-top position-relative">
                      <a href="view_farmacia.php">
                        <img class="card-img-top" src="../../biblioteca/img/restaurante/k3idwyj2p06lnumoogoz.jpg">
                      </a>

                      <div class="position-absolute bottom-0 left-0 mb-3 ml-3">
                        <div class="d-flex align-items-center flex-wrap">
                          <ul class="list-inline mt-n1 mb-0 mr-2">
                            <li class="list-inline-item mx-0"><img src="https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/illustrations/star.svg" alt="Review rating" width="14"></li>
                            <li class="list-inline-item mx-0"><img src="https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/illustrations/star.svg" alt="Review rating" width="14"></li>
                            <li class="list-inline-item mx-0"><img src="https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/illustrations/star.svg" alt="Review rating" width="14"></li>
                            <li class="list-inline-item mx-0"><img src="https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/illustrations/star.svg" alt="Review rating" width="14"></li>
                            <li class="list-inline-item mx-0"><img src="https://streamlabs.nyc3.cdn.digitaloceanspaces.com/assets/svg/illustrations/star.svg" alt="Review rating" width="14"></li>
                          </ul>
                          <span class="d-inline-block">
                            <small class="font-weight-bold text-white mr-1">4.91</small>
                          </span>
                        </div>
                      </div>

                    </div>
                      
                    <div class="card-body">
                      <h6 class="h5 font-weight-medium text-dark">
                        <a href="index.php?r=site/view_restaurante">Secreto Ibérico</a>
                      </h6>
                      <div class="row">
                        <div class="col-6">
                          <small class="collor-yellow">Palmarejo</small>
                        </div>
                        <div class="col-6 text-lg-right">
                          <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 12%;"> <strong>150ECV</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                  }
                ?>
              </div>

            </div>

            <div class="tab-pane fade pt-4" id="pills-two-example2" role="tabpanel" aria-labelledby="pills-two-example2-tab">
               <div class="bg-light border p-2 mb-2">
                <div class="row mx-gutters-2">
                  <div class="col-lg mb-0 mb-lg-0">
                    <input type="text" class="form-control form-control-sm" name="text" placeholder="Pesquisa aqui ...">
                  </div>

                  <div class="col-lg-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Escolhe o tipo de restaurante</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                  <div class="col-sm-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Por preço</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                </div>
              </div>

              <div class="row mx-gutters-2">
                <?php
                  foreach (range(1,9) as $ordem)
                  {
                ?>
                <div class="col-lg-4">
                  <div class="card shadow mb-3">
                    <a href="">
                      <img class="card-img-top" src="../../biblioteca/img/restaurante/1579694497249_GWB-menu-2patty-HD.png">
                    </a>
                    <div class="card-body">
                      <h6 class="h5 font-weight-medium text-dark">
                        <a href="index.php?r=site/view_restaurante">Timoté</a>
                      </h6>
                      <div class="row">
                        <div class="col-6">
                          <small class="collor-yellow">Fazenda</small>
                        </div>
                        <div class="col-6 text-lg-right">
                          <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 12%;"> <strong>150ECV</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                  }
                ?>
              </div>
            </div>

            <div class="tab-pane fade pt-4" id="pills-three-example2" role="tabpanel" aria-labelledby="pills-three-example2-tab">
              
              <div class="bg-light border p-2 mb-2">
                <div class="row mx-gutters-2">
                  <div class="col-lg mb-0 mb-lg-0">
                    <input type="text" class="form-control form-control-sm" name="text" placeholder="Pesquisa aqui ...">
                  </div>

                  <div class="col-lg-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Escolhe o tipo de restaurante</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                  <div class="col-sm-auto ml-md-auto mb-0 mb-lg-0">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Por preço</option>
                      <option>Menor Preço</option>
                      <option>Maior Preço</option>
                    </select>
                  </div>

                </div>
              </div>

              <div class="row mx-gutters-2">
                <?php
                  foreach (range(1,9) as $ordem)
                  {
                ?>
                <div class="col-lg-4">
                  <div class="card shadow mb-3">
                    <a href="index.php?r=site/view_restaurante">
                      <img class="card-img-top" src="../../biblioteca/img/restaurante/k3idwyj2p06lnumoogoz.jpg">
                    </a>
                    <div class="card-body">
                      <h6 class="h5 font-weight-medium text-dark">
                        <a href="index.php?r=site/view_restaurante">Secreto Ibérico</a>
                      </h6>
                      <div class="row">
                        <div class="col-6">
                          <small class="collor-yellow">Palmarejo</small>
                        </div>
                        <div class="col-6 text-lg-right">
                          <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 12%;"> <strong>150ECV</strong>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
                  }
                ?>
              </div>
            </div>
          </div>
          <!-- End Tab Content -->

      </div>
    </div>