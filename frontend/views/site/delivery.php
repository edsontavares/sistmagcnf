<!-- Breadcrumb Section -->
<div class="bg-yellow">
  <div class="container mt-0 space-top-3 pb-3">
    <div class="row">
      <div class="col-lg-5 order-lg-2 text-lg-right mb-4 mb-lg-0">
        <div class="d-flex d-lg-inline-block justify-content-between justify-content-lg-end align-items-center align-items-lg-start">
          <!-- Breadcrumb -->
          <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
            <li class="breadcrumb-item"><a class="breadcrumb-link" href="index.php">Pagina inicial</a></li> 
            <li class="breadcrumb-item active" aria-current="page">delivery</li>
          </ol>
          <!-- End Breadcrumb -->
        </div>
      </div>

      <div class="col-lg-7 order-lg-1">
        <!-- User Info -->
        <div class="media d-block d-sm-flex align-items-sm-center">
          <div class="u-avatar position-relative bg-white p-1 rounded-circle mb-3 mb-sm-0 mr-3">
            <img class="img-fluid rounded-circle" src="../../biblioteca/img/icons_principal/scooter.png">
            <!--span class="badge badge-md badge-outline-success badge-pos badge-pos--bottom-right rounded-circle">
              <span class="fas fa-check"></span>
            </span-->
          </div>
          <div class="media-body">
            <h1 class="h3 text-white font-weight-medium mb-1">Envios Rápidos</h1>
            <!--span class="d-block text-white">natalie.curtis@gmail.com</span-->
          </div>
        </div>
        <!-- End User Info -->
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumb Section -->

<div class="border-bottom">
  <div class="container space-2 space-top-0">
    
    <div class="row mx-gutters-2">
      <div class="col-lg-8">
        <div class="mb-3">
          <h1 class="font-weight-medium h4 mb-2">Checkout Estafeta</h1>
        </div>

        <div class="pedido mb-4">
          <h5>Detalhes do pedido</h5>
          <small for="exampleFormControlTextarea1">O que precisas?</small>
          <div class="mt-4">
            <textarea class="form-control" placeholder="escreva aqui o que desejas enviar" rows="3"></textarea>
          </div>
        </div>

        <hr>

        <div class="recolha mb-4">
          <h5><i class="fas fa-map-pin mr-2"></i>Local da Recolha</h5>
            <small for="exampleFormControlTextarea1">Define a morada exata de recolha</small>
            <div class="card card-body mt-4">
              <button type="button" class="btn btn collor-yellow btn-block">
                <i class="fas fa-plus mr-2"></i>Adicionar local de recolha</button>
            </div>
        </div>

        <hr>
        
        <div class="entrega mb-4">
          <h5><i class="fas fa-map-marker-alt mr-2"></i>Local da entrega</h5>
            <small for="exampleFormControlTextarea1">Define a tua morada de entrega exata</small>
            <!--div id="GMapRoadmap" class="js-g-map embed-responsive embed-responsive-21by9"
               data-lat="40.748866"
               data-lng="-73.988366"
               data-pin="true">
            </div-->
            <small>Morada da entrega</small>
            <div class="border card-body mt-4">
              <div class="row">

                <div class="col-lg-5">
                  <small class="text-secondary">
                    Rua
                  </small>
                  <h5 class="font-weight-medium h6">Avenida Santigo Palmarejo</h5>
                </div>
                <div class="col-lg-5">
                   <small class="text-secondary">
                    Detalhes (Porta nº 73 2 Esq)
                  </small>
                  <h5 class="font-weight-medium h6">Prédio orden dos Engenheiros</h5>
                </div>
                <div class="col-lg-2">
                  <div class="mt-2">
                    <a href="" class="btn btn-xs btn-soft-warning"><i class="far fa-edit"></i> Alterar</a>
                  </div>
                </div>

              </div>
            </div>
        </div>

        <hr>

        <div class="hora mb-4">
          <h5><i class="far fa-clock mr-2"></i>Hora da entrega</h5>
            <small for="exampleFormControlTextarea1">Define a hora de entrega</small>
            <div class="row mt-4">

              <div class="col-lg-6">
                <div class="card card-body text-center">
                  <h6>Assim que possível</h6>
                </div>
              </div>

              <div class="col-lg-6">
                <a href="">
                  <div class="card card-body text-center">
                    <h6 class="text-dark"><i class="far fa-calendar-alt mr-2"></i>Agendar hora</h6>
                  </div>
                </a>
              </div>

            </div>
        </div>

        <hr>

        <div class="contato mb-4">
          <h5><i class="far fa-user mr-2"></i>Informações de contato</h5>
          <small for="exampleFormControlTextarea1">Verifica a tua informação de contacto</small>
          <div class="card card-body mt-4">
            <button type="button" class="btn btn collor-yellow btn-block">
              <i class="fas fa-plus mr-2"></i>Adicionar numero telefone</button>
          </div> 
        </div>

        <hr>

        <div class="pagamento mb-4">
          <h5><i class="far fa-credit-card mr-2"></i>Dados de pagamento</h5>
            <small for="exampleFormControlTextarea1">Verifica a tua informação de pagamento</small>
            <div class="row mt-4">

              <div class="col-lg-6">
                <div class="card card-body text-center">
                  <h6>Adicionar novo cartão</h6>
                </div>
              </div>

              <div class="col-lg-6">
                <a href="">
                  <div class="card card-body text-center">
                    <h6 class="text-dark">Pagar com dinheiro</h6>
                  </div>
                </a>
              </div>

            </div>
        </div>

        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="stylishCheckbox">
          <label class="custom-control-label" for="stylishCheckbox">
            Ao clicar ai concorda com os termos de uso e politica de priacidade
          </label>
        </div>

      </div>

      <div class="col-lg-4">
        <!-- Subscribe -->
        <div id="stickyBlockStartPoint">
          <div class="js-sticky-block card bg-white text-black shadow-primary text-center shadow rounded p-5"
               data-parent="#stickyBlockStartPoint"
               data-sticky-view="lg"
               data-start-point="#stickyBlockStartPoint"
               data-end-point="#stickyBlockEndPoint"
               data-offset-top="24"
               data-offset-bottom="0">
            <h3 class="h5">O teu carinho</h3>
            <div class="">
              <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 10%;"> 150ECV
            </div>

            <div class="min-height-300">
              <p class="text-black-70 pt-10"><i class="fas fa-shopping-cart"></i><br>Vazio</p>
            </div>
            
          </div>
        </div>
        <!-- End Subscribe -->
      </div>
      
    </div>

    <div id="stickyBlockEndPoint"></div>

  </div>
</div>