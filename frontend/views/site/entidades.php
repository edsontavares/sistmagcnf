<!-- Breadcrumb Section -->
<div class="bg-yellow">
  <div class="container mt-0 space-top-3 pb-3">
    <div class="row">
      <div class="col-lg-5 order-lg-2 text-lg-right mb-4 mb-lg-0">
        <div class="d-flex d-lg-inline-block justify-content-between justify-content-lg-end align-items-center align-items-lg-start">
          <!-- Breadcrumb -->
          <ol class="breadcrumb breadcrumb-white breadcrumb-no-gutter mb-0">
            <li class="breadcrumb-item"><a class="breadcrumb-link" href="index.php">Pagina inicial</a></li> 
            <li class="breadcrumb-item active" aria-current="page">Mercados</li>
          </ol>
          <!-- End Breadcrumb -->
        </div>
      </div>

      <div class="col-lg-7 order-lg-1">
        <!-- User Info -->
        <div class="media d-block d-sm-flex align-items-sm-center">
          <div class="u-avatar position-relative bg-white p-1 rounded-circle mb-3 mb-sm-0 mr-3">
            <img class="img-fluid rounded-circle" src="../../biblioteca/img/icons_principal/delivery.png">
            <!--span class="badge badge-md badge-outline-success badge-pos badge-pos--bottom-right rounded-circle">
              <span class="fas fa-check"></span>
            </span-->
          </div>
          <div class="media-body">
            <h1 class="h3 text-white font-weight-medium mb-1">Mercados</h1>
            <!--span class="d-block text-white">natalie.curtis@gmail.com</span-->
          </div>
        </div>
        <!-- End User Info -->
      </div>
    </div>
  </div>
</div>
<!-- End Breadcrumb Section -->

    <div class="border-bottom">
      <div class="container space-2">
        <div class="row mx-gutters-2">
            <?php
              foreach (range(1,9) as $ordem)
              {
            ?>
            <div class="col-lg-4">
                <div class="card shadow mb-3">
                  <a href="index.php?r=site/view_um_mercado">
                    <img class="img-fluid" src="biblioteca/img/upload/mercado.jpg">
                  </a>
                  <div class="card-body">
                    <h6 class="h5 font-weight-medium text-dark">
                      <a href="index.php?r=site/view_um_mercado">Calu e Angela</a>
                    </h6>
                    <div class="row">
                      <div class="col-6">
                        <small class="collor-yellow">Palmarejo</small>
                      </div>
                      <div class="col-6 text-lg-right">
                        <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 12%;"> <strong>150ECV</strong>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <?php
              }
            ?>
          
        </div>
      </div>
    </div>