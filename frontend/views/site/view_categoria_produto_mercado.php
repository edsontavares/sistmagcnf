
<!-- Hero Section -->
    <div class="position-relative">
      <!-- Slick Carousel -->
      <div class="js-slick-carousel u-slick"
           data-fade="true"
           data-autoplay="true"
           data-speed="5000"
           data-infinite="true">
        <div class="js-slide">
          <div class="bg-img-hero min-height-450" style="background-image: url(../../biblioteca/img/1920x800/banner_farmacia.jpg);"></div>
        </div>
      </div>
      <!-- End Slick Carousel -->

      <div class="container position-absolute right-0 bottom-0 left-0 space-top-5 space-bottom-0">
        <!-- Info Link -->
        <a class="d-sm-inline-flex align-items-center bg-yellow text-white shadow rounded-pill p-2 pr-3 mb-3" href="#">
          <span class="btn btn-xs btn-soft-white btn-pill font-weight-semi-bold mr-3">Comece Agora</span>
          <span class="d-block d-sm-inline-block">
            Encontre todas as produtos aqui com rápides.
          </span>
        </a>
        <!-- End Info Link -->

        <div class="bg-white rounded-top">
          <div class="card-body p-4">
            <div class="mb-2">
              <span class="h4 d-block text-dark font-weight-semi-bold mb-0">Super Mercado Calu e Angela</span>
            </div>
            <!-- Search Jobs Form -->
            <form class="js-validate">
              <div class="row mx-gutters-2">
                <div class="col-lg-7 mb-4 mb-lg-0">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Selecione a categoria do produto</small>
                  </label>
                  <div class="js-focus-state">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Selecione a categoria do produto" aria-label="Selecione a categoria do produto" aria-describedby="keywordInputAddon">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="fas fa-search" id="keywordInputAddon"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <!-- End Input -->
                </div>

                <div class="col-lg-5 mb-4 mb-lg-0">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Pesquise por tudo aqui</small>
                  </label>
                  <div class="js-focus-state">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Pesquise por tudo aqui" aria-label="Pesquise por tudo aqui" aria-describedby="locationInputAddon">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="fas fa-map-marker-alt" id="locationInputAddon"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <!-- End Input -->
                </div>

              </div>
              <!-- End Checkbox -->
            </form>
            <!-- End Search Jobs Form -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Hero Section -->

<section class="bg-light border-bottom">
    <div class="container space-1">
      <div class="row mx-gutters-2">

        <div class="col-lg-8">
         
          <p>Bens Essenciais </p>
          <div class="row mx-gutters-2">
              <?php
                foreach (range(1,6) as $ordem)
                {
              ?>
              <div class="col-lg-6">

                  <div class="bg-white card-body p-4 shadow mb-3">
                    <a href="#mercadomodal" data-modal-target="#mercadomodal">
                      <div class="text-center mb-4">
                        <img class="img-fluid" src="../../biblioteca/img/mercado/transferir.jpg">
                      </div>
                      
                      <h6 class="h5 font-weight-medium text-dark mb-4">Óleo Alimentar Fula 1 lt</h6>
                    </a>
                    <div class="row">
                      <div class="col-4">
                        <img class="img-fluid" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 20%;"> <small>150 ECV</small>
                      </div>
                      <div class="col-8 text-right">
                        <a href="" class="btn-xs btn-pill btn bg-yellow">Adicionar<i class="fas fa-shopping-cart ml-1"></i></a>
                      </div>
                    </div>
                  </div>

              </div>
              <?php
                }
              ?>
          </div>
        </div>

        <div class="col-lg-4">
          <!-- Subscribe -->
          <div id="stickyBlockStartPoint">
            <div class="js-sticky-block card bg-white text-black shadow-primary text-center shadow rounded p-5"
                 data-parent="#stickyBlockStartPoint"
                 data-sticky-view="lg"
                 data-start-point="#stickyBlockStartPoint"
                 data-end-point="#stickyBlockEndPoint"
                 data-offset-top="72"
                 data-offset-bottom="0">
              <h3 class="h5">O teu carinho</h3>
              <div class="">
                <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 10%;"> 150ECV
              </div>

              <div class="min-height-300">
                <p class="text-black-70 pt-10"><i class="fas fa-shopping-cart"></i><br>Vazio</p>
              </div>
              
            </div>
          </div>
          <!-- End Subscribe -->
        </div>

      </div>

      <div id="stickyBlockEndPoint"></div>

    </div>
  </section>

<!-- Modal -->
<div id="mercadomodal" class="js-modal-window u-modal-window" style="width: 500px;">
  <div class="card">
    <!-- Header -->
    <header class="card-header py-3 px-5 border-bottom-0">
      <div class="d-flex justify-content-between align-items-center">
        <h3 class="h6 mb-0"></h3>

        <button type="button" class="close" aria-label="Close" onclick="Custombox.modal.close();">
          <span aria-hidden="true">×</span>
        </button>
      </div>
    </header>
    <!-- End Header -->

    <!-- Body -->
    <div class="card-body p-5">
      <div class="text-center mb-4">
        <img class="img-fluid" src="../../biblioteca/img/mercado/transferir.jpg">
      </div>
      
      <h6 class="mb-3 text-dark text-center">Óleo Alimentar Fula 1 lt</h6>
    </div>
    <!-- End Body -->

    <!-- Footer -->
    <div class="card-footer d-flex justify-content-center">
      <button type="button" class="btn btn-sm bg-yellow btn-block rounded transition-3d-hover mr-1">Adiciona ao carinho</button>
    </div>
    <!-- End Footer -->
  </div>
</div>
<!-- End Modal -->