<div class="position-relative">
      <!-- Slick Carousel -->
      <div class="js-slick-carousel u-slick"
           data-fade="true"
           data-autoplay="true"
           data-speed="5000"
           data-infinite="true">
        <div class="js-slide">
          <div class="bg-img-hero min-height-450" style="background-image: url(../../biblioteca/img/1920x800/banner_farmacia.jpg);"></div>
        </div>
      </div>
      <!-- End Slick Carousel -->

      <div class="container position-absolute right-0 bottom-0 left-0 space-top-5 space-bottom-0">
        <!-- Info Link -->
        <a class="d-sm-inline-flex align-items-center bg-yellow text-white shadow rounded-pill p-2 pr-3 mb-3" href="#">
          <span class="btn btn-xs btn-soft-white btn-pill font-weight-semi-bold mr-3">Comece Agora</span>
          <span class="d-block d-sm-inline-block">
            Encontre todas os seus alimentos preferido.
          </span>
        </a>
        <!-- End Info Link -->

        <a href="index.php?r=site/view_restaurante" class="float-right btn btn-dark btn-pill transition-3d-hover">
          <i class="fas fa-arrow-left mr-2"></i>voltar</a>

        <div class="bg-white rounded-top">
          <div class="card-body p-4">
            <div class="mb-2">
              <span class="h4 d-block text-dark font-weight-semi-bold mb-0">Secreto Ibérico</span>
            </div>
            <!-- Search Jobs Form -->
            <form class="js-validate">
              <div class="row mx-gutters-2">
                <div class="col-lg-5 mb-0 ">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Selecione a categoria do menu</small>
                  </label>
                  <div class="js-focus-state">
                    <div class="input-group">
                      <input type="text" class="form-control form-control-sm" placeholder="Selecione a categoria do menu" aria-label="Selecione a categoria do menu" aria-describedby="keywordInputAddon">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="fas fa-search" id="keywordInputAddon"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <!-- End Input -->
                </div>

                <div class="col-lg-4 mb-0 ">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Pesquise por palavra chave</small>
                  </label>
                  <div class="js-focus-state">
                      <input type="text" class="form-control form-control-sm" placeholder="Pesquise por palavra chave" aria-label="Pesquise por palavra chave" aria-describedby="locationInputAddon">
                  </div>
                  <!-- End Input -->
                </div>

                <div class="col-lg-3 mb-0 ">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Pesquise por preço</small>
                  </label>
                  <div class="form-group">
                    <select class="form-control form-control-sm" id="exampleFormControlSelect1">
                      <option>Pesquise por preço</option>
                      <option>Mais Barato</option>
                      <option>Mais carro</option>
                    </select>
                  </div>
                  <!-- End Input -->
                </div>

              </div>
              <!-- End Checkbox -->
            </form>
            <!-- End Search Jobs Form -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Hero Section -->

  <section class="bg-light border-bottom">
    <div class="container space-1">
      <div class="row mx-gutters-2">

        <div class="col-lg-8">
         
          <p>Medicamentos Não Sujeitos Receita Médica </p>
          <div class="row mx-gutters-2">
              <?php
                foreach (range(1,6) as $ordem)
                {
              ?>
              <div class="col-lg-6">

                  <div class="bg-white card-body p-4 shadow mb-3">
                    <a href="#restaurantemodal" data-modal-target="#restaurantemodal">
                      <div class="text-center mb-4">
                        <img class="img-fluid" src="../../biblioteca/img/restaurante/1171234266.png">
                      </div>
                      
                      <h6 class="h5 font-weight-medium text-dark mb-4">Hamburguer de Terra</h6>
                    </a>
                    <div class="row">
                      <div class="col-4">
                        <small>150 ECV</small>
                      </div>
                      <div class="col-8 text-right">
                        <a href="" class="btn-xs btn btn-pill bg-yellow">Adicionar ao<i class="fas fa-shopping-cart ml-1"></i></a>
                      </div>
                    </div>
                  </div>

              </div>
              <?php
                }
              ?>
          </div>
        </div>

        <div class="col-lg-4">
          <!-- Subscribe -->
          <div id="stickyBlockStartPoint">
            <div class="js-sticky-block card bg-white text-black shadow-primary text-center shadow rounded p-5"
                 data-parent="#stickyBlockStartPoint"
                 data-sticky-view="lg"
                 data-start-point="#stickyBlockStartPoint"
                 data-end-point="#stickyBlockEndPoint"
                 data-offset-top="24"
                 data-offset-bottom="0">
              <h3 class="h5">O teu carinho</h3>
              <div class="">
                <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 10%;"> 150ECV
              </div>

              <div class="min-height-300">
                <p class="text-black-70 pt-10"><i class="fas fa-shopping-cart"></i><br>Vazio</p>
              </div>
              
            </div>
          </div>
          <!-- End Subscribe -->
        </div>

      </div>

      <div id="stickyBlockEndPoint"></div>

    </div>
  </section>

  <!-- Modal -->
<div id="restaurantemodal" class="js-modal-window u-modal-window" style="width: 500px;">
  <div class="card">
    <!-- Header -->
    <header class="card-header py-3 px-5 border-bottom-0">
      <div class="d-flex justify-content-between align-items-center">
        <h3 class="h6 mb-0"></h3>

        <button type="button" class="close" aria-label="Close" onclick="Custombox.modal.close();">
          <span aria-hidden="true">×</span>
        </button>
      </div>
    </header>
    <!-- End Header -->

    <!-- Body -->
    <div class="card-body p-5">
      <div class="text-center mb-4">
        <img class="img-fluid" src="../../biblioteca/img/restaurante/1171234266.png">
      </div>
      
      <h6 class="mb-3 text-dark text-center">Hamburguer de Terra</h6>
    </div>
    <!-- End Body -->

    <!-- Footer -->
    <div class="card-footer d-flex justify-content-center">
      <button type="button" class="btn btn-sm bg-yellow btn-block rounded transition-3d-hover mr-1">Adiciona ao carinho</button>
    </div>
    <!-- End Footer -->
  </div>
</div>
<!-- End Modal -->