<!-- Hero Section -->
<div class="position-relative gradient-half-warning-v2 z-index-2 overflow-hidden">
  <!-- Slick Carousel -->
  <div class=""
       data-fade="true"
       data-autoplay="true"
       data-speed="5000"
       data-infinite="true">
    <div class="js-slide">
      <div class="bg-img-hero min-height-380" style="background-image: url(../../biblioteca/svg/components/back_mercado.svg);"></div>
    </div>
  </div>
  <!-- End Slick Carousel -->

  <div class="container position-absolute right-0 bottom-0 left-0 space-top-5 space-bottom-0">
    <!-- Info Link -->
    <a class="d-sm-inline-flex align-items-center bg-yellow text-white shadow rounded-pill p-2 pr-3 mb-3" href="#">
      <span class="btn btn-xs btn-soft-white btn-pill font-weight-semi-bold mr-3">Comece Agora</span>
      <span class="d-block d-sm-inline-block">
        Encontre todos produtos que necessitas aqui com rapidez.
      </span>
    </a>
    <!-- End Info Link -->

    <div class="bg-white rounded-top">
      <div class="card-body p-4">
        <div class="mb-2">
          <span class="h4 d-block text-dark font-weight-semi-bold mb-0">Super Mercado Calu e Angela</span>
        </div>
        <!-- Search Jobs Form -->
        <form class="js-validate">
          <div class="row mx-gutters-2">
            <div class="col-lg-7 mb-4 mb-lg-0">
              <!-- Input -->
              <label class="d-block">
                <small class="d-block text-secondary">Selecione a categoria do produto</small>
              </label>
              <div class="js-focus-state">
                <div class="input-group">
                  <input type="text" class="form-control form-control-sm" placeholder="Selecione a categoria do produto" aria-label="Selecione a categoria do produto" aria-describedby="keywordInputAddon">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <span class="fas fa-search" id="keywordInputAddon"></span>
                    </span>
                  </div>
                </div>
              </div>
              <!-- End Input -->
            </div>

            <div class="col-lg-5 mb-4 mb-lg-0">
              <!-- Input -->
              <label class="d-block">
                <small class="d-block text-secondary">Pesquise por tudo aqui</small>
              </label>
              <div class="js-focus-state">
                <div class="input-group">
                  <input type="text" class="form-control form-control-sm" placeholder="Pesquise por tudo aqui" aria-label="Pesquise por tudo aqui" aria-describedby="locationInputAddon">
                  <div class="input-group-append">
                    <span class="input-group-text">
                      <span class="fas fa-map-marker-alt" id="locationInputAddon"></span>
                    </span>
                  </div>
                </div>
              </div>
              <!-- End Input -->
            </div>

          </div>
          <!-- End Checkbox -->
        </form>
        <!-- End Search Jobs Form -->
      </div>
    </div>
  </div>
</div>
<!-- End Hero Section -->

<section class="bg-light border-bottom">
    <div class="container space-1">
      <div class="row mx-gutters-2">

        <div class="col-lg-8">
          <p>Categorias do produto </p>
          
          <div class="row mx-gutters-2">
              
              <div class="col-lg-4 mb-3">
                <a href="index.php?r=site/view_categoria_produto_mercado">
                  <div class="bg-white p-4 card-body shadow">
                    <img class="img-fluid" src="../../biblioteca/img/mercado/bensessenciais.jpg">
                    <div class="mt-2">
                      <h6 class="h5 font-weight-medium text-dark">Bens Essenciais</h6>
                    </div>
                  </div>
                </a>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/queijo.jpg">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Charcutaria e Queijos</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/legumes.jpg">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Legumes e frutas</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/bebidas.png">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Bebidas</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/gel.jpg">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Higiene e Beleza</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                  <div class="bg-white p-4 card-body shadow">
                    <img class="img-fluid" src="../../biblioteca/img/mercado/images.jpg">
                    <div class="mt-2">
                      <h6 class="h5 font-weight-medium text-dark">Padaria</h6>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/laticinios.jpg">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Laticinios</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/ProdutoDestaque_65_detalhe.png">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Mercearia Salgada</h6>
                  </div>
                </div>
              </div>

              <div class="col-lg-4 mb-3">
                <div class="bg-white p-4 card-body shadow">
                  <img class="img-fluid" src="../../biblioteca/img/mercado/3794b0b6e4.jpg">
                  <div class="mt-2">
                    <h6 class="h5 font-weight-medium text-dark">Limpeza do Lar</h6>
                  </div>
                </div>
              </div>
              
          </div>
        </div>

        <div class="col-lg-4">
          <!-- Subscribe -->
          <div id="stickyBlockStartPoint">
            <div class="js-sticky-block card bg-white text-black shadow-primary text-center shadow rounded p-5"
                 data-parent="#stickyBlockStartPoint"
                 data-sticky-view="lg"
                 data-start-point="#stickyBlockStartPoint"
                 data-end-point="#stickyBlockEndPoint"
                 data-offset-top="72"
                 data-offset-bottom="0">
              <h3 class="h5">O teu carinho</h3>
              <div class="text-center">
                <img class="card-img-top" src="../../biblioteca/img/icons_principal/bike_scooter.png" style="width: 10%;"> 
                <strong>150ECV</strong>
              </div>

              <div class="min-height-300">
                <p class="text-black-70 pt-10"><i class="fas fa-shopping-cart"></i><br>Vazio</p>
              </div>
              
            </div>
          </div>
          <!-- End Subscribe -->
        </div>

      </div>

      <div id="stickyBlockEndPoint"></div>

    </div>
</section>