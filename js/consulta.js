$(document).ready(function() {
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.split(search).join(replacement);
    };

    // Pagina cria curso
    $('#summernote').summernote({
        placeholder: 'Enquadramento da oferta formativa',
        tabsize: 2,
        height: 300,
        lang: 'pt-PT'
    });


    var dynamic_form = $("#dynamic_form_requisitos").dynamicForm("#dynamic_form_requisitos", "#plus_requisitos", "#minus_requisitos", {
        limit: 20,
        formPrefix: "Requisito",
        normalizeFullForm: false
    });

    var string_requisitos_data = $('#requisitos_list').val()
    string_requisitos_data = string_requisitos_data.split('@')
    var requisitos = string_requisitos_data.map(function(value) {
        return {
            nome: value
        }
    })
    dynamic_form.inject(requisitos);

    var dynamic_form_saidas_proficionais = $("#dynamic_form_saidas_proficionais").dynamicForm("#dynamic_form_saidas_proficionais", "#plus_saidas_proficionais", "#minus_saidas_proficionais", {
        limit: 20,
        formPrefix: "SaidasProficionais",
        normalizeFullForm: false,
    });

    var string_saida_proficional_data = $('#saida_proficional_list').val()
    string_saida_proficional_data = string_saida_proficional_data.split('@')
    var saida_proficional = string_saida_proficional_data.map(function(value) {
        return {
            nome: value
        }
    })
    dynamic_form_saidas_proficionais.inject(saida_proficional);

    var elt = $('input#tag-input-formador');
    var teacherList = [];

    elt.tagsinput({
        itemValue: 'email',
        itemText: 'name',
    });

    //
    elt.on('itemRemoved', function(event) {
        //console.log("removed", event.item)
        for (var i = 0; i < teacherList.length; i++) {
            if (teacherList[i].email === event.item.email) {
                teacherList.splice(i, 1);
                continue;
            }
        }
        updateTeacherList()
    });

    function updateTeacherList() {
        $('#teacherList').val(JSON.stringify(teacherList))
            //console.log("lista", teacherList)
    }


    // pegar list ade formadores
    var formadores = $("#formadores_antigos").val()
    if (formadores) {
        formadores = formadores.replaceAll("'", '"')
        formadores = JSON.parse(formadores)
            //console.log('formadores', formadores)
        formadores.forEach(function(item) {
            elt.tagsinput('add', item)
            teacherList.push(item)
        })
        updateTeacherList()

        elt.on('itemAdded', function(event) {
            console.log("item", event.item)
            teacherList.push(event.item)
            updateTeacherList()
            toastr.success(event.item.name + " adicionado com sucesso", "Novo Formador");
        });
    } else {
        elt.on('itemAdded', function(event) {
            console.log("item", event.item)
            teacherList.push(event.item)
            updateTeacherList()
            toastr.success(event.item.name + " adicionado com sucesso", "Novo Formador");
        });
    }

    var load_progress = $('#search-load'),
        search_body = $('#lista-formador')

    $('#search-for-formador').on("input", _.debounce(search_formador, 800));

    $('#btn_add').click(function(e) {
        var name = $('#input_name').val();
        var email = $('#input_email').val();
        var item = {
            "name": name,
            "email": email,
            "id": 0
        }
        elt.tagsinput('add', item)
        Custombox.modal.close();
    })

    function search_formador(event) {

        var keyword = $("#search-for-formador").val()

        if (keyword === '') {
            search_body.css('display', 'none');
            return;
        }

        event.preventDefault();

        load_progress.css('display', 'block');

        $.get(`${location.origin}/admin/user/formador/search?filter=${keyword}`, function(data) {
            load_progress.css('display', 'none');
            search_body.css('display', 'block');
            search_body.empty();

            search_body.append(data);
        })
    }

    $('#floorplanAttachmentInput').change(function() {

        var reader = new FileReader()

        reader.onload = function(e) {
            $('#capa_preview').attr('src', e.target.result)
            $('#image-capa-btn-attach').css('display', 'block')
            $('#change').css('display', 'none')
            $('#capa-continer').css('display', 'none')
        }
        reader.readAsDataURL(this.files[0])
    })

    $('#programa-curso').change(function() {
        toastr.success("Programa anexado com sucesso.", 'Sucesso')
    })

    $('#buton-curso').click(function(e) {
        var txt = $(this).attr('#nameCuros');
        $('nameC').html('txt');
    });
})