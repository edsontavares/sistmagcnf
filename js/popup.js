$(function () {

    const load_progress = $('#load-waiting');
    let loading = true;

    $(document).on('click', '.open-modal', function (e) {
        $('.modal-conteudo').html('')
        load_progress.css('display', 'block');
        var title = "";
        var url = $(this).attr('data-url');
        if ($(this).data('data-title') === '')
            title = $(this).text();
        else
            title = $(this).attr('data-title');

        var size = $(this).attr('data-modal-size');

        openModal(url, title, size);
    });

    $('.modal-conteudo').slimScroll({
        color: '#3399ff',
        allowPageScroll: true,
        alwaysVisible: false
    });

});


/**
 *
 * @param url
 * @param title
 * @param size
 */
function openModal(url, title, size = '') {
    //$('#load-waiting').css('display', 'none');
    $('#modal-open').find('.modal-header').find('.modal-title').html(title);
    $('#modal-open').find('#modal-size').removeClass().addClass('modal-dialog ' + size);
    $('#modal-open').find('.modal-conteudo').attr('style','overflow: hidden; width: auto; max-height: 67vh;');
    $('#modal-open').find('.slimScrollDiv').attr('style','position: relative; overflow: hidden; width: auto; max-height: 67vh;');
    $('#modal-open').find('.modal-conteudo').load(url,function () {
        $('#load-waiting').css('display', 'none');

    });
    $('#modal-open').modal('show');
}