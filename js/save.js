function save(_form,_form_data,form_type) {

    var type = _form.attr('method');
    var url = _form.attr('action');
    var formData = new FormData(_form_data);

    $.ajax({
        type: type,
        url: url,
        data: formData,
        dataType: 'json',
        success: function (data) {

            if (data.type === 'error')
                toastr.warning(data.message)
            else{
                toastr.success(data.message)
                 console.log(data.model);
                //if(data.model === 'actualizar_lesson' || data.model === 'model_exercice' || data.model === 'actualizar_teste' || data.model === 'actualizar_exercice'){
                //   $('#modal-open').modal('toggle');
                // }
                Custombox.modal.close();

            }

        },
        error: function (data) {
            console.log('Error:', data);
            if( data.status === 422) {

            }else if(data.status === 200){
                toastr.error(data.responseText,{timeOut: 5000} ).css("width","500px");
            }else if(data.status === 500){
                toastr.error(data.responseText,{timeOut: 5000} ).css("width","500px");
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    return false;


}
