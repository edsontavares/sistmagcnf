$(document).ready(function() {
    $('#floorplanAttachmentInput').change(function() {

        var reader = new FileReader()

        reader.onload = function(e) {
            $('#capa_preview').attr('src', e.target.result)
            $('#image-capa-btn-attach').css('display', 'block')
            $('#change').css('display', 'none')
            $('#capa-continer').css('display', 'none')
        }
        reader.readAsDataURL(this.files[0])
    })
})