<?php
	'components' => [

    'urlManager' => [
                'class' => bl\multilang\MultiLangUrlManager::className(),
                'baseUrl' => '/admin',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'rules' => [
                ],
            ],
            'urlManagerFrontend' => [
                'class' => bl\multilang\MultiLangUrlManager::className(),
                'baseUrl' => '/',
                'showScriptName' => false,
                'enablePrettyUrl' => true,
                'enableDefaultLanguageUrlCode' => false,
                'rules' => [
                    [
                        'class' => bl\articles\UrlRule::className()
                    ],
                    [
                        'class' => xalberteinsteinx\shop\UrlRule::className(),
                        'prefix' => 'shop'
                    ],
                ]
            ],
	]
?>