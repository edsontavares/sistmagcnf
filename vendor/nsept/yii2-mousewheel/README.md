#Yii2 jQuery Mouse Wheel Asset

A jQuery plugin that adds cross-browser mouse wheel support with delta normalization.

## Install

Either run

```composer require nsept/yii2-mousewheel "*"```

or add

```"nsept/yii2-mousewheel": "*"```

to the ```require``` section of your `composer.json` file.