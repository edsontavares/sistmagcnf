<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.1.2.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'nsept/yii2-mousewheel' => 
  array (
    'name' => 'nsept/yii2-mousewheel',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@nsept/mousewheel' => $vendorDir . '/nsept/yii2-mousewheel',
    ),
  ),
  'nsept/yii2-jscrollpane' => 
  array (
    'name' => 'nsept/yii2-jscrollpane',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@nsept/jscrollpane' => $vendorDir . '/nsept/yii2-jscrollpane',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'codemix/yii2-localeurls' => 
  array (
    'name' => 'codemix/yii2-localeurls',
    'version' => '1.7.1.0',
    'alias' => 
    array (
      '@codemix/localeurls' => $vendorDir . '/codemix/yii2-localeurls',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'philippfrenzel/yii2fullcalendar' => 
  array (
    'name' => 'philippfrenzel/yii2fullcalendar',
    'version' => '4.0.2.0',
    'alias' => 
    array (
      '@yii2fullcalendar' => $vendorDir . '/philippfrenzel/yii2fullcalendar',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'borales/yii2-phone-input' => 
  array (
    'name' => 'borales/yii2-phone-input',
    'version' => '0.3.0.0',
    'alias' => 
    array (
      '@borales/extensions/phoneInput' => $vendorDir . '/borales/yii2-phone-input/src',
    ),
  ),
  'himiklab/yii2-recaptcha-widget' => 
  array (
    'name' => 'himiklab/yii2-recaptcha-widget',
    'version' => '2.1.1.0',
    'alias' => 
    array (
      '@himiklab/yii2/recaptcha' => $vendorDir . '/himiklab/yii2-recaptcha-widget/src',
      '@himiklab/yii2/recaptcha/tests' => $vendorDir . '/himiklab/yii2-recaptcha-widget/tests',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.11.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'bizley/migration' => 
  array (
    'name' => 'bizley/migration',
    'version' => '4.2.0.0',
    'alias' => 
    array (
      '@bizley/migration' => $vendorDir . '/bizley/migration/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.18.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.14.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'perminder-klair/yii2-dropzone' => 
  array (
    'name' => 'perminder-klair/yii2-dropzone',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kato' => $vendorDir . '/perminder-klair/yii2-dropzone',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '3.0.1.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-money' => 
  array (
    'name' => 'kartik-v/yii2-money',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/money' => $vendorDir . '/kartik-v/yii2-money/src',
    ),
  ),
  'fedemotta/yii2-cronjob' => 
  array (
    'name' => 'fedemotta/yii2-cronjob',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@fedemotta/cronjob' => $vendorDir . '/fedemotta/yii2-cronjob',
    ),
  ),
  'execut/yii2-dropdown-content-input' => 
  array (
    'name' => 'execut/yii2-dropdown-content-input',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@execut/widget/dropdownContent' => $vendorDir . '/execut/yii2-dropdown-content-input',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
);
